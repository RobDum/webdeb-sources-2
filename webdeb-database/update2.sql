SET FOREIGN_KEY_CHECKS=0;

ALTER TABLE actor_has_affiliation ADD COLUMN `start_date_type` int(11) DEFAULT NULL after `end_date`;
ALTER TABLE actor_has_affiliation ADD COLUMN `end_date_type` int(11) DEFAULT NULL after `start_date_type`;
ALTER TABLE actor_has_affiliation ADD CONSTRAINT `fk_actor_has_affiliation_start_date_type1` FOREIGN KEY (`start_date_type`) REFERENCES `t_precision_date_type` (`id_type`) ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE actor_has_affiliation ADD CONSTRAINT `fk_actor_has_affiliation_end_date_type1` FOREIGN KEY (`end_date_type`) REFERENCES `t_precision_date_type` (`id_type`) ON DELETE NO ACTION ON UPDATE CASCADE;

ALTER TABLE contributor_has_affiliation ADD COLUMN `start_date_type` int(11) DEFAULT NULL after `end_date`;
ALTER TABLE contributor_has_affiliation ADD COLUMN `end_date_type` int(11) DEFAULT NULL after `start_date_type`;
ALTER TABLE contributor_has_affiliation ADD CONSTRAINT `fk_contributor_has_affiliation_start_date_type1` FOREIGN KEY (`start_date_type`) REFERENCES `t_precision_date_type` (`id_type`) ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE contributor_has_affiliation ADD CONSTRAINT `fk_contributor_has_affiliation_end_date_type1` FOREIGN KEY (`end_date_type`) REFERENCES `t_precision_date_type` (`id_type`) ON DELETE NO ACTION ON UPDATE CASCADE;

ALTER TABLE organization DROP FOREIGN KEY `fk_organization_t_country1`;
ALTER TABLE organization DROP COLUMN head_office;

DROP TABLE IF EXISTS `t_affiliation_actor_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_affiliation_actor_type` (
  `id_type` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `t_affiliation_subtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_affiliation_subtype` (
  `id_type` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

ALTER TABLE t_affiliation_type modify column `en` varchar(60) COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE t_affiliation_type modify column `fr` varchar(60) COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE t_affiliation_type modify column `nl` varchar(60) COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE t_affiliation_type ADD COLUMN `id_actor_type` int(11) NOT NULL after `id_type`;
ALTER TABLE t_affiliation_type ADD COLUMN `id_subtype` int(11) NOT NULL after `id_actor_type`;

INSERT INTO `t_gender` VALUES ('X', 'Neutral', 'Neutre', 'Neutral');

delete from `t_legal_status` where id_status >= 5;
INSERT INTO `t_legal_status` VALUES (5, 'Project or event', 'Projet ou événement', 'Project of evenement');
INSERT INTO `t_legal_status` VALUES (6, 'Prize, distinction, election, competition', 'Prix, distinction, élection, compétition', 'Prijs, onderscheid, verkiezing, competitie');
INSERT INTO `t_legal_status` VALUES (7, 'Product, brand', 'Produit, marque (y compris programme d’étude)', 'Product, merk');
INSERT INTO `t_legal_status` VALUES (8, 'Label', 'Label', 'Label');

INSERT INTO `t_business_sector` VALUES (20, 'All sectors', 'Tous les secteurs', 'Alle sectoren');

INSERT INTO `t_affiliation_actor_type` VALUES (-1, 'NONE');
INSERT INTO `t_affiliation_actor_type` VALUES (0, 'ORGANIZATION / PERSON');
INSERT INTO `t_affiliation_actor_type` VALUES (1, 'ORGANIZATION');
INSERT INTO `t_affiliation_actor_type` VALUES (2, 'PERSON');

INSERT INTO `t_affiliation_subtype` VALUES (-1, 'ALL');
INSERT INTO `t_affiliation_subtype` VALUES (0, 'AFFILIATION');
INSERT INTO `t_affiliation_subtype` VALUES (1, 'AFFILIATED');
INSERT INTO `t_affiliation_subtype` VALUES (2, 'FILIATION');

delete from `t_affiliation_type` where id_type >= -1;
INSERT INTO `t_affiliation_type` VALUES (-1, -1, -1, 'unset', 'indéfini', 'onbepaald');
INSERT INTO `t_affiliation_type` VALUES (0, 0, 1, '50% or more owned or financed by', 'détenu ou financé à 50 % ou plus par', 'Voor minstens 50% in handen van of gefinancierd door');
INSERT INTO `t_affiliation_type` VALUES (1, 0, 1, 'between 25 and 49,9% owned or financed by', 'détenu ou financé entre 25 et 49,9 % par', 'Tussen 25 and 49,9% in handen van of gefinancierd door');
INSERT INTO `t_affiliation_type` VALUES (2, 0, 1, 'less than 25% owned or financed by', 'détenu ou financé à moins de 25 % par', 'Minder dan 25% in handen van of gefinancierd door');
INSERT INTO `t_affiliation_type` VALUES (3, 0, 1, 'partially owned or financed by (unknown %)', 'détenu ou financé par (% indéterminé)', 'in handen van of gefinancierd door (onbekend %)');
INSERT INTO `t_affiliation_type` VALUES (4, 1, 1, 'department of', 'division de', 'Departement van');
INSERT INTO `t_affiliation_type` VALUES (5, 1, 1, 'member of', 'membre de', 'Lid van');
INSERT INTO `t_affiliation_type` VALUES (6, 1, 1, 'produced or organized by', 'produit ou organisé par', 'geproduceerd of georganiseerd door');
INSERT INTO `t_affiliation_type` VALUES (7, 1, 1, 'awarded or labbeled by', 'distingué ou labellisé par', 'Gecertifieerd door');
INSERT INTO `t_affiliation_type` VALUES (8, 1, 1, 'participating in', 'participant à', 'Neemt deel aan');
INSERT INTO `t_affiliation_type` VALUES (9, 2, 1, 'cabinet of', 'cabinet de', 'Kabinet van');
INSERT INTO `t_affiliation_type` VALUES (10, 2, 2, 'graduating from', 'diplomé de', 'graduating from');
INSERT INTO `t_affiliation_type` VALUES (11, 2, 2, 'has as parent', 'a comme parent', 'heeft als ouder');
INSERT INTO `t_affiliation_type` VALUES (12, 1, 0, 'owns or finances at least 50% of', 'possède ou finance au moins 50 % de', 'Bezit of financiert meer dan 50% van');
INSERT INTO `t_affiliation_type` VALUES (13, 1, 0, 'owns or finances between 25 and 49,9% of ', 'possède ou finance 25 à 49,9 % de', 'Bezit of financiert tussen 25 and 49,9% van');
INSERT INTO `t_affiliation_type` VALUES (14, 1, 0, 'owns or finances less than 25% of', 'possède ou finance moins de 25 % de', 'Bezit of financiert minder dan 25% van');
INSERT INTO `t_affiliation_type` VALUES (15, 1, 0, 'holds shares of or finances (unknown %)', 'est actionnaire ou finance (% indéterminé)', 'Bezit aandelen van of financiert (% onbekend)');
INSERT INTO `t_affiliation_type` VALUES (16, 1, 0, 'has as division', 'a pour département', 'Heeft als departement');
INSERT INTO `t_affiliation_type` VALUES (17, 1, 0, 'has as member', 'a pour membre', 'Heeft als lid');
INSERT INTO `t_affiliation_type` VALUES (18, 1, 0, 'produces or organizes', 'produit ou organise', 'Produceert of regelt');
INSERT INTO `t_affiliation_type` VALUES (19, 0, 0, 'has awarded or labelled', 'distingue ou prime', 'Certificeert');
INSERT INTO `t_affiliation_type` VALUES (20, 0, 0, 'has as participant', 'a comme participant', 'Heeft als deelnemers');
INSERT INTO `t_affiliation_type` VALUES (21, 2, 0, 'has as cabinet', 'est le cabinet de', 'Heeft als kabinet');

ALTER TABLE t_affiliation_type ADD CONSTRAINT `t_affiliation_type_t_affiliation_actor_type1` FOREIGN KEY (`id_actor_type`) REFERENCES `t_affiliation_actor_type` (`id_type`) ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE t_affiliation_type ADD CONSTRAINT `t_affiliation_type_t_affiliation_subtype1` FOREIGN KEY (`id_subtype`) REFERENCES `t_affiliation_subtype` (`id_type`) ON DELETE NO ACTION ON UPDATE CASCADE;

DROP TABLE IF EXISTS `t_profession_subtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_profession_subtype` (
  `id_type` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `t_profession_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_profession_type` (
  `id_type` int(11) NOT NULL,
  `id_subtype` int(11) NOT NULL,
  `en` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `fr` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `nl` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_type`),
  KEY `fk_t_profession_type_t_profession_subtype1` (`id_subtype`),
  CONSTRAINT `t_profession_type_t_profession_subtype1` FOREIGN KEY (`id_subtype`) REFERENCES `t_profession_subtype` (`id_type`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

ALTER TABLE profession ADD COLUMN `id_type` int(11) NOT NULL;
ALTER TABLE profession ADD CONSTRAINT `profession_t_profession_type1` FOREIGN KEY (`id_type`) REFERENCES `t_profession_type` (`id_type`) ON DELETE NO ACTION ON UPDATE CASCADE;

DROP TABLE IF EXISTS `t_precision_date_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_precision_date_type` (
  `id_type` int(11) NOT NULL,
  `is_past` tinyint(1) NOT NULL,
  `en` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `fr` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `nl` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

INSERT INTO `t_profession_subtype` VALUES (0, 'HIERARCHY');
INSERT INTO `t_profession_subtype` VALUES (1, 'TYPE');

INSERT INTO `t_profession_type` VALUES (0, 0, 'others', '', '');
INSERT INTO `t_profession_type` VALUES (1, 1, 'formation', '', '');

INSERT INTO `t_precision_date_type` VALUES (0, 1, 'Exactly since', 'exactement depuis', 'Precies sinds');
INSERT INTO `t_precision_date_type` VALUES (1, 1, 'At least since', 'au moins depuis', 'Ten minste sinds');
INSERT INTO `t_precision_date_type` VALUES (2, 0, 'Exactly until', 'exactement jusqu’à', 'Precies tot');
INSERT INTO `t_precision_date_type` VALUES (3, 0, 'At least until', 'au moins jusqu’à', 'Ten minste tot');
INSERT INTO `t_precision_date_type` VALUES (4, 0, 'Expected until', 'prévu jusqu’à', 'Gepland tot');
INSERT INTO `t_precision_date_type` VALUES (5, 0, 'Ongoing', 'en cours', 'Onderweg');

DROP TABLE IF EXISTS `picture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `picture` (
  `id_picture` bigint(20) NOT NULL,
  `avatar` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(2048) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_picture`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

SET FOREIGN_KEY_CHECKS=1;