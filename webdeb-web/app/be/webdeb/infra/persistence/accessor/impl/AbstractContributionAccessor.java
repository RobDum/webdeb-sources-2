/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.accessor.impl;

import be.webdeb.application.query.EQueryKey;
import be.webdeb.core.api.actor.*;
import be.webdeb.core.api.actor.Actor;
import be.webdeb.core.api.argument.Argument;
import be.webdeb.core.api.argument.ArgumentIllustration;
import be.webdeb.core.api.argument.EArgumentLinkShade;
import be.webdeb.core.api.contribution.*;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.ExternalContribution;
import be.webdeb.core.api.contribution.Place;
import be.webdeb.core.api.contribution.PlaceType;
import be.webdeb.core.api.contributor.EContributorRole;
import be.webdeb.core.api.folder.EFolderType;
import be.webdeb.core.api.text.*;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.ObjectNotFoundException;
import be.webdeb.core.exception.OutdatedVersionException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.core.impl.contribution.ConcretePartialContributions;
import be.webdeb.infra.fs.FileSystem;
import be.webdeb.infra.persistence.accessor.api.*;
import be.webdeb.infra.persistence.model.*;
import be.webdeb.infra.persistence.model.Contributor;
import be.webdeb.infra.persistence.model.ExternalAuthor;
import be.webdeb.infra.persistence.model.Folder;
import be.webdeb.infra.persistence.model.Group;
import be.webdeb.infra.persistence.model.Text;
import be.webdeb.util.ValuesHelper;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Transaction;
import com.avaje.ebean.TxScope;
import javax.inject.Inject;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This accessor handles common actions to persist or retrieve all types of contribution
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
abstract class AbstractContributionAccessor<T extends ContributionFactory> implements ContributionAccessor {

  @Inject
  protected T factory;

  @Inject
  private ArgumentAccessor argumentAccessor;

  @Inject
  private ActorAccessor actorAccessor;

  @Inject
  private FolderAccessor folderAccessor;

  @Inject
  protected APIObjectMapper mapper;

  @Inject
  protected ValuesHelper values;

  @Inject
  protected FileSystem files;

  private List<ContributionType> contributionTypes = null;
  private List<ValidationState> validationStates = null;
  private List<Language> languages = null;

  // string constant for logs
  private static final String TO_CONTRIBUTION = " to contribution ";
  private static final String CONTRIBUTION_NOT_FOUND = "no contribution found for given id ";
  private static final String INTO = " into ";
  private static final String MERGING = "merging ";
  private static final String UNABLE_TO_MERGE = "unable to merge ";

  // hadcoding default group id to avoid circular refs
  private static final int DEFAULT_GROUP = 0;

  // custom logger
  protected static final org.slf4j.Logger logger = play.Logger.underlying();

  @Override
  public Contribution retrieve(Long id, EContributionType type) {
    // if no type passed, return null
    if (type == null) {
      logger.warn("no contribution type passed");
      return null;
    }

    // retrieve object
    be.webdeb.infra.persistence.model.Contribution contribution =
        be.webdeb.infra.persistence.model.Contribution.findById(id);

    // if we got one and it has the right type (or the ALL type)
    if (contribution != null &&
        (contribution.getContributionType().getIdContributionType() == type.id() || EContributionType.ALL.equals(type))) {
      try {
        return mapper.toContribution(contribution);
      } catch (FormatException e) {
        logger.error("unable to cast generic contribution " + id, e);
      }
    }
    logger.warn("no contribution found for id " +  id);
    return null;
  }

  @Override
  public Contribution retrieveContribution(Long id) {
    // retrieve object
    be.webdeb.infra.persistence.model.Contribution contribution =
            be.webdeb.infra.persistence.model.Contribution.findById(id);

    // if we got one
    if (contribution != null) {
      try {
        return mapper.toContribution(contribution);
      } catch (FormatException e) {
        logger.error("unable to cast generic contribution " + id, e);
      }
    }
    logger.warn("no contribution found for id " +  id);
    return null;
  }

  @Override
  public ContextContribution retrieveContextContribution(Long id) {
    // retrieve object
    be.webdeb.infra.persistence.model.Contribution contribution =
            be.webdeb.infra.persistence.model.Contribution.findById(id);

    // if we got one
    if (contribution != null) {
      try {
        return mapper.toContextContribution(contribution);
      } catch (FormatException e) {
        logger.error("unable to cast generic context contribution " + id, e);
      }
    }
    logger.warn("no context contribution found for id " +  id);
    return null;
  }

  @Override
  public List<ContributionType> getContributionTypes() {
    if (contributionTypes == null) {
      contributionTypes = TContributionType.find.all().stream().map(t ->
        factory.createContributionType(EContributionType.value(t.getIdContributionType()), new LinkedHashMap<>(t.getTechnicalNames()))
      ).collect(Collectors.toList());
    }
    return contributionTypes;
  }

  @Override
  public int getAmountOf(EContributionType type, int group) {
    return be.webdeb.infra.persistence.model.Contribution.getAmountOf(type.id(), group);
  }

  @Override
  public final be.webdeb.core.api.contributor.Contributor getCreator(Long contribution) {
    be.webdeb.infra.persistence.model.Contributor contributor =
        be.webdeb.infra.persistence.model.Contribution.getCreator(contribution);
    if (contributor != null) {
      try {
        return mapper.toContributor(contributor);
      } catch (FormatException e) {
        logger.error("unable to construct API contributor for creator of " + contribution, e);
      }
    } else {
      logger.error("given contribution id does not exist or it has no creator " + contribution);
    }
    return null;
  }

  @Override
  public final be.webdeb.core.api.contributor.Contributor getLastContributorInGroup(Long contribution, int group){
    be.webdeb.infra.persistence.model.Contributor contributor =
            be.webdeb.infra.persistence.model.Contribution.getLastContributorInGroup(contribution, group);
    if (contributor != null) {
      try {
        return mapper.toContributor(contributor);
      } catch (FormatException e) {
        logger.error("unable to construct API contributor for creator of " + contribution, e);
      }
    } else {
      logger.error("given contribution id does not exist or it has no creator " + contribution + " or given grop does not exist " + group);
    }
    return null;
  }

  @Override
  public List<be.webdeb.core.api.contributor.Contributor> getContributors(Long contribution) {
    be.webdeb.infra.persistence.model.Contribution c =
        be.webdeb.infra.persistence.model.Contribution.findById(contribution);
    List<be.webdeb.core.api.contributor.Contributor> contributors = new ArrayList<>();
    if (c != null) {
      try {
        for (ContributionHasContributor chc : c.getContributionHasContributors()) {
          contributors.add(mapper.toContributor(chc.getContributor()));
        }
      } catch (FormatException e) {
        logger.error("unable to construct API contributor for creator of " + contribution, e);
      }
    } else {
      logger.error("given contribution id does not exist " + contribution);
    }
    return contributors;
  }

  @Override
  public List<ActorRole> getActors(Long contribution) {
    Transaction transaction = Ebean.getDefaultServer().currentTransaction();
    boolean commitNeeded = transaction == null;
    if (commitNeeded) {
      transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
    }
    be.webdeb.infra.persistence.model.Contribution contrib =
        be.webdeb.infra.persistence.model.Contribution.findById(contribution);
    try {
      if (contrib != null) {
          List<ActorRole> result = new ArrayList<>();
          for (ContributionHasActor cha : contrib.getActors()) {
            result.add(mapper.toActorRole(cha, mapper.toActor(cha.getActor()), mapper.toContribution(contrib)));
          }
          return result;

      } else {
        logger.error("given id does not exist " + contribution);
      }
    } catch (FormatException e) {
      logger.error("unable to construct API actors for bound actors of " + contribution, e);

    } finally {
      if (commitNeeded) {
        transaction.end();
      }
    }
    return new ArrayList<>();
  }

  @Override
  public Place findPlace(Long id) {
    try {
      return mapper.toPlace(be.webdeb.infra.persistence.model.Place.find.byId(id));
    } catch (Exception e) {
      logger.error("unable create place from db  " + id, e);
    }
    return null;
  }

  @Override
  public List<Place> findPlace(String name, int fromIndex, int toIndex) {

    try {
      return buildPlacesList(be.webdeb.infra.persistence.model.Place.findByPartialTitle(name, fromIndex, toIndex));
    } catch (Exception e) {
      logger.error("unable to search for value " + name, e);
    }
    return new ArrayList<>();

  }

  @Override
  public List<Contribution> findByValue(String value, int group, int fromIndex, int toIndex) {
    Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
    try {
      return toContributions(be.webdeb.infra.persistence.model.Contribution.findBySortKey(value, group, fromIndex, toIndex));
    } catch (Exception e) {
      logger.error("unable to search for value " + value, e);
    } finally {
      transaction.end();
    }
    return new ArrayList<>();

  }

  @Override
  public List<Contribution> findByCriteria(List<Map.Entry<EQueryKey, String>> criteria, boolean strict, int fromIndex, int toIndex) {
    Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
    try {
      return toContributions(be.webdeb.infra.persistence.model.Contribution.findByCriteria(criteria, strict, fromIndex, toIndex));
    } catch (Exception e) {
      logger.error("unable to execute query with given criteria " + criteria, e);

    } finally {
      transaction.end();
    }
    return new ArrayList<>();
  }

  @Override
  public List<Contribution> getLatestEntries(EContributionType type, Long contributor, int amount, int group) {
    return toContributions(be.webdeb.infra.persistence.model.Contribution.findLatestEntries(type.id(), contributor, amount, group));
  }

  @Override
  public List<Contribution> getPopularEntries(EContributionType type, Long contributor, int amount, int group) {
    return toContributions(be.webdeb.infra.persistence.model.Contribution.findPopularEntries(type.id(), contributor, amount, group));
  }

  @Override
  public final List<Contribution> bindActor(Long contribution, ActorRole role, int currentGroup, Long contributor)
      throws PermissionException, PersistenceException {

    logger.debug("try to bind actor " + role.toString() + TO_CONTRIBUTION + contribution);

    // check if contributor can be found
    be.webdeb.infra.persistence.model.Contributor dbContributor = checkContributor(contributor, currentGroup);

    // check if contribution exists
    be.webdeb.infra.persistence.model.Contribution dbContribution = be.webdeb.infra.persistence.model.Contribution.findById(contribution);
    if (dbContribution == null) {
      logger.error(CONTRIBUTION_NOT_FOUND + contribution);
      throw new ObjectNotFoundException(be.webdeb.infra.persistence.model.Contribution.class, contribution);
    }

    Transaction transaction = Ebean.getDefaultServer().currentTransaction();
    boolean commitNeeded = transaction == null;

    if (commitNeeded) {
      logger.debug("no transaction running (binding of actor saved by itself), create one");
      transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
    }
    List<Contribution> result = new ArrayList<>();

    try {
      // check if actor does not exit or its affiliation role does not exist
      if (role.getActor().getId() == -1L) {
        logger.debug("unknown actor " + role.getActor().getFullname(factory.getDefaultLanguage()));
        // does he have an affiliation ? if so, add it to actor
        if (role.getAffiliation() != null && role.getAffiliation().getId() == -1L) {
          role.getActor().addAffiliation(role.getAffiliation());
        }
        // set inGroup value for actor
        role.getActor().addInGroup(currentGroup);
        // we will get at most two actors back (but one at least) since, first one is the actor to be bound
        Map<Integer, List<Contribution>> c = actorAccessor.save(role.getActor(), currentGroup, contributor);
        if(c.containsKey(EContributionType.ACTOR.id()))
          result.addAll(c.get(EContributionType.ACTOR.id()));
        // update role.getByActor id with his new id (since it has been auto-created)
        role.getActor().setId(result.get(0).getId());
      }

      // if given role's affiliation does not exist, add it
      if (role.getAffiliation() != null && role.getAffiliation().getId() == -1L) {
        // add this affiliation to actor and save it
        int index = role.getActor().getAffiliations().indexOf(role.getAffiliation());
        if (index != -1) {
          logger.debug("unknown affiliation has been bound to " + role.getActor().getAffiliations().get(index));
          role.setAffiliation(role.getActor().getAffiliations().get(index));
        } else {
          logger.info("register unknown affiliation for actor " + role.getActor().getFullname(factory.getDefaultLanguage()));
          // add this affiliation to actor to be saved
          Map<Integer, List<Contribution>> c = role.getAffiliation().save(role.getActor().getId(), currentGroup, contributor);
          if(c.containsKey(EContributionType.ACTOR.id()))
            result.addAll(c.get(EContributionType.ACTOR.id()));
        }
      }

      // now we can store the mapping from the contribution to this actor
      ContributionHasActor cha = ContributionHasActor.findContributionHasActor(contribution, role.getActor().getId());
      if (cha == null) {
        cha = new ContributionHasActor(contribution, role.getActor().getId());
      }
      cha.setIsAuthor(role.isAuthor());
      cha.setIsReporter(role.isReporter());
      cha.setIsAbout(role.isJustCited());
      if (role.getAffiliation() != null) {
        cha.setActorIdAha(role.getAffiliation().getId());
      } else {
        // force null value (in cas of update)
        cha.setActorIdAha(null);
      }

      cha.save();
      // only commit if we created a dedicated transaction for it
      if (commitNeeded) {
        transaction.commit();
        bindContributor(dbContribution, dbContributor, EModificationStatus.UPDATE);
      }
      logger.info("bound actor to " + cha.toString());

    } catch (PermissionException e) {
      // wrap permission error key as persistence error message
      logger.error("permission error while binding actor " + role.getActor().getFullname(factory.getDefaultLanguage())
          + TO_CONTRIBUTION + contribution, e);
      throw e;

    } catch (Exception e) {
      String more = "error while binding actor " + role.getActor().getFullname(factory.getDefaultLanguage())
          + TO_CONTRIBUTION + contribution;
      logger.error(more, e);
      throw new PersistenceException(PersistenceException.Key.BIND_ACTOR, more, e);

    } finally {
      // only commit if we created a dedicated transaction for it
      if (commitNeeded) {
        transaction.end();
      }
    }

    // return newly created actor, if needed
    return result.stream().filter(Objects::nonNull).collect(Collectors.toList());
  }

  @Override
  public final void unbindActor(Long contribution, Long actor, Long contributor) throws PersistenceException {

    // check we may retrieve all needed info
    be.webdeb.infra.persistence.model.Contributor dbContributor = be.webdeb.infra.persistence.model.Contributor.findById(contributor);
    if (dbContributor == null) {
      throw new ObjectNotFoundException(Contributor.class, contributor);
    }

    ContributionHasActor cha = ContributionHasActor.findContributionHasActor(contribution, actor);
    if (cha == null) {
      throw new ObjectNotFoundException(ContributionHasActor.class, contribution);
    }

    // now unbind
    Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
    try {
      cha.delete();
      bindContributor(be.webdeb.infra.persistence.model.Contribution.findById(contribution),
          dbContributor, EModificationStatus.UPDATE);

      logger.info("deleted binding between " + contribution + " and actor " + actor);
      transaction.commit();

    } catch (Exception e) {
      String more = "error while deleting binding from contribution " + contribution + " to actor " + actor;
      logger.error(more, e);
      throw new PersistenceException(PersistenceException.Key.UNBIND_ACTOR, more, e);

    } finally {
      transaction.end();
    }
  }

  @Override
  public final List<Contribution> bindFolders(Long contribution, List<be.webdeb.core.api.folder.Folder> folders, Long contributor)
      throws PermissionException, PersistenceException {
    return  bindFolders(contribution, folders, contributor, true);
  }

  @Override
  public final List<Contribution> bindFolders(Long contribution, List<be.webdeb.core.api.folder.Folder> folders, Long contributor, boolean createComposite)
          throws PermissionException, PersistenceException {
    logger.debug("try to bind folders" + TO_CONTRIBUTION + contribution);
    List<Contribution> results = new ArrayList<>();

    // check if contribution exists
    be.webdeb.infra.persistence.model.Contribution dbContribution = be.webdeb.infra.persistence.model.Contribution.findById(contribution);
    if (dbContribution == null) {
      logger.error(CONTRIBUTION_NOT_FOUND + contribution);
      throw new ObjectNotFoundException(be.webdeb.infra.persistence.model.Contribution.class, contribution);
    }

    Transaction transaction = Ebean.getDefaultServer().currentTransaction();
    boolean commitNeeded = transaction == null;
    if (commitNeeded) {
      logger.debug("no transaction running (binding of folder saved by itself), create one");
      transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
    }

    try {
      List<Folder> dbFolders = new ArrayList<>();
      for(be.webdeb.core.api.folder.Folder folder : folders){
        // check if folder does not exit
        if (folder.getId() == -1L) {
          logger.debug("unknown folder " + folder.getDefaultName());
          // we will get the saved folder
          folder.addInGroup(Group.getPublicGroup().getIdGroup());
          Map<Integer, List<Contribution>> c = folderAccessor.save(folder, contributor);
          // update folder id with his new id (since it has been auto-created)
          if(c.containsKey(EContributionType.FOLDER.id()) && !c.get(EContributionType.FOLDER.id()).isEmpty()) {
            folder.setId(c.get(EContributionType.FOLDER.id()).get(0).getId());
            results.add(c.get(EContributionType.FOLDER.id()).get(0));
          }
        }
        Folder dbFolder = Folder.findById(folder.getId());
        if(dbFolder != null) {
          dbFolders.add(dbFolder);
        }
      }

      dbContribution.setFolders(dbFolders);
      dbContribution.update();

      if(createComposite) {
        dbContribution.setFolders(createCompositeFolders(dbContribution.getFolders()));
        dbContribution.update();
      }

      // only commit if we created a dedicated transaction for it
      if (commitNeeded) {
        transaction.commit();
      }
    } catch (PermissionException e) {
      // wrap permission error key as persistence error message
      logger.error("permission error while binding contribution " + contribution.toString()
              + TO_CONTRIBUTION + contribution, e);
      throw e;
    } finally {
      // only commit if we created a dedicated transaction for it
      if (commitNeeded) {
        transaction.end();
      }
    }

    // return newly created folders, if needed
    return results.stream().filter(Objects::nonNull).collect(Collectors.toList());
  }

  /*
   * Create composite folders from
   */
  private List<Folder> createCompositeFolders(List<Folder> folders) throws PersistenceException{
    List<Folder> created = new ArrayList<>();
    if(folders.size() >= 2 && folders.size() <= 3){
      created.add(createCompositeFolder(folders));
    }
    if(folders.size() == 3){
      for(int i = 0; i < folders.size() - 1; i++){
        for(int j = i + 1; j < folders.size(); j++){
          created.add(createCompositeFolder(Arrays.asList(folders.get(i), folders.get(j))));
        }
      }
    }

    folders.addAll(created);
    return folders;
  }

  /*
   * Create composite folders from
   */
  private Folder createCompositeFolder(List<Folder> folders) throws PersistenceException{
    List<Long> parents = folders.stream().map(Folder::getIdContribution).collect(Collectors.toList());
    Folder composite = Folder.findExistingCompositeFolderFromParentId(parents);
    if(composite == null){
      List<String> names = folders.stream().map(Folder::getDefaultName).collect(Collectors.toList());
      be.webdeb.infra.persistence.model.Contribution c = initContribution(EContributionType.FOLDER.id(),  String.join(",", names));

      composite = new Folder();
      composite.setFolderType(TFolderType.find.byId(EFolderType.COMPOSED.id()));
      c.setFolder(composite);
      composite.setContribution(c);

      // update groups
      c.addGroup(Group.getPublicGroup());
      c.save();

      // set id of folder
      composite.setIdContribution(c.getIdContribution());
      composite.save();

      Contributor dbContributor = Contributor.getWebdebContributor();

      bindContributor(c, dbContributor, EModificationStatus.CREATE);

      for (Folder folder : folders) {
        folderAccessor.saveFolderLink(folder, composite, null, dbContributor);
      }
    }

    return composite;
  }

  @Override
  public Set<be.webdeb.core.api.folder.Folder> getContributionsFolders(Long contribution){
    be.webdeb.infra.persistence.model.Contribution c = be.webdeb.infra.persistence.model.Contribution.findById(contribution);
    if(c != null){
      List<Folder> folders = c.getFolders();
      return (folders != null ? buildFolderSet(folders) : new HashSet<>());
    }

    return new HashSet<>();
  }

  @Override
  public List<be.webdeb.core.api.contribution.Place> getContributionsPlaces(Long contribution){
    be.webdeb.infra.persistence.model.Contribution c = be.webdeb.infra.persistence.model.Contribution.findById(contribution);
    if(c != null){
      List<be.webdeb.infra.persistence.model.Place> places = c.getPlaces();
      return (places != null ? buildPlacesList(places) : new ArrayList<>());
    }

    return new ArrayList<>();
  }

  @Override
  public void remove(Long contribution, EContributionType type, Long contributor) throws PermissionException,  PersistenceException {
    remove(contribution, type, contributor, ERemoveOption.TOTAL);
  }

  @Override
  public void remove(Long contribution, EContributionType type, Long contributor, ERemoveOption option) throws PermissionException,  PersistenceException {
    logger.debug("will remove " + contribution + " of type " + type.name() + " for contributor " + contributor);
    be.webdeb.infra.persistence.model.Contributor cor =
            be.webdeb.infra.persistence.model.Contributor.findById(contributor);
    be.webdeb.infra.persistence.model.Contribution cib =
            be.webdeb.infra.persistence.model.Contribution.findById(contribution);

      if (cor == null) {
      throw new ObjectNotFoundException(Contributor.class, contributor);
    }

      if (cib == null || type.id() != cib.getContributionType().getIdContributionType()) {
      throw new ObjectNotFoundException(Contribution.class, contribution);
    }

    // in case of many groups, user is admin, or in case of one group user is owner of group
      if (!contributorIsOwnerOfContribution(cor, cib)) {
      throw new PermissionException(PermissionException.Key.NOT_GROUP_OWNER);
    }

    Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
      boolean notDeleted = false;
      try {
      // must handle deletion of bound contributions by hand because of soft delete
      switch (EContributionType.value(cib.getContributionType().getIdContributionType())) {
        case TEXT:
          if(cib.getText().getFirstContextArgument() != null)
            remove(cib.getText().getFirstContextArgument().getIdContribution(), EContributionType.ARGUMENT_CONTEXTUALIZED, contributor);

          be.webdeb.infra.persistence.model.ExternalContribution origin = cib.getOriginExternalContribution();
          if(origin != null) {
            remove(origin.getIdContribution(), origin.getContribution().getContributionType().getEContributionType(), contributor);
          }

          for (be.webdeb.infra.persistence.model.Excerpt excerpt : cib.getText().getExcerpts()) {
            remove(excerpt.getIdContribution(), EContributionType.EXCERPT, contributor);
          }

          for (ArgumentContext a : cib.getArgumentContexts()) {
            remove(a.getIdContribution(), EContributionType.ARG_JUSTIFICATION, contributor, ERemoveOption.CONTEXT);
          }
          break;

        case ARGUMENT:
          for (ArgumentSimilarity link : cib.getArgument().getSimilarArguments()) {
            remove(link.getIdContribution(), EContributionType.ARG_SIMILARITY, contributor);
          }

          for (ArgumentContext context : cib.getArgument().getContextualizedArguments()) {
            remove(context.getIdContribution(), EContributionType.ARGUMENT_CONTEXTUALIZED, contributor);
          }
          break;

        case ARGUMENT_CONTEXTUALIZED:

          if(cib.getArgumentContext().getDebate() != null) {
            remove(cib.getArgumentContext().getDebate().getIdContribution(), EContributionType.DEBATE, contributor);
            notDeleted = true;
          }else {
            for (ArgumentJustification link : cib.getArgumentContext().getArgumentJustificationsFrom()) {
              remove(link.getIdContribution(), EContributionType.ARG_JUSTIFICATION, contributor, option == ERemoveOption.CONTEXT || option == ERemoveOption.TOTAL ? ERemoveOption.TOTAL : ERemoveOption.KEEP_LINK_FROM);
            }

            for (ArgumentJustification link : cib.getArgumentContext().getArgumentJustificationsTo()) {
              remove(link.getIdContribution(), EContributionType.ARG_JUSTIFICATION, contributor, option == ERemoveOption.CONTEXT || option == ERemoveOption.TOTAL? ERemoveOption.TOTAL : ERemoveOption.KEEP_LINK_TO);
            }

            for (ArgumentHasExcerpt link : cib.getArgumentContext().getArgumentIllustrations()) {
              remove(link.getIdContribution(), EContributionType.ARG_ILLUSTRATION, contributor);
            }
          }
          break;

        case DEBATE:

          ArgumentContext arg = cib.getDebate().getFirstContextArgument();
          if(arg != null) {
            cib.getDebate().setFirstContextArgument(null);
            cib.getDebate().update();
            arg.setDebate(null);
            arg.update();
            remove(arg.getIdContribution(), EContributionType.ARGUMENT_CONTEXTUALIZED, contributor);
          }

          for (ArgumentContext a : cib.getArgumentContexts()) {
            remove(a.getIdContribution(), EContributionType.ARGUMENT_CONTEXTUALIZED, contributor, ERemoveOption.CONTEXT);
          }
          break;

        case EXCERPT:

          origin = cib.getOriginExternalContribution();
          if(origin != null) {
            remove(origin.getIdContribution(), origin.getContribution().getContributionType().getEContributionType(), contributor);
          }

          for (ArgumentHasExcerpt link : cib.getExcerpt().getArguments()) {
            remove(link.getIdContribution(), EContributionType.ARG_ILLUSTRATION, contributor);
          }
          break;

        case ARG_JUSTIFICATION:
          // TODO simplifier
          ArgumentJustification justification = cib.getJustificationLink();
          if(option == ERemoveOption.KEEP_LINK || option == ERemoveOption.KEEP_LINK_FROM) {
            if (justification.getArgumentFrom().getContext().getIdContribution().equals(justification.getArgumentTo().getContext().getIdContribution())) {
              for (ArgumentJustification just : justification.getArgumentFrom().getArgumentJustificationsTo()) {
                EArgumentLinkShade newShade = just.getShade().getELinkShade().isApprobation() ?
                        just.getShade().getELinkShade() : justification.getShade().getELinkShade().getOpposite();
                changeJustificationLink(just.getArgumentFrom(), justification.getArgumentTo(), justification.getContext(), newShade, justification.getContribution().getGroups(), cor);
              }
            }
          }else if(option == ERemoveOption.KEEP_LINK_TO){
            for (ArgumentJustification just : justification.getArgumentTo().getArgumentJustificationsFrom()) {
              EArgumentLinkShade newShade = just.getShade().getELinkShade().isApprobation() ?
                      just.getShade().getELinkShade() : justification.getShade().getELinkShade().getOpposite();
              changeJustificationLink(justification.getArgumentFrom(), just.getArgumentTo(), justification.getContext(), newShade, justification.getContribution().getGroups(), cor);
            }
          }
          break;

        case EXTERNAL_TEXT:
          be.webdeb.infra.persistence.model.ExternalContribution external = cib.getExternalContribution();
          external.getAuthors().forEach(ExternalAuthor::delete);

          for (ExternalExcerpt e :  external.getText().getExternalExcerpts()) {
            remove(e.getIdContribution(), EContributionType.EXTERNAL_EXCERPT, contributor);
          }

          break;

        case EXTERNAL_EXCERPT:

          external = cib.getExternalContribution();
          external.getAuthors().forEach(ExternalAuthor::delete);

          break;

        default:
          // ignore others
      }

      if(!notDeleted) {
        // trace deletion
        bindContributor(cib, cor, EModificationStatus.DELETE);
        // now delete this contribution
        cib.delete();
        logger.info("removed contribution " + contribution);
      }
      transaction.commit();
    } catch (Exception e) {
      logger.error("error while removing contribution " + contribution);
      throw new PersistenceException(PersistenceException.Key.DELETE_CONTRIBUTION, e);

    } finally {
      transaction.end();
    }
  }

  private void changeJustificationLink(ArgumentContext argumentFrom, ArgumentContext argumentTo,
       be.webdeb.infra.persistence.model.Contribution context, EArgumentLinkShade shade, List<Group> groups, Contributor cor) throws PersistenceException{
    be.webdeb.infra.persistence.model.Contribution c = initContribution(EContributionType.ARG_JUSTIFICATION.id(), null);
    c.save();

    ArgumentJustification newJust = new ArgumentJustification();
    c.setJustificationLink(newJust);
    newJust.setContribution(c);
    newJust.setIdContribution(c.getIdContribution());

    newJust.setArgumentTo(argumentTo);
    newJust.setArgumentFrom(argumentFrom);
    newJust.setContext(context);
    newJust.setValidationState(TValidationState.find.byId(EValidationState.VALIDATED.id()));
    newJust.setShade(TLinkShadeType.find.byId(shade.id()));
    newJust.save();
    c.setGroups(groups);
    c.update();

    bindContributor(c, cor, EModificationStatus.CREATE);
  }

  @Override
  public void saveMarkings(List<Contribution> contributions) throws PersistenceException {
    logger.debug("will save markings");

    Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
    try {
      for (Contribution contribution : contributions) {
        // get contribution
        be.webdeb.infra.persistence.model.Contribution c =
            be.webdeb.infra.persistence.model.Contribution.findById(contribution.getId());
        if (c != null) {
          logger.debug("set validated " + contribution.getValidated().getState() + " for contribution " + contribution.getId());
          // set marking and validated state
          c.setValidated(TValidationState.findById(contribution.getValidated().getState()));
          c.update();
        }
      }
      transaction.commit();
    } catch (Exception e){
      logger.error("unable to update validated state", e);
      throw new PersistenceException(PersistenceException.Key.MARK_GROUP, e);
    } finally {
      transaction.end();
    }
  }

  @Override
  public void merge(Long origin, Long replacement, Long contributor) throws PersistenceException, PermissionException {
    logger.debug("try to merge contributions " + origin + INTO + replacement);
    // check all preconditions
    be.webdeb.infra.persistence.model.Contributor cor =
        be.webdeb.infra.persistence.model.Contributor.findById(contributor);

    if (cor == null) {
      logger.error("no contributor found for given id " + contributor);
      throw new ObjectNotFoundException(be.webdeb.infra.persistence.model.Contributor.class, contributor);
    }

    be.webdeb.infra.persistence.model.Contribution old = be.webdeb.infra.persistence.model.Contribution.findById(origin);
    if (old == null) {
      logger.error(CONTRIBUTION_NOT_FOUND + origin);
      throw new ObjectNotFoundException(be.webdeb.infra.persistence.model.Contribution.class, origin);
    }

    be.webdeb.infra.persistence.model.Contribution repl = be.webdeb.infra.persistence.model.Contribution.findById(replacement);
    if (repl == null) {
      logger.error(CONTRIBUTION_NOT_FOUND + replacement);
      throw new ObjectNotFoundException(be.webdeb.infra.persistence.model.Contribution.class, replacement);
    }

    if (!contributorIsOwnerOfContribution(cor, old) || !contributorIsOwnerOfContribution(cor, repl)) {
      logger.error("contributor is not ozner of either " + old.getIdContribution() + " or " + repl.getIdContribution());
      throw new PermissionException(PermissionException.Key.NOT_GROUP_OWNER);
    }

    if (old.getContributionType().getIdContributionType() != repl.getContributionType().getIdContributionType()) {
      String more = "contributions types missmatch, old is " + old.getContributionType().getEn()
          + " replacement is " + repl.getContributionType().getEn();
      logger.error(more);
      throw new PersistenceException(PersistenceException.Key.MERGE_MISMATCH, more);
    }

    // now switch on concrete contribution type
    switch (EContributionType.value(old.getContributionType().getIdContributionType())) {
      case ACTOR:
        merge(old.getActor(), repl.getActor(), cor);
        break;
      case TEXT:
        merge(old.getText(), repl.getText(), cor);
        break;
      case DEBATE:
        merge(old.getDebate(), repl.getDebate(), cor);
        break;
      case ARGUMENT:
        merge(old.getArgument(), repl.getArgument(), cor);
        break;
      case ARGUMENT_CONTEXTUALIZED:
        merge(old.getArgumentContext(), repl.getArgumentContext(), cor);
        break;
      case EXCERPT:
        merge(old.getExcerpt(), repl.getExcerpt(), cor);
        break;
      case FOLDER:
        merge(old.getFolder(), repl.getFolder(), cor);
        break;
      default:
        logger.info("no way to merge this contribution type " + old.getContributionType().getEn());
    }
  }

  @Override
  public void removeFromGroup(Long contribution, int group, Long contributor) throws PermissionException, PersistenceException {
    logger.debug("will remove contribution " + contribution + " from group " + group);

    be.webdeb.infra.persistence.model.Contribution c = be.webdeb.infra.persistence.model.Contribution.findById(contribution);
    if (c == null) {
      logger.error(CONTRIBUTION_NOT_FOUND + contribution);
      throw new ObjectNotFoundException(be.webdeb.infra.persistence.model.Contribution.class, contribution);
    }

    // check if group can be found
    be.webdeb.infra.persistence.model.Group g = be.webdeb.infra.persistence.model.Group.findById(group);
    if (g == null) {
      logger.error("no group found for given id " + group);
      throw new ObjectNotFoundException(be.webdeb.infra.persistence.model.Group.class, (long) group);
    }

    // contributor exists and has sufficient rights ?
    Contributor cor = checkContributor(contributor, group);
    if (cor == null ||
        cor.getContributorHasGroups().stream().noneMatch(gr -> gr.getGroup().getIdGroup() == group
            && gr.getRole().getIdRole() > EContributorRole.CONTRIBUTOR.id())) {
      logger.error("contributor " + contributor + " has no right on " + c.getIdContribution());
      throw new PermissionException(PermissionException.Key.NOT_GROUP_OWNER);
    }

    // now remove group from given contribution
    Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
    try {
      // if this contribution does not belong to any group anymore, delete it
      if (c.getGroups().size() == 1) {
        remove(contribution, EContributionType.value(c.getContributionType().getIdContributionType()), contributor);
      } else {
        // trace removal of group in db
        bindContributor(c, cor, EModificationStatus.GROUP_REMOVAL);
        c.getGroups().removeIf(gr -> gr.getIdGroup() == group);
        c.update();
      }
      transaction.commit();
    } catch (Exception e) {
      logger.error("unable to remove group " + group + " from contribution " + contribution, e);
    } finally {
      transaction.end();
    }
  }

  @Override
  public boolean isMemberOfAPublicGroup(Long contribution) {
    be.webdeb.infra.persistence.model.Contribution c = be.webdeb.infra.persistence.model.Contribution.findById(contribution);
    if(c != null){

      Set<Integer> publicGroupIds = Group.findPublicGroupIds();

      for(Integer id : c.getGroupIds()){
        if(publicGroupIds.contains(id)){
          return true;
        }
      }
    }

    return false;
  }

  @Override
  public List<ContributionHistory> getHistory(Long contribution) {
    List<ContributionHistory> result = new ArrayList<>();
    for (ContributionHasContributor chc : ContributionHasContributor.findByContribution(contribution)) {
      try {
        result.add(factory.createHistory(mapper.toContributor(chc.getContributor()), EModificationStatus.value(chc.getStatus().getIdStatus()),
            chc.getSerialization(), new Date(chc.getId().getVersion().getTime())));

      } catch (FormatException e) {
        logger.warn("unable to create history line for " + chc, e);
      }
    }
    return result;
  }

  @Override
  public void saveJustificationLinks(List<be.webdeb.core.api.argument.ArgumentJustification> links, Contribution context, int currentGroup, Long contributor) throws PermissionException, PersistenceException {
    logger.debug("try to save justification links for context " + context.getId());

    // if context does not belong to this group, virtually add it, check will verify this group exists anyway
    if (context.getInGroups().stream().noneMatch(g -> g.getGroupId() == currentGroup)) {
      context.addInGroup(currentGroup);
    }
    checkContribution(context, contributor, currentGroup);
    checkContributor(contributor, currentGroup);

    Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
    try {
      be.webdeb.infra.persistence.model.Contribution c = be.webdeb.infra.persistence.model.Contribution.findById(context.getId());
      // get all previously existing justification links and keep only no-more existing ones
      List<Long> old = c.getArgumentJustificationLinks().stream().map(ArgumentJustification::getIdContribution).collect(Collectors.toList());

      for (be.webdeb.core.api.argument.ArgumentJustification l : links) {
        l.addInGroup(currentGroup);
        l.save(contributor, currentGroup);
        old.remove(l.getId());
      }

      // now remove remaining old links
      for (Long id : old) {
        be.webdeb.infra.persistence.model.ArgumentJustification link = be.webdeb.infra.persistence.model.ArgumentJustification.findById(id);
        logger.debug("will now remove justification link " + id);
        link.getContribution().delete();
      }

      transaction.commit();

    } catch (Exception e) {
      logger.error("unable to update justification links for context " + context.getId(), e);
      throw new PersistenceException(PersistenceException.Key.SAVE_JUSTIFICATION_LINK, e);

    } finally {
      transaction.end();
    }
  }

  @Override
  public List<be.webdeb.core.api.argument.ArgumentContext> getContextualizedArgumentsFromContext(Long id) {
    return getContextualizedArgumentsFromContext(id, 0, Integer.MAX_VALUE).getContributions();
  }

  @Override
  public PartialContributions<be.webdeb.core.api.argument.ArgumentContext> getContextualizedArgumentsFromContext(Long id, int fromIndex, int toIndex) {
    PartialContributions<be.webdeb.core.api.argument.ArgumentContext> results = new ConcretePartialContributions<>();

    if(checkSubIndexes(fromIndex, toIndex)) {
      be.webdeb.infra.persistence.model.Contribution c = be.webdeb.infra.persistence.model.Contribution.findById(id);

      if (c != null) {
        List<ArgumentContext> args = c.getArgumentContexts().stream().skip(fromIndex).limit(toIndex - fromIndex)
                .filter(e -> !e.getContribution().isHidden())
                .sorted(Comparator.comparing(ArgumentContext::getVersion).reversed()).collect(Collectors.toList());

        if (c.getContributionType().getIdContributionType() == EContributionType.DEBATE.id()) {
          Debate d = be.webdeb.infra.persistence.model.Debate.findById(id);
          if (d != null && !d.getFirstContextArgument().getContext().getIdContribution().equals(d.getIdContribution())) {
            args.add(d.getFirstContextArgument());
          }
        }

        results.setContributions(buildContextualizedArgList(args));
        results.setNumberOfLoadedContributions(args.size());
      }
    }
    return results;
  }

  @Override
  public List<be.webdeb.core.api.argument.ArgumentJustification> getArgumentJustificationLinks(Long id) {
    be.webdeb.infra.persistence.model.Contribution c = be.webdeb.infra.persistence.model.Contribution.findById(id);
    if(c != null){
      return buildJustificationLinkList(c.getArgumentJustificationLinks());
    }
    return new ArrayList<>();
  }

  @Override
  public List<Language> getLanguages() {
    if (languages == null) {
      languages = TLanguage.find.all().stream().map(t -> {
        LinkedHashMap<String, String> names = new LinkedHashMap<>(t.getTechnicalNames());
        names.put("own", t.getOwn());
        return factory.createLanguage(t.getCode(), names);
      }).collect(Collectors.toList());
    }
    return languages;
  }

  @Override
  public be.webdeb.core.api.argument.ArgumentContext getOrCreateFirstArgument(Long id){
    ContextContribution context = retrieveContextContribution(id);
    if(context != null){
      ArgumentContext arg = null;

      switch(context.getType()){
        case TEXT:
          arg = Text.findById(id).getFirstContextArgument();
          break;
        case DEBATE:
          arg = Debate.findById(id).getFirstContextArgument();
          break;
        default:
          logger.debug("Should not append...");
      }
      try {
        if(arg == null && context.getType() == EContributionType.TEXT){
          // open transaction and handle the whole save chain
          Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
          be.webdeb.infra.persistence.model.Contribution ctx = be.webdeb.infra.persistence.model.Contribution.findById(id);

          be.webdeb.infra.persistence.model.Contribution c =
                  initContribution(EContributionType.ARGUMENT_CONTEXTUALIZED.id(), "", true);

          arg = new ArgumentContext();
          c.setArgumentContext(arg);
          arg.setContribution(c);

          c.addGroup(Group.getPublicGroup());
          c.save();

          arg.setIdContribution(c.getIdContribution());
          arg.setContext(ctx);
          arg.setArgument(be.webdeb.infra.persistence.model.Argument.getDefaultArgument());
          arg.save();

          bindContributor(c, ctx.getContributionHasContributors().get(0).getContributor(), EModificationStatus.CREATE);

          Text text = Text.findById(id);
          text.setFirstContextArgument(arg);
          text.update();

          transaction.commit();
          transaction.end();
        }

        if(arg != null)
          return mapper.toArgumentContext(arg);
      }catch(Exception e){
        logger.debug("Unparsable argument context : " + e.getMessage());
      }
    }
    return null;
  }

  @Override
  public Map<EContributionType, Integer> getCountRelationsMap(Long id, Long contributorId, int groupId) {
    be.webdeb.infra.persistence.model.Contribution contribution = be.webdeb.infra.persistence.model.Contribution.findById(id);
    return contribution != null ? contribution.getCountRelationsMap(contributorId, groupId) : new HashMap<>();
  }

  @Override
  public List<Long> getAllIdByContributionType(EContributionType type) {
    return be.webdeb.infra.persistence.model.Contribution.getAllIdByContributionType(type);
  }

  /*
   * Convenience methods
   */

  /**
   * Bind a contributor to a contribution, i.e, we track that a given contributor made some changes to a
   * contribution.
   *
   * @param contribution a contribution id, must exist
   * @param contributor the contributor that created or updated the given contribution
   * @param status the status of the action performed on the contribution
   */
  protected final void bindContributor(be.webdeb.infra.persistence.model.Contribution contribution,
      be.webdeb.infra.persistence.model.Contributor contributor, EModificationStatus status)
      throws PersistenceException {

    logger.debug("try to bind contributor with id " + contributor.getIdContributor() + " to " + contribution.getIdContribution());
    try {
      // enforce refresh from contribution because we are in a transaction and must read uncommited data
      contribution.refresh();
      ContributionHasContributor chc = new ContributionHasContributor(contribution, contributor, TModificationStatus.find.byId(status.id()));
      chc.save();
      logger.info("saved " + chc.toString());

    } catch (Exception e) {
      String more = "error while binding contributor " + contributor + TO_CONTRIBUTION + contribution;
      logger.error(more, e);
      throw new PersistenceException(PersistenceException.Key.BIND_CONTRIBUTOR, more, e);
    }
  }

  /**
   * Bind a contributor to a merge action of two contributions
   *
   * @param origin a contribution id, must exist
   * @param replacement another contribution id, must exist
   * @param contributor the contributor that created or updated the given contribution
   */
  protected final void bindContributor(be.webdeb.infra.persistence.model.Contribution origin,
      be.webdeb.infra.persistence.model.Contribution replacement,
      be.webdeb.infra.persistence.model.Contributor contributor)
      throws PersistenceException {

    logger.debug("try to bind contributor with id " + contributor.getIdContributor() + " to " + replacement.getIdContribution());
    try {
      ContributionHasContributor chc = new ContributionHasContributor(origin, replacement, contributor);
      chc.save();
      logger.info("saved " + chc.toString());

    } catch (Exception e) {
      String more = "error while binding contributor " + contributor + " for replacement of contribution "
          + origin + " by " + replacement;
      logger.error(more, e);
      throw new PersistenceException(PersistenceException.Key.BIND_CONTRIBUTOR, more, e);
    }
  }

  /**
   * This method checks the common preconditions to any save actions of contribution.
   * If given contribution has an id, it must have the same type as the one retrieved from the
   * repository.
   *
   * @param contribution a contribution to check before being persisted into the repository
   * @param contributor the id of the user that works with the contribution
   * @param currentGroup the current group id from which the contributor triggered the save action (for auto-created actors)
   * @return the db contribution corresponding to given api contribution, if it exists, null otherwise
   * @throws OutdatedVersionException if modifications to an existing element are performed on an outdated
   * element (version numbers do not match)
   * @throws ObjectNotFoundException if given contribution id does not exist
   * @throws PermissionException if given contribution does not belong to given current group
   */
  protected be.webdeb.infra.persistence.model.Contribution checkContribution(Contribution contribution, Long contributor, int currentGroup)
      throws PermissionException, ObjectNotFoundException, OutdatedVersionException {
    // check if contribution exists, right type and version number
    if (contribution == null) {
      logger.error("given contribution is null");
      throw new ObjectNotFoundException(Contribution.class, null);
    }

    be.webdeb.infra.persistence.model.Contribution c = null;
    if (contribution.getId() != null && contribution.getId() != -1L) {
      c = be.webdeb.infra.persistence.model.Contribution.findById(contribution.getId());

      if (c == null || c.getContributionType().getIdContributionType() != contribution.getType().id()) {
        logger.error(CONTRIBUTION_NOT_FOUND + contribution.getId());
        throw new ObjectNotFoundException(contribution.getClass(), contribution.getId());

      } else {
        long version = c.getVersion().getTime();
        if (version != contribution.getVersion()) {
          logger.error("old version for given object " + contribution.getId()
              + " actual is " + version + " (" + new Date(version) + ") and yours is " + contribution.getVersion()
              + " (" + new Date(contribution.getVersion()) + ")");
          Contribution newOne = null;
          try {
            newOne = mapper.toContribution(c);
          } catch (FormatException e) {
            logger.error("unable to cast retrieved contribution " + c.getIdContribution(), e);
          }
          throw new OutdatedVersionException(contribution, newOne);
        }
      }
    }

    // check if groups can be found
    for (be.webdeb.core.api.contributor.Group group : contribution.getInGroups()) {
      if (be.webdeb.infra.persistence.model.Group.findById(group.getGroupId()) == null) {
        logger.error("no group found for given id " + group);
        throw new ObjectNotFoundException(be.webdeb.infra.persistence.model.Group.class, (long) group.getGroupId());
      }
    }

    be.webdeb.infra.persistence.model.Contributor user = Contributor.findById(contributor);
    // check if current group belongs to contribution groups
    // currently, we don't check for actor contribution due to private group problem (to change)
    if ((user == null || !user.isAdmin()) && contribution.getType().id() != EContributionType.ACTOR.id() &&
        contribution.getInGroups().stream().noneMatch(g -> g.getGroupId() == currentGroup)) {
      String more = "current group " + currentGroup + " is not in contribution group list " + contribution.getInGroups();
      logger.error(more);
      throw new PermissionException(PermissionException.Key.ERROR_SCOPE, more);
    }
    return c;
  }

  /**
   * This method checks the common preconditions to any save actions of contribution regarding a contributor.
   *
   * @param contributor a contributor id to check if existing
   * @param currentGroup the current group id from which the contributor triggered the save action (for auto-created actors)
   * @return the db contributor corresponding to given api contributor
   * @throws PersistenceException if contributor is not found, there is a type mismatch, or version numbers do not correspond
   * @throws PermissionException if contributor does not belong to given group
   */
  be.webdeb.infra.persistence.model.Contributor checkContributor(Long contributor, int currentGroup) throws PersistenceException, PermissionException {
    // check if contributor can be found
    be.webdeb.infra.persistence.model.Contributor c = be.webdeb.infra.persistence.model.Contributor.findById(contributor);
    if (c == null) {
      logger.error("no contributor found for given id " + contributor);
      throw new ObjectNotFoundException(be.webdeb.infra.persistence.model.Contributor.class, contributor);
    }

    // check if contributor can publish in group
    if (c.getContributorHasGroups().stream().noneMatch(g -> g.getGroup().getIdGroup() == currentGroup)) {
      logger.error("contributor " + contributor + " may not publish in group " + currentGroup);
      throw new PermissionException(PermissionException.Key.NOT_GROUP_MEMBER);
    }
    return c;
  }

  /**
   * Update groups in given db contribution with given api contribution groups. Will only add all groups
   * in given api object into given db object, groups that are not present in api object are not removed
   * from given db object. To explicitly remove groups, you must use the dedicated removeFromGroup method
   *
   * @param api an api contribution
   * @param db a db contribution that will be updated with groups in given api contribution
   */
  protected final void updateGroups(Contribution api, be.webdeb.infra.persistence.model.Contribution db) {
    switch(api.getType()){
      case ACTOR:
      case ARGUMENT:
      case FOLDER:
      case FOLDERLINK:
      case EXTERNAL_EXCERPT:
      case EXTERNAL_TEXT:
        db.setGroups(Collections.singletonList(Group.getPublicGroup()));
        break;
      default:
        api.getInGroups().forEach(g -> db.addGroup(be.webdeb.infra.persistence.model.Group.findById(g.getGroupId())));
    }
  }

  /**
   * Manage bindings to actors, retrieve them, save unexisting ones and remove others
   *
   * @param apiContribution the api contribution as updated by a contributor
   * @param currentGroup the current group id to be used for auto-saved actors
   * @param contribution the existing contribution as persisted in database
   * @return the (possibly empty) list of auto-created actors (as contribution)
   */
  protected final List<Contribution> updateActorBindings(TextualContribution apiContribution,
      be.webdeb.infra.persistence.model.Contribution contribution, int currentGroup, Long contributor)
      throws PersistenceException, PermissionException {
    List<Contribution> actors = new ArrayList<>();
    Map<Long, Long> oldBindings = new HashMap<>();

    contribution.getActors().forEach(cha ->
        oldBindings.put(cha.getActor().getIdContribution(), cha.getIdCha()));

    for (ActorRole a : apiContribution.getActors()) {
      logger.debug("will bind actor " + a.toString());
      // either create or update mapping
      actors.addAll(bindActor(contribution.getIdContribution(), a, currentGroup, contributor));
      oldBindings.remove(a.getActor().getId());
    }

    oldBindings.forEach((a, id) -> {
      ContributionHasActor.deleteStatic(id);
    });
    return actors;
  }

  /**
   * Update the content of a given text with given excerpt
   *
   * @param excerpt an excerpt
   * @param text a text
   * @param filename the filename where the content of the text is actually stored
   */
  protected final void updateTextContent(be.webdeb.core.api.excerpt.Excerpt excerpt, Text text, String filename) {
    if ("text/plain".equals(files.getContentType(filename))) {
      // get content, check if excerpt is in it, if not, append to current content
      String content = files.getContributionTextFile(filename);
      if (!files.textContainsExcerpt(content, excerpt.getOriginalExcerpt())) {
        StringBuilder builder = new StringBuilder(content);
        if (builder.length() > 0) {
          builder.append("\n\n(...)\n\n").append(excerpt.getOriginalExcerpt());
        }
        files.deleteAnnotatedText(filename);
        logger.info("saved new content in text " + text.getOriginalTitle() + " with filename " + filename);
        files.saveContributionTextFile(filename, builder.toString());
      }
    }
  }

  /**
   * Create a list of Actor18name for given db actor from given api actor
   *
   * @param api the api actor with a list of names
   * @param db the db actor to which those names will be bound
   * @return the db names from given api names
   */
  protected List<ActorI18name> toI18names(Actor api, be.webdeb.infra.persistence.model.Actor db) {
    List<ActorI18name> result = api.getNames().stream().map(n -> toI18name(db, n, false)).collect(Collectors.toList());
    if (EActorType.ORGANIZATION.equals(api.getActorType())) {
      result.addAll(((be.webdeb.core.api.actor.Organization) api).getOldNames().stream().map(n ->
          toI18name(db, n, true)).collect(Collectors.toList()));
    }
    return result;
  }

  @Override
  public List<WarnedWord> getWarnedWords(int contextType, int type) {
    List<WarnedWord> words = new ArrayList<>();
    TWarnedWordType cType = TWarnedWordType.find.byId(contextType);
    if(cType != null) {
      cType.getWarnedWords().stream().filter(e -> e.getType() == type).forEach(t ->
        words.add(factory.createWarnedWord(t.getIdWarnedWord(),
                new LinkedHashMap<>(t.getTechnicalNames()), t.getType(), t.getContextType().getIdWarnedWordType()))
      );
    }
    return words;

  }

  @Override
  public Long retrievePlaceContinentCode(String code){
    be.webdeb.infra.persistence.model.Place p = be.webdeb.infra.persistence.model.Place.findContinentByCode(code);
    if(p != null){
      return p.getId();
    }
    return null;
  }

  @Override
  public Long retrievePlaceByGeonameIdOrPlaceId(Long geonameId, Long placeId){
    be.webdeb.infra.persistence.model.Place p = be.webdeb.infra.persistence.model.Place.findByGeonameIdorPlaceId(geonameId, placeId);
    if(p != null){
      return p.getId();
    }
    return null;
  }

  @Override
  public PlaceType findPlaceTypeByCode(int code){
    TPlaceType t = TPlaceType.find.byId(code);
    if(t != null){
      return factory.createPlaceType(t.getIdType(), new LinkedHashMap<>(t.getTechnicalNames()));
    }
    return null;
  }

  @Override
  public void savePlaces(List<Place> places, be.webdeb.infra.persistence.model.Contribution contribution){
    contribution.setPlaces(new ArrayList<>());

    for(Place place : places) {
      be.webdeb.infra.persistence.model.Place p = savePlace(place);
      if(p != null) {
        contribution.getPlaces().add(p);
      }
    }
  }

  @Override
  public be.webdeb.infra.persistence.model.Place savePlace(Place place){
    if(place != null) {
      be.webdeb.infra.persistence.model.Place p =
          be.webdeb.infra.persistence.model.Place.findByGeonameIdorPlaceId(place.getGeonameId(), place.getId());
      if (p == null && place.getPlaceType() != null) {
        p = new be.webdeb.infra.persistence.model.Place(place.getGeonameId(), place.getCode(), place.getLatitude(), place.getLongitude());
        p.setPlaceType(TPlaceType.find.byId(place.getPlaceType().getType()));
        if(p.getPlaceType() != null) {
          p.save();
          final long pId = p.getId();
          p.setSpellings(pId, place.getNames());
          p.setContinent(savePlace(place.getContinent()));
          p.setCountry(savePlace(place.getCountry()));
          p.setRegion(savePlace(place.getRegion()));
          p.setSubregion(savePlace(place.getSubregion()));
          p.update();
        }
      }
      return p;
    }
    return null;
  }

  @Override
  public List<ValidationState> getValidationStates() {
    if (validationStates == null) {
      validationStates = TValidationState.find.all().stream().map(t ->
              factory.createValidationState(t.getIdValidationState(), new LinkedHashMap<>(t.getTechnicalNames()))
      ).collect(Collectors.toList());
    }
    return validationStates;
  }

  @Override
  public void updateDiscoveredExternalState(Long id, boolean rejected) throws PersistenceException {
    logger.debug("try to set rejected state " + rejected + " to external contribution " + id);

    be.webdeb.infra.persistence.model.ExternalContribution c = be.webdeb.infra.persistence.model.ExternalContribution.findById(id);
    if (c == null) {
      throw new PersistenceException(PersistenceException.Key.SAVE_EXTERNAL);
    }

    try {
      c.setRejected(rejected);
      c.update();
      logger.info("set rejected state of " + id + " to " + (rejected ? "rejected" : "re-added"));
    } catch (Exception e) {
      logger.error("unable to update rejected state of external contribution " + id, e);
    }
  }

  @Override
  public void saveContextContribution(ContextContribution contribution, EContributionType type, Long contributor, int currentGroup) throws PermissionException, PersistenceException {
    logger.debug("try to save context contribution with id " + contribution.getId() + " in group " + contribution.getInGroups());
    checkContribution(contribution, contributor, currentGroup);
    Contributor dbContributor = checkContributor(contributor, currentGroup);

    Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
    be.webdeb.infra.persistence.model.Contribution c = initContribution(type.id(), "");
    c.save();
    // update groups
    updateGroups(contribution, c);

    switch(type){
      case DEBATE:
        Debate d = new Debate();
        c.setDebate(d);
        d.setContribution(c);
        d.setIdContribution(c.getIdContribution());
        d.save();
        break;
      case TEXT:
        Text t = new Text();
        c.setText(t);
        t.setContribution(c);
        t.setIdContribution(c.getIdContribution());
        t.save();
        break;
    }
    c.update();
    bindContributor(c, dbContributor, EModificationStatus.CREATE);

    contribution.setId(c.getIdContribution());
    transaction.commit();
    transaction.end();

  }

  @Override
  public void deleteContextContribution(Long contribution, Long contributor) throws PermissionException {
    be.webdeb.infra.persistence.model.Contribution c = be.webdeb.infra.persistence.model.Contribution.findById(contribution);
    Contributor dbContributor = Contributor.findById(contributor);
    if(c != null && dbContributor != null && ContributionHasContributor.checkContributionCreator(contributor, contribution)){
      c.delete();
    }
  }

  /**
   * Update linked folders with a given contribution
   *
   * @param folders the api list of folders linked with a givent contribution
   * @param contribution the contribution to link with folders
   */
  /*protected void updateFolder(List<be.webdeb.core.api.folder.Folder> folders, be.webdeb.infra.persistence.model.Contribution contribution){
    List<Folder> dbFolders = new ArrayList<>();
    for(be.webdeb.core.api.folder.Folder folder : folders){
      Folder dbFolder = Folder.findById(folder.getId());
      dbFolders.add(dbFolder);
    }
    contribution.setFolders(dbFolders);
  }*/

  /**
   * Convert given actor name into a db actori18name
   *
   * @param db the db actor holding the link to the new actor name
   * @param name the name to convert
   * @param isOld true if this is a previous name (only effective for organizations)
   * @return the mapped actori18name for this given name and linked to given db actor
   */
  private ActorI18name toI18name(be.webdeb.infra.persistence.model.Actor db, ActorName name, boolean isOld) {
    ActorI18name result = new ActorI18name(db, name.getLang());
    result.setPseudo(name.getPseudo());
    result.setName(name.getLast());
    result.setFirstOrAccro(name.getFirst());
    result.isOld(isOld);
    return result;
  }

  /**
   * Create a list of TextI18namefor given db text from given api text
   *
   * @param api the api text with a list of titles
   * @param db the db text to which those titles will be bound
   * @return the db titles from given api titles
   */
  protected List<TextI18name> toTextI18titles(be.webdeb.core.api.text.Text api, Text db) {
    return api.getTitles().entrySet().stream().map(n -> toTextI18title(db, n.getKey(), n.getValue())).collect(Collectors.toList());
  }

  /**
   * Convert given text ntitle into a db textI18name
   *
   * @param db the db text holding the link to the new text title
   * @param lang the lang to convert
   * @param name the name to convert
   * @return the mapped textI18name for this given title and linked to given db text
   */
  private TextI18name toTextI18title(Text db, String lang, String name) {
    return new TextI18name(db, lang, name);
  }

  /**
   * Helper method to wrap DB contributions to API contributions
   * @param contributions a list of DB contributions
   * @return the corresponding list of API contributions
   */
  protected List<Contribution> toContributions(List<be.webdeb.infra.persistence.model.Contribution> contributions) {
    List<Contribution> result = new ArrayList<>();
    if (!contributions.isEmpty()) {
      for (be.webdeb.infra.persistence.model.Contribution c : contributions) {
        try {
          result.add(mapper.toContribution(c));
        } catch (FormatException e) {
          logger.error("unable to cast contribution " + c.getIdContribution(), e);
        }
      }
    }
    return result;
  }

  /**
   * Helper method to build a list of API affiliations from DB actor has affiliations as member. All uncastable elements are ignored.
   *
   * @param affiliations a list of DB affiliations
   * @return a list of API affiliations with elements that could have actually been casted to API element (may be empty)
   */
  protected List<Affiliation> buildMemberList(List<ActorHasAffiliation> affiliations) {
    List<Affiliation> result = new ArrayList<>();
    for (ActorHasAffiliation aha : affiliations) {
      try {
        result.add(mapper.toMember(aha));
      } catch (FormatException e) {
        logger.error("unable to cast affiliation " + aha.getId(), e);
      }
    }
    return result;
  }

  /**
   * Helper method to build a list of API affiliations from DB actor has affiliations as affiliation. All uncastable elements are ignored.
   *
   * @param affiliations a list of DB affiliations
   * @return a list of API affiliations with elements that could have actually been casted to API element (may be empty)
   */
  protected List<Affiliation> buildAffiliationList(List<ActorHasAffiliation> affiliations) {
    List<Affiliation> result = new ArrayList<>();
    for (ActorHasAffiliation aha : affiliations) {
      try {
        result.add(mapper.toAffiliation(aha));
      } catch (FormatException e) {
        logger.error("unable to cast affiliation " + aha.getId(), e);
      }
    }
    return result;
  }

  /**
   * Helper method to build a list of API texts from DB texts. All uncastable elements are ignored.
   *
   * @param texts a list of DB texts
   * @return a list of API texts with elements that could have actually been casted to API element (may be empty)
   */
  protected List<be.webdeb.core.api.text.Text> buildTextList(List<be.webdeb.infra.persistence.model.Text> texts) {
    List<be.webdeb.core.api.text.Text> result = new ArrayList<>();
    for (be.webdeb.infra.persistence.model.Text t : texts) {
      try {
        result.add(mapper.toText(t));
      } catch (FormatException e) {
        logger.error("unable to cast text " + t.getIdContribution(), e);
      }
    }
    return result;
  }

  /**
   * Helper method to build a list of API debates from DB debates. All uncastable elements are ignored.
   *
   * @param debates a list of DB debates
   * @return a list of API debates with elements that could have actually been casted to API element (may be empty)
   */
  protected List<be.webdeb.core.api.debate.Debate> buildDebateList(List<be.webdeb.infra.persistence.model.Debate> debates) {
    List<be.webdeb.core.api.debate.Debate> result = new ArrayList<>();
    for (be.webdeb.infra.persistence.model.Debate d : debates) {
      try {
        result.add(mapper.toDebate(d));
      } catch (FormatException e) {
        logger.error("unable to cast debate " + d.getIdContribution(), e);
      }
    }
    return result;
  }

  /**
   * Helper method to build a list of API arguments dictionary from DB arguments dictionary. All uncastable elements are ignored.
   *
   * @param arguments a list of DB arguments dictionary
   * @return a list of API arguments dictionary with elements that could have actually been casted to API element (may be empty)
   */
  protected List<be.webdeb.core.api.argument.ArgumentDictionary> buildArgDictionaryList(List<ArgumentDictionary> arguments) {
    List<be.webdeb.core.api.argument.ArgumentDictionary> result = new ArrayList<>();
    for (ArgumentDictionary a : arguments) {
      try {
        result.add(mapper.toArgumentDictionary(a));
      } catch (FormatException e) {
        logger.error("unable to cast argument dictionary " + a.getIdDictionary(), e);
      }
    }
    return result;
  }

  /**
   * Helper method to build a list of API arguments from DB arguments. All uncastable elements are ignored.
   *
   * @param arguments a list of DB arguments
   * @return a list of API arguments with elements that could have actually been casted to API element (may be empty)
   */
  protected List<Argument> buildArgList(List<be.webdeb.infra.persistence.model.Argument> arguments) {
    List<Argument> result = new ArrayList<>();
    for (be.webdeb.infra.persistence.model.Argument a : arguments) {
      try {
        result.add(mapper.toArgument(a));
      } catch (FormatException e) {
        logger.error("unable to cast argument " + a.getIdContribution(), e);
      }
    }
    return result;
  }

  /**
   * Helper method to build a list of API contextualized arguments from DB contextualized arguments. All uncastable elements are ignored.
   *
   * @param arguments a list of DB contextualized arguments
   * @return a list of API contextualized arguments with elements that could have actually been casted to API element (may be empty)
   */
  protected List<be.webdeb.core.api.argument.ArgumentContext> buildContextualizedArgList(List<ArgumentContext> arguments) {
    List<be.webdeb.core.api.argument.ArgumentContext> result = new ArrayList<>();
    for (ArgumentContext a : arguments) {
      try {
        result.add(mapper.toArgumentContext(a));
      } catch (FormatException e) {
        logger.error("unable to cast contextualized argument " + a.getIdContribution(), e);
      }
    }
    return result;
  }

  /**
   * Helper method to build a list of API excerpts from DB excerpts. All uncastable elements are ignored.
   *
   * @param excerpts a list of DB excerpts
   * @return a list of API excerpts with elements that could have actually been casted to API element (may be empty)
   */
  protected List<be.webdeb.core.api.excerpt.Excerpt> buildExcerptList(List<Excerpt> excerpts) {
    List<be.webdeb.core.api.excerpt.Excerpt> result = new ArrayList<>();
    for (be.webdeb.infra.persistence.model.Excerpt e : excerpts) {
      try {
        result.add(mapper.toExcerpt(e));
      } catch (FormatException ex) {
        logger.error("unable to cast excerpt " + e.getIdContribution(), ex);
      }
    }
    return result;
  }

  /**
   * Helper method to build a list of API external excerpts from DB external excerpts. All uncastable elements are ignored.
   *
   * @param excerpts a list of DB external excerpts
   * @return a list of API external excerpts with elements that could have actually been casted to API element (may be empty)
   */
  protected List<be.webdeb.core.api.excerpt.ExternalExcerpt> buildExternalExcerptList(List<ExternalExcerpt> excerpts) {
    List<be.webdeb.core.api.excerpt.ExternalExcerpt> result = new ArrayList<>();
    for (ExternalExcerpt e : excerpts) {
      try {
        result.add(mapper.toExternalExcerpt(e));
      } catch (FormatException ex) {
        logger.error("unable to cast external argument " + e.getIdContribution(), ex);
      }
    }
    return result;
  }

  /**
   * Helper method to build a list of API Folders from DB folders. All uncastable elements are ignored.
   *
   * @param folders a list of DB folders
   * @return a set of API folders with elements that could have actually been casted to API element (may be empty)
   */
  protected Set<be.webdeb.core.api.folder.Folder> buildFolderSet(List<Folder> folders) {
    Set<be.webdeb.core.api.folder.Folder> result = new HashSet<>();
    for (be.webdeb.infra.persistence.model.Folder f : folders) {
      try {
        result.add(mapper.toFolder(f));
      } catch (FormatException e) {
        logger.error("unable to cast folder " + f.getIdContribution(), e);
      }
    }
    return result;
  }

  /**
   * Helper method to build a list of API Places from DB places.
   *
   * @param places a list of DB places
   * @return a list of API places with elements that could have actually been casted to API element (may be empty)
   */
  protected List<Place> buildPlacesList(List<be.webdeb.infra.persistence.model.Place> places) {
    List<be.webdeb.core.api.contribution.Place> result = new ArrayList<>();
    for (be.webdeb.infra.persistence.model.Place p : places) {
        result.add(mapper.toPlace(p));
    }
    return result;
  }

  /**
   * Merge two actors, filling all details not present in replacement with the ones from origin.
   * <br><br>
   * Any bound contribution to the origin will be bound to the replacement actor. Reconciliations regarding affiliation
   * will be performed on affiliation actors. For all origin affiliations, if no such affiliation to an actor exists
   * in replacement, it will be copied from origin. If the affiliation has no link to an actor, and such function does not
   * exists, it will be copied in replacement actor too.
   *
   * @param origin an actor to be merged into the replacement
   * @param replacement the actor that will replace given origin
   * @param contributor the contributor asking for that merge action
   * @throws PersistenceException if any error occurred at the database level
   */
  private void merge(be.webdeb.infra.persistence.model.Actor origin, be.webdeb.infra.persistence.model.Actor replacement,
      Contributor contributor) throws PersistenceException {
    logger.debug(MERGING + origin.toString() + INTO + replacement.toString());

    Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
    try {
      // for all affiliations of origin, check if we have a perfect match, otherwise, bind it to replacement
      origin.getAffiliations().forEach(aff -> {
        if (aff.getAffiliation() != null) {
          // try to find a matching organization, if none, add it
          List<ActorHasAffiliation> matches = replacement.getAffiliations().stream().filter(a -> a.getAffiliation() != null
              && a.getAffiliation().getIdContribution().equals(aff.getAffiliation().getIdContribution())).collect(Collectors.toList());
          if (matches.isEmpty()) {
            aff.setActor(replacement);
            aff.update();
          } else {
            // we have a match, if functions corresponds, rebind possible
            Optional<ActorHasAffiliation> found = matches.stream().filter(aha ->
                // either both are null
                (aha.getFunction() == null && aff.getFunction() == null)
                // or they are equal
                || (aha.getFunction() != null && aff.getFunction() != null && aha.getFunction().getIdProfession() == aff.getFunction().getIdProfession())).findAny();
            if (found.isPresent()) {
              // rebind contribution_has_actor aha id to this replacement affiliation
              ContributionHasActor.findByAffiliation(aff.getId()).forEach(a -> {
                a.setActorIdAha(found.get().getId());
                a.update();
              });

            } else {
              // we found an affiliation, but they do not perfectly match on function, add it to this replacement actor
              aff.setActor(replacement);
              aff.update();
            }
          }

        } else {
          // no affiliation actor, then there is a function, try to find match
          Optional<ActorHasAffiliation> match = replacement.getAffiliations().stream().filter(a -> a.getFunction() != null
              && a.getFunction().getIdProfession() == aff.getFunction().getIdProfession()).findAny();
          if (!match.isPresent()) {
            aff.setActor(replacement);
            aff.update();
          } else {
            // rebind contribution_has_actor aha ids to this replacement affiliation
            ContributionHasActor.findByAffiliation(aff.getId()).forEach(a -> {
              a.setActorIdAha(match.get().getId());
              a.update();
            });
          }
        }
      });

      // update link to contributions
      origin.getContributions(-1).forEach(c -> {
        c.setActor(replacement);
        c.update();
      });

      // update affiliated actors
      origin.getActors().forEach(a -> {
        a.setAffiliation(replacement);
        a.update();
      });

      // update names, if any
      origin.getNames().forEach(n -> {
        if (replacement.getNames().stream().noneMatch(name -> n.getLang().equals(name.getLang()))) {
          // create new name because replacing the linked contribution to this name doesn't work
          ActorI18name cpy = new ActorI18name(replacement, n.getLang());
          cpy.setPseudo(n.getPseudo());
          cpy.setName(n.getName());
          cpy.setFirstOrAccro(n.getFirstOrAcro());
          cpy.isOld(n.isOld());
          replacement.addName(cpy);
        }
      });

      // if origin has a picture but not replacement, use it
      if (origin.getAvatar() != null && replacement.getAvatar() == null
          && files.movePictureFile(origin.getIdContribution() + origin.getAvatar(),
            replacement.getIdContribution() + origin.getAvatar())) {
        replacement.setAvatar(origin.getAvatar());
      }

      replacement.update();
      bindContributor(origin.getContribution(), replacement.getContribution(), contributor);
      origin.getContribution().delete();
      transaction.commit();
      logger.info("merged actor" + INTO + replacement.toString());
    } catch (Exception e) {
      String more = UNABLE_TO_MERGE + origin.getIdContribution() + INTO + replacement.getIdContribution();
      logger.error(more, e);
      throw new PersistenceException(PersistenceException.Key.MERGE, more, e);
    } finally {
      transaction.end();
    }
  }

  /**
   * Merge two texts, filling all unfilled data from replacement with the one present in origin.
   * <br><br>
   * Any excerpt will be attached to the replacement text. Any justification link thatt have origin has context will be attached to the replacement.
   *
   * @param origin a text to be merged into the replacement
   * @param replacement the text that will replace given origin
   * @param contributor the contributor asking for that merge action
   * @throws PersistenceException if any error occurred at the database level
   */
  private void merge(Text origin, Text replacement, Contributor contributor) throws PersistenceException {
    logger.debug(MERGING + origin.toString() + INTO + replacement.toString());

    Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
    try {
      // update optional properties if not set in replacement and origin had some
      if (replacement.getUrl() == null && origin.getUrl() != null) {
        replacement.setUrl(origin.getUrl());
      }
      if (replacement.getPublicationDate() == null && origin.getPublicationDate() != null) {
        replacement.setPublicationDate(origin.getPublicationDate());
      }
      if (replacement.getSourceName() == null && origin.getSourceName() != null) {
        replacement.setSourceName(origin.getSourceName());
      }

      // rebind contents if there are private ones
      if (ETextVisibility.PRIVATE.equals(ETextVisibility.value(origin.getVisibility().getIdVisibility()))) {
        origin.getContents().forEach(c -> {
          // bind new content object
          TextContent content = c.getFilename().contains(".") ?
              new TextContent(replacement, contributor, c.getFilename().substring(c.getFilename().lastIndexOf('.')))
              : new TextContent(replacement, contributor);

          // try to add content, if content is not added, such private content already existed in replacement,
          // do not overwrite it
          if (replacement.addContent(content)) {
            // copy file, not moving since an error may still occur
            logger.debug("will copy private content of " + c.getFilename());
            files.copyContributionFile(c.getFilename(), content.getFilename());
          }
        });
      } else {
        // retrieve shared content, if in replacement empty, copy the one from the origin (that may also be empty)
        Optional<TextContent> originContent = origin.getContents().stream().filter(c -> c.getContributor() == null).findAny();
        Optional<TextContent> replContent = replacement.getContents().stream().filter(c -> c.getContributor() == null).findAny();
        if (originContent.isPresent() && replContent.isPresent() && files.isEmpty(replContent.get().getFilename())) {
          logger.debug("will copy shared content of " + originContent.get().getFilename());
          files.copyContributionFile(originContent.get().getFilename(), replContent.get().getFilename());
        }
      }

      // bind all excerpts from origin to replacement
      origin.getExcerpts().forEach(a -> {
        a.setText(replacement);
        a.update();
      });

      // for all justification links where this text is the context
      Iterator<ArgumentJustification> i = origin.getContribution().getArgumentJustificationLinks().iterator();
      while (i.hasNext()) {
        ArgumentJustification l = i.next();
        // if no such justification exists in replacement, add it
        if (replacement.getContribution().getArgumentJustificationLinks().stream().noneMatch(s ->
                s.getArgumentTo().getIdContribution().equals(l.getArgumentTo().getIdContribution()) &&
                s.getArgumentFrom().getIdContribution().equals(l.getArgumentFrom().getIdContribution()))) {
          l.getArgumentTo().setContext(replacement.getContribution());
          l.getArgumentFrom().setContext(replacement.getContribution());
          l.setContext(replacement.getContribution());
          l.update();
        }
      }

      // update titles, if any
      origin.getTitles().forEach(n -> {
        if (replacement.getTitles().stream().noneMatch(name -> n.getLang().equals(name.getLang()))) {
          // create new name because replacing the linked contribution to this name doesn't work
          TextI18name cpy = new TextI18name(replacement, n.getLang(), n.getSpelling());
          replacement.addTitle(cpy);
        }
      });

      // update places and folders
      mergeContributionLinks(replacement.getContribution(), origin.getContribution());

      replacement.update();
      bindContributor(origin.getContribution(), replacement.getContribution(), contributor);
      origin.getContribution().delete();
      transaction.commit();
      logger.info("merged text" + INTO + replacement.toString());
    } catch (Exception e) {
      String more = UNABLE_TO_MERGE + origin.getIdContribution() + INTO + replacement.getIdContribution();
      logger.error(more, e);
      throw new PersistenceException(PersistenceException.Key.MERGE, more, e);
    } finally {
      transaction.end();
    }
  }

  /**
   * Replace origin argument by given replacement
   * <br><br>
   * Any similarity link and translations to the origin will be attached to the replacement.
   *
   * @param origin an argument to be merged into the replacement
   * @param replacement the argument that will replace given origin
   * @param contributor the contributor asking for that merge action
   * @throws PersistenceException if any error occurred at the database level
   */
  private void merge(be.webdeb.infra.persistence.model.Argument origin, be.webdeb.infra.persistence.model.Argument replacement,
      Contributor contributor) throws PersistenceException {
    logger.debug(MERGING + origin.toString() + INTO + replacement.toString());

    Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
    try {
      // for all similarity links where origin is the destination
      Iterator<ArgumentSimilarity> i = origin.getSimilarArguments().iterator();
      while (i.hasNext()) {
        ArgumentSimilarity l = i.next();
        // if no such similarity exists in replacement, add it
        if (l.getArgumentTo().getIdContribution().equals(origin.getIdContribution()) && replacement.getSimilarArguments().stream().noneMatch(s ->
            s.getArgumentFrom().getIdContribution().equals(l.getArgumentFrom().getIdContribution()))) {
          l.setArgumentTo(replacement);
          l.update();
        }else if (l.getArgumentFrom().getIdContribution().equals(origin.getIdContribution()) && replacement.getSimilarArguments().stream().noneMatch(s ->
                s.getArgumentTo().getIdContribution().equals(l.getArgumentTo().getIdContribution()))) {
          l.setArgumentFrom(replacement);
          l.update();
        }
      }

      bindContributor(origin.getContribution(), replacement.getContribution(), contributor);
      // now delete origin
      origin.getContribution().delete();
      transaction.commit();
      logger.info("merged argument" + INTO + replacement.getIdContribution());
    } catch (Exception e) {
      String more = UNABLE_TO_MERGE + origin.getIdContribution() + INTO + replacement.getIdContribution();
      logger.error(more, e);
      throw new PersistenceException(PersistenceException.Key.MERGE, more, e);
    } finally {
      transaction.end();
    }
  }

  /**
   * Replace origin contextualized argument by given replacement
   * <br><br>
   * Any justification or illustration link to the origin will be attached to the replacement.
   *
   * @param origin a contextualized argument to be merged into the replacement
   * @param replacement the contextualized argument that will replace given origin
   * @param contributor the contributor asking for that merge action
   * @throws PersistenceException if any error occurred at the database level
   */
  private void merge(be.webdeb.infra.persistence.model.ArgumentContext origin, be.webdeb.infra.persistence.model.ArgumentContext replacement,
                     Contributor contributor) throws PersistenceException {
    logger.debug(MERGING + origin.toString() + INTO + replacement.toString());

    Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
    try {

      // for all links where origin is the destination
      Iterator<ArgumentJustification> i = origin.getArgumentJustificationsTo().iterator();
      while (i.hasNext()) {
        ArgumentJustification l = i.next();
        // if no such justification exists in replacement, add it
        if (replacement.getArgumentJustificationsTo().stream().noneMatch(s ->
                s.getArgumentFrom().getIdContribution().equals(l.getArgumentFrom().getIdContribution()))) {
          l.setArgumentTo(replacement);
          l.getArgumentTo().setContext(replacement.getContribution());
          l.getArgumentFrom().setContext(replacement.getContribution());
          l.update();
        }
      }

      // for all links where origin is the source
      i = origin.getArgumentJustificationsFrom().iterator();
      while (i.hasNext()) {
        ArgumentJustification l = i.next();
        // if no such justification exists in replacement, add it
        if (replacement.getArgumentJustificationsFrom().stream().noneMatch(s ->
                s.getArgumentTo().getIdContribution().equals(l.getArgumentTo().getIdContribution()))) {
          l.setArgumentFrom(replacement);
          l.getArgumentTo().setContext(replacement.getContribution());
          l.getArgumentFrom().setContext(replacement.getContribution());
          l.update();
        }
      }

      // for all illustration links where argument is origin
      Iterator<ArgumentHasExcerpt> ahe = origin.getArgumentIllustrations().iterator();
      while (ahe.hasNext()) {
        ArgumentHasExcerpt l = ahe.next();
        // if no such illustration exists in replacement, add it
        if (replacement.getArgumentIllustrations().stream().noneMatch(s ->
                s.getExcerpt().getIdContribution().equals(l.getExcerpt().getIdContribution()))) {
          l.setArgument(replacement);
          l.getArgument().setContext(replacement.getContribution());
          l.update();
        }
      }

      // update places and folders
      mergeContributionLinks(replacement.getContribution(), origin.getContribution());

      bindContributor(origin.getContribution(), replacement.getContribution(), contributor);
      // now delete origin
      origin.getContribution().delete();
      transaction.commit();
      logger.info("merged contextualized argument" + INTO + replacement.getIdContribution());
    } catch (Exception e) {
      String more = UNABLE_TO_MERGE + origin.getIdContribution() + INTO + replacement.getIdContribution();
      logger.error(more, e);
      throw new PersistenceException(PersistenceException.Key.MERGE, more, e);
    } finally {
      transaction.end();
    }
  }

  /**
   * Replace origin excerpt by given replacement
   * <br><br>
   * Any illustration link to the origin will be attached to the replacement.
   *
   * @param origin an excerpt to be merged into the replacement
   * @param replacement the excerpt that will replace given origin
   * @param contributor the contributor asking for that merge action
   * @throws PersistenceException if any error occurred at the database level
   */
  private void merge(be.webdeb.infra.persistence.model.Excerpt origin, be.webdeb.infra.persistence.model.Excerpt replacement,
                     Contributor contributor) throws PersistenceException {
    logger.debug(MERGING + origin.toString() + INTO + replacement.toString());

    Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
    try {
      // for all illustration links where excerpt is origin
      Iterator<ArgumentHasExcerpt> ahe = origin.getArguments().iterator();
      while (ahe.hasNext()) {
        ArgumentHasExcerpt l = ahe.next();
        // if no such illustration exists in replacement, add it
        if (replacement.getArguments().stream().noneMatch(s ->
                s.getArgument().getIdContribution().equals(l.getArgument().getIdContribution()))) {
          l.setExcerpt(replacement);
          l.getArgument().setContext(replacement.getContribution());
          l.update();
        }
      }

      // update places and folders
      mergeContributionLinks(replacement.getContribution(), origin.getContribution());

      bindContributor(origin.getContribution(), replacement.getContribution(), contributor);
      // now delete origin
      origin.getContribution().delete();
      transaction.commit();
      logger.info("merged excerpt" + INTO + replacement.getIdContribution());
    } catch (Exception e) {
      String more = UNABLE_TO_MERGE + origin.getIdContribution() + INTO + replacement.getIdContribution();
      logger.error(more, e);
      throw new PersistenceException(PersistenceException.Key.MERGE, more, e);
    } finally {
      transaction.end();
    }
  }

  /**
   * Replace origin debate by given replacement
   * <br><br>
   * Any justification link that have origin has context will be attached to the replacement
   *
   * @param origin a debate to be merged into the replacement
   * @param replacement the debate that will replace given origin
   * @param contributor the contributor asking for that merge action
   * @throws PersistenceException if any error occurred at the database level
   */
  private void merge(be.webdeb.infra.persistence.model.Debate origin, be.webdeb.infra.persistence.model.Debate replacement,
                     Contributor contributor) throws PersistenceException {
    logger.debug(MERGING + origin.toString() + INTO + replacement.toString());

    Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
    try {
      // for all justification links where this debate is the context
      replacement.setFirstContextArgument(origin.getFirstContextArgument());

      bindContributor(origin.getContribution(), replacement.getContribution(), contributor);
      // now delete origin
      origin.getContribution().delete();
      transaction.commit();
      logger.info("merged debate" + INTO + replacement.getIdContribution());
    } catch (Exception e) {
      String more = UNABLE_TO_MERGE + origin.getIdContribution() + INTO + replacement.getIdContribution();
      logger.error(more, e);
      throw new PersistenceException(PersistenceException.Key.MERGE, more, e);
    } finally {
      transaction.end();
    }
  }

  /**
   * Merge two folders, filling all unfilled data from replacement with the one present in origin.
   * <br><br>
   * Any linked arguments and texts will be attached to the replacement folder.
   * Any parents and children in hierarchy will be attached to the replacement folder.
   *
   * @param origin a folder to be merged into the replacement
   * @param replacement the folder that will replace given origin
   * @param contributor the contributor asking for that merge action
   * @throws PersistenceException if any error occurred at the database level
   */
  private void merge(be.webdeb.infra.persistence.model.Folder origin, be.webdeb.infra.persistence.model.Folder replacement,
                     Contributor contributor) throws PersistenceException {
    logger.debug(MERGING + origin.toString() + INTO + replacement.toString());

    Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
    try {
      // update parents links
      origin.getParentsAsLinks().forEach(link -> {
        if(replacement.getParentsAsLinks().stream().noneMatch(l ->
                l.getFolderParent().getIdContribution().equals(link.getFolderParent().getIdContribution())
                || replacement.getIdContribution().equals(l.getFolderParent().getIdContribution()))) {
          link.setFolderChild(replacement);
          link.update();
        }
      });

      // update children links
      origin.getChildrenAsLinks().forEach(link -> {
        if(replacement.getChildrenAsLinks().stream().noneMatch(l ->
                l.getFolderChild().getIdContribution().equals(link.getFolderChild().getIdContribution())
                || replacement.getIdContribution().equals(l.getFolderChild().getIdContribution()))) {
          link.setFolderParent(replacement);
          link.update();
        }
      });

      // update link to contributions
      origin.getContributions().forEach(c -> {
        c.removeFolder(origin);
        c.addFolder(replacement);
        c.update();
      });

      // update names, if any
      origin.getNames().forEach(n -> {
        if (replacement.getNames().stream().noneMatch(name -> n.getLang().equals(name.getLang()))) {
          // create new name because replacing the linked contribution to this name doesn't work
          FolderI18name cpy = new FolderI18name(replacement, n.getLang(), n.getName());
          replacement.addName(cpy);
        }else{
          FolderRewordingI18name cpy = new FolderRewordingI18name(origin, n.getLang(), n.getName());
          origin.addRewordingName(cpy);
        }
      });

      // update rewording names, if any
      origin.getRewordingNames().forEach(n -> {
        if (replacement.getRewordingNames().stream()
                .noneMatch(name -> n.getLang().equals(name.getLang()) && n.getName().equalsIgnoreCase(name.getName()))) {
          FolderRewordingI18name cpy = new FolderRewordingI18name(replacement, n.getLang(), n.getName());
          replacement.addRewordingName(cpy);
        }
      });

      replacement.update();
      bindContributor(origin.getContribution(), replacement.getContribution(), contributor);
      origin.getContribution().delete();
      transaction.commit();
      logger.info("merged actor" + INTO + replacement.toString());
    } catch (Exception e) {
      String more = UNABLE_TO_MERGE + origin.getIdContribution() + INTO + replacement.getIdContribution();
      logger.error(more, e);
      throw new PersistenceException(PersistenceException.Key.MERGE, more, e);
    } finally {
      transaction.end();
    }
  }

  /**
   * Merge origin actors, places and folders into replacement contribution
   *
   * @param origin a folder to be merged into the replacement
   * @param replacement the folder that will replace given origin
   */
  private void mergeContributionLinks(be.webdeb.infra.persistence.model.Contribution replacement,
                                      be.webdeb.infra.persistence.model.Contribution origin){

    // update linked places
    origin.getActors().forEach(a -> {
      if (replacement.getActors().stream().noneMatch(actor -> a.getActor().getIdContribution().equals(actor.getActor().getIdContribution()))) {
        replacement.getActors().add(a);
      }
    });

    // update linked places
    origin.getPlaces().forEach(p -> {
      if (replacement.getPlaces().stream().noneMatch(place -> p.getId().equals(place.getId()))) {
        replacement.getPlaces().add(p);
      }
    });

    // update linked folders
    origin.getFolders().forEach(f -> {
      if (replacement.getFolders().stream().noneMatch(folder -> f.getIdContribution().equals(folder.getIdContribution()))) {
        replacement.getFolders().add(f);
      }
    });
  }

  /**
   * Check if given contributor is the owner if this contribution, ie
   * <ul>
   *   <li>he's an admin</li>
   *   <li>contribution belongs to only one group and he's an owner of that group</li>
   * </ul>
   * @param contributor a contributor, may not be null
   * @param contribution a contribution, may not be null
   * @return true is the above conditions are met, false otherwise
   */
  private boolean contributorIsOwnerOfContribution(Contributor contributor, be.webdeb.infra.persistence.model.Contribution contribution) {
    // contributor is an admin of public group (with id 0)
    return be.webdeb.infra.persistence.model.Contribution.getCreator(contribution.getIdContribution()).getIdContributor().equals(contributor.getIdContributor())
        || contributor.getContributorHasGroups().stream().anyMatch(g ->
          g.getGroup().getIdGroup() == 0 && g.getRole().getIdRole() == EContributorRole.ADMIN.id())

          // contribution is present in one group and contributor is at least owner of it
          || (contribution.getGroups().size() == 1 && contributor.getContributorHasGroups().stream().anyMatch(g ->
          g.getGroup().getIdGroup() == contribution.getGroups().get(DEFAULT_GROUP).getIdGroup()
              && g.getRole().getIdRole() >= EContributorRole.OWNER.id()));
  }

  /**
   * Init a contribution from an api contribution
   *
   * @param contributionType the contribution type id
   * @param sortkey an absolute search/sort key
   * @return the db initialized contribution
   */
  protected be.webdeb.infra.persistence.model.Contribution initContribution(int contributionType, String sortkey){
    return initContribution(contributionType, sortkey, false);
  }

  /**
   * Copy a new contribution from a given contribution
   *
   * @param contribution the given contribution to copy
   * @return the db initialized contribution
   */
  protected be.webdeb.infra.persistence.model.Contribution initContribution(be.webdeb.infra.persistence.model.Contribution contribution){
    return initContribution(contribution.getContributionType().getIdContributionType(), contribution.getSortkey(), contribution.isHidden());
  }

  /**
   * Init a contribution from an api contribution with hidden option
   *
   * @param contributionType the contribution type id
   * @param sortkey an absolute search/sort key
   * @param hidden true if this contribution must is hidden
   * @return the db initialized contribution
   */
  protected be.webdeb.infra.persistence.model.Contribution initContribution(int contributionType, String sortkey, boolean hidden){
    be.webdeb.infra.persistence.model.Contribution c = new be.webdeb.infra.persistence.model.Contribution();
    c.setIdContribution(0L);
    c.setContributionType(TContributionType.find.byId(contributionType));
    c.setValidated(TValidationState.findById(EValidationState.UNSET.id()));
    c.setSortkey(sortkey);
    c.isHidden(hidden);
    return c;
  }

  /**
   * Init an external contribution from an api external contribution
   *
   * @param externalApi the external api
   * @param contribution the corresponding db contribution
   * @param externalSource the external source where the external contribution comes from
   * @return true is the above conditions are met, false otherwise
   */
  protected be.webdeb.infra.persistence.model.ExternalContribution initExternalContribution(ExternalContribution externalApi,
                                        be.webdeb.infra.persistence.model.Contribution contribution, TExternalSourceName externalSource){

    be.webdeb.infra.persistence.model.ExternalContribution externalContribution =
            new be.webdeb.infra.persistence.model.ExternalContribution();

    externalContribution.setContribution(contribution);
    externalContribution.setIdContribution(contribution.getIdContribution());
    externalContribution.setSourceUrl(externalApi.getSourceUrl());
    externalContribution.setExternalSource(externalSource);
    externalContribution.setLanguage(TLanguage.find.byId(externalApi.getLanguage().getCode()));
    externalContribution.setPublicationDate(externalApi.getPublicationDate());
    externalContribution.save();

    for(be.webdeb.core.api.actor.ExternalAuthor a : externalApi.getAuthors()){
      ExternalAuthor author = new ExternalAuthor();
      author.setExternalAuthorId(0L);
      author.setName(a.getName());
      author.setInternalContributionId(a.getId());
      author.setExternalContribution(externalContribution);
      author.save();
    }

    return externalContribution;
  }

  /**
   * Build a list of API argument justification links from DB links
   *
   * @param links a list of DB argument justification links
   * @return the corresponding lit of API argument justification list
   */
  protected List<be.webdeb.core.api.argument.ArgumentJustification> buildJustificationLinkList(List<be.webdeb.infra.persistence.model.ArgumentJustification> links) {
    return buildJustificationLinkList(links, null, null);
  }

  /**
   * Build a list of API argument justification links from DB links
   *
   * @param links a list of DB argument justification links
   * @param argument the API contextualized argument being one of the arguments in the link
   * @param isSource true if given contextualized argument is the source of the link
   * @return the corresponding lit of API argument justification list
   */
  protected List<be.webdeb.core.api.argument.ArgumentJustification> buildJustificationLinkList(List<be.webdeb.infra.persistence.model.ArgumentJustification> links, be.webdeb.core.api.argument.ArgumentContext argument, Boolean isSource) {
    List<be.webdeb.core.api.argument.ArgumentJustification> result = new ArrayList<>();
    for (be.webdeb.infra.persistence.model.ArgumentJustification l : links) {
      try {
        if(argument == null || isSource == null){
          result.add(mapper.toArgumentJustification(l));
        }else{
          result.add(mapper.toArgumentJustification(l, argument, isSource));
        }
      } catch (FormatException e) {
        logger.error("unable to cast justification link " + l.getIdContribution(), e);
      }
    }
    return result;
  }

  /**
   * Build a list of API argument illustration links from DB links
   *
   * @param links a list of DB argument illustration links
   * @return the corresponding lit of API argument illustration links list
   */
  protected List<ArgumentIllustration> buildIllustrationLinkList(List<be.webdeb.infra.persistence.model.ArgumentHasExcerpt> links) {
    List<ArgumentIllustration> result = new ArrayList<>();
    for (be.webdeb.infra.persistence.model.ArgumentHasExcerpt l : links) {
      try {
        result.add(mapper.toArgumentIllustration(l));
      } catch (FormatException e) {
        logger.error("unable to cast illustration link " + l.getIdContribution(), e);
      }
    }
    return result;
  }

  /**
   * Build a list of API argument illustration links from DB links
   *
   * @param links a list of DB argument illustration links
   * @param excerpt the API excerpt being the source of the link
   * @return the corresponding lit of API argument illustration links list
   */
  protected List<ArgumentIllustration> buildIllustrationLinkList(List<be.webdeb.infra.persistence.model.ArgumentHasExcerpt> links, Excerpt excerpt) {
    List<ArgumentIllustration> result = new ArrayList<>();
    for (be.webdeb.infra.persistence.model.ArgumentHasExcerpt l : links) {
      try {
        result.add(mapper.toArgumentIllustration(l, mapper.toExcerpt(excerpt)));
      } catch (FormatException e) {
        logger.error("unable to cast illustration link " + l.getIdContribution(), e);
      }
    }
    return result;
  }

  /**
   * Build a list of API argument illustration links from DB links
   *
   * @param links a list of DB argument illustration links
   * @param argument the API contextualized argument being the source of the link
   * @return the corresponding lit of API argument illustration list
   */
  protected List<be.webdeb.core.api.argument.ArgumentIllustration> buildIllustrationLinkList(List<be.webdeb.infra.persistence.model.ArgumentHasExcerpt> links, be.webdeb.core.api.argument.ArgumentContext argument) {
    List<be.webdeb.core.api.argument.ArgumentIllustration> result = new ArrayList<>();
    for (be.webdeb.infra.persistence.model.ArgumentHasExcerpt l : links) {
      try {
        result.add(mapper.toArgumentIllustration(l, argument));
      } catch (FormatException e) {
        logger.error("unable to cast illustration link " + l.getIdContribution(), e);
      }
    }
    return result;
  }

  /**
   * Build a list of API argument similarity links from DB links
   *
   * @param links a list of DB argument similarity links
   * @return the corresponding lit of API argument list
   */
  protected List<be.webdeb.core.api.argument.ArgumentSimilarity> buildSimilarityLinkList(List<be.webdeb.infra.persistence.model.ArgumentSimilarity> links) {
    List<be.webdeb.core.api.argument.ArgumentSimilarity> result = new ArrayList<>();
    for (be.webdeb.infra.persistence.model.ArgumentSimilarity l : links) {
      try {
        result.add(mapper.toArgumentSimilarity(l));
      } catch (FormatException e) {
        logger.error("unable to cast similarity link " + l.getIdContribution(), e);
      }
    }
    return result;
  }

  /**
   * Check if given indexes are correct
   *
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return true if the indexes are correct
   */
  protected boolean checkSubIndexes(int fromIndex, int toIndex) {
    return fromIndex >= 0 && toIndex > fromIndex;
  }

  /**
   * Convert the given string to UT8
   *
   * @param toConvert the string to convert into UT8
   * @return the converted string
   */
  protected String convertToUTF8(String toConvert){
    return toConvert.replaceAll("[^\\w0-9&\\[\\]\\s.;:=+-/°!?§'|@(){}<>_#²³/\"*%\\p{Sc}\\p{L}`~¨]","");
  }
}
