/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.accessor.impl;

import be.webdeb.core.api.actor.*;
import be.webdeb.core.api.argument.*;
import be.webdeb.core.api.argument.ArgumentContext;
import be.webdeb.core.api.argument.ArgumentDictionary;
import be.webdeb.core.api.argument.ArgumentJustification;
import be.webdeb.core.api.argument.ArgumentSimilarity;
import be.webdeb.core.api.contribution.*;
import be.webdeb.core.api.contribution.ContributionToExplore;
import be.webdeb.core.api.contributor.*;
import be.webdeb.core.api.contributor.Advice;
import be.webdeb.core.api.debate.DebateFactory;
import be.webdeb.core.api.excerpt.ExcerptFactory;
import be.webdeb.core.api.folder.*;
import be.webdeb.core.api.project.ProjectFactory;
import be.webdeb.core.api.text.*;
import be.webdeb.core.exception.FormatException;
import be.webdeb.infra.persistence.accessor.api.APIObjectMapper;
import be.webdeb.infra.persistence.accessor.api.AffiliationAccessor;
import be.webdeb.infra.persistence.model.*;
import be.webdeb.infra.persistence.model.Actor;
import be.webdeb.infra.persistence.model.Argument;
import be.webdeb.infra.persistence.model.Contribution;
import be.webdeb.infra.persistence.model.ExternalAuthor;
import be.webdeb.infra.persistence.model.ExternalExcerpt;
import be.webdeb.infra.persistence.model.ExternalContribution;
import be.webdeb.infra.persistence.model.ExternalText;
import be.webdeb.infra.persistence.model.Folder;
import be.webdeb.infra.persistence.model.FolderLink;
import be.webdeb.infra.persistence.model.Group;
import be.webdeb.infra.persistence.model.Organization;
import be.webdeb.infra.persistence.model.Person;
import be.webdeb.infra.persistence.model.Place;
import be.webdeb.infra.persistence.model.Text;
import be.webdeb.infra.persistence.model.TextSourceName;
import be.webdeb.infra.persistence.model.TCopyrightfreeSource;
import be.webdeb.util.ValuesHelper;
import javax.inject.Inject;

import com.google.inject.Singleton;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.stream.Collectors;

/**
 * This class serves as a mapper from DB objects to core.api objects. Integrity of objects are not checked
 * here and methods are supposed to be used correctly, with right objects to be mapped.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
@Singleton
public class EbeanAPIMapper implements APIObjectMapper {

  @Inject
  private ContributorFactory contributorFactory;

  @Inject
  private ActorFactory actorFactory;

  @Inject
  private TextFactory textFactory;

  @Inject
  private ArgumentFactory argumentFactory;

  @Inject
  private ExcerptFactory excerptFactory;

  @Inject
  private DebateFactory debateFactory;

  @Inject
  private FolderFactory folderFactory;

  @Inject
  private ProjectFactory projectFactory;

  @Inject
  private ValuesHelper values;

  @Inject
  private AffiliationAccessor affiliationAccessor;

  private static final Logger logger = play.Logger.underlying();

  @Override
  public be.webdeb.core.api.contributor.Contributor toContributor(
      be.webdeb.infra.persistence.model.Contributor contributor) throws FormatException {
    be.webdeb.core.api.contributor.Contributor wrapped = contributorFactory.getContributor();
    wrapped.setId(contributor.getIdContributor());
    if(!contributor.isDeleted())
      wrapped.setEmail(contributor.getEmail());

    if(contributor.getPseudo() != null) {
      wrapped.setPseudo(contributor.getPseudo());
    }

    // check if this contributor is not a skeleton, ie, has been invited to join so details are not known yet
    if (contributor.getFirstname() != null) {
      if(contributor.getTmpContributor() != null)
        wrapped.setTmpContributor(toTmpContributor(contributor.getTmpContributor()));
      wrapped.setFirstname(contributor.getFirstname());
      wrapped.setLastname(contributor.getLastname());
      wrapped.setBirthyear(contributor.getBirthYear());
      wrapped.setGender(contributor.getGender() != null ?
          actorFactory.getGender(contributor.getGender().getIdGender()) : null);
      wrapped.setResidence(contributor.getResidence() != null ?
          actorFactory.getCountry(contributor.getResidence().getIdCountry()) : null);


      for (ContributorHasAffiliation cha : contributor.getContributorHasAffiliations()) {
        wrapped.addAffiliation(toAffiliation(cha));
      }
    }

    // all these fields are required
    wrapped.setPassword(contributor.getPasswordHash());
    wrapped.isValidated(contributor.isValidated());
    wrapped.isBanned(contributor.isBanned());
    wrapped.isDeleted(contributor.isDeleted());
    wrapped.isPedagogic(contributor.isPedagogic());
    wrapped.isNewsletter(contributor.isNewsletter());
    wrapped.isBrowserWarned(contributor.isBrowserWarned());
    wrapped.setSubscriptionDate(contributor.getRegistrationDate());
    wrapped.setVersion(contributor.getVersion().getTime());

    wrapped.setAvatar(contributor.getAvatar());

    return wrapped;
  }

  @Override
  public be.webdeb.core.api.contributor.TmpContributor toTmpContributor(
          be.webdeb.infra.persistence.model.TmpContributor contributor) throws FormatException {
    be.webdeb.core.api.contributor.TmpContributor wrapped = contributorFactory.getTmpContributor();

    wrapped.setId(contributor.getIdContributor());
    wrapped.setPseudo(contributor.getPseudo());
    wrapped.setPassword(contributor.getPasswordHash());
    wrapped.setProject(toProject(contributor.getProject()));
    wrapped.setProjectSubgroup(toProjectSubgroup(contributor.getProjectSubgroup()));

    return wrapped;
  }

  @Override
  public be.webdeb.core.api.actor.Affiliation toAffiliation(ContributorHasAffiliation affiliation) throws FormatException {
    be.webdeb.core.api.actor.Affiliation wrapped = actorFactory.getAffiliation();
    wrapped.setId(affiliation.getIdCha());
    wrapped.setVersion(affiliation.getVersion().getTime());
    wrapped.setFunction(affiliation.getFunction() != null ?
        actorFactory.getProfession(affiliation.getFunction().getIdProfession()) : null);
    if (affiliation.getActor() != null) {
      wrapped.setActor(toActor(affiliation.getActor()));
    }
    wrapped.setStartDate(values.fromDBFormat(affiliation.getStartDate()));
    wrapped.setEndDate(values.fromDBFormat(affiliation.getEndDate()));
    if(affiliation.getStartDateType() != null)
      wrapped.setStartDateType(affiliation.getStartDateType().getEPrecisionDateType());
    if(affiliation.getEndDateType() != null)
      wrapped.setEndDateType(affiliation.getEndDateType().getEPrecisionDateType());
    return wrapped;
  }

  @Override
  public be.webdeb.core.api.actor.Affiliation toAffiliation(ActorHasAffiliation affiliation) throws FormatException {
    return toAffiliation(affiliation, false);
  }

  @Override
  public be.webdeb.core.api.actor.Affiliation toMember(ActorHasAffiliation affiliation) throws FormatException {
    return toAffiliation(affiliation, true);
  }

  /**
   * Wrap a DB affiliation into an API affiliation. Effective actor pointed as the affiliation actor depends
   * on the reverse attribute. Affiliation is reversed if the actor referenced in the affiliation is an affiliated
   * actor (member) instead of an affiliation
   *
   * @param affiliation an affiliation to wrap
   * @param reverse saying if the actor (true) or the affiliation (false) must be pointed as the affiliation actor
   * @return the wrapped DB affiliation
   * @throws FormatException if any unexpected data was passed as function or affiliation date
   */
  private be.webdeb.core.api.actor.Affiliation toAffiliation(ActorHasAffiliation affiliation, boolean reverse) throws FormatException {
    be.webdeb.core.api.actor.Affiliation wrapped = actorFactory.getAffiliation();
    wrapped.setId(affiliation.getId());
    wrapped.setVersion(affiliation.getVersion().getTime());
    wrapped.setFunction(affiliation.getFunction() != null ?
        actorFactory.getProfession(affiliation.getFunction().getIdProfession()) : null);
    if (affiliation.getAffiliation() != null) {
      wrapped.setActor(reverse ? toActor(affiliation.getActor()) : toActor(affiliation.getAffiliation()));
      wrapped.setAffiliated(reverse ? toActor(affiliation.getAffiliation()) : toActor(affiliation.getActor()));
    }

    if (affiliation.getType() != null) {
      wrapped.setAffiliationType(affiliation.getType().getEAffiliationType());
    }
    wrapped.setStartDate(values.fromDBFormat(affiliation.getStartDate()));
    wrapped.setEndDate(values.fromDBFormat(affiliation.getEndDate()));
    if(affiliation.getStartDateType() != null)
      wrapped.setStartDateType(affiliation.getStartDateType().getEPrecisionDateType());
    if(affiliation.getEndDateType() != null)
      wrapped.setEndDateType(affiliation.getEndDateType().getEPrecisionDateType());
    return wrapped;
  }

  @Override
  public be.webdeb.core.api.actor.Actor toActor(Actor actor) throws FormatException {
    EActorType type = EActorType.value(actor.getActortype().getIdActorType());
    be.webdeb.core.api.actor.Actor result;
    switch (type) {
      case PERSON:
        result = toPerson(actor);
        break;
      case ORGANIZATION:
        result = toOrganization(actor);
        break;
      default:
        result = null;
    }

    if (result == null) {
      result = actorFactory.getActor();
    }
    setContributionData(result, actor.getContribution());
    setNames(result, actor);
    result.setCrossReference(actor.getCrossref());
    result.setAvatar(actor.getAvatar());

    setGroups(result, actor.getContribution());
    return result;
  }

  @Override
  public be.webdeb.core.api.actor.Person toPerson(Actor actor) throws FormatException {
    be.webdeb.core.api.actor.Person wrapped = actorFactory.getPerson();
    Person p = actor.getPerson();
    if (p == null) {
      logger.error("unable to get person from actor");
      return null;
    }

    wrapped.setBirthdate(values.fromDBFormat(p.getBirthdate()));
    wrapped.setDeathdate(values.fromDBFormat(p.getDeathdate()));
    wrapped.setGender(p.getGender() != null ? actorFactory.getGender(p.getGender().getIdGender()) : null);
    wrapped.setResidence(p.getResidence() != null ?
        actorFactory.getCountry(p.getResidence().getIdCountry()) : null);
    return wrapped;
  }

  @Override
  public be.webdeb.core.api.actor.Organization toOrganization(Actor actor) throws FormatException {
    be.webdeb.core.api.actor.Organization wrapped = actorFactory.getOrganization();

    Organization o = actor.getOrganization();
    if (o == null) {
      logger.debug("unable to get organization from actor " + actor.getIdContribution() + " sending partial " +
          "organization");
      return wrapped;
    }

    wrapped.setOfficialNumber(o.getOfficialNumber());
    wrapped.setCreationDate(values.fromDBFormat(o.getCreationDate()));
    wrapped.setTerminationDate(values.fromDBFormat(o.getTerminationDate()));
    wrapped.setLegalStatus(o.getLegalStatus() != null ?
        actorFactory.getLegalStatus(o.getLegalStatus().getIdStatus()) : null);

    for (TBusinessSector sector : o.getSectors()) {
      wrapped.addBusinessSector(actorFactory.getBusinessSector(sector.getIdBusinessSector()));
    }
    return wrapped;
  }

  @Override
  public be.webdeb.core.api.debate.Debate toDebate(Debate debate) throws FormatException {
    be.webdeb.core.api.debate.Debate wrapped = debateFactory.getDebate();
    setContributionData(wrapped, debate.getContribution());

    if(debate.getFirstContextArgument() != null) {
      wrapped.setFirstArgumentId(debate.getFirstContextArgument().getIdContribution());
      wrapped.setFirstArgument(toArgumentContext(debate.getFirstContextArgument()));
    }

    if(debate.getShade() != null) {
      wrapped.setFirstArgumentType(argumentFactory.getArgumentType(debate.getShade().getIdShade()));
    }
    setGroups(wrapped, debate.getContribution());
    return wrapped;
  }

  @Override
  public be.webdeb.core.api.text.Text toText(Text text) throws FormatException {
    be.webdeb.core.api.text.Text wrapped = textFactory.getText();
    setContributionData(wrapped, text.getContribution());

    wrapped.setTitles(text.getTitles().stream().collect(Collectors.toMap(TextI18name::getLang, TextI18name::getSpelling)));
    wrapped.setLanguage(textFactory.getLanguage(text.getLanguage().getCode()));

    wrapped.setPublicationDate(values.fromDBFormat(text.getPublicationDate()));
    wrapped.setTextType(textFactory.getTextType(text.getTextType().getIdType()));

    wrapped.setTextVisibility(textFactory.getTextVisibility(text.getVisibility().getIdVisibility()));

    wrapped.isHidden(text.getContribution().isHidden());
    if (text.getUrl() != null) {
      wrapped.setUrl(text.getUrl());
    }

    wrapped.setSourceTitle(text.getSourceName() != null ? text.getSourceName().getName() : null);

    if(text.getFirstContextArgument() != null) {
      wrapped.setFirstArgumentId(text.getFirstContextArgument().getIdContribution());
      wrapped.setFirstArgument(toArgumentContext(text.getFirstContextArgument()));
    }

    // filenames are a map of contributor ids and filenames, if no contributor, then use the -1L key
    wrapped.setFilenames(text.getContents().stream().collect(Collectors
        .toMap(c -> c.getContributor() != null ? c.getContributor().getIdContributor() : -1L, TextContent::getFilename)));

    setGroups(wrapped, text.getContribution());
    return wrapped;
  }

  @Override
  public be.webdeb.core.api.text.ExternalText toExternalText(ExternalText text) throws FormatException {
    be.webdeb.core.api.text.ExternalText wrapped = textFactory.getExternalText();
    setContributionData(wrapped, text.getExternalContribution().getContribution());

    wrapped.setSourceUrl(text.getExternalContribution().getSourceUrl());
    wrapped.setSourceId(text.getExternalContribution().getExternalSource().getIdSource());
    wrapped.setLanguage(textFactory.getLanguage(text.getExternalContribution().getLanguage().getCode()));
    Contribution c = text.getExternalContribution().getInternalContribution();
    wrapped.setInternalContribution(c != null ? c.getIdContribution() : null);

    wrapped.setTitle(text.getTitle());
    wrapped.setPublicationDate(text.getExternalContribution().getPublicationDate());

    for (ExternalAuthor a : text.getExternalContribution().getAuthors()) {
      wrapped.addAuthor(toExternalAuthor(a));
    }

    return wrapped;
  }

  @Override
  public be.webdeb.core.api.text.TextSourceName toSourceName(TextSourceName source) throws FormatException {
    be.webdeb.core.api.text.TextSourceName wrapped = textFactory.getTextSourceName();
    wrapped.setSourceId(source.getIdSource());
    wrapped.setSourceName(source.getName());
    return wrapped;
  }

  @Override
  public be.webdeb.core.api.text.TextCopyrightfreeSource toFreeSource(TCopyrightfreeSource freeSource) throws FormatException {
    be.webdeb.core.api.text.TextCopyrightfreeSource wrapped = textFactory.getTextCopyrightfreeSource();
    wrapped.setId(freeSource.getIdCopyrightfreeSource());
    wrapped.setDomainName(freeSource.getDomainName());
    return wrapped;
  }

  @Override
  public ArgumentDictionary toArgumentDictionary(be.webdeb.infra.persistence.model.ArgumentDictionary argument) throws FormatException {
    be.webdeb.core.api.argument.ArgumentDictionary wrapped = argumentFactory.getArgumentDictionary();
    wrapped.setId(argument.getIdDictionary());
    wrapped.setTitle(argument.getTitle());
    wrapped.setLanguage(textFactory.getLanguage(argument.getLanguage().getCode()));
    return wrapped;
  }

  @Override
  public be.webdeb.core.api.argument.Argument toArgument(Argument argument) throws FormatException {
    be.webdeb.core.api.argument.Argument wrapped = argumentFactory.getArgument();
    setContributionData(wrapped, argument.getContribution());
    wrapped.setArgumentType(argumentFactory.getArgumentType(argument.getShade().getIdShade()));
    wrapped.setDictionary(toArgumentDictionary(argument.getTitleDictionary()));

    setGroups(wrapped, argument.getContribution());
    return wrapped;
  }

  @Override
  public ArgumentContext toArgumentContext(be.webdeb.infra.persistence.model.ArgumentContext argumentContext) throws FormatException {
    be.webdeb.core.api.argument.ArgumentContext wrapped = argumentFactory.getContextualizedArgument();
    setContributionData(wrapped, argumentContext.getContribution());

    wrapped.setArgumentId(argumentContext.getArgument().getIdContribution());
    if(argumentContext.getContext() == null || argumentContext.getContext().isDeleted()) throw new FormatException(FormatException.Key.ARGUMENT_ERROR);
    wrapped.setContextId(argumentContext.getContext().getIdContribution());

    setGroups(wrapped, argumentContext.getContribution());
    return wrapped;
  }

  @Override
  public be.webdeb.core.api.excerpt.Excerpt toExcerpt(Excerpt excerpt) throws FormatException {
    be.webdeb.core.api.excerpt.Excerpt wrapped = excerptFactory.getExcerpt();
    setContributionData(wrapped, excerpt.getContribution());

    wrapped.setTextId(excerpt.getText().getIdContribution());
    wrapped.setOriginalExcerpt(excerpt.getOriginalExcerpt());
    wrapped.setTechWorkingExcerpt(excerpt.getWorkingExcerpt());

    setGroups(wrapped, excerpt.getContribution());
    return wrapped;
  }

  @Override
  public be.webdeb.core.api.excerpt.ExternalExcerpt toExternalExcerpt(ExternalExcerpt excerpt) throws FormatException {
    be.webdeb.core.api.excerpt.ExternalExcerpt wrapped = excerptFactory.getExternalExcerpt();
    setContributionData(wrapped, excerpt.getExternalContribution().getContribution());

    wrapped.setSourceUrl(excerpt.getExternalContribution().getSourceUrl());
    wrapped.setSourceId(excerpt.getExternalContribution().getExternalSource().getIdSource());
    wrapped.setLanguage(textFactory.getLanguage((excerpt.getExternalContribution().getLanguage().getCode())));
    Contribution c = excerpt.getExternalContribution().getInternalContribution();
    wrapped.setInternalContribution(c != null ? c.getIdContribution() : null);

    if(excerpt.getText() != null)
      wrapped.setText(toText(excerpt.getText()));
    if(excerpt.getExternalText() != null)
      wrapped.setExternalText(toExternalText(excerpt.getExternalText()));
    wrapped.setOriginalExcerpt(excerpt.getOriginalExcerpt());
    wrapped.setWorkingExcerpt(excerpt.getWorkingExcerpt());

    return wrapped;
  }

  @Override
  public be.webdeb.core.api.excerpt.ContextElement toContextElement(ContextElement contextElement) throws FormatException {
    be.webdeb.core.api.excerpt.ContextElement wrapped = excerptFactory.getContextElement();

    wrapped.setId(wrapped.getId());
    wrapped.setContribution(toContribution(contextElement.getContribution()));
    contextElement.getSpellings().forEach(e -> wrapped.addSpelling(e.getLang(), e.getSpelling()));

    return wrapped;
  }

  @Override
  public be.webdeb.core.api.actor.ExternalAuthor toExternalAuthor(ExternalAuthor author){
    be.webdeb.core.api.actor.ExternalAuthor wrapped = actorFactory.getExternalAuthor();

    wrapped.setId(author.getExternalAuthorId());
    wrapped.setName(author.getName());

    return wrapped;
  }

  @Override
  public ArgumentJustification toArgumentJustification(be.webdeb.infra.persistence.model.ArgumentJustification link) throws FormatException {
    return toArgumentJustification(link, toArgumentContext(link.getArgumentFrom()), true);
  }

  @Override
  public ArgumentJustification toArgumentJustification(be.webdeb.infra.persistence.model.ArgumentJustification link,
                                                       ArgumentContext argumentContext, boolean isSource) throws FormatException {
    be.webdeb.core.api.argument.ArgumentJustification wrapped = argumentFactory.getArgumentJustificationLink();
    setContributionData(wrapped, link.getContribution());

    wrapped.setOrigin(isSource ? argumentContext : toArgumentContext(link.getArgumentFrom()));
    wrapped.setDestination(isSource ? toArgumentContext(link.getArgumentTo()) : argumentContext);
    wrapped.setContextId(link.getContext().getIdContribution());

    wrapped.setArgumentLinkType(argumentFactory.getArgumentLinkType(EArgumentLinkShade.value(link.getShade().getIdLinkShade())));
    wrapped.setValidationState(textFactory.getValidationState(link.getValidationState().getIdValidationState()));
    setGroups(wrapped, link.getContribution());
    return wrapped;
  }

  @Override
  public ArgumentIllustration toArgumentIllustration(ArgumentHasExcerpt link) throws FormatException {
    be.webdeb.core.api.argument.ArgumentIllustration wrapped = argumentFactory.getArgumentIllustrationLink();
    setContributionData(wrapped, link.getContribution());

    wrapped.setArgument(link.getArgument() == null ? null : toArgumentContext(link.getArgument()));
    wrapped.setExcerpt(link.getExcerpt() == null ? null : toExcerpt(link.getExcerpt()));
    wrapped.setArgumentLinkType(argumentFactory.getArgumentLinkType(EArgumentLinkShade.value(link.getShade().getIdLinkShade())));
    wrapped.setValidationState(textFactory.getValidationState(link.getValidationState().getIdValidationState()));
    setGroups(wrapped, link.getContribution());
    return wrapped;
  }

  @Override
  public ArgumentIllustration toArgumentIllustration(ArgumentHasExcerpt link, be.webdeb.core.api.excerpt.Excerpt excerpt) throws FormatException {
    link.setExcerpt(null);
    be.webdeb.core.api.argument.ArgumentIllustration wrapped = toArgumentIllustration(link);
    wrapped.setExcerpt(excerpt);
    return wrapped;
  }

  @Override
  public ArgumentIllustration toArgumentIllustration(ArgumentHasExcerpt link, ArgumentContext argumentContext) throws FormatException {
    link.setArgument(null);
    be.webdeb.core.api.argument.ArgumentIllustration wrapped = toArgumentIllustration(link);
    wrapped.setArgument(argumentContext);
    return wrapped;
  }

  @Override
  public ArgumentSimilarity toArgumentSimilarity(be.webdeb.infra.persistence.model.ArgumentSimilarity link) throws FormatException {
    return toArgumentSimilarity(link, toArgument(link.getArgumentFrom()), true);
  }

  @Override
  public ArgumentSimilarity toArgumentSimilarity(be.webdeb.infra.persistence.model.ArgumentSimilarity link, be.webdeb.core.api.argument.Argument argument, boolean isSource) throws FormatException {
    be.webdeb.core.api.argument.ArgumentSimilarity wrapped = argumentFactory.getArgumentSimilarityLink();
    setContributionData(wrapped, link.getContribution());

    wrapped.setOrigin(isSource ? argument : toArgument(link.getArgumentFrom()));
    wrapped.setDestination(isSource ? toArgument(link.getArgumentTo()) : argument);
    wrapped.setArgumentLinkType(argumentFactory.getArgumentLinkType(EArgumentLinkShade.value(link.getShade().getIdLinkShade())));
    setGroups(wrapped, link.getContribution());
    return wrapped;
  }

  @Override
  public be.webdeb.core.api.actor.ActorRole toActorRole(
      ContributionHasActor cha, be.webdeb.core.api.actor.Actor actor, be.webdeb.core.api.contribution.Contribution contribution) {
    ActorRole role = actorFactory.getActorRole(actor, contribution);
    role.setIsAuthor(cha.isAuthor());
    role.setIsReporter(cha.isReporter());
    role.setIsJustCited(cha.isAbout() || (!cha.isAuthor() && !cha.isReporter()));
    if (cha.getActorIdAha() != null) {
      role.setAffiliation(affiliationAccessor.retrieve(cha.getActorIdAha(), false));
    }
    return role;
  }

  @Override
  public be.webdeb.core.api.contribution.Contribution toContribution(Contribution contribution) throws FormatException {
    EContributionType type = EContributionType.value(contribution.getContributionType().getIdContributionType());
    switch (type) {
      case ACTOR:
        if(contribution.getActor() != null)return toActor(contribution.getActor());
        break;
      case ARGUMENT:
        if(contribution.getArgument() != null)return toArgument(contribution.getArgument());
        break;
      case ARGUMENT_CONTEXTUALIZED:
        if(contribution.getArgumentContext() != null)return toArgumentContext(contribution.getArgumentContext());
        break;
      case EXCERPT:
        if(contribution.getExcerpt() != null)return toExcerpt(contribution.getExcerpt());
        break;
      case DEBATE:
        if(contribution.getDebate() != null)return toDebate(contribution.getDebate());
        break;
      case TEXT:
        if(contribution.getText() != null)return toText(contribution.getText());
        break;
      case ARG_JUSTIFICATION:
        if(contribution.getJustificationLink() != null)
          return toArgumentJustification(contribution.getJustificationLink(), toArgumentContext(contribution.getJustificationLink().getArgumentFrom()), true);
        break;
      case ARG_SIMILARITY:
        if(contribution.getSimilarityLink() != null)
          return toArgumentSimilarity(contribution.getSimilarityLink(), toArgument(contribution.getSimilarityLink().getArgumentFrom()), true);
        break;
      case ARG_ILLUSTRATION:
        if(contribution.getIllustrationLink() != null)
          return toArgumentIllustration(contribution.getIllustrationLink(), toArgumentContext(contribution.getIllustrationLink().getArgument()));
        break;
      case FOLDER:
        if(contribution.getFolder() != null)return toFolder(contribution.getFolder());
        break;
      case FOLDERLINK:
        if(contribution.getFolderLink() != null)return toFolderLink(contribution.getFolderLink());
        break;
      default:
        logger.error("unknown contribution type for " + contribution.getIdContribution());
    }
    return null;
  }

    @Override
    public ContextContribution toContextContribution(Contribution contribution) throws FormatException {
        EContributionType type = EContributionType.value(contribution.getContributionType().getIdContributionType());
        switch (type) {
            case DEBATE:
                if(contribution.getDebate() != null)return toDebate(contribution.getDebate());
                break;
            case TEXT:
                if(contribution.getText() != null)return toText(contribution.getText());
                break;
            default:
                logger.error("unknown context contribution type for " + contribution.getIdContribution());
        }
        return null;
    }

    @Override
  public be.webdeb.core.api.contribution.ExternalContribution toExternalContribution(ExternalContribution externalContribution) throws FormatException{
    EContributionType type = EContributionType.value(externalContribution.getContribution().getContributionType().getIdContributionType());
    switch (type) {
      case EXTERNAL_TEXT:
        if(externalContribution.getText() != null)return toExternalText(externalContribution.getText());
        break;
      case EXTERNAL_EXCERPT:
        if(externalContribution.getExcerpt() != null)return toExternalExcerpt(externalContribution.getExcerpt());
        break;
      default:
        logger.error("unknown external contribution type for " + externalContribution.getIdContribution());
    }
    return null;
  }

  @Override
  public be.webdeb.core.api.contributor.Group toGroup(Group group) throws FormatException {
    be.webdeb.core.api.contributor.Group wrapped = contributorFactory.getGroup();
    wrapped.setGroupId(group.getIdGroup());
    wrapped.setGroupName(group.getGroupName());
    wrapped.setDescription(group.getGroupDescription());
    wrapped.isOpen(group.isOpen());
    wrapped.setGroupColor(group.getGroupColor());
    wrapped.setVersion(group.getVersion().getTime());
    wrapped.setContributionVisibility(EContributionVisibility.value(group.getContributionVisibility().getId()));
    wrapped.setMemberVisibility(EMemberVisibility.value(group.getMemberVisibility().getId()));
    wrapped.setPermissions(group.getPermissions().stream().map(p ->
        EPermission.value(p.getIdPermission())).collect(Collectors.toList()));
    return wrapped;
  }

  @Override
  public GroupSubscription toGroupSubscription(ContributorHasGroup chg,
      be.webdeb.core.api.contributor.Contributor contributor,
      be.webdeb.core.api.contributor.Group group) throws FormatException {

    be.webdeb.core.api.contributor.GroupSubscription wrapped = contributorFactory.getGroupSubscription();
    wrapped.setContributor(contributor);
    wrapped.setGroup(group);
    wrapped.setJoinDate(new Date(chg.getVersion().getTime()));
    wrapped.isBanned(chg.isBanned());
    wrapped.isFollowed(chg.isFollowed());
    wrapped.setRole(EContributorRole.value(chg.getRole().getIdRole()));
    wrapped.isDefault(chg.getContributor().getDefaultGroup().getIdGroup() == chg.getGroup().getIdGroup());
    wrapped.setInvitation(chg.getInvitation());
    return wrapped;
  }

  @Override
  public GroupSubscription toGroupSubscription(be.webdeb.core.api.contributor.Group group) throws FormatException {
    be.webdeb.core.api.contributor.GroupSubscription wrapped = contributorFactory.getGroupSubscription();
    wrapped.setGroup(group);
    wrapped.setRole(EContributorRole.VIEWER);
    return wrapped;
  }

  @Override
  public be.webdeb.core.api.contribution.Place toPlace(Place place){
    if(place != null) {
      be.webdeb.core.api.contribution.Place wrapped = textFactory.createPlace(
          place.getId(),
          place.getGeonameId(),
          place.getCode(),
          place.getLatitude(),
          place.getLongitude(),
          place.mapSpellings()
      );
      wrapped.setPlaceType(textFactory.findPlaceTypeByCode(place.getPlaceType().getIdType()));
      wrapped.setSubregion(toPlace(place.getSubregion()));
      wrapped.setRegion(toPlace(place.getRegion()));
      wrapped.setCountry(toPlace(place.getCountry()));
      wrapped.setContinent(toPlace(place.getContinent()));
      return wrapped;
    }
    return null;
  }

  /**
   * Set the names from given db actor into given api actor
   *
   * @param api an api actor to update
   * @param db a db actor
   */
  private void setNames(be.webdeb.core.api.actor.Actor api, Actor db) {
    api.setNames(db.getNames().stream().filter(n -> !n.isOld()).map(this::toActoName).collect(Collectors.toList()));
    if (EActorType.ORGANIZATION.equals(api.getActorType())) {
      ((be.webdeb.core.api.actor.Organization) api).setOldNames(db.getNames().stream().filter(ActorI18name::isOld)
          .map(this::toActoName).collect(Collectors.toList()));
    }
  }

  /**
   * Map a DB actor name to an API one
   *
   * @param name a DB actor name
   * @return the corresponding API name
   */
  private ActorName toActoName(ActorI18name name) {
    ActorName result = actorFactory.getActorName(name.getLang());
    result.setFirst(name.getFirstOrAcro());
    result.setLast(name.getName());
    result.setPseudo(name.getPseudo());
    return result;
  }

  /**
   * Set groups from given DB contribution into given API contribution
   *
   * @param api an api contribution to update
   * @param db a db contribution
   */
  private void setGroups(be.webdeb.core.api.contribution.Contribution api, Contribution db) {
    api.setInGroups(db.getGroups().stream().map(g -> {
      try {
        return toGroup(g);
      } catch (FormatException e) {
        logger.warn("unable to map group " + g.getIdGroup(), e);
        return null;
      }
    }).collect(Collectors.toList()));
  }

  @Override
  public be.webdeb.core.api.folder.Folder toFolder(Folder folder) throws FormatException {
    be.webdeb.core.api.folder.Folder wrapped = folderFactory.getFolder();
    setContributionData(wrapped, folder.getContribution());
    wrapped.setFolderType(folderFactory.getFolderType(folder.getFoldertype().getIdType()));

    folder.getNames().forEach(n -> wrapped.addName(n.getLang(), n.getName()));
    folder.getRewordingNames().forEach(n -> wrapped.addRewordingName(n.getLang(), n.getName()));

    setGroups(wrapped, folder.getContribution());
    return wrapped;
  }

  @Override
  public be.webdeb.core.api.folder.FolderLink toFolderLink(FolderLink link) throws FormatException {
    be.webdeb.core.api.folder.FolderLink wrapped = folderFactory.getFolderLink();
    wrapped.setParent(toFolder(link.getFolderParent()));
    wrapped.setChild(toFolder(link.getFolderChild()));
    wrapped.setInGroups(new ArrayList<>());
    return wrapped;
  }

  @Override
  public be.webdeb.core.api.project.Project toProject(Project project) throws FormatException {
    be.webdeb.core.api.project.Project wrapped = projectFactory.getProject();

    wrapped.setId(project.getIdProject());
    wrapped.setName(project.getName());
    wrapped.setTechnicalName(project.getTechnicalName());

    wrapped.setBeginDate(project.getBeginDate());
    wrapped.setEndDate(project.getEndDate());
    wrapped.setPedagogic(project.isPedagogic());

    return wrapped;
  }

  @Override
  public be.webdeb.core.api.project.ProjectGroup toProjectGroup(ProjectGroup group) throws FormatException {
    be.webdeb.core.api.project.ProjectGroup wrapped = projectFactory.getProjectGroup();

    wrapped.setId(group.getIdProjectGroup());
    wrapped.setName(group.getName());
    wrapped.setTechnicalName(group.getTechnicalName());

    return wrapped;
  }

  @Override
  public be.webdeb.core.api.project.ProjectSubgroup toProjectSubgroup(ProjectSubgroup subgroup) throws FormatException {
    be.webdeb.core.api.project.ProjectSubgroup wrapped = projectFactory.getProjectSubgroup();

    wrapped.setId(subgroup.getIdProjectSubgroup());
    wrapped.setName(subgroup.getName());
    wrapped.setTechnicalName(subgroup.getTechnicalName());
    wrapped.setNbContributors(subgroup.getNbContributors());

    wrapped.setProjectId(subgroup.getProjectGroup().getIdProjectGroup());
    wrapped.setProjectGroupId(subgroup.getProjectGroup().getIdProjectGroup());
    wrapped.setProjectGroupTechnicalName(subgroup.getProjectGroup().getTechnicalName());

    return wrapped;
  }

  @Override
  public ContributionToExplore toContributionToExplore(be.webdeb.infra.persistence.model.ContributionToExplore contirbutionToExplore) throws FormatException {
    ContributionToExplore wrapped = contributorFactory.getContributionToExplore();
    wrapped.setContributionToExploreId(contirbutionToExplore.getIdContributionToExplore());
    wrapped.setContributionId(contirbutionToExplore.getContribution().getIdContribution());
    wrapped.setContribution(toContribution(contirbutionToExplore.getContribution()));
    wrapped.setGroup(toGroup(contirbutionToExplore.getGroup()));
    wrapped.setOrder(contirbutionToExplore.getOrder());

    return wrapped;
  }

  @Override
  public Advice toAdvice(be.webdeb.infra.persistence.model.Advice advice) throws FormatException {
    Advice wrapped = contributorFactory.getAdvice();

    wrapped.setId(advice.getIdAdvice());
    for(AdviceI18name name : advice.getTitles()){
      wrapped.addName(name.getTitle(), name.getLang());
    }

    return wrapped;
  }

  /**
   * Map the contribution date for a given contribution API and contribution DB
   *
   * @param c the API contribution
   * @param contribution the DB contribution
   */
  private void setContributionData(be.webdeb.core.api.contribution.Contribution c, Contribution contribution){
    c.setId(contribution.getIdContribution());
    c.setVersion(contribution.getVersion().getTime());
    c.setValidated(textFactory.getValidationState(contribution.getValidated().getIdValidationState()));
    c.setLocked(contribution.isLocked());
  }
}
