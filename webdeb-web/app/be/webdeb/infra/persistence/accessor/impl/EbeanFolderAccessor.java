/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.accessor.impl;

import be.webdeb.core.api.argument.ArgumentContext;
import be.webdeb.core.api.debate.Debate;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.core.api.folder.*;
import be.webdeb.core.api.contribution.*;
import be.webdeb.core.api.folder.Folder;
import be.webdeb.core.api.folder.FolderLink;
import be.webdeb.core.api.text.Text;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.ObjectNotFoundException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.core.impl.contribution.ConcretePartialContributions;
import be.webdeb.infra.persistence.accessor.api.FolderAccessor;
import be.webdeb.infra.persistence.model.*;
import be.webdeb.infra.persistence.model.Contribution;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Transaction;
import com.avaje.ebean.TxScope;

import javax.inject.Singleton;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This accessor handles folder-related persistence actions.
 *
 * @author Martin Rouffiange
 */
@Singleton
public class EbeanFolderAccessor extends AbstractContributionAccessor<FolderFactory> implements FolderAccessor {

  private final static int MAX_HIERARCHY = 10;

  private List<FolderType> folderTypes;

  // string constant for logs
  private static final String AND_LINK = " and ";

  @Override
  public Folder retrieve(Long id, boolean hit) {
    be.webdeb.infra.persistence.model.Folder folder = be.webdeb.infra.persistence.model.Folder.findById(id);

    if (folder != null) {
      try {
        Folder api =  mapper.toFolder(folder);
        if (hit) {
          folder.getContribution().addHit();
          folder.getContribution().update();
        }
        return api;
      } catch (FormatException e) {
        logger.error("unable to cast retrieved folder " + id, e);
      }
    } else {
      logger.warn("no folder found for id " + id);
    }
    return null;
  }

  @Override
  public FolderLink retrieveLink(Long parent, Long child) {
    be.webdeb.infra.persistence.model.FolderLink l =
        be.webdeb.infra.persistence.model.FolderLink.findByParentChild(parent, child);
    if (l != null) {
      try {
        return mapper.toFolderLink(l);
      } catch (FormatException e) {
        logger.error("unable to cast retrieved folderlink between " + parent + AND_LINK + child, e);
      }
    } else {
      logger.warn("no folder link found between " + parent + AND_LINK + child);
    }
    return null;
  }

  @Override
  public List<Folder> findByName(String name, EFolderType type) {
    List<be.webdeb.infra.persistence.model.Folder> folders =
        be.webdeb.infra.persistence.model.Folder.findByPartialName(name, null, (type == null ? -1 : type.id()));
    if (!folders.isEmpty()) {
      return buildList(folders);
    }
    return new ArrayList<>();
  }

  @Override
  public Folder findUniqueByNameAndLang(String name, String lang){
    List<be.webdeb.infra.persistence.model.Folder> folders =
            be.webdeb.infra.persistence.model.Folder.findByPartialName(name, lang, -1);
    if(!folders.isEmpty()){
      try {
        mapper.toFolder(folders.get(0));
      } catch (FormatException e) {
        logger.error("unable to cast retrieved folder", e);
      }
    }
    return null;
  }

  @Override
  public Folder random() {
    try {
      return mapper.toFolder(be.webdeb.infra.persistence.model.Folder.random());
    } catch (FormatException e) {
      logger.error("unable to cast retrieved random folder", e);
    }
    return null;
  }

  @Override
  public Map<Integer, List<be.webdeb.core.api.contribution.Contribution>> save(Folder contribution, Long contributor) throws PermissionException, PersistenceException {
    logger.debug("try to save folder " + contribution.toString() + " in group " + contribution.getInGroups());
    Map<Integer, List<be.webdeb.core.api.contribution.Contribution>> createdFolders = new HashMap<>();
    createdFolders.put(EContributionType.FOLDER.id(), new ArrayList<>());
    int currentGroup = Group.getPublicGroup().getIdGroup();

    Contribution c = checkContribution(contribution, contributor, currentGroup);
    Contributor dbContributor = checkContributor(contributor, currentGroup);

    // open transaction and handle the whole save chain
    Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());

    try {
      EModificationStatus status;
      be.webdeb.infra.persistence.model.Folder folder;

      if (c == null) {
        // new folder
        logger.debug("start creation of new folder " + contribution.getDefaultName());
        status = EModificationStatus.CREATE;

        // create contribution super type
        c = initContribution(EContributionType.FOLDER.id(), contribution.getDefaultName());

        // create argument and binding
        folder = updateFolder(contribution, new be.webdeb.infra.persistence.model.Folder());
        c.setFolder(folder);
        folder.setContribution(c);

        // update groups
        updateGroups(contribution, c);
        c.save();

        // set id of folder
        folder.setIdContribution(c.getIdContribution());
        folder.save();

        // set new id for given contribution
        contribution.setId(c.getIdContribution());
        folder.setNames(toFolderI18names(contribution, folder));
        folder.setRewordingNames(toFolderRewordingI18names(contribution, folder));
        folder.update();
        createdFolders.get(EContributionType.FOLDER.id()).add(mapper.toFolder(folder));

      } else {
        logger.debug("start update of existing folder " + contribution.getId());
        folder = updateFolder(contribution, c.getFolder());
        status = EModificationStatus.UPDATE;

        // update sort key
        folder.getContribution().setSortkey(contribution.getDefaultName());
        folder.setNames(toFolderI18names(contribution, folder));
        folder.setRewordingNames(toFolderRewordingI18names(contribution, folder));
        folder.update();
      }
      try {
        List<FolderLink> parents = contribution.getParentsAsLink();
        List<FolderLink> children = contribution.getChildrenAsLink();
        createdFolders.get(EContributionType.FOLDER.id()).addAll(createUnknownFolders(contribution.getHierarchy(), contributor));
        folder.initParentFolders();
        folder.initChildFolders();
        folder.update();
        saveFolderHierarchy(parents, contributor);
        saveFolderHierarchy(children, contributor);
      } catch (PersistenceException | PermissionException e) {
        logger.error("permission error while saving/updating folder link ", e);
        throw e;
      }

      // bind contributor to this folder
      bindContributor(folder.getContribution(), dbContributor, status);

      transaction.commit();
      logger.info("saved " + folder.toString());

    } catch (Exception e) {
      logger.error("unable to save folder " + contribution.getDefaultName());
      throw new PersistenceException(PersistenceException.Key.SAVE_FOLDER, e);
    }
    finally {
      transaction.end();
    }

    return createdFolders;
  }

  @Override
  public void save(FolderLink link, Long contributor) throws PermissionException, PersistenceException {
    logger.debug("try to save folder link " + link.toString() + " in group public");
    int currentGroup = Group.getPublicGroup().getIdGroup();

    // ensure given parameters are ok
    Contribution contribution = checkContribution(link, contributor, currentGroup);
    Contributor dbContributor = checkContributor(contributor, currentGroup);

    // check if given link contains valid folders
    be.webdeb.infra.persistence.model.Folder parentFolder =
        be.webdeb.infra.persistence.model.Folder.findById(link.getParent().getId());
    if (parentFolder == null) {
      logger.error("unable to retrieve parent folder " + link.getParent().getId());
      throw new ObjectNotFoundException(Folder.class, link.getParent().getId());
    }

    be.webdeb.infra.persistence.model.Folder childFolder =
        be.webdeb.infra.persistence.model.Folder.findById(link.getChild().getId());
    if (childFolder == null) {
      logger.error("unable to retrieve child folder " + link.getChild().getId());
      throw new ObjectNotFoundException(Folder.class, link.getChild().getId());
    }

    saveFolderLink(parentFolder, childFolder, contribution, dbContributor);
  }

  /**
   * Save a folder link between two given db folders from for a given contributor.
   *
   * @param parentFolder the db folder as parent
   * @param childFolder the db folder as child
   * @param contribution the contribution in case of link update
   * @param dbContributor the contributor id that asked to save the contribution
   *
   * @throws PersistenceException if an error occurred at the database layer (concrete error will be wrapped)
   */
  @Override
  public void saveFolderLink(be.webdeb.infra.persistence.model.Folder parentFolder, be.webdeb.infra.persistence.model.Folder childFolder,
                              Contribution contribution, Contributor dbContributor) throws PersistenceException {
    Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
    try {
      EModificationStatus status;
      be.webdeb.infra.persistence.model.FolderLink dbLink;

      if (contribution == null) {
        status = EModificationStatus.CREATE;

        // create and save contribution supertype
        contribution = initContribution(EContributionType.FOLDERLINK.id(), null);
        try {
          contribution.save();
        } catch (Exception e) {
          logger.error("error while saving contribution for folder link between " +
                  parentFolder.getIdContribution() + AND_LINK + childFolder.getIdContribution());
          throw new PersistenceException(PersistenceException.Key.SAVE_FOLDER_LINK, e);
        }

        // create link
        dbLink = new be.webdeb.infra.persistence.model.FolderLink();
        contribution.setFolderLink(dbLink);
        dbLink.setContribution(contribution);
        dbLink = updateLink(contribution.getFolderLink(), parentFolder, childFolder);
        contribution.addGroup(Group.getPublicGroup());
        // save link
        try {
          contribution.save();
          dbLink.setIdContribution(contribution.getIdContribution());
          dbLink.save();
          logger.debug("saved folder link " + dbLink);

        } catch (Exception e) {
          logger.error("error while saving link between " + parentFolder.getIdContribution() + AND_LINK + childFolder.getIdContribution());
          throw new PersistenceException(PersistenceException.Key.SAVE_FOLDER_LINK, e);
        }

      } else {
        status = EModificationStatus.UPDATE;
        // update existing link
        dbLink = updateLink(contribution.getFolderLink(), parentFolder, childFolder);
        try {
          dbLink.update();
          logger.debug("updated folder link " + dbLink);
        } catch (Exception e) {
          logger.error("error while updating folder link " + dbLink.toString(), e);
          throw new PersistenceException(PersistenceException.Key.SAVE_FOLDER_LINK, e);
        }
      }
      // save link to contributor
      bindContributor(dbLink.getContribution(), dbContributor, status);

      transaction.commit();
      logger.info("saved " + dbLink.toString());

    } finally {
      transaction.end();
    }
  }

  /*
   * GETTERS FOR PREDEFINED VALUES
   */

  @Override
  public List<FolderType> getFolderTypes() {
    if (folderTypes == null) {
      folderTypes = TFolderType.find.all().stream().map(t ->
        factory.createFolderType(t.getIdType(), new LinkedHashMap<>(t.getTechnicalNames()))
      ).collect(Collectors.toList());
    }
    return folderTypes;
  }

  @Override
  public List<Folder> getParents(Long folder) {
    List<Folder> parents = new ArrayList<>();
    be.webdeb.infra.persistence.model.Folder folderDb = be.webdeb.infra.persistence.model.Folder.findById(folder);
    if(folderDb != null){
      parents = buildList(folderDb.getParentsAsLinks().stream()
          .map(be.webdeb.infra.persistence.model.FolderLink::getFolderParent).collect(Collectors.toList()));
    }
    return parents;
  }

  @Override
  public List<Folder> getChildren(Long folder) {
    List<Folder> children = new ArrayList<>();
    be.webdeb.infra.persistence.model.Folder folderDb = be.webdeb.infra.persistence.model.Folder.findById(folder);

    if(folderDb != null){
      children = buildList(folderDb.getChildrenAsLinks().stream()
          .map(be.webdeb.infra.persistence.model.FolderLink::getFolderChild).collect(Collectors.toList()));
    }
    return children;
  }

  @Override
  public List<be.webdeb.core.api.contribution.Contribution> getFolderContributions(Long folder){
    List<be.webdeb.core.api.contribution.Contribution> contributions = new ArrayList<>();
    be.webdeb.infra.persistence.model.Folder folderDb = be.webdeb.infra.persistence.model.Folder.findById(folder);

    if(folderDb != null){
      contributions = toContributions(folderDb.getContributions());
    }
    return contributions;
  }

  @Override
  public PartialContributions<ArgumentContext> getContextualizedArguments(Long folder, int fromIndex, int toIndex) {
    PartialContributions<ArgumentContext> response = new ConcretePartialContributions<>();
    be.webdeb.infra.persistence.model.Folder folderDb = be.webdeb.infra.persistence.model.Folder.findById(folder);

    if(folderDb != null){
      List<be.webdeb.infra.persistence.model.ArgumentContext> args = folderDb.getLinkedContextualizedArguments(fromIndex, toIndex);
      response.setNumberOfLoadedContributions(args.size());
      response.setContributions(buildContextualizedArgList(args));
    }
    return response;
  }

  @Override
  public PartialContributions<Excerpt> getExcerpts(Long folder, int fromIndex, int toIndex) {
    PartialContributions<Excerpt> response = new ConcretePartialContributions<>();
    be.webdeb.infra.persistence.model.Folder folderDb = be.webdeb.infra.persistence.model.Folder.findById(folder);

    if(folderDb != null){
      List<be.webdeb.infra.persistence.model.Excerpt> excs = folderDb.getLinkedExcerpts(fromIndex, toIndex);
      response.setNumberOfLoadedContributions(excs.size());
      response.setContributions(buildExcerptList(excs));
    }
    return response;
  }

  @Override
  public PartialContributions<Debate> getDebates(Long folder, int fromIndex, int toIndex) {
    PartialContributions<Debate> response = new ConcretePartialContributions<>();
    be.webdeb.infra.persistence.model.Folder folderDb = be.webdeb.infra.persistence.model.Folder.findById(folder);

    if(folderDb != null){
      List<be.webdeb.infra.persistence.model.Debate> debates = folderDb.getLinkedDebates(fromIndex, toIndex);
      response.setNumberOfLoadedContributions(debates.size());
      response.setContributions(buildDebateList(debates));
    }
    return response;
  }

  @Override
  public PartialContributions<Text> getTexts(Long folder, int fromIndex, int toIndex) {
    PartialContributions<Text> response = new ConcretePartialContributions<>();
    be.webdeb.infra.persistence.model.Folder folderDb = be.webdeb.infra.persistence.model.Folder.findById(folder);

    if(folderDb != null){
      List<be.webdeb.infra.persistence.model.Text> texts = folderDb.getLinkedTexts(fromIndex, toIndex);
      response.setNumberOfLoadedContributions(texts.size());
      response.setContributions(buildTextList(texts));
    }
    return response;
  }

  @Override
  public List<FolderName> getFolderRewordingNames(Long folder){
    be.webdeb.infra.persistence.model.Folder f = be.webdeb.infra.persistence.model.Folder.findById(folder);
    List<FolderName> names = new ArrayList<>();
    if(f != null){
      names = f.getRewordingNames().stream()
          .map(n -> factory.createFolderName(n.getLang(), n.getName())).collect(Collectors.toList());
    }
    return names;
  }

  @Override
  public HierarchyTree getFolderHierarchyTree(Long folder){
    return madeFolderHierarchyTree(factory.createHierarchyTree(), folder);
  }

  @Override
  public EHierarchyCode checkHierarchy(Folder folder, Folder hierarchy, boolean isParent){
    EHierarchyCode hierarchyCode;

    if(folder != null && hierarchy != null) {
      HierarchyTree folderTree = factory.createHierarchyTree();
      HierarchyTree hierarchyTree = factory.createHierarchyTree();
      folderTree = madeFolderHierarchyTree(folderTree, folder.getId());
      hierarchyTree = madeFolderHierarchyTree(hierarchyTree, hierarchy.getId());

      HierarchyNode hierarchyNode = hierarchyTree.searchNode(hierarchy.getId());
      if(isParent){
        hierarchyCode = folderTree.addChildToNode(hierarchyNode, hierarchyTree, folder.getId());
      }else{
        hierarchyCode = folderTree.addParentToNode(hierarchyNode, hierarchyTree, folder.getId());
      }
    }else{
      hierarchyCode = EHierarchyCode.NULL_OR_NO_ID;
    }
    return hierarchyCode;
  }

  @Override
  public int getNbContributions(Long folder, Long contributor, int group) {
    return be.webdeb.infra.persistence.model.Folder.getNbContributions(folder, contributor, group);
  }

  private HierarchyTree madeFolderHierarchyTree(HierarchyTree tree, Long folder){
    be.webdeb.infra.persistence.model.Folder folderDb = be.webdeb.infra.persistence.model.Folder.findById(folder);
    tree = fillHierarchy(tree, tree.getRoot(), true, folderDb);
    tree.removeIfNodeAsRootAndOthersAsParents(folder);
    return tree;
  }

  private HierarchyTree fillHierarchy(HierarchyTree tree, HierarchyNode currentNode, boolean isParent, be.webdeb.infra.persistence.model.Folder folder){
    HierarchyNode node = getNodeFromFolder(folder);
    if(node != null && currentNode != null) {
      //logger.debug(node.toString());
      EHierarchyCode code = tree.addNodeInHierarchy(node, null, currentNode.getId(), isParent);
      //logger.debug(code+"");
      if(code == EHierarchyCode.OK) {
        List<be.webdeb.infra.persistence.model.Folder> parents = folder.getParentsAsFolders();
        List<be.webdeb.infra.persistence.model.Folder> children = folder.getChildrenAsFolders();
        //logger.debug(parents.isEmpty()+"");
        if (!parents.isEmpty()) {
          for (be.webdeb.infra.persistence.model.Folder parent : parents) {
            tree = fillHierarchy(tree, node, false, parent);
          }
        } else {
          tree.addNodeToRoot(node);
        }
        for (be.webdeb.infra.persistence.model.Folder child : children) {
          tree = fillHierarchy(tree, node, true, child);
        }
      }
    }
    return tree;
  }

  private HierarchyNode getNodeFromFolder(be.webdeb.infra.persistence.model.Folder folder){
    HierarchyNode node = null;
    if(folder != null){
      node = factory.createHierarchyNode(folder.getIdContribution(), folder.getDefaultName());
    }
    return node;
  }

  /*
   * PRIVATE HELPERS
   */

  /**
   * Update a DB folder with given API folder data
   *
   * @param apiFolder an API folder with data to store
   * @param folder a DB folder recipient (may contain data to be updated)
   * @return given folder updated with given apiFolder data
   */
  private be.webdeb.infra.persistence.model.Folder updateFolder(Folder apiFolder, be.webdeb.infra.persistence.model.Folder folder) {
    folder.setFolderType(TFolderType.find.byId((apiFolder.getFolderType() == null ?
        EFolderType.SIMPLE.id() : apiFolder.getFolderType().getType())));
    return folder;
  }

  /**
   * Create all unknown folders in hierarchy
   *
   * @param hierarchy the hierarchy of the folder to save
   * @param contributor the contributor id that asked to save the contribution
   * @return the list of created folders
   */
  private List<be.webdeb.core.api.contribution.Contribution> createUnknownFolders(List<Folder> hierarchy, Long contributor) throws PermissionException, PersistenceException {
    List<be.webdeb.core.api.contribution.Contribution> foldersToCreate = new ArrayList<>();
    for(Folder folder : hierarchy) {
      if (folder.isValid().isEmpty()) {
        be.webdeb.infra.persistence.model.Folder folderDb = be.webdeb.infra.persistence.model.Folder.findById(folder.getId());
        if (folderDb == null) {
          foldersToCreate.addAll(save(folder, contributor).get(EContributionType.FOLDER.id()));
        }
      }
    }
    return foldersToCreate;
  }

  /**
   * Make the DB folder hierarchy from hierarchy list
   *
   * @param hierarchy the api parents or children of the given folder
   * @param contributor the contributor id that asked to save the contribution
   * @return given folder updated with given apiFolder data
   */
  private List<be.webdeb.infra.persistence.model.FolderLink> saveFolderHierarchy(List<FolderLink> hierarchy, Long contributor)
      throws PermissionException, PersistenceException {
    List<be.webdeb.infra.persistence.model.FolderLink> dbHierarchy = new ArrayList<>();

    for(FolderLink link : hierarchy){
      if(link.isValid().isEmpty()){
        be.webdeb.infra.persistence.model.FolderLink linkDb =
            be.webdeb.infra.persistence.model.FolderLink.findByParentChild(link.getParent().getId(), link.getChild().getId());

        if (linkDb == null) {
          save(link, contributor);
        }
      }else{
        logger.debug("Error in folder link " + link.isValid().get(0));
      }
    }
    return dbHierarchy;
  }

  /**
   * Update given db folder link with api folder link
   *
   * @param link a DB link to update
   * @param parent the parent DB folder (corresponding to apiLink.getParent)
   * @param child the "to" DB folder (corresponding to apiLink.getChild)
   * @return the updated DB folder link
   */
  private be.webdeb.infra.persistence.model.FolderLink updateLink(be.webdeb.infra.persistence.model.FolderLink link,
                                                                    be.webdeb.infra.persistence.model.Folder parent,
                                                                    be.webdeb.infra.persistence.model.Folder child) {
    link.setFolderParent(parent);
    link.setFolderChild(child);
    return link;
  }

  /**
   * Helper method to build a list of API folder from DB folders. All uncastable elements are ignored.
   *
   * @param folders a list of DB folders
   * @return a list of API folders with elements that could have actually been casted to API element (may be
   * empty)
   */
  private List<Folder> buildList(List<be.webdeb.infra.persistence.model.Folder> folders) {
    List<Folder> result = new ArrayList<>();
    for (be.webdeb.infra.persistence.model.Folder f : folders) {
      try {
        result.add(mapper.toFolder(f));
      } catch (FormatException e) {
        logger.error("unable to cast folder " + f.getIdContribution() + " Reason: " + e.getMessage(), e);
      }
    }
    return result;
  }

  /**
   * Create a list of FolderI18name for given db folder from given api folder
   *
   * @param api the api folder with a list of names
   * @param db the db folder to which those names will be bound
   * @return the db names from given api names
   */
  protected List<FolderI18name> toFolderI18names(Folder api, be.webdeb.infra.persistence.model.Folder db) {
    return api.getNames().entrySet().stream().map(n -> toFolderI18name(db, n.getKey(), n.getValue())).collect(Collectors.toList());
  }

  /**
   * Convert given folder name into a db folderI18name
   *
   * @param db the db folder holding the link to the new folder nae
   * @param lang the lang to convert
   * @param name the name to convert
   * @return the mapped folderI18name for this given name and linked to given db folder
   */
  private FolderI18name toFolderI18name(be.webdeb.infra.persistence.model.Folder db, String lang, String name) {
    return new FolderI18name(db, lang, name);
  }

  /**
   * Create a list of FolderRewordingI18name for given db folder from given api folder
   *
   * @param api the api folder with a list of rewording names
   * @param db the db folder to which those rewording names will be bound
   * @return the db rewording names from given api rewording names
   */
  protected List<FolderRewordingI18name> toFolderRewordingI18names(Folder api, be.webdeb.infra.persistence.model.Folder db) {
    List<FolderRewordingI18name> names = new ArrayList<>();
    api.getRewordingNames().entrySet().forEach(n ->
        names.addAll(n.getValue().stream().map(
            v -> toFolderRewordingI18name(db, v.getLang(), v.getName())).collect(Collectors.toList())));
    return names;
  }

  /**
   * Convert given folder rewording name into a db folderRewordingI18name
   *
   * @param db the db folder holding the link to the new folder rewording name
   * @param lang the lang to convert
   * @param name the name to convert
   * @return the mapped folderRewordingI18name for this given rewording name and linked to given db folder
   */
  private FolderRewordingI18name toFolderRewordingI18name(be.webdeb.infra.persistence.model.Folder db, String lang, String name) {
    return new FolderRewordingI18name(db, lang, name);
  }
}
