/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.accessor.api;

import be.webdeb.core.api.argument.ArgumentIllustration;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.excerpt.ContextElement;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.core.api.excerpt.ExternalExcerpt;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;

import java.util.List;
import java.util.Map;

/**
 * This interface represents an accessor for excerpts persisted into the database
 *
 * @author Martin Rouffiange
 */
public interface ExcerptAccessor extends ContributionAccessor  {

    /**
     * Retrieve an Excerpt by its id
     *
     * @param id an excerpt id
     * @param hit true if this retrieval must be counted as a visualization
     * @return the excerpt concrete object corresponding to the given id, null if not found
     */
   Excerpt retrieve(Long id, boolean hit);

    /**
     * Retrieve an ExternalExcerpt by its id
     *
     * @param id a Contribution id
     * @return an ExternalExcerpt if given id is an external excerpt, null otherwise
     */
    ExternalExcerpt retrieveExternal(Long id);

    /**
     * Retrieve an external excerpt by its id and the external source id
     *
     * @param id a Contribution id
     * @param externalSource the source where the excerpts come from
     * @return an ExternalExcerpt if given id is an external excerpt, null otherwise
     */
    ExternalExcerpt retrieveExternal(Long id, int externalSource);

    /**
     * Retrieve a context element by its idd
     *
     * @param id a context contribution id
     * @return a ContextElement if given id is an ContextElement, null otherwise
     */
    ContextElement retrieveContextElement(Long id);

    /**
     * Retrieve an illustration link by argid and excid.
     *
     * @param argid an contextualized argument id
     * @param excid an excerpt id
     * @return the ArgumentIllustration, null if not found
     */
    ArgumentIllustration retrieveIllustrationLink(Long argid, Long excid );

    /**
     * Save an excerpt on behalf of a given contributor If excerpt.getId has been set, update the
     * excerpt, otherwise create argument and update contribution id.
     *
     * All passed contribution (affiliation ids (aha) and folders) are also considered as valid.If an contribution has no id,
     * the contribution is considered as non-existing and created. This contribution is then returned.
     *
     * @param contribution a contribution excerpt to save
     * @param currentGroup the current group id from which the contributor triggered the save action (for auto-created actors)
     * @param contributor the contributor id that asked to save the contribution
     * @return a map of Contribution type and a possibly empty list of Contributions (Actors or Folders) created automatically with this
     * save action (new contributions)
     *
     * @throws PermissionException if given contributor may not publish in current group or given contribution may not
     * be published in current group, or given contribution does not belong to current group
     * @throws PersistenceException if an error occurred, a.o., unset required field or no version number for
     * an existing contribution (id set). The exception message will contain a more complete description of the
     * error.
     */
    Map<Integer, List<Contribution>> save(Excerpt contribution, int currentGroup, Long contributor) throws PermissionException, PersistenceException;

    /**
     * Save an external excerpt on behalf of a given contributor If externalExcerpt.getId has been set, update the
     * external excerpt, otherwise create argument and update contribution id.
     *
     * All passed contribution (affiliation ids (aha) and folders) are also considered as valid.If an contribution has no id,
     * the contribution is considered as non-existing and created. This contribution is then returned.
     *
     * @param contribution a contribution external excerpt to save
     * @param currentGroup the current group id from which the contributor triggered the save action (for auto-created actors)
     * @param contributor the contributor id that asked to save the contribution
     * @return a map of Contribution type and a possibly empty list of Contributions (Actors or Folders) created automatically with this
     * save action (new contributions)
     *
     * @throws PermissionException if given contributor may not publish in current group or given contribution may not
     * be published in current group, or given contribution does not belong to current group
     * @throws PersistenceException if an error occurred, a.o., unset required field or no version number for
     * an existing contribution (id set). The exception message will contain a more complete description of the
     * error.
     */
    Map<Integer, List<Contribution>> save(ExternalExcerpt contribution, int currentGroup, Long contributor) throws PermissionException, PersistenceException;

    /**
     * Save a context element
     *
     * @param element an API context element to save
     *
     * @throws PersistenceException if the save action(s) could not been performed because of an issue with
     * the persistence layer
     */
    void save(ContextElement element) throws PersistenceException;

    /**
     * Find already recorded external excerpt that hasn't has been added to webdeb yet.
     *
     * @param contributor a contributor id
     * @param originalExcerpt the external excerpt title
     * @param textId the text id of the argument
     * @return the retrieved external external or null
     */
    ExternalExcerpt findExistingExternalExcerpt(Long contributor, String originalExcerpt, Long textId);

    /**
     * Get the list of all external excerpts waiting for contributors' validation that come from a given external source
     * and with a maximum of results. Those excerpts have been retrieved from external services like WDTAL services
     * (eg twitter) and are waiting for validation before being transformed to Excerpts and sent into the webdeb database.
     *
     * @param externalSource the source where the excerpts come from
     * @param maxResults the maximum of excerpts to return
     * @return a (possibly empty) list of temporary excerpts
     */
    List<ExternalExcerpt> getExternalExcerptsByExternalSource(int externalSource, int maxResults);

 /**
  * Get the list of directly linked arguments
  *
  * @param id the excerpt id
  * @return the list of similar arguments
  */
   List<ArgumentIllustration> getArguments(Long id);

   /**
    * Get the list of similar excerpts of the given excerpt. A similar excerpt is an excerpt that illustrate the same
    * contextualized argument in the same context with the same or different shade.
    *
    * @param id the excerpt id
    * @return the list of similar excerpts of this one
    */
   List<ArgumentIllustration> similars(Long id);


}
