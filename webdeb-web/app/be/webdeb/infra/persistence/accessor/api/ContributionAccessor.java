/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.accessor.api;

import be.webdeb.application.query.EQueryKey;
import be.webdeb.core.api.actor.ActorRole;
import be.webdeb.core.api.argument.ArgumentContext;
import be.webdeb.core.api.argument.ArgumentJustification;
import be.webdeb.core.api.contribution.*;
import be.webdeb.core.api.contributor.Contributor;
import be.webdeb.core.api.folder.Folder;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This interface represents an accessor for contribution-related management,i.e,retrieving generic
 * contributions from various properties,or retrieve specific contributions(actors,texts and arguments and
 * argument)
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public interface ContributionAccessor {

  /**
   * Retrieve a Contribution by its id
   *
   * @param id a Contribution id
   * @param hit true if this retrieval must be counted as a visualization
   * @return the Contribution concrete object corresponding to the given id, null if not found
   */
  Contribution retrieve(Long id, boolean hit);

  /**
   * Retrieve a Contribution by its id and type. Invoker must explicitly cast returned value into
   * concrete type (as given in "type" parameter) to access concrete methods.
   *
   * @param id a Contribution id
   * @param type a contribution type (may pass the ALL type)
   * @return the Contribution concrete object corresponding to the given id, null if not found
   */
  Contribution retrieve(Long id, EContributionType type);

  /**
   * Retrieve a Contribution by its id. Invoker must explicitly cast returned value into concrete type to
   * access concrete methods.
   *
   * @param id a Contribution id
   * @return the Contribution concrete object corresponding to the given id, null if not found
   */
  Contribution retrieveContribution(Long id);

  /**
   * Retrieve a context Contribution by its id. Invoker must explicitly cast returned value into concrete type to
   * access concrete methods.
   *
   * @param id a ContextContribution id
   * @return the ContextContribution concrete object corresponding to the given id, null if not found
   */
  ContextContribution retrieveContextContribution(Long id);

  /**
   * Get all actors directly involved in a given contribution. Only retrieve actors directly connected to
   * given contribution, not all actors connected to all contributions bound to this contribution.
   *
   * @param contribution a contribution id
   * @return a list of Actor roles in given contribution, empty list if contribution not found
   */
  List<ActorRole> getActors(Long contribution);

  /**
   * Find a place by its id
   *
   * @param id a place id
   * @return a place
   */
  Place findPlace(Long id);

  /**
   * Find a list of place by given partial place name
   *
   * @param name a place name
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return a list of find places
   */
  List<Place> findPlace(String name, int fromIndex, int toIndex);

  /**
   * Find a list of Contributions by a value. Will search in text titles, argument standard forms and
   * actor names
   *
   * @param value a value to search for
   * @param group a group id
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return the (possibly empty) list of Contributions that contains given value
   */
  List<Contribution> findByValue(String value, int group, int fromIndex, int toIndex);

  /**
   * Find a list of Contribution by a list of criteria, being key-value pairs.
   *
   * @param criteria a list of key-value pairs to search for
   * @param strict check if we must perform a strict search
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return the list of Contribution that fulfils the given criteria, may be empty
   * @see EQueryKey for the list of valid keys
   */
  List<Contribution> findByCriteria(List<Map.Entry<EQueryKey, String>> criteria, boolean strict, int fromIndex, int toIndex);

  /**
   * Find a list of Contribution ordered by version time (actor, text and argument only)
   *
   * @param type the contribution type we are interested in
   * @param contributor a contributor id (-1 to ignore it)
   * @param amount the amount of entries to retrieve
   * @param group the group id where to look for contributions (if public group is passed, any public contribution is returned)
   * @return the list of latest touched Contributions of given type for given contributor
   */
  List<Contribution> getLatestEntries(EContributionType type, Long contributor, int amount, int group);

  /**
   * Find a list of Contribution ordered by number of visualization hits (actor, text and argument only)
   *
   * @param type the contribution type we are interested in
   * @param contributor a contributor id (-1 to ignore it)
   * @param amount the amount of entries to retrieve
   * @param group the group id where to look for contributions (if public group is passed, any public contribution is returned)
   * @return the list of most hit Contributions of given type for given contributor
   */
  List<Contribution> getPopularEntries(EContributionType type, Long contributor, int amount, int group);

  /**
   * Retrieve the creator of a contribution
   *
   * @param contribution a Contribution id
   * @return the creator of the given contribution, or null if contribution does not exist
   */
  Contributor getCreator(Long contribution);

  /**
   * Get the last contributor that is in a given group that update a given contribution
   *
   * @param contribution a Contribution id
   * @param group a Group id
   * @return the last Contributor of the given Contribution in the given group
   */
  Contributor getLastContributorInGroup(Long contribution, int group);

  /**
   * Get all contributors that created or updated given contribution
   *
   * @param contribution a Contribution id
   * @return the list of contributors of this Contribution, an empty list if contribution is not found
   */
  List<Contributor> getContributors(Long contribution);

  /**
   * Merge data from two contributions. Any detail present in the origin for which no detail exist in the replacement
   * contribution will be stored in this replacement contribution, any conflicting detail will be ignored.
   * <br><br>
   * Any link to the origin contribution will be replaced by the replacement contribution, ie
   * <ul>
   *   <li>for actors, any bound contribution to the origin will be bound to the replacement actor. Reconciliations
   *   regarding affiliation will be performed on affiliation actors. For all origin affiliations, if no such affiliation
   *   to an actor exists in replacement, it will be copied from origin. If the affiliation has no link to an actor, and
   *   such function does not exists, it will be copied in replacement actor too. Affiliated actors to the origin will
   *   be rebound to replacement actor.</li>
   *
   *   <li>for texts, any argument will be attached to the replacement text. In case of private contents, they will be
   *   copied into the replacement one (if not already existing) and if no shared content exist in the replacement,
   *   the origin content will be copied too.</li>
   *
   *   <li>for arguments, any similarity link to the origin will be attached to the replacement, justification links
   *   will be deleted</li>
   * </ul>
   *
   * @param origin a contribution id to be merged into and replaced by given replacement contribution
   * @param replacement the replacement contribution id
   * @param contributor the contributor id asking for the merge
   * @throws PersistenceException if any given contribution or contributor does not exist, or if both contributions
   * haven't the same type, or if any other error occurred while saving into the database
   * @throws PermissionException if given contributor is not owner of both groups containing given contributions
   */
  void merge(Long origin, Long replacement, Long contributor) throws PermissionException, PersistenceException;

  /**
   * Remove a given contribution from the repository. If given contribution belonged to many groups, it
   * is removed from all groups.
   *
   * Such removal will not be allowed if
   * <ul>
   *   <li>this contribution is a text and arguments have been extracted from it</li>
   *   <li>this contribution is an actor and it has been bound to other contributions</li>
   * </ul>
   *
   * @param contribution a contribution id
   * @param type the type of the contribution (used to crosscheck id and type)
   * @param contributor a contributor id that issued the removal action
   * @throws PersistenceException if given contribution was not found in the database or any of the aforementioned
   * constraint was violated.
   * @throws PermissionException if given contributor is not at least an owner of the only group to which this contribution
   * belongs to, or a platform admin
   */
  void remove(Long contribution, EContributionType type, Long contributor) throws PermissionException, PersistenceException;

  /**
   * Remove a given contribution from the repository. If given contribution belonged to many groups, it
   * is removed from all groups.
   *
   * Such removal will not be allowed if
   * <ul>
   *   <li>this contribution is a text and arguments have been extracted from it</li>
   *   <li>this contribution is an actor and it has been bound to other contributions</li>
   * </ul>
   *
   * @param contribution a contribution id
   * @param type the type of the contribution (used to crosscheck id and type)
   * @param contributor a contributor id that issued the removal action
   * @param option remove option
   * @throws PersistenceException if given contribution was not found in the database or any of the aforementioned
   * constraint was violated.
   * @throws PermissionException if given contributor is not at least an owner of the only group to which this contribution
   * belongs to, or a platform admin
   */
  void remove(Long contribution, EContributionType type, Long contributor, ERemoveOption option) throws PermissionException, PersistenceException;

  /**
   * Bind an author to a contribution, return the updated Actor if it was unknown (after creating it in
   * database). Existence check is performed on id.
   *
   * @param contribution a Contribution object (must exist)
   * @param role the role of a bound Actor (in this role) in given contribution
   * @param currentGroup the current group id, will be used if the actor in given role is not yet known
   * @param contributor contributor  id that issued the binding action
   * @return a possibly empty list of Contributions created automatically with this save action (new contributions)
   *
   * @throws PermissionException if given contributor may not bind given actor to given contribution in given group
   * @throws PersistenceException if an error occurred, a.o., unset parameter or not found element from database
   */
  List<Contribution> bindActor(Long contribution, ActorRole role, int currentGroup, Long contributor) throws PermissionException, PersistenceException;

  /**
   * Unbind an author to a contribution, return the updated Actor if it was unknown (after creating it in
   * database). Existence check is performed on id.
   *
   * @param contribution a Contribution id
   * @param actor an Actor representing an author
   * @param contributor a contributor that issued the unbind action
   * @throws PersistenceException if the binding removal did not complete
   */
  void unbindActor(Long contribution, Long actor, Long contributor) throws PersistenceException;

  /**
   * Bind a folder linked to a contribution, return the updated Folder if it was unknown (after creating it in
   * database). Existence check is performed on id.
   *
   * @param contribution a Contribution id
   * @param folders a list of folder to bound in given contribution
   * @param contributor contributor  id that issued the binding action
   * @return a possibly empty list of Contributions created automatically with this save action (new contributions)
   *
   * @throws PermissionException if given contributor may not bind given actor to given contribution in given group
   * @throws PersistenceException if an error occurred, a.o., unset parameter or not found element from database
   */
  List<Contribution> bindFolders(Long contribution, List<Folder> folders, Long contributor) throws PermissionException, PersistenceException;

  /**
   * Bind a folder linked to a contribution, return the updated Folder if it was unknown (after creating it in
   * database). Existence check is performed on id.
   *
   * @param contribution a Contribution id
   * @param folders a list of folder to bound in given contribution
   * @param contributor contributor  id that issued the binding action
   * @param createComposite true if we need to create composite folders
   * @return a possibly empty list of Contributions created automatically with this save action (new contributions)
   *
   * @throws PermissionException if given contributor may not bind given actor to given contribution in given group
   * @throws PersistenceException if an error occurred, a.o., unset parameter or not found element from database
   */
  List<Contribution> bindFolders(Long contribution, List<Folder> folders, Long contributor, boolean createComposite) throws PermissionException, PersistenceException;

  /**
   * Get all folders linked with a given contribution
   *
   * @param contribution a Contribution id
   * @return a possibly empty set of folders
   */
  Set<Folder> getContributionsFolders(Long contribution);

  /**
   * Get all places linked with a given contribution
   *
   * @param contribution a Contribution id
   * @return a possibly empty set of geographical places
   */
  List<Place> getContributionsPlaces(Long contribution);

  /**
   * Retrieve all contribution types
   *
   * @return a list of contribution types
   */
  List<ContributionType> getContributionTypes();

  /**
   * Get the amount of given type of contribution
   *
   * @param type a type of contribution
   * @param group the group id where to look for contributions (if public group is passed, any public contribution is returned)
   * @return the amount of contribution of given type
   */
  int getAmountOf(EContributionType type, int group);

  /**
   * Save validated state for given contributions (formerly also save marking, but it was judged later as a non-pedagogical way to learn)
   *
   * @param contributions a list of contributions
   * @throws PersistenceException if any validations could not be saved (all or nothing)
   */
  void saveMarkings(List<Contribution> contributions) throws PersistenceException;

  /**
   * Remove given contribution from given group. If this contribution only belonged to this group, it is simply
   * removed completely from the repository.
   *
   * @param contribution a contribution id
   * @param group a group id
   * @param contributor id the contributor that issued that removal
   * @throws PersistenceException if given contribution or group does not exists or if an other database error occurred
   * @throws PermissionException if given contributor is not owner of given group
   */
  void removeFromGroup(Long contribution, int group, Long contributor) throws PermissionException, PersistenceException;

  /**
   * Check if the given contribution is member of a group with public visibility
   *
   * @param contribution a contribution id
   * @return true if a public group contains the given contribution
   */
  boolean isMemberOfAPublicGroup(Long contribution);

  /**
   * Get the full history of given contribution, ie, all modifications with their owners made to given contribution
   *
   * @param contribution a contribution id
   * @return a (possibly empty) list of history traces (list will be empty for non found contributions)
   */
  List<ContributionHistory> getHistory(Long contribution);

  /**
   * Retrieve all word profession banned
   *
   * @param contextType the context of banned words
   * @param type the type of banned words (beginning of the word, ...)
   * @return a list of words profession banned
   */
  List<WarnedWord> getWarnedWords(int contextType, int type);

  /**
   * Found a Place continent by code code
   *
   * @param code the code of the place
   * @return the matched place continent id, or null
   */
  Long retrievePlaceContinentCode(String code);

  /**
   * Found a Place continent by code code
   *
   * @param geonameId the geoname id of the place
   * @param placeId the id of the place
   * @return the matched place id, or null
   */
  Long retrievePlaceByGeonameIdOrPlaceId(Long geonameId, Long placeId);

  /**
   * Find PlaceType by id
   *
   * @param code the id of the place type
   * @return a PlaceType
   */
  PlaceType findPlaceTypeByCode(int code);

  /**
   * Save place and return the save results
   *
   * @param places the places to save
   * @param contribution the contribution where link place
   */
  void savePlaces(List<Place> places, be.webdeb.infra.persistence.model.Contribution contribution);

  /**
   * Get the db Place that match geoname id or code, otherwise create it
   *
   * @param place the api place to found
   * @return the db place
   */
  be.webdeb.infra.persistence.model.Place savePlace(Place place);

  /**
   * Retrieve all validation states
   *
   * @return a list of validation states
   */
  List<ValidationState> getValidationStates();

  /**
   * Update rejected state of given external contribution from temporary external service list.
   *
   * @param id an external contribution id to be update
   * @param rejected true if this external contribution must be set as rejected, false otherwise
   * @throws PersistenceException if given id does not correspond to an external contribution or an error occurred when
   * persisting the new state in the database
   */
  void updateDiscoveredExternalState(Long id, boolean rejected) throws PersistenceException;

  /**
   * Save given list of argument justification links linked to a contribution context
   *
   * @param links a list of justification links
   * @param context the context contribution of the links
   * @param currentGroup the current group id from which the contributor triggered the save action (for auto-created actors)
   * @param contributor the Contributor id that initiated the save action
   *
   * @throws PermissionException if the save action(s) could not been performed because of an issue regarding a
   * contributor permissions or because this operation would cause a problem of integrity.
   * @throws PersistenceException if the save action(s) could not been performed because of an issue with
   * the persistence layer
   */
  void saveJustificationLinks(List<ArgumentJustification> links, Contribution context, int currentGroup, Long contributor) throws PersistenceException, PermissionException;

  /**
   * Get all argument linked to this contribution if this contribution is a contextualized contribution
   *
   * @param id an contextualized argument id
   * @return a possibly empty list of contextualized arguments
   */
  List<ArgumentContext> getContextualizedArgumentsFromContext(Long id);

  /**
   * Get all argument linked to this contribution if this contribution is a contextualized contribution limited by indexes
   *
   * @param id an contextualized argument id
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return a possibly empty list of contextualized arguments
   */
  PartialContributions<ArgumentContext> getContextualizedArgumentsFromContext(Long id, int fromIndex, int toIndex);

  /**
   * Get all justification linked to this contribution if this contribution is a contextualized contribution
   *
   * @return a possibly empty list of justifications
   */
  List<ArgumentJustification> getArgumentJustificationLinks(Long id);

  /**
   * Retrieve all languages
   *
   * @return a list of languages
   */
  List<Language> getLanguages();

  /**
   * Temporary save this context contribution to get an id for sub objects
   *
   * @param contribution the context contribution id to save
   * @param type the context contribution type
   * @param contributor the Contributor id that initiated the save action
   * @param currentGroup the current group id from which the contributor triggered the save action (for auto-created actors)
   *
   * @throws PermissionException if the save action(s) could not been performed because of an issue regarding a
   * contributor permissions or because this operation would cause a problem of integrity.
   * @throws PersistenceException if the save action(s) could not been performed because of an issue with
   * the persistence layer
   */
  void saveContextContribution(ContextContribution contribution, EContributionType type, Long contributor, int currentGroup) throws PermissionException, PersistenceException;

  /**
   * Delete the temporary context contribution in case of error
   *
   * @param contribution the context contribution id to delete
   * @param contributor the Contributor id that initiated the save action
   *
   * @throws PermissionException if the save action(s) could not been performed because of an issue regarding a
   * contributor permissions or because this operation would cause a problem of integrity.
   */
  void deleteContextContribution(Long contribution, Long contributor) throws PermissionException;

  /**
   * Get the first argument of the given context contribution if exists, otherwise create it.
   *
   * @param id a context contribution id
   * @return the context contribution first argument if given text exists, null otherwise
   */
  ArgumentContext getOrCreateFirstArgument(Long id);


  /**
   * Get the map of number of linked elements with this contribution
   *
   * @param id a contribution id
   * @param contributorId the id of the contributor for which we need that stats
   * @param groupId the id of the group where stats must be counted
   * @return the map of number of linked elements with this contribution by contribution type
   */
  Map<EContributionType, Integer> getCountRelationsMap(Long id, Long contributorId, int groupId);

  /**
   * Get all contribution id for a given contribution type
   *
   * @param type the contribution type to focus
   * @return a possibly empty list of contribution id for the given type
   */
  List<Long> getAllIdByContributionType(EContributionType type);

}
