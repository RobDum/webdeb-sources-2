/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.model;

import be.webdeb.infra.persistence.model.annotation.Unqueryable;
import com.avaje.ebean.*;
import com.avaje.ebean.annotation.CacheBeanTuning;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for temporary texts from external services (like browser extension) that will be added
 * manually by contributor.
 *
 * Used as temporary repository for an easiest communication between services and the platform.
 *
 * @author Martin Rouffiange
 */
@Entity
@CacheBeanTuning
@Table(name = "external_text")
@Unqueryable
public class ExternalText extends Model {

    protected static final org.slf4j.Logger logger = play.Logger.underlying();
    private static final Model.Finder<Long, ExternalText> find = new Model.Finder<>(ExternalText.class);

    @Id
    @Column(name = "id_contribution", unique = true, nullable = false)
    private Long idContribution;

    // forcing updates from this object, deletions are handled at the contribution level
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
    @JoinColumn(name = "id_contribution", nullable = false, insertable = false, updatable = false)
    private ExternalContribution contribution;

    @Column(name = "title")
    private String title;

    @OneToMany(mappedBy = "externalText", fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    private List<ExternalExcerpt> externalExcerpts;

    /**
     * Get parent external contribution object
     *
     * @return the parent external contribution
     */
    public ExternalContribution getExternalContribution() {
        return contribution;
    }

    /**
     * Set parent external contribution object
     *
     * @param contribution the parent external contribution
     */
    public void setExternalContribution(ExternalContribution contribution) {
        this.contribution = contribution;
    }

    /**
     * Get the id of external text
     *
     * @return the external text id
     */
    public Long getIdContribution() {
        return idContribution;
    }

    /**
     * Set the id of external text
     *
     * @param idContribution the external text id
     */
    public void setIdContribution(Long idContribution) {
        this.idContribution = idContribution;
    }

    /**
     * Get the title for this external text
     *
     * @return the text title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set the title for this external text
     *
     * @param title the text title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Get all external excerpts linked to this text
     *
     * @return a possibly empty list of external excerpts
     */
    public List<ExternalExcerpt> getExternalExcerpts() {
        return externalExcerpts;
    }

    /**
     * Set all external excerpts linked to this text
     *
     * @param externalExcerpts the list of external excerpts
     */
    public void setExternalExcerpts(List<ExternalExcerpt> externalExcerpts) {
        this.externalExcerpts = externalExcerpts;
    }

    /**
     * Get the current version of this external text
     *
     * @return a timestamp with the latest update moment of this external text
     */
    public Timestamp getVersion() {
        return getExternalContribution().getContribution().getVersion();
    }

    @Override
    public String toString() {
        // must use getters and explicitly loop into references, otherwise ebean may send back deferred beanlist
        // (lazy load not triggered from toString methods)
        return new StringBuffer("external text [").append(getIdContribution())
                .append(" [version:").append(getExternalContribution().getContribution().getVersion()).append("]").toString();
    }

    /*
     * QUERIES
     */

    /**
     * Retrieve an external text by its id
     *
     * @param id an id
     * @return the external text corresponding to the given id, null if not found
     */
    public static ExternalText findById(Long id) {
        return id == null || id == -1L ? null : find.byId(id);
    }

    /**
     * Retrieve an external text by its id or internal text id and its contributor id
     *
     * @param contributor a contributor id
     * @param id a internal or external text id
     * @return the external text corresponding to the given id and added by the given contributor, null if not found
     */
    public static ExternalText findByIdAndContributor(Long contributor, Long id) {
        String select = "select contribution.id_contribution from contribution left join contribution_has_contributor on " +
                "contribution.id_contribution = contribution_has_contributor.id_contribution left join external_contribution on " +
                "contribution.id_contribution = external_contribution.id_contribution " +
                "where contribution_has_contributor.id_contributor = " + contributor + " and " +
                "(external_contribution.id_contribution = " + id + " or external_contribution.id_internal_contribution = " + id + ")";
        List<ExternalText> results = Ebean.find(ExternalText.class).setRawSql(RawSqlBuilder.parse(select).create()).findList();
        return results != null && !results.isEmpty() ? results.get(0) : null;
    }

    /**
     * Retrieve an external text by url and contributor
     *
     * @param contributor a contributor id
     * @param url the text url
     * @return the external text corresponding to the given contributor and url, null if not found
     */
    public static ExternalText findByContributorAndUrl(Long contributor, String url) {
        List<ExternalText> results = null;
        if(url != null) {
            String select = "select contribution.id_contribution from contribution left join contribution_has_contributor on " +
                    "contribution.id_contribution = contribution_has_contributor.id_contribution  left join external_contribution on " +
                    "contribution.id_contribution = external_contribution.id_contribution  left join external_text on " +
                    "contribution.id_contribution = external_text.id_contribution " +
                    "where contribution_has_contributor.id_contributor = " + contributor + " and external_contribution.source_url like \"%" + url + "\"";
           results = Ebean.find(ExternalText.class).setRawSql(RawSqlBuilder.parse(select).create()).findList();
        }
        return results != null && !results.isEmpty() ? results.get(0) : null;
    }

    /**
     * Retrieve a external text linked to the text where it comes from, by text id and contributor
     *
     * @param text a text id
     * @param contributor a contributor id
     * @return the external text linked to the given text and contributor
     */
    public static ExternalText findByTextAndContributor(Long text, Long contributor) {
        List<ExternalText> results = null;
        String select = "select contribution.id_contribution from contribution left join contribution_has_contributor on " +
                "contribution.id_contribution = contribution_has_contributor.id_contribution left join external_contribution on " +
                "contribution.id_contribution = external_contribution.id_contribution " +
                "where contribution_has_contributor.id_contributor = " + contributor + " and external_contribution.id_internal_contribution = " + text;
        results = Ebean.find(ExternalText.class).setRawSql(RawSqlBuilder.parse(select).create()).findList();
        return results != null && !results.isEmpty() ? results.get(0) : null;
    }

    /**
     * Retrieve a external text linked to the text where it comes from, by text id and contributor
     *
     * @param id an internal or external text id
     * @return the external text, or null
     */
    public static ExternalText findTextByInternalOrExternalId(Long id) {
        ExternalText text = findById(id);
        if(text == null) {
            List<ExternalText> texts = find.where().eq("id_internal_contribution", id).findList();
            text = texts != null && !texts.isEmpty() ? texts.get(0) : null;
        }
        return text;
    }
}
