/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.model;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CacheBeanTuning;


import javax.persistence.*;


/**
 * The persistent class for the t_argument_type database table, used to select argument shades to produce
 * the argument standardized form.
 *
 * @author Fabian Gilson
 * @see be.webdeb.core.api.argument.EArgumentLinkShade
 */
@Entity
@Table(name = "t_argument_type")
@CacheBeanTuning
public class TArgumentType extends TechnicalTable {

  /**
   * Finder to access predefined values
   */
  public static final Model.Finder<Integer, TArgumentType> find = new Model.Finder<>(TArgumentType.class);

  @Id
  @Column(name = "id_argtype", unique = true, nullable = false)
  private int idArgtype;

  /*
   * GETTERS / SETTERS
   */

  /**
   * Get the argument type id
   *
   * @return an id
   */
  public int getIdArgtype() {
    return this.idArgtype;
  }

  /**
   * Set this argument type id
   *
   * @param idArgtype an id
   */
  public void setIdArgtype(int idArgtype) {
    this.idArgtype = idArgtype;
  }

}
