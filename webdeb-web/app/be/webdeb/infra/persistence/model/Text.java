/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.model;

import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.text.ETextVisibility;
import be.webdeb.infra.persistence.model.annotation.Unqueryable;
import com.avaje.ebean.*;
import com.avaje.ebean.annotation.PrivateOwned;
import com.avaje.ebean.annotation.CacheBeanTuning;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * The persistent class for the text database table, subtype of contributions, representing a text of any kind,
 * like press articles, book chapters, blog post, etc. A text may contains excerpts, ie, excerpts have been
 * extracted from a text to be translated into Excerpts
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
@Entity
@CacheBeanTuning
@Table(name = "text")
public class Text extends WebdebModel {

  private static final Model.Finder<Long, Text> find = new Model.Finder<>(Text.class);

  // custom logger
  protected static final org.slf4j.Logger logger = play.Logger.underlying();

  @Id
  @Column(name = "id_contribution", unique = true, nullable = false)
  private Long idContribution;

  @OneToMany(mappedBy = "text", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  private List<TextI18name> titles;

  // forcing updates from this object, deletions are handled at the contribution level
  @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
  @JoinColumn(name = "id_contribution", nullable = false, insertable = false, updatable = false)
  private Contribution contribution;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "language", nullable = true)
  private TLanguage language;

  @Column(name = "publication_date")
  private String publicationDate;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "text_type", nullable = true)
  private TTextType textType;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "source_name")
  @PrivateOwned
  private TextSourceName sourceName;

  @Column(name = "url", length = 2048)
  private String url;

  @Column(name = "fetched")
  private int fetched;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "visibility", nullable = false)
  private TTextVisibility visibility;

  @OneToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "id_first_argument", nullable = false)
  private ArgumentContext firstContextArgument;

  @OneToMany(mappedBy = "text", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @Unqueryable
  private List<TextContent> contents;

  @OneToMany(mappedBy = "text", fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
  private List<Excerpt> excerpts;

  /**
   * Get the "supertype" contribution object
   *
   * @return a contribution object
   */
  public Contribution getContribution() {
    return contribution;
  }

  /**
   * Set the "supertype" contribution object
   *
   * @param contribution a contribion
   */
  public void setContribution(Contribution contribution) {
    this.contribution = contribution;
  }

  /**
   * Get this text id
   *
   * @return an id
   */
  public Long getIdContribution() {
    return idContribution;
  }

  /**
   * Set this text id
   *
   * @param idContribution an id
   */
  public void setIdContribution(Long idContribution) {
    this.idContribution = idContribution;
  }

  /**
   * Get the list of titles for this text (names may have multiple spellings)
   *
   * @return a list of titles
   */
  public List<TextI18name> getTitles() {
    return titles;
  }

  /**
   * Set the list of titles for this text (names may have multiple spellings)
   *
   * @param titles a list of titles to set
   */
  public void setTitles(List<TextI18name> titles) {
    if (titles != null) {
      if (this.titles == null) {
        this.titles = new ArrayList<>();
      }

      // get previous languages for current names
      List<String> currentlangs = this.titles.stream().map(TextI18name::getLang).collect(Collectors.toList());

      // add/update new names
      titles.forEach(this::addTitle);

      currentlangs.stream().filter(lang -> titles.stream().noneMatch(n -> n.getLang().equals(lang))).forEach(lang ->
              this.titles.removeIf(current -> current.getLang().equals(lang))
      );
    }
  }

  /**
   * Add a title to this text, if such language already exists, will update existing title
   *
   * @param title a title structure
   */
  public void addTitle(TextI18name title) {
    if (titles == null) {
      titles = new ArrayList<>();
    }
    Optional<TextI18name> match = titles.stream().filter(n ->
        n.getLang().equals(title.getLang())).findAny();
    if (!match.isPresent()) {
      titles.add(title);
    }else{
      int i = titles.lastIndexOf(match.get());
      if(i >= 0){
        titles.get(i).setSpelling(title.getSpelling());
      }
    }
  }

  public String getOriginalTitle(){
    Optional<TextI18name> firstMatchedLanguage = titles.stream().filter(e -> language.getCode().equals(e.getLang())).findFirst();
    Optional<TextI18name> firstMatched = titles.stream().findFirst();
    String title = "";
    if(firstMatchedLanguage .isPresent()){
      title = firstMatchedLanguage.get().getSpelling();
    } else if(firstMatched.isPresent()){
      title = firstMatched.get().getSpelling();
    }
    return title;
  }

  /**
   * Get the contents associated to this text
   *
   * @return a (possibly empty) list of text content objects
   */
  public List<TextContent> getContents() {
    return contents != null ? contents : new ArrayList<>();
  }

  /**
   * Set the contents associated to this text
   *
   * @param contents a list of text content objects
   */
  public void setContents(List<TextContent> contents) {
    this.contents = contents;
  }

  /**
   * Add given content to this text. If text is private, a new content will be added for this contributor (if necessary),
   * otherwise, existing content will be replaced by given content.
   *
   * @param content a text content to bind to this text
   */
  public boolean addContent(TextContent content) {
    if (contents == null) {
      contents = new ArrayList<>();
    }

    // if this content already exists, ignore
    if (contents.stream().anyMatch(c -> c.getFilename().equals(content.getFilename()))) {
      return false;
    }

    // if this text is not private, force clear of all existing contents (ebean does not want to cascade here, dunno why)
    if (visibility.getIdVisibility() != ETextVisibility.PRIVATE.id()) {
      contents.clear();
      contents.add(content);
    } else {
      Optional<TextContent> toUpdate = contents.stream().filter(c ->
          c.getContributor() != null && c.getContributor().getIdContributor().equals(content.getContributor().getIdContributor())).findAny();
      if (toUpdate.isPresent()) {
        logger.debug("update " + toUpdate.get().toString());
        toUpdate.get().setFilename(content.getFilename());

      } else {
        logger.debug("create new " + content.toString());
        contents.add(content);
      }
    }
    return true;
  }

  /**
   * Get the language of this text
   *
   * @return a language
   */
  public TLanguage getLanguage() {
    return language;
  }

  /**
   * Set the language of this text
   *
   * @param language a language
   */
  public void setLanguage(TLanguage language) {
    this.language = language;
  }

  /**
   * Get the publication date, may be null
   *
   * @return the publication date as a string of the form YYYYMMDD, where MM and DD may be zeros and Y may be preceded by a "-"
   */
  public String getPublicationDate() {
    return publicationDate;
  }

  /**
   * Set the publication date
   *
   * @param publicationDate the publication date as a string of the form YYYYMMDD, where MM and DD may be zeros and Y may be preceded by a "-"
   */
  public void setPublicationDate(String publicationDate) {
    this.publicationDate = publicationDate;
  }

  /**
   * Get the text type
   *
   * @return this text's type
   */
  public TTextType getTextType() {
    return textType;
  }

  /**
   * Set the text type
   *
   * @param textType a type for this text
   */
  public void setTextType(TTextType textType) {
    this.textType = textType;
  }

  /**
   * Get the text visibility
   *
   * @return the visibility for this text
   */
  public TTextVisibility getVisibility() {
    return visibility;
  }

  /**
   * Set the text visibility
   *
   * @param visibility the visibility for this text
   */
  public void setVisibility(TTextVisibility visibility) {
    this.visibility = visibility;
  }

  /**
   * Get the external url where this text has been copied from
   *
   * @return an url, or null if this text does not come from an internet page
   */
  public String getUrl() {
    return url;
  }

  /**
   * Set the external url where this text has been copied from
   *
   * @param url an url if this text does not come from an internet page
   */
  public void setUrl(String url) {
    this.url = url;
  }

  /**
   * Get the source name (joint-object), if any
   *
   * @return a source name (joint-object), null if this text does not come from an identified source
   */
  public TextSourceName getSourceName() {
    return sourceName;
  }

  /**
   * Set the source name (joint-object), if any
   *
   * @param sourceName a source name (joint-object)
   */
  public void setSourceName(TextSourceName sourceName) {
    this.sourceName = sourceName;
  }

  /**
   * Get the first contextualized argument of this text.
   *
   * @return the first contextualized argument
   */
  public ArgumentContext getFirstContextArgument() {
    return firstContextArgument;
  }

  /**
   * Set the first contextualized argument of this text.
   *
   * @param firstContextArgument the debate first contextualized argument
   */
  public void setFirstContextArgument(ArgumentContext firstContextArgument) {
    this.firstContextArgument = firstContextArgument;
  }

  /**
   * Get the list excepts coming from this text
   *
   * @return a (possibly empty) list of except
   */
  public List<Excerpt> getExcerpts() {
    return excerpts != null ? excerpts : new ArrayList<>();
  }

  /**
   * Set the list excerpts coming from this text
   *
   * @param excerpts a list of excerpt
   */
  public void setExcerpts(List<Excerpt> excerpts) {
    this.excerpts = excerpts;
  }

  /**
   * Check whether this text has been automatically fetched from an external source (RSS feed client for example)
   *
   * @return true if this text has been automatically fetched
   */
  public boolean isFetched() {
    return fetched == 1;
  }

  /**
   * Set whether this text has been automatically fetched from an external source (RSS feed client for example)
   *
   * @param fetched true if this text has been automatically fetched
   */
  public void isFetched(boolean fetched) {
    this.fetched = fetched ? 1 : 0;
  }

  /*
   * CONVENIENCE METHODS
   */

  /**
   * Get the current version of this text
   *
   * @return a timestamp with the latest update moment of this text
   */
  public Timestamp getVersion() {
    return getContribution().getVersion();
  }

  @Override
  public String toString() {
    // must use getters and explicitly loop into references, otherwise ebean may send back deferred beanlist
    // (lazy load not triggered from toString methods)
    String title = (titles != null && !titles.isEmpty() ? titles.get(0).getSpelling() : "");
    return new StringBuffer("text [").append(getIdContribution())
        .append("] entitled ").append(title)
        .append(", actors: {").append(getContribution().getActors().stream()
            .map(ContributionHasActor::toString).collect(Collectors.joining(", "))).append("}")
        .append(", lang: ").append(getLanguage().getCode())
        .append(", published: ").append(getPublicationDate())
        .append(", type: ").append(getTextType().getIdType())
        .append(", source: ").append(getSourceName() != null ? getSourceName().toString() : "none")
        .append(", url: ").append(getUrl())
        .append(", visibility: ").append(getVisibility().getIdVisibility())
        .append(", contents: ").append(getContents().stream().map(TextContent::toString).collect(Collectors.joining(", ")))
        .append(", groups: ").append(getContribution().getGroups().stream()
            .map(g -> String.valueOf(g.getIdGroup())).collect(Collectors.joining(",")))
        .append(" [version:").append(getContribution().getVersion()).append("]").toString();
  }

  /*
   * QUERIES
   */

  /**
   * Find a text by its id
   *
   * @param id a text (contribution) id
   * @return the Text if it has been found, null otherwise
   */
  public static Text findById(Long id) {
    return id == null || id == -1L ? null : find.byId(id);
  }

  /**
   * Find a list of texts by a partial title
   *
   * @param title a partial title
   * @return the list of Text matching the given partial title
   */
  public static List<Text> findByPartialTitle(String title) {
    List<Text> result = null;
    if (title != null) {
      // will be automatically ordered by relevance
      String token = /* "\"" + */ title.trim()
          // protect single quotes
          .replace("'", "\\'")
          // add '%' for spaces
          .replace(" ", "%");

      String select = "select distinct text.id_contribution from text right join text_i18names " +
          "on text.id_contribution = text_i18names.id_contribution where spelling like '" + token + "'";

      logger.debug("search for text : " + select);
      result = Ebean.find(Text.class).setRawSql(RawSqlBuilder.parse(select).create()).findList();
    }
    return result != null ? result : new ArrayList<>();
  }

  /**
   * Find a text by its url
   *
   * @param url a url to look for
   * @return the Text matched or null
   */
  public static Text findByUrl(String url) {
    if(url != null && !url.equals("")) {
      String select = "select id_contribution from text where url like \"%" + url.trim() + "\"";
      List<Text> result = Ebean.find(Text.class).setRawSql(RawSqlBuilder.parse(select).create()).findList();
      return result != null && !result.isEmpty() ? result.get(0) : null;
    }

    return null;
  }

  /**
   * Get a randomly chosen publicly visible text from the database
   *
   * @return a random Text
   */
  public static Text random() {
    List<String> visibleGroups = Group.getVisibleGroupsFor(Group.getPublicGroup().getIdGroup());
    int type = EContributionType.TEXT.id();
    String sql = "select contribution.id_contribution from contribution right join contribution_in_group " +
        "on contribution_in_group.id_contribution = contribution.id_contribution where contribution_type = " + type +
        " and id_group in (" + String.join(",", visibleGroups) + ") order by rand() limit 1";
    return findById(Ebean.createSqlQuery(sql).findUnique().getLong("id_contribution"));
  }

  /*
   * PRIVATE HELPERS
   */

  /**
   * Get the number of excerpts linked to this text
   *
   * @param contributorId the id of the contributor for which we need that stats
   * @param groupId the group where see the stats
   * @return the number of excerpts linked to this text
   */
  public int getNbExcerpts(Long contributorId, int groupId){
    String select = "SELECT count(distinct e.id_contribution) as 'count' FROM excerpt e " +
            getContributionStatsJoins("e.id_contribution", contributorId) +
            " where e.id_text = " + idContribution + getContributionStatsWhereClause(contributorId, groupId);
    return Ebean.createSqlQuery(select).findUnique().getInteger("count");
  }

  /**
   * Get the number of arguments linked to this text
   *
   * @param contributorId the id of the contributor for which we need that stats
   * @param groupId the group where see the stats
   * @return the number of arguments linked to this text
   */
  public int getNbArguments(Long contributorId, int groupId){
    String select = "SELECT count(distinct a.id_contribution) as 'count' FROM argument_context a " +
            getContributionStatsJoins("a.id_contribution", contributorId) +
            " where a.id_context = " + idContribution + getContributionStatsWhereClause(contributorId, groupId);
    int nbArguments = Ebean.createSqlQuery(select).findUnique().getInteger("count");
    return nbArguments > 0 ? nbArguments - 1 : 0;
  }
}
