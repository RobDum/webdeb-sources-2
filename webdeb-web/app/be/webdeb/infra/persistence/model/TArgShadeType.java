/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.model;

import be.webdeb.core.api.argument.EArgumentShade;
import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CacheBeanTuning;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


/**
 * The persistent class for the t_arg_shade_type database table holding available argument shades. Shades are
 * specific to an argument type and timing.
 *
 * @author Fabian Gilson
 * @see be.webdeb.core.api.argument.EArgumentType
 */
@Entity
@Table(name = "t_arg_shade_type")
@CacheBeanTuning
public class TArgShadeType extends TechnicalTable {

  /**
   * Finder to access predefined values
   */
  public static final Model.Finder<Integer, TArgShadeType> find = new Model.Finder<>(TArgShadeType.class);

  @Id
  @Column(name = "id_shade", unique = true, nullable = false)
  private int idShade;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "id_argtype", referencedColumnName = "id_argtype", nullable = false, insertable = false, updatable = false)
  private TArgumentType argumentType;

  @OneToMany(mappedBy = "shade", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  private List<Argument> arguments;

  /**
   * Get the shade id
   *
   * @return an argument shade id
   */
  public int getIdShade() {
    return idShade;
  }

  /**
   * Set the shade id
   *
   * @param idShade an argument shade id
   */
  public void setIdShade(int idShade) {
    this.idShade = idShade;
  }

  /**
   * Get the argument type to which this shade belongs to
   *
   * @return an argument type
   */
  public TArgumentType getArgumentType() {
    return argumentType;
  }

  /**
   * Set the argument type to which this shade belongs to
   *
   * @param argumentType an argument type
   */
  public void setArgumentType(TArgumentType argumentType) {
    this.argumentType = argumentType;
  }

  /**
   * Get the list of arguments of this shade
   *
   * @return a (possibly empty) list of arguments
   */
  public List<Argument> getArguments() {
    return arguments != null ? arguments : new ArrayList<>();
  }

  /**
   * Set the list of arguments of this shade
   *
   * @param arguments a list of arguments
   */
  public void setArguments(List<Argument> arguments) {
    this.arguments = arguments;
  }

  /**
   * Get the EArgumentShade corresponding to this db shade link
   *
   * @return the EArgumentShade corresponding
   */
  public EArgumentShade getEArgumentShade(){
    return EArgumentShade.value(idShade);
  }
}
