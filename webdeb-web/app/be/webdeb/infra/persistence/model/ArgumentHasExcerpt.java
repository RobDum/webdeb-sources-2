/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.model;

import be.webdeb.core.api.argument.EArgumentLinkShade;
import com.avaje.ebean.*;
import com.avaje.ebean.annotation.CacheBeanTuning;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the argument_has_excerpt database table. Holds an illustration link between
 * an argument in a context and an excerpt, as well as the type of link between them and the validation state.
 *
 * @author Martin Rouffiange
 * @see be.webdeb.core.api.argument.EArgumentLinkShade
 */
@Entity
@CacheBeanTuning
@Table(name = "argument_context_has_excerpt")
public class ArgumentHasExcerpt extends Model {

    protected static final org.slf4j.Logger logger = play.Logger.underlying();
    private static final Model.Finder<Long, ArgumentHasExcerpt> find = new Model.Finder<>(ArgumentHasExcerpt.class);

    @Id
    @Column(name = "id_contribution", unique = true, nullable = false)
    private Long idContribution;

    // forcing updates from this object, deletions are handled at the contribution level
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
    @JoinColumn(name = "id_contribution", nullable = false, insertable = false, updatable = false)
    private Contribution contribution;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_argument_context", nullable = false)
    private ArgumentContext argument;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_excerpt", nullable = false)
    private Excerpt excerpt;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "shade", nullable = false)
    private TLinkShadeType shade;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "validated", nullable = false)
    private TValidationState validation;

    /*
     * GETTERS / SETTERS
     */

    /**
     * Get the justification link id
     *
     * @return an id
     */
    public Long getIdContribution() {
        return idContribution;
    }

    /**
     * Set the justification link id
     *
     * @param idContribution an id
     */
    public void setIdContribution(Long idContribution) {
        this.idContribution = idContribution;
    }

    /**
     * Get "supertype" contribution object
     *
     * @return a contribution
     */
    public Contribution getContribution() {
        return contribution;
    }

    /**
     * Set "supertype" contribution object
     *
     * @param contribution a contribution
     */
    public void setContribution(Contribution contribution) {
        this.contribution = contribution;
    }

    /**
     * Get the justification link shade
     *
     * @return a link shade
     */
    public TLinkShadeType getShade() {
        return shade;
    }

    /**
     * Set the justification link shade
     *
     * @param shade a link shade
     */
    public void setShade(TLinkShadeType shade) {
        this.shade = shade;
    }

    /**
     * Set the justification link shade from EArgumentLinkShade
     *
     * @param shade a EArgumentLinkShade
     */
    public void setShade(EArgumentLinkShade shade) {
        this.shade = TLinkShadeType.find.byId(shade.id());
    }

    /**
     * Get the contextualized argument of this illustration link
     *
     * @return a contextualized argument
     */
    public ArgumentContext getArgument() {
        return argument;
    }

    /**
     * Set the contextualized argument of this illustration link
     *
     * @param argument a contextualized argument
     */
    public void setArgument(ArgumentContext argument) {
        this.argument = argument;
    }

    /**
     * Get the excerpt of this illustration link
     *
     * @return an excerpt
     */
    public Excerpt getExcerpt() {
        return excerpt;
    }

    /**
     * Set the excerpt of this illustration link
     *
     * @param excerpt an excerpt
     */
    public void setExcerpt(Excerpt excerpt) {
        this.excerpt = excerpt;
    }

    /**
     * Get the validation state of this justification link
     *
     * @return the validation state
     */
    public TValidationState getValidationState() {
        return validation;
    }

    /**
     * Set the validation state of this justification link
     *
     * @param validation a validation state
     */
    public void setValidationState(TValidationState validation) {
        this.validation = validation;
    }

    /*
     * CONVENIENCE METHODS
     */

    /**
     * Get the current version of this argument link
     *
     * @return a timestamp with the latest update moment of this argument link
     */
    public Timestamp getVersion() {
        return getContribution().getVersion();
    }

    @Override
    public String toString() {
        // because of lazy load, must explicitly call getter
        return new StringBuffer("illustration link [").append(getIdContribution()).append("] between ")
                .append(getArgument().getContribution().getIdContribution()).append(" and ")
                .append(getExcerpt().getContribution().getIdContribution())
                .append(", shade: [").append(getShade().getIdLinkShade()).append("] ").append(getShade().getEn())
                .append(", validation: ").append(getValidationState().getEn())
                .append(" [version:").append(getContribution().getVersion()).append("]").toString();
    }

    /*
     * QUERIES
     */

    /**
     * Retrieve an illustration link by its id
     *
     * @param id the illustration link id
     * @return the illustration link corresponding to that id, null otherwise
     */
    public static ArgumentHasExcerpt findById(Long id) {
        return id == null || id == -1L ? null : find.byId(id);
    }

    /**
     * Retrieve an illustration link by argument and excerpt.
     *
     * @param argument the argument id
     * @param excerpt the excerpt id
     * @return the corresponding illustration link, null otherwise
     */
    public static ArgumentHasExcerpt findByArgumentAndExcerpt(Long argument, Long excerpt) {
        return find.where().eq("id_argument_context", argument).eq("id_excerpt", excerpt).findUnique();
    }

    /**
     * Get the list of similar excerpts with a given excerpt. Similars excerpt are excerpts linked with all contextualized
     * arguments that are linked with the given one.
     *
     * @param id the excerpt id
     * @return the list of similar excerpts of this one
     */
    public static List<ArgumentHasExcerpt> getSimilars(Long id) {
        String select = "select distinct ache2.id_contribution from excerpt e" +
                " left join argument_context_has_excerpt ache on ache.id_excerpt = " + id +
                " left join argument_context_has_excerpt ache2 on ache2.id_argument_context = ache.id_argument_context" +
                " where e.id_contribution in (ache2.id_excerpt)";

        List<ArgumentHasExcerpt> result = Ebean.find(ArgumentHasExcerpt.class).setRawSql(RawSqlBuilder.parse(select).create()).findList();
        return result != null && !result.isEmpty() ? result : new ArrayList<>();
    }

    /**
     * Get all illustrations linked with the given argument
     *
     * @param id a context argument id
     * @return a list of argumentHasExcerpt
     */
    public static List<ArgumentHasExcerpt> getIllustrations(Long id) {
        return find.where().eq("id_argument_context", id).findList();
    }

}
