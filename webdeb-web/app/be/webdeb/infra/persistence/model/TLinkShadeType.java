/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.model;

import be.webdeb.core.api.argument.EArgumentLinkShade;
import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CacheBeanTuning;

import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the t_link_shade_type database table, used to specify the semantics of a link between
 * two arguments (similarity), two contextualized arguments (justification) or a contextualized argument and a excerpt (illustration).
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 * @see be.webdeb.core.api.argument.EArgumentLinkShade
 */
@Entity
@CacheBeanTuning
@Table(name = "t_link_shade_type")
public class TLinkShadeType extends TechnicalTable {

  /**
   * Finder to access predefined values
   */
  public static final Model.Finder<Integer, TLinkShadeType> find = new Model.Finder<>(TLinkShadeType.class);

  @Id
  @Column(name = "id_link_shade")
  private int idLinkShade;

  // "super type" link type
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "id_linktype", nullable = false, insertable = false, updatable = false)
  private TArgumentLinktype argumentLinktype;

  @OneToMany(mappedBy = "shade", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  private List<ArgumentJustification> justificationLinks;

  @OneToMany(mappedBy = "shade", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  private List<ArgumentSimilarity> similarityLinks;

  @OneToMany(mappedBy = "shade", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  private List<ArgumentHasExcerpt> illustrationLinks;

  /**
   * Get the link shade id
   *
   * @return an id
   */
  public int getIdLinkShade() {
    return idLinkShade;
  }

  /**
   * Set the link shade id
   *
   * @param idLinkShade an id
   */
  public void setIdLinkShade(int idLinkShade) {
    this.idLinkShade = idLinkShade;
  }

  /**
   * Get the parent link type
   *
   * @return a lintype object
   */
  public TArgumentLinktype getArgumentLinktype() {
    return this.argumentLinktype;
  }

  /**
   * Set the parent link type
   *
   * @param argumentLinktype  a lintype object
   */
  public void setArgumentLinktype(TArgumentLinktype argumentLinktype) {
    this.argumentLinktype = argumentLinktype;
  }

  /**
   * Get the list of argument justification links with this shade
   *
   * @return the list of justification links
   */
  public List<ArgumentJustification> getJustificationLinks() {
    return justificationLinks;
  }

  /**
   * Set the list of argument justification links with this shade
   *
   * @param links the list of justification links
   */
  public void setJustificationLinks(List<ArgumentJustification> links) {
    this.justificationLinks = links;
  }

  /**
   * Get the list of argument justification links with this shade
   *
   * @return the list of justification links
   */
  public List<ArgumentSimilarity> getSimilarityLinks() {
    return similarityLinks;
  }

  /**
   * Set the list of argument justification links with this shade
   *
   * @param links the list of justification links
   */
  public void setSimilarityLinks(List<ArgumentSimilarity> links) {
    this.similarityLinks = links;
  }

  /**
   * Get the list of argument justification links with this shade
   *
   * @return the list of justification links
   */
  public List<ArgumentHasExcerpt> getIllustrationLinks() {
    return illustrationLinks;
  }

  /**
   * Set the list of argument justification links with this shade
   *
   * @param links the list of justification links
   */
  public void setIllustrationLinks(List<ArgumentHasExcerpt> links) {
    this.illustrationLinks = links;
  }

  /**
   * Get the EArgumentLinkShade corresponding to this db shade link
   *
   * @return the EArgumentLinkShade corresponding
   */
  public EArgumentLinkShade getELinkShade(){
    return EArgumentLinkShade.value(idLinkShade);
  }

  /*
   * QUERIES
   */

  /**
   * Retrieve a linkby its id
   *
   * @param id an id
   * @return the link corresponding to the given id, null if not found
   */
  public static TLinkShadeType findById(Integer id) {
    return id == null || id == -1L ? null : find.byId(id);
  }

}
