/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.model;

import be.webdeb.infra.persistence.model.annotation.Unqueryable;
import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CacheBeanTuning;

import javax.persistence.*;
import java.util.*;


/**
 * The persistent class for the context_element database table. Holds string element to add description on element like
 * citation.
 *
 * @author Martin Rouffiange
 */
@Entity
@CacheBeanTuning
@Table(name = "context_element")
@Unqueryable
public class ContextElement extends Model {

    /**
     * Finder to access predefined values
     */
    public static final Model.Finder<Long, ContextElement> find = new Model.Finder<>(ContextElement.class);

    @Id
    @Column(name = "id_context_element", unique = true, nullable = false)
    private Long idElement;

    @ManyToOne
    @JoinColumn(name = "id_contribution")
    private Contribution contribution;

    @OneToMany(mappedBy = "element", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<ContextElementI18name> spellings;


    public ContextElement(){
        this.idElement = 0L;
    }

    public ContextElement(Long id, Contribution contribution, Map<String, String> spellings){
        this.idElement = id;
        this.contribution = contribution;
        setSpellings(spellings);
    }

    /**
     * Get the element id
     *
     * @return the element id
     */
    public Long getId() {
        return idElement;
    }

    /**
     * Get the linked contribution to this element if any
     *
     * @return a contribution
     */
    public Contribution getContribution() {
        return contribution;
    }

    /**
     * Set the linked contribution to this element if any
     *
     * @param contribution a contribution
     */
    public void setContribution(Contribution contribution) {
        this.contribution = contribution;
    }

    /**
     * Get all spellings for this place
     *
     * @return a list of spellings as PlaceI18names
     */
    public List<ContextElementI18name> getSpellings() {
        return spellings;
    }

    /**
     * Set all spellings for this element
     *
     * @param spellings a list of spellings
     */
    public void setSpellings(Map<String, String> spellings) {
        this.spellings = new ArrayList<>();
        for(Map.Entry<String, String> spelling : spellings.entrySet()) {
            this.spellings.add(new ContextElementI18name(idElement, spelling.getKey(), spelling.getValue()));
        }
    }

    /*
     * QUERIES
     */

    /**
     * Retrieve a context element by its id
     *
     * @param id an id
     * @return the context element corresponding to the given id, null if not found
     */
    public static ContextElement findById(Long id) {
        return id == null || id == -1L ? null : find.byId(id);
    }
}
