/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.model;

import be.webdeb.infra.persistence.model.annotation.Unqueryable;
import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CacheBeanTuning;

import javax.persistence.*;


/**
 * The persistent class for the t_warned_word database table, holding predefined values for warned words in different case.
 *
 * @author Martin Rouffiange
 */
@Entity
@Table(name = "t_warned_word")
@CacheBeanTuning
@Unqueryable
public class TWarnedWord extends TechnicalTable {

  /**
   * Finder to access predefined values
   */
  public static final Model.Finder<Integer, TWarnedWord> find = new Model.Finder<>(TWarnedWord.class);

  @Id
  @Column(name = "id_warned_word", unique = true, nullable = false)
  private int idWarnedWord;

  @Column(name = "type")
  private int type;

  @ManyToOne(targetEntity = TWarnedWordType.class, fetch = FetchType.EAGER)
  @JoinColumn(name = "warned_word_type", nullable = false)
  private TWarnedWordType warnedWordType;

  /**
   * Get this banned word id
   *
   * @return an id
   */
  public int getIdWarnedWord() {
    return idWarnedWord;
  }

  /**
   * Set this banned word id
   *
   * @param idBannedWord an id
   */
  public void setIdWarnedWord(int idBannedWord) {
    this.idWarnedWord = idWarnedWord;
  }

  /**
   * Get the word type (begin of sentence, ...)
   *
   * @return the type
   */
  public int getType() {
    return type;
  }

  /**
   * Set the word type (begin of sentence, ...)
   *
   * @param type the type
   */
  public void setType(int type) {
    this.type = type;
  }

  /**
   * Get the word type context. It means where the word need a warning
   *
   * @return the context type
   */
  public TWarnedWordType getContextType() {
    return  warnedWordType;
  }

  /**
   * Set the word type context. It means where the word need a warning
   *
   * @param contextType the context type
   */
  public void setContextType(TWarnedWordType contextType) {
    this.warnedWordType = contextType;
  }
}
