/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.model;

import be.webdeb.infra.persistence.model.annotation.Unqueryable;
import com.avaje.ebean.*;
import com.avaje.ebean.annotation.CacheBeanTuning;

import javax.persistence.*;


/**
 * The persistent class for temporary contributions authors from external services (like browser extension) that will be added
 * manually by contributor.
 *
 * Used as temporary repository for an easiest communication between services and the platform.
 *
 * @author Martin Rouffiange
 */
@Entity
@CacheBeanTuning
@Table(name = "external_author")
@Unqueryable
public class ExternalAuthor extends Model {

    protected static final org.slf4j.Logger logger = play.Logger.underlying();
    private static final Model.Finder<Long, ExternalAuthor> find = new Model.Finder<>(ExternalAuthor.class);

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_external_author", unique = true, nullable = false)
    private Long idExternalAuthor;

    @Column(name = "name")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_external_contribution", nullable = false)
    private ExternalContribution externalContribution;

    @Column(name = "id_internal_contribution")
    private Long internalAuthorId;

    /*
     * GETTERS AND SETTERS
     */

    /**
     * Get the id of this external author
     *
     * @return an external author id
     */
    public Long getExternalAuthorId() {
        return idExternalAuthor;
    }

    /**
     * Set the id of this external author
     *
     * @param idExternalAuthor an external author id
     */
    public void setExternalAuthorId(Long idExternalAuthor) {
        this.idExternalAuthor = idExternalAuthor;
    }

    /**
     * Get the name of the external author
     *
     * @return an author name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name of the external author
     *
     * @param name an author name
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * Get the external contribution from which this author is linked
     *
     * @return a external contribution
     */
    public ExternalContribution getExternalContribution() {
        return externalContribution;
    }

    /**
     * Set the external contribution from which this author is linked
     *
     * @param externalContribution the externalContribution
     */
    public void setExternalContribution(ExternalContribution externalContribution) {
        this.externalContribution = externalContribution;
    }

    /**
     * Get the internal author id
     *
     * @return a author id
     */
    public Long getInternalContributionId() {
        return internalAuthorId;
    }

    /**
     * Set the internal author id
     *
     * @param internalAuthorId a author id
     */
    public void setInternalContributionId(Long internalAuthorId) {
        this.internalAuthorId = internalAuthorId;
    }

    @Override
    public String toString() {
        // must use getters and explicitly loop into references, otherwise ebean may send back deferred beanlist
        // (lazy load not triggered from toString methods)
        return new StringBuffer("external author [").append(name).append(", with id: ").append(idExternalAuthor).toString();
    }

    /*
     * QUERIES
     */

    /**
     * Retrieve an external author by its id
     *
     * @param id an id
     * @return the external author corresponding to the given id, null if not found
     */
    public static ExternalAuthor findById(Long id) {
        return id == null || id == -1L ? null : find.byId(id);
    }

}
