/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.ws.external;

import be.webdeb.core.api.actor.ActorFactory;
import be.webdeb.core.api.actor.ExternalAuthor;
import be.webdeb.core.api.contribution.EExternalSource;
import be.webdeb.core.api.contribution.ExternalContribution;
import be.webdeb.core.api.contribution.Language;
import be.webdeb.core.api.excerpt.ExcerptFactory;
import be.webdeb.core.api.text.TextFactory;
import be.webdeb.core.exception.FormatException;
import be.webdeb.infra.ws.external.auth.AuthForm;
import be.webdeb.util.ValuesHelper;
import play.api.Play;

import javax.inject.Inject;
import java.util.List;

/**
 * This form is used to request from external service that need a token to authenticate
 *
 * @author Martin Rouffiange
 */
public abstract class ExternalForm {

    @Inject
    protected ValuesHelper values = Play.current().injector().instanceOf(ValuesHelper.class);

    @Inject
    protected ActorFactory actorFactory = Play.current().injector().instanceOf(ActorFactory.class);

    @Inject
    protected ExcerptFactory excerptFactory = Play.current().injector().instanceOf(ExcerptFactory.class);

    @Inject
    protected TextFactory textFactory = Play.current().injector().instanceOf(TextFactory.class);

    // custom logger
    protected static final org.slf4j.Logger logger = play.Logger.underlying();

    /**
     * The external contribution id
     */
    protected Long id;

    /**
     * The external contribution url
     */
    protected String url;

    /**
     * The external contribution language
     */
    protected String language;

    /**
     * The external contribution source name
     */
    protected String sourceName;

    /**
     * The external contribution publication date
     */
    protected String publicationDate;

    /**
     * External contribution authors
     */
    protected List<AuthorRequest> authors;

    /**
     * The user auth data
     */
    protected AuthForm user = new AuthForm();

    /**
     * Play / JSON compliant constructor
     */
    public ExternalForm() {

    }

    /**
     * Initialize an external contribution api from this request
     *
     * @param contribution an external contribution api
     */
    protected void initExternalContribution(ExternalContribution contribution){
        contribution.setSourceUrl(url);
        contribution.setPublicationDate(publicationDate);
        contribution.setSourceId(EExternalSource.valueOf(sourceName).id());
        // contribution language
        try {
            contribution.setLanguage(textFactory.getLanguage(language.trim()));
        } catch (FormatException e) {
            logger.error("unknown language code " + language, e);
        }

        if(authors != null) {
            for (AuthorRequest author : authors) {
                ExternalAuthor a = actorFactory.getExternalAuthor();
                //a.setId(author.getId());
                a.setId(author.getId());
                a.setName(author.getName());
                contribution.addAuthor(a);
            }
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public AuthForm getUser() {
        return user;
    }

    public void setUser(AuthForm user) {
        this.user = user;
    }

    public String getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(String publicationDate) {
        this.publicationDate = publicationDate;
    }

    public List<AuthorRequest> getAuthors() {
        return authors;
    }

    public void setAuthors(List<AuthorRequest> authors) {
        this.authors = authors;
    }

    @Override
    public String toString() {
        return "ExternalForm{" +
                "id='" + id + '\'' +
                ", url='" + url + '\'' +
                ", language='" + language + '\'' +
                ", sourceName='" + sourceName + '\'' +
                ", publicationDate='" + publicationDate + '\'' +
                ", authors=" + authors +
                '}';
    }
}