/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */
package be.webdeb.infra.ws.external.excerpt;

import be.webdeb.core.api.excerpt.ExternalExcerpt;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.infra.ws.external.VizExternalContributionResponse;
import be.webdeb.infra.ws.external.text.VizExternalTextResponse;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class VizExternalArgumentResponse extends VizExternalContributionResponse {

    /**
     * The original excerpt
     */
    @JsonSerialize
    protected String originalExcerpt;

    /**
     * The text linked to this argument
     */
    @JsonSerialize
    protected VizExternalTextResponse text;

    /**
     * Default constructor
     *
     * @param excerpt the external excerpt to send
     * @param contributor the contributor id that ask for the data
     * @param lang the contributor language
     */
    public VizExternalArgumentResponse(ExternalExcerpt excerpt, Long contributor, String lang){
        super(excerpt.getId(), excerpt.getSourceUrl(), excerpt.getLanguage().getCode(), EContributionType.EXTERNAL_EXCERPT.id(), excerpt.getInternalContribution(), excerpt.getVersionAsString());
        originalExcerpt = excerpt.getOriginalExcerpt();
        text = new VizExternalTextResponse(excerpt.getExternalText(), contributor, lang);
    }

}
