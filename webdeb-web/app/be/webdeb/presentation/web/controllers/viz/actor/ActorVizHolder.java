/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.viz.actor;

import be.webdeb.core.api.actor.*;
import be.webdeb.core.api.argument.Argument;
import be.webdeb.core.api.argument.ArgumentContext;
import be.webdeb.core.api.argument.ArgumentIllustration;
import be.webdeb.core.api.argument.EArgumentLinkShade;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.core.api.text.Text;
import be.webdeb.presentation.web.controllers.entry.ContributionHolder;
import be.webdeb.presentation.web.controllers.entry.actor.ActorHolder;
import be.webdeb.presentation.web.controllers.entry.argument.ArgumentContextHolder;
import be.webdeb.presentation.web.controllers.entry.excerpt.ExcerptHolder;
import be.webdeb.presentation.web.controllers.entry.text.TextHolder;
import be.webdeb.presentation.web.controllers.permission.WebdebUser;
import be.webdeb.presentation.web.controllers.viz.ETextsView;
import be.webdeb.presentation.web.controllers.viz.EVizPane;

import be.webdeb.presentation.web.controllers.entry.excerpt.ExcerptHolder;
import com.fasterxml.jackson.annotation.JsonIgnore;
import play.i18n.Lang;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


/**
 * This class holds actor values to build visualisation pages
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public class ActorVizHolder extends ActorHolder {

  private final Map<Integer, String> ages = new HashMap<>();

  private Map<ETextsView, Integer> textsMetrics;
  private Map<ETextsView, List<TextHolder>> texts;
  private Map<ETextsView, Map<Long, Integer>> ownExcerptMetrics;
  private Map<ETextsView, Map<Long, Integer>> totalExcerptMetrics;
  private Map<EActorType, Set<ActorHolder>> actorsAffiliatedMap = new LinkedHashMap<>();
  private Map<EActorType, Map<ActorHolder, List<AffiliationViz>>> affiliationsMap = new LinkedHashMap<>();
  private Map<EActorType, Map<ActorHolder, List<AffiliationViz>>> affiliatedMap = new LinkedHashMap<>();

  private Map<Contribution, ActorRole> contributions;

  private Map<Long, Excerpt> excerptsMap;
  private Map<Long, EArgumentLinkShade> excerptsShadeMap = new HashMap<>();

  // keep data to not load them twice
  // socio data
  private Map<EActorGroupKey, ActorSociography> socioData = null;

  /**
   * Default Constructor
   *
   * @param actor the actor to visualize
   * @param lang  2-char ISO 639-1 code of context language (among play accepted languages)
   */
  public ActorVizHolder(Actor actor, String lang) {
    super(actor, lang);
    this.actor = actor;

    // build categories for ages
    int[] rangeLimit = new int[]{15, 25, 35, 45, 55, 65};
    int low = 0;
    for (int range : rangeLimit) {
      for (int i = low; i < range; i++) {
        ages.put(i, i18n.get(Lang.forCode(lang), "viz.actor.socio.age." + range));
      }
      low = range;
    }

    actorsAffiliatedMap.put(EActorType.PERSON, new HashSet<>());
    actorsAffiliatedMap.put(EActorType.ORGANIZATION, new HashSet<>());
    actorsAffiliatedMap.put(EActorType.PROJECT, new HashSet<>());

    textsMetrics = new EnumMap<>(ETextsView.class);
    textsMetrics.put(ETextsView.AUTHOR, 0);
    textsMetrics.put(ETextsView.EXCERPT_AUTHOR, 0);
    textsMetrics.put(ETextsView.EXCERPT_CITED, 0);

    texts = new EnumMap<>(ETextsView.class);

    ownExcerptMetrics = createMetrics();
    totalExcerptMetrics = createMetrics();

    // initialize list of contributions for this actor
    contributions = actor.getContributions(EContributionType.ALL);
  }

  /**
   * Get all contributions for this actor
   *
   * @return the list of contributions where this actor appears
   */
  public List<ContributionHolder> getContributions(WebdebUser user) {
    return helper.toHolders(user.filterContributionList(contributions.keySet()), user, lang);
  }

  /**
   * Get all affiliations of this actor separate by actor type. Will regroup all affiliation instances under the same map key
   * (since an actor my have multiple affiliation to the same actor with different dates and/or functions)
   *
   * @param type the actor type for the affiliated
   * @return the map of affiliation actors with this actor affiliation dates on a map with actor type has key
   */
  public Map<ActorHolder, List<AffiliationViz>> getAllAffiliationByActorType(EActorType type) {
    if(!affiliationsMap.containsKey(type)){

      if(type == EActorType.UNKNOWN){
        affiliationsMap.put(EActorType.UNKNOWN, getAllAffByActorType(getAffiliations(EActorType.PERSON), EActorType.PERSON));
        affiliationsMap.get(EActorType.UNKNOWN).putAll(getAllAffByActorType(getAffiliations(EActorType.ORGANIZATION), EActorType.ORGANIZATION));
      }else{
        affiliationsMap.put(type, getAllAffByActorType(getAffiliations(type), type));
      }
    }
    return affiliationsMap.get(type);
  }

  /**
   * Get all actors affiliated to this actor separate by actor type. Will regroup all affiliation instances under the same map key
   * (since an actor my have multiple affiliation to the same actor with different dates and/or functions)
   * Affiliated actors will be sorted on their last affiliation date, from the latest to the earliest,
   * then alphabetically on their names
   *
   * @param type the actor type for the affiliated
   * @return the map of affiliated actors with their affiliation dates on a map with actor type has key
   */
  public Map<ActorHolder, List<AffiliationViz>> getAllAffiliatedByActorType(EActorType type) {
    if(!affiliatedMap.containsKey(type)){
      affiliatedMap.put(type, getAllAffByActorType(getAffiliated(type), type));
    }
    return affiliatedMap.get(type);
  }

  /**
   * Get all affiliations and affiliated actors of this actor separate by actor type.
   *
   * @param type the actor type for the affiliation and affiliated
   * @return the map of affiliation and affiliated actors with this actor affiliation dates on a map with actor type has key
   */
  public Map<ActorHolder, List<AffiliationViz>> getAllAffByActorType(EActorType type) {
    Map<ActorHolder, List<AffiliationViz>> map = new LinkedHashMap<>();
    map.putAll(getAllAffiliationByActorType(type));
    map.putAll(getAllAffiliatedByActorType(type == EActorType.UNKNOWN ? EActorType.ORGANIZATION : type));
    return map;
  }

  /**
   * Get all affiliations of this actor separate by actor type. Will regroup all affiliation instances under the same map key
   * (since an actor my have multiple affiliation to the same actor with different dates and/or functions)
   *
   * @param aff the map of affiliation actors with this actor affiliation dates on a map with actor type has key
   * @return the aff map by affiliation type
   */
  public Map<String, Map<String, Map<ActorHolder, List<AffiliationViz>>>> getAllAffByAffType(Map<ActorHolder, List<AffiliationViz>> aff) {
    Map<String, Map<String, Map<ActorHolder, List<AffiliationViz>>>> map = new LinkedHashMap<>();

    aff.forEach((k, v) -> v.forEach(e ->
            updateAffMap(map, k, e, e.getAffTypeNameOrSubstitude(), "")));

    return sortAffMap(map, true);
  }

  /**
   * Get all affiliations of this actor separate by actor type. Will regroup all affiliation instances under the same map key
   * (since an actor my have multiple affiliation to the same actor with different dates and/or functions)
   *
   * @param aff the map of affiliation actors with this actor affiliation dates on a map with actor type has key
   * @return the aff map by organization type
   */
  public Map<String, Map<String, Map<ActorHolder, List<AffiliationViz>>>> getAllAffByOrgType(Map<ActorHolder, List<AffiliationViz>> aff) {
    Map<String, Map<String, Map<ActorHolder, List<AffiliationViz>>>> map = new LinkedHashMap<>();

    aff.forEach((k, v) -> v.forEach(e ->
            updateAffMap(map, k, e, e.getOrgType(), "")));

    return sortAffMap(map, true);
  }

  /**
   * Get all affiliations of this actor separate by actor type. Will regroup all affiliation instances under the same map key
   * (since an actor my have multiple affiliation to the same actor with different dates and/or functions)
   *
   * @param aff the map of affiliation actors with this actor affiliation dates on a map with actor type has key
   * @return the aff map by function
   */
  public Map<String, Map<String, Map<ActorHolder, List<AffiliationViz>>>> getAllAffByFunction(Map<ActorHolder, List<AffiliationViz>> aff) {
    Map<String, Map<String, Map<ActorHolder, List<AffiliationViz>>>> map = new LinkedHashMap<>();

    aff.forEach((k, v) -> v.forEach(e ->
      updateAffMap(map, k, e, e.getGenericFunctionOrDefault(), e.getFunction())));

    return sortAffMap(map, true);
  }

  /**
   * Update the affiliation map with given affiliation
   *
   * @param affMap the affmap to sort
   * @param actor the actor linked to the aff
   * @param aff the affiliation to add
   * @param key1 the first key
   * @param key2 the second key
   */
  private void updateAffMap(Map<String, Map<String, Map<ActorHolder, List<AffiliationViz>>>> affMap, ActorHolder actor, AffiliationViz aff, String key1, String key2) {
    if (!values.isBlank(key1)) {

      if (!affMap.containsKey(key1)) {
        affMap.put(key1, new LinkedHashMap<>());
      }

      if (!affMap.get(key1).containsKey(key2)) {
        affMap.get(key1).put(key2, new LinkedHashMap<>());
      }

      if (!affMap.get(key1).get(key2).containsKey(actor)) {
        affMap.get(key1).get(key2).put(actor, new ArrayList<>());
      }

      affMap.get(key1).get(key2).get(actor).add(aff);
    }
  }

  /**
   * Sort the affiliation map
   *
   * @param affMap the affmap to sort
   * @param completeSort true if label must be sorted
   * @return the affmap sorted
   */
  private Map<String, Map<String, Map<ActorHolder, List<AffiliationViz>>>> sortAffMap(Map<String, Map<String, Map<ActorHolder, List<AffiliationViz>>>> affMap, boolean completeSort) {
    Map<String, Map<String, Map<ActorHolder, List<AffiliationViz>>>> sortedMap = new LinkedHashMap<>();

    List<String> genericList = new ArrayList<>(affMap.keySet());
    if(completeSort)
      Collections.sort(genericList);
    genericList.forEach(e -> sortedMap.put(e, new LinkedHashMap<>()));

    for (Map.Entry<String, Map<String, Map<ActorHolder, List<AffiliationViz>>>> entry : affMap.entrySet()) {
      List<String> functionList = new ArrayList<>(entry.getValue().keySet());
      if(completeSort)
        Collections.sort(functionList);
      functionList.forEach(e -> sortedMap.get(entry.getKey()).put(e, new LinkedHashMap<>()));

      for (Map.Entry<String, Map<ActorHolder, List<AffiliationViz>>> entry2 : entry.getValue().entrySet()) {
        List<ActorHolder> actorList = new ArrayList<>(entry2.getValue().keySet());
        Collections.sort(actorList);
        actorList.forEach(e -> sortedMap.get(entry.getKey()).get(entry2.getKey()).put(e, affMap.get(entry.getKey()).get(entry2.getKey()).get(e)));

        for (Map.Entry<ActorHolder, List<AffiliationViz>> entry3 : entry2.getValue().entrySet()) {
          Collections.sort(entry3.getValue());
        }
      }

    }

    return sortedMap;
  }

  /**
   * Get all actors aff (affiliations or affiliated)
   *
   * @param affiliations the affiliations to map
   * @param type type the related actor type
   * @return the map of aff actors with their affiliation dates on a map with actor type has key
   */
  private Map<ActorHolder, List<AffiliationViz>> getAllAffByActorType(List<Affiliation> affiliations, EActorType type) {
    return toAffiliationsMap(affiliations.stream().filter(a -> a.getActor() != null).collect(Collectors.toList()), type);
  }

  /**
   * Build the sociography view data (allies/opponents) for a given key, i.e., a list of nodes (whose semantics depends on given groupKey)
   * where all excerpts having a similarity link to an excerpt stated by this actor are grouped following
   * given groupKey. The nodes defines aggregated similarity statistics regarding this actor.
   * <p>
   * Sociography nodes are grouped regarding the given groupKey (eg. individual actors, functions, countries, etc)
   *
   * @param user     the WebDeb user
   * @return a collection of ActorSociographyNode containing labels and statistics
   */
  public Map<EActorGroupKey, ActorSociography> viewSociography(WebdebUser user) {
    if(socioData == null){
      //EActorGroupKey groupKey
      Map<EActorGroupKey, Map<String, ActorSociographyNode>> result = new LinkedHashMap<>();
      EActorGroupKey.getAll().forEach(e -> result.put(e, new LinkedHashMap<>()));
      List<ExcerptHolder> excs = new ArrayList<>();
      initExcerptWhereAuthor(user);

      // for all these excerpts, get all similar excerpts if not already in list
      LocalDate now = LocalDate.now();
      excerptsMap.forEach((excId, exc) -> {
        EArgumentLinkShade shade = excerptsShadeMap.get(excId);
        List<ActorRole> thinkers = exc.getThinkerActors().stream().filter(e -> !e.getActor().getId().equals(id)).collect(Collectors.toList());

        if(!thinkers.isEmpty()) {
          // add excerpt to excerpt list holder for filtered
          excs.add(new ExcerptHolder(exc, lang));
          // create sociography node for all authors (depending on actual view key)
          // O^3, but last loop iterates on very few elements, probably much faster than twice O2
          thinkers.forEach(role -> EActorGroupKey.getAll().forEach(groupKey -> {
            String label = null;
            // now switch on viewKey
            switch (groupKey) {
              case AUTHOR:
                updateNodeMap(result, groupKey, role.getActor(), shade);
                break;
              case AGE:
                // only use persons
                if (role.getActor().getActorType().equals(EActorType.PERSON)) {
                  Person p = (Person) role.getActor();
                  if (p.getBirthdate() != null) {
                    LocalDate birth = values.toDate(p.getBirthdate()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                    String temp = ages.get(Period.between(birth, now).getYears());
                    label = temp != null ? temp : i18n.get(Lang.forCode(lang), "viz.actor.socio.age.65plus");
                  }
                  updateNodeMap(result, groupKey, label, shade);
                }
                break;
              case COUNTRY:
                switch (role.getActor().getActorType()) {
                  case PERSON:
                    Person p = (Person) role.getActor();
                    if (p.getResidence() != null) {
                      label = p.getResidence().getName(lang);
                    }
                    break;
                  case ORGANIZATION:
                    Organization o = (Organization) role.getActor();
                    if (o.getPlaces() != null && !o.getPlaces().isEmpty() && o.getPlaces().get(0).getCountry() != null) {
                      label = o.getPlaces().get(0).getCountry().getName(lang);
                    }
                    break;
                  default:
                    // do nothing
                }
                updateNodeMap(result, groupKey, label, shade);
                break;
              case FUNCTION:
                if (role.getAffiliation() != null && role.getAffiliation().getFunction() != null) {
                  // use substitute, if any
                  Profession substitute = role.getAffiliation().getFunction().getSubstitute();
                  label = substitute != null ? substitute.getName(lang) : role.getAffiliation().getFunction().getName(lang);
                }
                updateNodeMap(result, groupKey, label, shade);
                break;
              case ORGANIZATION:
                if (role.getAffiliation() != null && role.getAffiliation().getActor() != null) {
                  updateNodeMap(result, groupKey, role.getAffiliation().getActor(), shade);
                } else {
                  updateNodeMap(result, groupKey, i18n.get(Lang.forCode(lang), "viz.actor.socio.org.unknown"), shade);
                }
                break;
              default:
                logger.warn("unknown key " + groupKey.name());
            }
          }));
        }
      });

      socioData = new LinkedHashMap<>();
      result.forEach((k, v) -> {
        ArrayList<ActorSociographyNode> nodes = new ArrayList<>(v.values());
        nodes.sort(new ActorSocioComparator());
        socioData.put(k, new ActorSociography(excs, nodes));
      });
    }

    return socioData;
  }

  /**
   * Get all texts where this actor appears.
   *
   * @param user the current user (to filter viewable elements)
   * @return a list of texts where this actor plays the role as described above
   */
  public synchronized List<TextHolder> getAllTexts(WebdebUser user) {
    List<TextHolder> texts = new ArrayList<>();
    texts.addAll(getTexts(ETextsView.EXCERPT_AUTHOR, user));
    texts.addAll(getTexts(ETextsView.EXCERPT_CITED, user));
    texts.addAll(getTexts(ETextsView.AUTHOR, user));
    return texts;
  }

  /**
   * Get the list of texts where this actor appears. Concrete role of actor in text depend on given ETextsView, ie
   * <ul>
   * <li>ETextsView.AUTHOR for all texts where this actor is an author of these texts</li>
   * <li>ETextsView.EXCERPT_AUTHOR for all texts where this actor is an author of an excerpt extracted from these texts</li>
   * <li>ETextsView.EXCERPT_CITED for all texts where this actor is cited in the excerpt extracted from these texts</li>
   * </ul>
   *
   * @param view the current view to get the list of texts for
   * @param user the current user (to filter viewable elements)
   * @return a list of texts where this actor plays the role as described above
   */
  public List<TextHolder> getTexts(ETextsView view, WebdebUser user) {
    //ETextsView view
    if (texts.get(view) == null) {
      Map<Long, Text> working = new TreeMap<>();
      switch (view) {
        case AUTHOR:
          getTextWhereAuthor(user).values().forEach(t -> updateTextMap(working, t, true, view, user));
          break;
        case EXCERPT_AUTHOR:
          actor.getTextsWhereExcerptAuthor().stream().filter(user::mayView).
                  forEach(t -> updateTextMap(working, t, true, view, user));
          break;
        case EXCERPT_CITED:
          actor.getTextsWhereExcerptCited().stream().filter(user::mayView).
                  forEach(t -> updateTextMap(working, t, false, view, user));
          break;
        default:
          // do nothing (should be unreachable)
      }
      textsMetrics.put(view, working.size());
      texts.put(view, working.values().stream().map(t ->
              new TextHolder(t, user.getId(), lang)).collect(Collectors.toList()));

    }
    return texts.get(view);
  }

  private void updateTextMap( Map<Long, Text> working, Text text, boolean whereAuthor, ETextsView view, WebdebUser user){
    working.put(text.getId(), text);
    ownExcerptMetrics.get(view).put(text.getId(), actor.getNbExcerptsCount(text.getId(), whereAuthor, user.getId(), user.getGroupId()));
    // count the amount of excerpts this actor authored
    totalExcerptMetrics.get(view).put(text.getId(), text.getCountRelationsMap(user.getId(), user.getGroupId())
            .getOrDefault(EContributionType.EXCERPT, 0));
  }

  /**
   * Get the amount of excerpts of this actor for given role (aka ETextsView) in given text
   *
   * @param view a particular view, ie role of this actor in given text
   * @param text a text id
   * @return the amount of excerpts for given role in given text for this actor
   */
  public int getOwnExcerptMetrics(ETextsView view, Long text) {
    if (ownExcerptMetrics.get(view).containsKey(text)) {
      return ownExcerptMetrics.get(view).get(text);
    }
    return 0;
  }

  /**
   * Get the total amount of excerpts for given role (aka ETextsView) in given text
   *
   * @param view a particular view, ie role of this actor in given text
   * @param text a text id
   * @return the total amount of excerpts for given role in given text
   */
  public int getTotalExcerptMetrics(ETextsView view, Long text) {
    if (totalExcerptMetrics.get(view).containsKey(text)) {
      return totalExcerptMetrics.get(view).get(text);
    }
    return 0;
  }

  /**
   * Get the amount of texts of this actor for given view as viewable for given user
   *
   * @param view a TextsView
   * @param user a user having a look at given view
   * @return the amount of texts for given view
   */
  public int getTextsMetrics(ETextsView view, WebdebUser user) {
    if (textsMetrics.get(view) == null) {
      getTexts(view, user);
    }
    return textsMetrics.get(view);
  }

  /**
   * Get all texts where this actor appears as author
   *
   * @param user the webdeb user
   * @return a map of id-texts
   */
  @JsonIgnore
  public synchronized Map<Long, Text> getTextWhereAuthor(WebdebUser user) {
    return contributions.entrySet().parallelStream().filter(e ->
            e.getKey().getContributionType().getContributionType() == EContributionType.TEXT.id()
                    && e.getValue().isAuthor() && user.mayView(e.getKey()))
            .collect(Collectors.toMap(e -> e.getKey().getId(), e -> (Text) e.getKey()));
  }

  /**
   * Init all excerpts and arguments where this actor appears as author
   *
   * @param user the webdeb user
   */
  @JsonIgnore
  public synchronized void initExcerptWhereAuthor(WebdebUser user) {
    if(excerptsMap == null) {
      excerptsMap = new LinkedHashMap<>();

      contributions.entrySet().parallelStream().filter(e ->
        e.getKey().getContributionType().getContributionType() == EContributionType.EXCERPT.id() && user.mayView(e.getKey()))
      .forEach(e -> {
            Excerpt excerpt = (Excerpt) e.getKey();

            if(excerpt.actorIsThinker(id)) {
              excerpt.getSimilars().forEach(s ->
                      addExcerptAndArgumentInMaps(s.getExcerpt(), s.getArgumentLinkType().getEType()));
              if (excerpt.getSimilars().isEmpty()) {
                addExcerptAndArgumentInMaps(excerpt, null);
              }
            }
      });
    }
  }

  /*
   * PRIVATE HELPER
   */

  /**
   * Add discovered excerpts and arguments in their maps
   *
   * @param excerpt an possibly null excerpt to add (if not null)
   * @param shade a link shade between this excerpt and this argumentContext (if both not null)
   */
  private void addExcerptAndArgumentInMaps(Excerpt excerpt, EArgumentLinkShade shade){
    if (excerpt != null && !excerptsMap.containsKey(excerpt.getId())) {
      excerptsMap.put(excerpt.getId(), excerpt);
      excerptsShadeMap.put(excerpt.getId(), shade == null ? EArgumentLinkShade.ILLUSTRATE : shade);
    }
  }

  /**
   * Create metrics holder for the EVizPane.TEXTS
   *
   * @return an initialized metrics map for all ETextsView of an actor's EVizPane.TEXTS
   */
  private Map<ETextsView, Map<Long, Integer>> createMetrics() {
    Map<ETextsView, Map<Long, Integer>> result = new EnumMap<>(ETextsView.class);
    result.put(ETextsView.AUTHOR, new LinkedHashMap<>());
    result.put(ETextsView.EXCERPT_AUTHOR, new LinkedHashMap<>());
    result.put(ETextsView.EXCERPT_CITED, new LinkedHashMap<>());
    return result;
  }

  /**
   * Helper method to update a given map of sociography nodes with a new ActorSociographyNode for given actor
   *
   * @param list  a list of sociography node (will be updated)
   * @param actor the actor to put in node map
   * @param type  the link type depicting the position of a particular argument for given label
   */
  private void updateNodeMap(Map<EActorGroupKey, Map<String, ActorSociographyNode>> list, EActorGroupKey key, Actor actor, EArgumentLinkShade type) {
    String label = actor.getFullname(lang);
    String sortkey = actor.getActorType().equals(EActorType.PERSON) ? actor.getName(lang).getLast() : label;
    Gender gender = actor.getActorType().equals(EActorType.PERSON) ? ((Person) actor).getGender() : null;
    String avatar = actor.getAvatar() != null ? actor.getAvatar() :
            helper.computeAvatar(actor.getActorType().id(), gender != null ? gender.getId() : null);
    ActorSociographyNode node = list.get(key).containsKey(label) ? list.get(key).get(label)
            : new ActorSociographyNode(actor.getId(), sortkey, label, avatar,
            new ActorHolder(actor, lang).getFilterable(), false);
    list.get(key).put(label, node.increment(type));
  }


  /**
   * Helper method to update a given map of sociography nodes with a new ActorSociographyNode with given label and type
   *
   * @param list  a list of sociography node (will be updated)
   * @param key   the key that will be used to group sociography node in the map
   * @param label a label (semantics depend on the actual key used to group nodes behind a label), may be null
   * @param type  the link type depicting the position of a particular argument for given label
   */
  private void updateNodeMap(Map<EActorGroupKey, Map<String, ActorSociographyNode>> list, EActorGroupKey key, String label, EArgumentLinkShade type) {
    boolean isUnknown = false;
    String updatedLabel = label;
    if (updatedLabel == null) {
      isUnknown = true;
      switch (key) {
        case AGE:
          updatedLabel = i18n.get(Lang.forCode(lang), "viz.actor.socio.age.unknown");
          break;

        case COUNTRY:
          updatedLabel = i18n.get(Lang.forCode(lang), "viz.actor.socio.country.unknown");
          break;

        case ORGANIZATION:
          updatedLabel = i18n.get(Lang.forCode(lang), "viz.actor.socio.org.unknown");
          break;

        case FUNCTION:
          updatedLabel = i18n.get(Lang.forCode(lang), "viz.actor.socio.function.unknown");
          break;

        default:
          // do nothing
      }
    }
    ActorSociographyNode node = list.get(key).containsKey(updatedLabel) ? list.get(key).get(updatedLabel)
            : new ActorSociographyNode(updatedLabel, isUnknown);
    list.get(key).put(updatedLabel, node.increment(type));
  }

  /**
   * Build a map of actor viz holders with their respective affiliation viz. The map will be sorted on the last
   * affiliation date and then, alphabetically on the actor name
   *
   * @param affiliations a list of affiliations
   * @param type the related actor type
   * @return the map of actor-affiliations to be displayed, where all affiliations are regrouped by actor
   */
  private Map<ActorHolder, List<AffiliationViz>> toAffiliationsMap(List<Affiliation> affiliations, EActorType type) {
    Map<Long, List<AffiliationViz>> affResult = new LinkedHashMap<>();
    Map<Long, ActorHolder> actorResult = new LinkedHashMap<>();
    Map<ActorHolder, List<AffiliationViz>> results = new LinkedHashMap<>();

    affiliations.forEach(a -> {
      // check if already present (affiliations will be grouped per actor)
      AffiliationViz toadd = new AffiliationViz(a, lang);
      Long idActor = a.getActor().getId();
      List<AffiliationViz> list = affResult.get(idActor);
      if (list != null) {
        list.add(toadd);
      } else {
        affResult.put(idActor, new ArrayList<>(Collections.singletonList(toadd)));
        ActorHolder holder = new ActorHolder(a, lang, actor.getAffMap(type, idActor));
        actorResult.put(idActor, holder);
        actorsAffiliatedMap.get(type).add(holder);
      }
    });

    affResult.forEach((k, v) -> results.put(actorResult.get(k), affResult.get(k)));

    return results;
  }

  /**
   * Inner class from ActorVizHolder to handle actor sociography nodes, ie, data series objects used to build a sociography graph.
   * Contains an id in case the node refers to an actor (author or organization), a label to be displayed and
   * an array of integer values corresponding to the amount of arguments resp. similar, qualifying or opposed
   * to all arguments of the actor holding those nodes.
   *
   * @author Fabian Gilson
   */
  public class ActorSociographyNode {

    private Long id;
    private String label;
    private String avatar;
    private String sortkey;
    // amount of similar, qualifying and opposed arguments
    private Map<EArgumentLinkShade, Integer> statistics = new LinkedHashMap<>();
    private boolean isUnknown;
    private String filterable;

    /**
     * Construct a node for any type of sortkey (being the label itself) except actors
     *
     * @param label      the label displayed in the sociography graph
     * @param isUnknown  boolean saying if this node is unknown (to be put at end of list in graph)
     */
    ActorSociographyNode(String label, boolean isUnknown) {
      this(-1L, label, label, null, "", isUnknown);
    }

    /**
     * Construct a node for actors
     *
     * @param id         the actor id
     * @param sortkey    the key on which this node will be ordered
     * @param label      the label displayed in the sociography graph
     * @param avatar     the name of the actor's avatar (may be null)
     * @param filterable the filterable key-values for this node
     * @param isUnknown  boolean saying if this node is unknown (to be put at end of list in graph)
     */
    ActorSociographyNode(Long id, String sortkey, String label, String avatar, String filterable, boolean isUnknown) {
      this.id = id;
      this.sortkey = sortkey;
      this.label = label;
      this.avatar = avatar;
      this.filterable = filterable;
      this.isUnknown = isUnknown;

      statistics.put(EArgumentLinkShade.ILLUSTRATE, 0);
      statistics.put(EArgumentLinkShade.SHADED_EXAMPLE, 0);
      statistics.put(EArgumentLinkShade.COUNTER_EXAMPLE, 0);
    }

    /**
     * Get the idof this node, if any
     *
     * @return either an actor id, or -1 if this node is not an actor
     */
    public Long getId() {
      return id;
    }

    /**
     * Get the value on which the nodes are sorted
     *
     * @return a sorting value
     */
    String getSortkey() {
      return sortkey;
    }

    /**
     * Get the label used to show this node
     *
     * @return a label
     */
    public String getLabel() {
      return label;
    }

    /**
     * Get the avatar file name, if this node is an actor
     *
     * @return a file name, null if unset
     */
    public String getAvatar() {
      return avatar;
    }

    /**
     * Get the statistics array with, resp. IS_SUPPORTED, IS_SHADED and IS_REJECTED amounts for this node
     *
     * @return the statistics array
     */
    public Map<EArgumentLinkShade, Integer> getStatistics() {
      return statistics;
    }

    /**
     * Get the statistics as string separated by ","
     *
     * @return the statistics as string
     */
    public String getStatisticsAsString() {
      return statistics.values().stream().map(String::valueOf).collect(Collectors.joining(","));
    }

    /**
     * Re-implement the ContributionHolder's filterable property, see {@link ContributionHolder#getFilterable() getFilterable method} for
     * more details.
     *
     * @return a stringified list of (key, value) pairs
     */
    public String getFilterable() {
      // does not need to jsonify it as it has been added in dedicated constructor
      return filterable;
    }

    /**
     * Boolean flag to kwow if this node is the special "unknown" node
     *
     * @return true if this node is the unknown one (will be put at the end of visualization)
     */
    public boolean isUnknown() {
      return isUnknown;
    }

    /**
     * Increment statistics value depending on given link shade
     *
     * @param type a link shade to increment
     * @return this updated node
     */
    public ActorSociographyNode increment(EArgumentLinkShade type) {
      statistics.put(type, statistics.get(type) + 1);
      return this;
    }
  }

  /**
   * Comparator class to sort sociography nodes based on the statistics
   */
  private class ActorSocioComparator implements Comparator<ActorSociographyNode> {
    @Override
    public int compare(ActorSociographyNode o1, ActorSociographyNode o2) {
      if (o1 == null || o1.getStatistics().isEmpty() || o1.isUnknown()) {
        return 1;
      }
      if (o2 == null || o2.getStatistics().isEmpty() || o2.isUnknown()) {
        return -1;
      }

      int sumO1 = o1.getStatistics().values().stream().mapToInt(Number::intValue).sum();
      int sumO2 = o2.getStatistics().values().stream().mapToInt(Number::intValue).sum();

      // (similar - opposed from second node relative to the sum of all elements)
      // - (similar - opposed from first node relative to the sum of all elements)
      double stat = ((double)(o2.getStatistics().get(EArgumentLinkShade.ILLUSTRATE) - o2.getStatistics().get(EArgumentLinkShade.COUNTER_EXAMPLE)) / sumO1)
              - ((double)(o1.getStatistics().get(EArgumentLinkShade.ILLUSTRATE) - o1.getStatistics().get(EArgumentLinkShade.COUNTER_EXAMPLE)) / sumO2);

      if (BigDecimal.valueOf(stat).compareTo(BigDecimal.ZERO) == 0) {
        return (int) Math.round(stat);
      }

      int total = sumO2 - sumO1;
      if (total != 0) {
        return total;
      }

      return o1.getSortkey().compareTo(o2.getSortkey());
    }
  }

  /**
   * Get all affiliated persons to this actor for the given actor type
   *
   * @param type the actor type for the affiliated
   * @return a possibly empty list of affiliations
   */
  public List<Affiliation> getAffiliated(EActorType type) {
    return actor.getActorsAffiliated(type);
  }

  /**
   * Get all affiliation to this actor for the given actor type
   *
   * @param type the actor type for the affiliation
   * @return a possibly empty list of affiliations
   */
  public List<Affiliation> getAffiliations(EActorType type) {
    List<Affiliation> aff = actor.getActorsAffiliations(type);
    aff.addAll(actor.getAffiliations(EAffiliationType.GRADUATING_FROM));
    return aff;
  }

  /**
   * Get all actors affiliated or affiliations of the given type
   *
   * @param type the actor type for the affiliation
   * @return a possibly empty list of actor holders
   */
  public synchronized List<ActorHolder> getAffiliationsActors(EActorType type){
    if(actortype == EActorType.ORGANIZATION.id())
      getAllAffiliatedByActorType(type);
    getAllAffiliationByActorType(type);
    return new ArrayList<>(actorsAffiliatedMap.get(type));
  }
}