/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.viz.excerpt;

import be.webdeb.core.api.actor.Actor;
import be.webdeb.core.api.actor.ActorRole;
import be.webdeb.core.api.actor.Affiliation;
import be.webdeb.presentation.web.controllers.entry.actor.ActorHolder;
import be.webdeb.presentation.web.controllers.entry.excerpt.ExcerptHolder;

import java.util.List;
import java.util.Map;

/**
 * This class contains all excerpts where an actor is author, reporter or cited.
 * It also contains the actor himself (as ActorHolder) and a map (of excerpt id, ActorRole)
 * that contains all role for a specified excerpt.
 *
 * @author Martin Rouffiange
 */
public class ExcerptCitedViz {

    private ActorHolder actor;
    private List<ExcerptHolder> excerpts;
    private Map<Long, ActorRole> roles;

    /**
     * Default Constructor
     *
     * @param actor an actor holder
     * @param roles all actor roles for given arguments
     * @param excerpts a list of excerpts where the actors is referenced
     */
    public ExcerptCitedViz(ActorHolder actor, Map<Long, ActorRole> roles, List<ExcerptHolder> excerpts) {
        this.actor = actor;
        this.excerpts = excerpts;
        this.roles = roles;
    }

    /**
     * Get the actor holder
     *
     * @return an actor holder
     */
    public ActorHolder getActor() {
        return actor;
    }

    /**
     * Get the list of excerpts that are concerned by the actor
     *
     * @return a possibly empty list of excerpts
     */
    public List<ExcerptHolder> getExcerpts() {
        return excerpts;
    }

    /**
     * Get the excerpts size to sort ExcerptCitedViz by number of excerpts.
     *
     * @return the number of excerpts
     */
    public int comparator() {
        return excerpts.size();
    }

    /**
     * Get the actor role for a given excerpt
     *
     * @param excerpt an excerpt holder
     * @return actor role message
     */
    public String getActorRole(ExcerptHolder excerpt) {
        ActorRole actorRole = roles.get(excerpt.getId());

        if(actorRole != null) {
            boolean multipleActors = excerpt.getAuthors().size() > 1;
            if (actorRole.isAuthor()) {
                return (multipleActors ? "actor.role.cosigned" : "actor.role.signed"); }
            if (actorRole.isReporter()) {
                return "actor.role.reported";
            }
        }
        return "actor.role.none";
    }

    /**
     * Get the actor role function for a given excerpt in a fiven lang
     *
     * @param excerpt an excerpt holder
     * @param lang the user lang
     * @return actor role function
     */
    public String getActorFunction(ExcerptHolder excerpt, String lang) {
        ActorRole actorRole = roles.get(excerpt.getId());
        Affiliation aff = (actorRole != null ? actorRole.getAffiliation() : null);
        return aff != null ? aff.getFullfunction(lang, true, false) : null;
    }

    /**
     * Get the actor role affiliated organization for a given excerpt
     *
     * @param excerpt an excerpt holder
     * @return actor role affiliated organization
     */
    public Actor getActorOrganisation(ExcerptHolder excerpt) {
        ActorRole actorRole = roles.get(excerpt.getId());
        Affiliation aff = (actorRole != null ? actorRole.getAffiliation() : null);
        if(aff != null && aff.getActor() != null){
            return aff.getActor();
        }
        return null;
    }

}