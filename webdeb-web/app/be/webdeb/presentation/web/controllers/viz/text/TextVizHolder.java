/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.viz.text;

import be.webdeb.core.api.actor.ActorRole;
import be.webdeb.core.api.actor.EActorType;
import be.webdeb.core.api.argument.Argument;
import be.webdeb.core.api.argument.ArgumentContext;
import be.webdeb.core.api.argument.ArgumentIllustration;
import be.webdeb.core.api.argument.EArgumentLinkShade;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.core.api.text.Text;
import be.webdeb.presentation.web.controllers.entry.ContributionHolder;
import be.webdeb.presentation.web.controllers.entry.EFilterName;
import be.webdeb.presentation.web.controllers.entry.actor.ActorSimpleForm;
import be.webdeb.presentation.web.controllers.entry.argument.ArgumentContextHolder;
import be.webdeb.presentation.web.controllers.entry.excerpt.ExcerptHolder;
import be.webdeb.presentation.web.controllers.entry.text.TextHolder;
import be.webdeb.presentation.web.controllers.permission.WebdebUser;
import be.webdeb.presentation.web.controllers.viz.ERadiographyViewKey;
import be.webdeb.presentation.web.controllers.viz.EVizPane;
import be.webdeb.presentation.web.controllers.viz.RadiographyKey;
import be.webdeb.presentation.web.controllers.viz.VizActions;
import play.api.Play;
import play.i18n.Lang;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;


/**
 * This class holds text values to build visualisation pages.
 *
 * @author Fabian Gilson
 */
public class TextVizHolder extends TextHolder {

  @Inject
  private VizActions vizActions = Play.current().injector().instanceOf(VizActions.class);

  private Map<EVizPane, Map<EArgumentLinkShade, Integer>> excerptMetrics;
  private Map<EVizPane, Map<Long, Map<EArgumentLinkShade, Integer>>> metricsByAuthor;
  private Map<EVizPane, Map<Long, Map<EArgumentLinkShade, Integer>>> metricsByText;
  private Map<EVizPane, Map<EArgumentLinkShade, Integer>> aggregatedMetricsByAuthor;
  private Map<EVizPane, Map<EArgumentLinkShade, Integer>> aggregatedMetricsByText;

  // radiography data
  private Map<ERadiographyViewKey, Map<String, List<TextVizHolder>>> radioData = null;

  private Map<Long, ExcerptHolder> excerptsMap = null;
  private Map<Long, ArgumentContextHolder> argumentsMap = null;

  private Map<Long, Set<Long>> excerptArgumentsMap = new LinkedHashMap<>();
  private Map<Long, Set<Long>> argumentExcerptsMap = new LinkedHashMap<>();

  private  Map<Long, Text> textMap = null;

  // this shade is used when this viz holder is part of a list of others that have been put in relation to others
  // ie, for radiography visualizations
  private EArgumentLinkShade shade;

  /**
   * Construct a Text viz holder with given text and in given interface language
   *
   * @param text an API text
   * @param contributor the contributor for whom the content must be displayed
   * @param lang two-char ISO 639-1 code of the user interface language
   */
  public TextVizHolder(Text text, Long contributor, String lang) {
    super(text, contributor, lang);
    initAllMetrics();

    // add more filterable elements
    text.getActors().stream().filter(a -> authors.stream().anyMatch(actor -> actor.getId().equals(a.getActor().getId()))).forEach(a -> {
      addFilterable(EFilterName.ATYPE, i18n.get(Lang.forCode(lang), "actor.label.actortype." + a.getActor().getActorType().id()));
      setAuthorFilterable(a.getActor());
    });
  }

  /**
   * Construct a Text viz holder with given text and in given interface language, restricting the list
   * of authors to given role.
   *
   * @param text an API text
   * @param contributor the contributor for whom the content must be displayed
   * @param role the actor role to keep in this viz holder
   * @param lang two-char ISO 639-1 code of the user interface language
   */
  public TextVizHolder(Text text, Long contributor, ActorRole role, String lang) {
    this(text, contributor, lang);
    authors.removeIf(a -> !role.getActor().getId().equals(a.getId()));
  }

  /**
   * Build the radiography view data for a given viewkey. Will build a map of key, list of arguments (as
   * LinkVizHolders) to be displayed.
   *
   * Excerpt radiography shows a list of arguments having (indirect) similarity links to this argument. This argument
   * list is sorted based on given viewKey
   *
   * @param user the WebDeb user
   * @return a map of key, list with for each key, the list of associated link visualization holders
   */
  public Map<ERadiographyViewKey, Map<String, List<TextVizHolder>>> viewRadiography(WebdebUser user) {
    /*if(radioData == null) {
      Map<ERadiographyViewKey, Map<RadiographyKey, List<TextVizHolder>>> working = new LinkedHashMap<>();
      for(ERadiographyViewKey viewKey : ERadiographyViewKey.values()) {
        working.put(viewKey, new LinkedHashMap<>());
      }

      initExcerptAndArgumentsFromText(user);

      // now aggregate final author/text metrics (from per author basis to global amounts)
      aggregatedMetricsByAuthor.put(EVizPane.RADIO, vizActions.aggregateMetricsById(metricsByAuthor, EVizPane.RADIO, -1L));
      aggregatedMetricsByText.put(EVizPane.RADIO, vizActions.aggregateMetricsById(metricsByText, EVizPane.RADIO, -1L));

      // now cross over the map of texts and build radiography
      textMap.forEach((id, textInMap) -> {
        TextVizHolder holder = new TextVizHolder(textInMap, contributor, lang);
        holder.shade = vizActions.getDominantShade(metricsByText.get(EVizPane.RADIO).get(id));

        for(ERadiographyViewKey viewKey : ERadiographyViewKey.values()) {
          List<RadiographyKey> lookFor = new ArrayList<>();

          if(ERadiographyViewKey.AUTHOR.equals(viewKey)){
            for(ActorSimpleForm actor : holder.getReporters()) {
              lookFor.add(actor.getActortype() == EActorType.PERSON.id() ?
                      new RadiographyKey(actor.getLastname(), actor.getFullname(), ERadiographyViewKey.AUTHOR)
                      : new RadiographyKey(actor.getFullname(), ERadiographyViewKey.AUTHOR));
            }
          }else{
            switch (viewKey) {
              case SOURCE:
                lookFor.add(new RadiographyKey(holder.getSourceTitle(), ERadiographyViewKey.SOURCE));
                break;
              case DATE:
                // date must be aggregated to months (do not consider days)
                String pubDate = holder.getPublicationDate();
                lookFor.add(new RadiographyKey(pubDate != null && pubDate.length() > "MM/YYYY".length() ?
                        pubDate.substring("DD/".length()) : pubDate, ERadiographyViewKey.DATE));
                break;
              case TEXT:
                lookFor.add(new RadiographyKey(holder.getTitle(), ERadiographyViewKey.TEXT));
                break;
              default:
                logger.error("unknown text view key " + viewKey.name());
                lookFor.add(null);
            }
          }

          lookFor.forEach(e -> {
            if (working.get(viewKey).containsKey(e)) {
              updateRadiographyMap(working.get(viewKey), holder, e, viewKey);
            } else {
              List<TextVizHolder> t = new ArrayList<>();
              t.add(holder);
              working.get(viewKey).put(e, t);
            }
          });
        }
      });
      // post processing for sorting based on the key (not using collectors, otherwise order is lost)
      radioData = new LinkedHashMap<>();
      for(ERadiographyViewKey viewKey : ERadiographyViewKey.values()) {
        radioData.put(viewKey, new HashMap<>());
      }
      working.forEach((k, v) -> vizActions.sortByRadioKey(working.get(k)).forEach(
              (k2, v2) -> radioData.get(k).put(k2.getName(), v2)));
    }
    return radioData;*/
    return new HashMap<>();
  }


  /*
   * STATS AND METRICS MANAGEMENT
   */

  /**
   * Retrieve the amount of argument linked to this argument for a specific shade
   *
   * @param shade a shade
   * @return the amount of argument of given shade that are connected to this text
   */
  public int getAmountByShade(EArgumentLinkShade shade, EVizPane pane) {
    return excerptMetrics.get(pane).get(shade);
  }

  /**
   * Get the amount of excerpts of given shade for given text in given viz pane
   *
   * @param text a text id
   * @param shade the shade type
   * @param pane the visualization pane for which the metrics must be retrieved
   * @return the amount of excerpts of given shade in given text (displayed in given pane)
   */
  public int getExcerptByShadeInText(Long text, EArgumentLinkShade shade, EVizPane pane) {
    return metricsByText.get(pane).get(text).get(shade);
  }

  /**
   * Get the amount of excerpts for given text in given viz pane
   *
   * @param text a text id
   * @param pane the visualization pane for which the metrics must be retrieved
   * @return the amount of excerpts in given text (displayed in given pane)
   */
  public int getExceptInText(Long text, EVizPane pane) {
    return metricsByText.get(pane).get(text).values().stream().mapToInt(Integer::intValue).sum();
  }

  /**
   * Retrieve the amount of actors being author of arguments having similarity links with any argument extracted
   * from this text for given shade
   *
   * @param pane the visualization pane to get authors statistics for
   * @param shade a shade
   * @return the amount of actors of given shade that are connected to this text
   */
  public int getActorsAmountByShade(EVizPane pane, EArgumentLinkShade shade) {
    return aggregatedMetricsByAuthor.get(pane).get(shade);
  }

  /**
   * Retrieve the amount of text being where arguments have similarity links with any argument extracted
   * from this text for given shade
   *
   * @param pane the visualization pane to get text statistics for
   * @param shade a shade
   * @return the amount of text of given shade that are connected to this text
   */
  public int getTextAmountByShade(EVizPane pane, EArgumentLinkShade shade) {
    return aggregatedMetricsByText.get(pane).get(shade);
  }

  /**
   * Retrieve the amount of actors involved in arguments (as authors) having similarity links to any argument from this text
   *
   * @param pane the visualization pane to get the authors statistics for
   * @return an int array with the amount of actors aggregated by similarity shades (similar, qualifies, opposes)
   */
  public int[] getActorsAmountByShade(EVizPane pane) {
    return new int[] {
        aggregatedMetricsByAuthor.get(pane).get(EArgumentLinkShade.SIMILAR),
        aggregatedMetricsByAuthor.get(pane).get(EArgumentLinkShade.OPPOSES)
    };
  }

  /**
   * Get the aggregated amount of texts for a given actor (being an author of this argument)
   *
   * @param pane the visualization pane to get the aggregated amount for
   * @param id an actor id
   * @return the amount of texts linked to this text for a given author
   */
  public int getAmountByAuthor(EVizPane pane, Long id) {
    if (metricsByAuthor.get(pane).get(id) != null) {
      return metricsByAuthor.get(pane).get(id).values().stream().reduce(0, Integer::sum);
    }
    return 0;
  }

  /**
   * Get the amount of links for a given actor (being an author of this text) and for a given shade
   *
   * @param pane the visualization pane to get the aggregated amount for
   * @param id an actor id
   * @param shade an shade id
   * @return the amount of texts of given author and shade
   * @see EArgumentLinkShade
   */
  public int getAmountByAuthor(EVizPane pane, Long id, EArgumentLinkShade shade) {
    if (metricsByAuthor.get(pane).get(id) != null) {
      return metricsByAuthor.get(pane).get(id).get(shade);
    }
    return 0;
  }

  /**
   * Get current shade for this text
   *
   * @return this text's shade, if any
   */
  public EArgumentLinkShade getShade() {
    return shade;
  }


  /*
   * PRIVATE HELPER METHODS
   */


  /**
   * Init all excerpts and arguments linked to this text
   */
  private synchronized void initExcerptAndArgumentsFromText(WebdebUser user) {
    if(excerptsMap == null || argumentsMap == null || textMap == null) {
      excerptsMap = new LinkedHashMap<>();
      argumentsMap = new LinkedHashMap<>();
      textMap = new LinkedHashMap<>();
      // get all bound arguments with their texts and update metricsByText
      Map<Long, Set<Long>> textExcerptsMap = new LinkedHashMap<>();
      Map<Long, Text> textMap = new LinkedHashMap<>();
      // The hashset of author id of this text
      Set<Long> textAuthorsSet = new HashSet<>();
      getReporters().forEach(e -> textAuthorsSet.add(e.getId()));

      text.getExcerpts().forEach(excerpt -> {
        excerpt.getSimilars().stream().filter(e -> user.mayView(e.getExcerpt())).forEach(e -> {
          addExcerptAndArgumentInMaps(e.getExcerpt(), user.mayView(e.getArgument()) ? e.getArgument() : null);

          Excerpt excerptInText = e.getExcerpt();
          if (!id.equals(excerptInText.getTextId())) {
            Text text = excerptInText.getText();
            // add text to list if not yet present
            if (!textMap.containsKey(text.getId())) {
              textMap.put(text.getId(), text);
              textExcerptsMap.put(text.getId(), new HashSet<>());
            }
            // if this excerpt has not yet been crosses, add it
            if (!textExcerptsMap.get(text.getId()).contains(excerptInText.getId())) {
              textExcerptsMap.get(text.getId()).add(excerptInText.getId());
              EArgumentLinkShade shade = e.getArgumentLinkType().getEType();
              // and update text metrics
              vizActions.updateMetricsById(metricsByText, EVizPane.RADIO, text.getId(), shade);
              excerptMetrics.get(EVizPane.RADIO).put(shade, excerptMetrics.get(EVizPane.RADIO).get(shade) + 1);
              excerptInText.getThinkerActors().stream().filter(a -> !textAuthorsSet.contains(a.getActor().getId())).forEach(a ->
                      vizActions.updateMetricsById(metricsByAuthor, EVizPane.RADIO, a.getActor().getId(), shade));
            }
          }
        });
        if(excerpt.getSimilars().isEmpty() && user.mayView(excerpt)) {
          addExcerptAndArgumentInMaps(excerpt, null);
        }
      });

      // Add arguments of the text argument structure that are not yet linked with a citation
      text.getAllContextualizedArguments().stream().filter(user::mayView).forEach(e -> {
        if(!argumentsMap.containsKey(e.getId())){
          argumentsMap.put(e.getId(), new ArgumentContextHolder(e, lang));
          argumentExcerptsMap.put(e.getId(), new HashSet<>());
        }
      });
    }
  }

  /**
   * Add discovered excerpts and arguments in their maps
   */
  private void addExcerptAndArgumentInMaps(Excerpt excerpt, ArgumentContext argumentContext){

    if(!excerptsMap.containsKey(excerpt.getId())){
      excerptsMap.put(excerpt.getId(), new ExcerptHolder(excerpt, lang));
      excerptArgumentsMap.put(excerpt.getId(), new HashSet<>());
    }

    if(argumentContext != null){
      if(!argumentsMap.containsKey(argumentContext.getId())){
        argumentsMap.put(argumentContext.getId(), new ArgumentContextHolder(argumentContext, lang));
        argumentExcerptsMap.put(argumentContext.getId(), new HashSet<>());
      }
      argumentExcerptsMap.get(argumentContext.getId()).add(excerpt.getId());
      excerptArgumentsMap.get(excerpt.getId()).add(argumentContext.getId());
    }
  }

  /**
   *
   */
  private void updateRadiographyMap(Map<RadiographyKey, List<TextVizHolder>> map, TextVizHolder text, RadiographyKey key,
                                    ERadiographyViewKey view) {
    List< TextVizHolder> holders = map.get(key);
    boolean found = false;

    // loop the whole list of values corresponding to given radio key (view key) and insert given text at right place
    // break when found right place (so not using for-each to make explicit break in loop condition)
    // will not add text if already present
    for (int index = 0; index < holders.size() && !found; index++) {
      TextVizHolder lholder = holders.get(index);

      if (lholder.getShade().equals(shade)) {
        switch (view) {
          case AUTHOR:
            found = updateRadiographyOnAuthor(holders, lholder, text, index);
            break;

          case SOURCE:
            found = updateRadiographyOnSource(holders, lholder, text, index);
            break;

          case DATE:
            found = updateRadiographyOnDate(holders, lholder, text, index);
            break;

          case TEXT:
            // may only have one node of this type
            if (!holders.contains(text)) {
              holders.add(index, text);
            }
            found = true;
            break;

          default:
            logger.warn("unknown view key " + view.name());
        }
      } else {
        // shades are different, check if we have to put it here or not
        if (lholder.getShade().id() > text.getShade().id()) {
          holders.add(index, text);
          found = true;
        }
      }
    }

    // simply add at the end of list
    if (!found) {
      holders.add(text);
    }
  }


  /**
   * Update given list of texts (having the same author) with given text when sorting radiography on author key.
   * Given text holders list will be internally sorted on titles, dates than sources.
   *
   * @param holders a list of texts
   * @param tholder current link in given holders list to compare with given link to insert
   * @param text the text to insert into holders list if the placement conditions are satisfied
   * @param index the current index in holders list (to insert given link if placement conditions are met)
   * @return true if given text has been inserted into given holders list, false otherwise
   */
  private boolean updateRadiographyOnAuthor(List<TextVizHolder> holders, TextVizHolder tholder, TextVizHolder text, int index) {
    String currentDate;
    String currentSource;
    String holderDate;
    String holderSource;

    currentDate = text.getPublicationDate();
    holderDate = tholder.getPublicationDate();

    if (!values.isBefore(currentDate, holderDate)) {
      // must check if they are equal, if yes must decide on source name, otherwise, place it and break
      // checking if both are null or if current is not null, if they are equals
      if (currentDate == holderDate || (currentDate != null && currentDate.equals(holderDate))) {
        // must check on source name
        currentSource = text.getSourceTitle();
        holderSource = tholder.getSourceTitle();

        if (currentSource == holderSource || (currentSource != null && currentSource.compareTo(holderSource) > 1)) {
          holders.add(index, text);
          return true;
        }
      } else {
        holders.add(index, text);
        return true;
      }
    }
    return false;
  }

  /**
   * Update given list of texts (having the same source) with given text when sorting radiography on source key.
   * Given text holders list will be internally sorted on titles than dates.
   *
   * @param holders a list of text
   * @param current current text in given holders list to compare with given text to insert
   * @param text the text to insert into holders list if the placement conditions are satisfied
   * @param index the current index in holders list (to insert given link if placement conditions are met)
   * @return true if given link has been inserted into given holders list, false otherwise
   */
  private boolean updateRadiographyOnSource(List<TextVizHolder> holders, TextVizHolder current, TextVizHolder text, int index) {
    String currentDate;
    String holderDate;

    ActorSimpleForm name1;
    ActorSimpleForm name2;

    // we know there is only one author in viz holders
    name1 = text.getAuthors().get(0);
    name2 = current.getAuthors().get(0);
    if (name1.getLastname().compareTo(name2.getLastname()) > 0) {
      holders.add(index, text);
      return true;
    } else {
      if (name1.getFullname().equals(name2.getFullname())) {
        // check on date
        currentDate = text.getPublicationDate();
        holderDate = current.getPublicationDate();

        if (values.isBefore(currentDate, holderDate)) {
          holders.add(index, text);
          return true;
        }
      } else {
        if (name1.getFullname().compareTo(name2.getFullname()) > 0) {
          holders.add(index, text);
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Update given list of texts (all texts having the same publication date -ie month-) with given link when
   * sorting radiography on date key. Given text holders list will be internally sorted on titles than sources.
   *
   * @param holders a list of texts
   * @param current current link in given holders list to compare with given link to insert
   * @param text the link to insert into holders list if the placement conditions are satisfied
   * @param index the current index in holders list (to insert given link if placement conditions are met)
   * @return true if given link has been inserted into given holders list, false otherwise
   */
  private boolean updateRadiographyOnDate(List<TextVizHolder> holders, TextVizHolder current, TextVizHolder text, int index) {

    String currentSource;
    String holderSource;

    ActorSimpleForm name1;
    ActorSimpleForm name2;

    // we know there is only one author in viz holders
    name1 = text.getAuthors().get(0);
    name2 = current.getAuthors().get(0);
    if (name1.getLastname().compareTo(name2.getLastname()) > 0) {
      holders.add(index, text);
      return true;
    } else {
      if (name1.getFullname().equals(name2.getFullname())) {
        // check on source
        currentSource = text.getSourceTitle();
        holderSource = current.getSourceTitle();

        if (currentSource == holderSource || (currentSource != null && currentSource.compareTo(holderSource) > 1)) {
          holders.add(index, text);
          return true;
        }
      } else {
        if (name1.getFullname().compareTo(name2.getFullname()) > 0) {
          holders.add(index, text);
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Initialize metrics maps with default values.
   */
  private void initAllMetrics() {
    // init excerptMetrics
    int[] defaults = new int[] {0, 0, 0};
    excerptMetrics = new EnumMap<>(EVizPane.class);
    excerptMetrics.put(EVizPane.RADIO, new LinkedHashMap<>());
    vizActions.initMetrics(excerptMetrics.get(EVizPane.RADIO), defaults);

    metricsByAuthor = new EnumMap<>(EVizPane.class);
    metricsByAuthor.put(EVizPane.RADIO, new LinkedHashMap<>());
    metricsByText = new EnumMap<>(EVizPane.class);
    metricsByText.put(EVizPane.RADIO, new LinkedHashMap<>());

    aggregatedMetricsByAuthor = new EnumMap<>(EVizPane.class);
    aggregatedMetricsByAuthor.put(EVizPane.RADIO, new LinkedHashMap<>());
    vizActions.initMetrics(aggregatedMetricsByAuthor.get(EVizPane.RADIO), defaults);
    aggregatedMetricsByText = new EnumMap<>(EVizPane.class);
    aggregatedMetricsByText.put(EVizPane.RADIO, new LinkedHashMap<>());
    vizActions.initMetrics(aggregatedMetricsByText.get(EVizPane.RADIO), defaults);
  }

}
