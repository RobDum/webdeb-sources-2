/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers;

import be.webdeb.application.query.QueryExecutor;
import be.webdeb.core.api.actor.Actor;
import be.webdeb.core.api.actor.ActorFactory;
import be.webdeb.core.api.argument.ArgumentContext;
import be.webdeb.core.api.argument.ArgumentFactory;
import be.webdeb.core.api.argument.ArgumentJustification;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.contribution.PartialContributions;
import be.webdeb.core.api.contributor.ContributorFactory;
import be.webdeb.core.api.debate.Debate;
import be.webdeb.core.api.debate.DebateFactory;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.core.api.excerpt.ExcerptFactory;
import be.webdeb.core.api.folder.FolderFactory;
import be.webdeb.core.api.text.Text;
import be.webdeb.core.api.text.TextFactory;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.presentation.web.controllers.entry.*;
import be.webdeb.presentation.web.controllers.entry.argument.ArgumentContextHolder;
import be.webdeb.presentation.web.controllers.entry.argument.JustificationLinkForm;
import be.webdeb.presentation.web.controllers.entry.debate.DebateHolder;
import be.webdeb.presentation.web.controllers.entry.excerpt.ExcerptHolder;
import be.webdeb.presentation.web.controllers.entry.text.TextHolder;
import be.webdeb.presentation.web.controllers.permission.WebdebUser;
import be.webdeb.presentation.web.views.html.browse.searchResult;
import be.webdeb.presentation.web.views.html.util.noResultMessage2;
import be.webdeb.presentation.web.views.html.viz.argumentExcerptsDiv;
import be.webdeb.presentation.web.views.html.viz.excerptArgumentsDiv;
import be.webdeb.presentation.web.views.html.viz.folder.folderTextsTextBox;
import be.webdeb.util.ValuesHelper;
import com.fasterxml.jackson.databind.JsonNode;
import play.Configuration;
import play.api.Play;
import play.data.FormFactory;
import play.i18n.MessagesApi;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Call;
import play.mvc.Controller;
import play.mvc.Result;
import play.twirl.api.Html;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

/**
 * Common ie controller for avoid repetition
 *
 * @author Martin Rouffiange
 */
public abstract class CommonController extends Controller {

    @Inject
    protected FormFactory formFactory;

    @Inject
    protected ActorFactory actorFactory;

    @Inject
    protected ArgumentFactory argumentFactory;

    @Inject
    protected ExcerptFactory excerptFactory;

    @Inject
    protected FolderFactory folderFactory;

    @Inject
    protected DebateFactory debateFactory;

    @Inject
    protected TextFactory textFactory;

    @Inject
    protected MessagesApi i18n;

    @Inject
    protected ValuesHelper values;

    @Inject
    protected SessionHelper sessionHelper;

    @Inject
    protected ContributionHelper helper;

    @Inject
    protected QueryExecutor executor;

    @Inject
    protected ContributorFactory contributorFactory;

    @Inject
    protected HttpExecutionContext context;

    @Inject
    protected be.webdeb.infra.ws.geonames.RequestProxy geonames;

    @Inject
    protected Configuration configuration;

    // custom logger
    protected static final org.slf4j.Logger logger = play.Logger.underlying();

    /**
     * Redirect to go to session key.
     *
     * @param defaultCall the default rediction if go to session key is null
     * @return either the session "goto" page or the given defaultCall, or if is null the index page
     */
    protected CompletionStage<Result> redirectToGoTo(Call defaultCall){
        String goTo = sessionHelper.get(ctx(), SessionHelper.KEY_GOTO);
        sessionHelper.remove(ctx(), SessionHelper.KEY_GOTO);

        if (goTo == null || goTo.contains("/entry/text/annotated")) {
            if(defaultCall == null){
                logger.info("REDIRECT to index");
                return CompletableFuture.completedFuture(redirect(be.webdeb.presentation.web.controllers.routes.Application.index()));
            }else{
                logger.info("REDIRECT to " + defaultCall.url());
                return CompletableFuture.completedFuture(redirect(defaultCall));
            }
        }
        logger.info("REDIRECT to " + goTo);
        return CompletableFuture.completedFuture(redirect(goTo));
    }

    /**
     * Convert links form from request to api links
     *
     * @return the links as api
     */
    protected List<ArgumentJustification> convertLinksFormToApi() {
        // get all justification links a json (ajax call) and deserialize them
        JustificationLinkForm[] links = Json.fromJson(request().body().asJson().elements().next(), JustificationLinkForm[].class);
        // convert to argument justification links and set current group
        List<ArgumentJustification> api = new ArrayList<>();
        for (JustificationLinkForm l : links) {
            try {
                api.add(l.toLink());
            }catch(PersistenceException e){
                logger.debug("Uncastable link : " + e);
            }
        }
        return api;
    }

    /**
     * Fill given place form from geonames data
     *
     * @param places the place forms to fill
     * @param defaultWorld true if at least one place must be added
     * @return the filled place forms
     */
    protected List<PlaceForm> savePlaces(List<PlaceForm> places, boolean defaultWorld){
        // save context places
        for(int i = 0; i < places.size(); i++){
            try{
                PlaceForm p = geonames.fillPlace(places.get(i));
                if(p != null && !values.isBlank(places.get(i).getCompleteName()))
                    places.set(i, p);
                else
                    places.remove(i);
            } catch (InterruptedException | ExecutionException e) {
                logger.error("Error in fill placeForm : ", e);
            }
        }

        if(defaultWorld && places.isEmpty()){
            places.add(new PlaceForm(0L));
        }

        return places;
    }

    /**
     * Build list of forms for unknown contributions to be proposed to the user
     *
     * @param wrappers the map of Contribution type and a list of contribution (actors or folders) that have been created during
     * this insertion(for all unknown contributions), an empty list if none had been created
     */
    protected void treatSaveContribution(Map<Integer, List<Contribution>> wrappers){
        if(wrappers != null) {
            if (wrappers.containsKey(EContributionType.ACTOR.id()))
                wrappers.get(EContributionType.ACTOR.id())
                        .forEach(a -> sessionHelper.addValue(ctx(), SessionHelper.KEY_NEWACTOR, a.getId().toString()));
            if (wrappers.containsKey(EContributionType.FOLDER.id()))
                wrappers.get(EContributionType.FOLDER.id())
                        .forEach(f -> sessionHelper.addValue(ctx(), SessionHelper.KEY_NEWFOLDER, f.getId().toString()));
        }
    }

    /**
     * Return the api Key for language dectection from configuration file
     *
     * @return the detectlanguage api key
     */
    protected String getLanguageDetectionApiKey(){
        return configuration.getString("detectlanguage.api.key");
    }

    /**
     * Delete all keys abotu link creation
     */
    protected void deleteNewLinkKeys(){
        sessionHelper.remove(ctx(), SessionHelper.KEY_NEW_JUS_LINK);
        sessionHelper.remove(ctx(), SessionHelper.KEY_NEW_ILLU_LINK);
        sessionHelper.remove(ctx(), SessionHelper.KEY_NEW_SIM_LINK);
    }

    /**
     * Send partial given loaded contributions as Json
     *
     * @param partial the number of loaded data and the the list of loaded contributions to send as Json or html
     * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
     * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
     * @param linkedContributionId a linked contribution id if needed
     * @param contributionType the type on contribution to send
     * @param responseType the format of the response
     * @return status 200 if all data as been send, 206 if more data could be over loaded
     */
    protected CompletionStage<Result> sendPartialLoadedContributions(PartialContributions<? extends Contribution> partial, int fromIndex, int toIndex, Long linkedContributionId, EContributionType contributionType, EDataSendType responseType) {
        WebdebUser user = sessionHelper.getUser(ctx());
        String lang = ctx().lang().code();
        PartialResponseBuilder builder = new PartialResponseBuilder(partial, linkedContributionId, contributionType, responseType, user, lang);

        return sendJsonPartialLoadedData(Json.toJson(builder.build()), partial.getNumberOfLoadedContributions(), toIndex - fromIndex);
    }

    /**
     * Send partial given loaded data as Json
     *
     * @param data the Json data to send
     * @param nbDataLoaded the total number of loaded data (could be different of send data because of user may view constraint)
     * @param nbDataToLoad the number of maximum data that should have been loaded
     * @return status 200 if all data as been send, 206 if more data could be over loaded
     */
    protected CompletionStage<Result> sendJsonPartialLoadedData(JsonNode data, int nbDataLoaded, int nbDataToLoad) {
        return CompletableFuture.supplyAsync(() ->
                nbDataLoaded >= nbDataToLoad ? status(206, data) : ok(data), context.current());
    }

    /**
     * Create the Json / html response for a partial query
     */
    private class PartialResponseBuilder {

        @Inject
        private ContributionHelper helper = Play.current().injector().instanceOf(ContributionHelper.class);

        private PartialContributions<? extends Contribution> partial;
        private Long linkedContributionId;
        private EContributionType contributionType;
        private EDataSendType responseType;
        private WebdebUser user;
        private String lang;

        PartialResponseBuilder(PartialContributions<? extends Contribution> partial, Long linkedContributionId, EContributionType contributionType, EDataSendType responseType, WebdebUser user, String lang){
            this.partial = partial;
            this.linkedContributionId = linkedContributionId;
            this.contributionType = contributionType;
            this.responseType = responseType;
            this.user = user;
            this.lang = lang;
        }

        public Map<String, String> build() {
            Map<String, String> response = new HashMap<>();
            AsHtmlResponse htmlResponse = null;

            List<? extends Contribution> contributions =
                    partial.getContributions().stream().filter(user::mayView).collect(Collectors.toList());

            if(!contributions.isEmpty()) {
                if (responseType != EDataSendType.AS_JSON) {

                    switch (contributionType) {
                        case ARGUMENT_CONTEXTUALIZED:
                            htmlResponse = sendArgumentContextsAsHtml(contributions);
                            break;
                        case EXCERPT:
                            htmlResponse = sendExcerptsAsHtml(contributions);
                            break;
                        case DEBATE:
                            htmlResponse = sendDebatesAsHtml(contributions);
                            break;
                        case TEXT:
                            htmlResponse = sendTextsAsHtml(contributions);
                            break;

                    }
                }

                if(htmlResponse != null){
                    response.put("filters", Json.toJson(helper.buildFilters(htmlResponse.holders, lang)).toString());
                    response.put("data", htmlResponse.html.toString());
                }else{
                    List<? extends ContributionHolder> holders = helper.toHolders(contributions, user, lang);
                    response.put("filters", Json.toJson(helper.buildFilters(holders, lang)).toString());
                    response.put("data", Json.toJson(holders).toString());
                }

            }else{
                response.put("nodata", Json.toJson(noResultMessage2.render(contributionType).toString()).toString());
            }


            return response;
        }

        private AsHtmlResponse sendArgumentContextsAsHtml(List<? extends Contribution> contributions){

            List<ArgumentContextHolder> holders = contributions.stream()
                    .map(e -> new ArgumentContextHolder((ArgumentContext) e, lang)).collect(Collectors.toList());

            Html html;

            switch (responseType){
                case AS_HTML_TEMPLATE_1:
                    html = argumentExcerptsDiv.render(holders, linkedContributionId, user);
                    break;
                default:
                    html = defaultContributionAsHtml(holders);
            }

            return new AsHtmlResponse(holders, html);
        }

        private AsHtmlResponse sendExcerptsAsHtml(List<? extends Contribution> contributions){

            List<ExcerptHolder> holders = contributions.stream()
                    .map(e -> new ExcerptHolder((Excerpt) e, lang)).collect(Collectors.toList());

            Html html;

            switch (responseType){
                case AS_HTML_TEMPLATE_1:
                    html = excerptArgumentsDiv.render(holders, linkedContributionId, user);
                    break;
                default:
                    html = defaultContributionAsHtml(holders);
            }

            return new AsHtmlResponse(holders, html);
        }

        private AsHtmlResponse sendDebatesAsHtml(List<? extends Contribution> contributions){

            List<DebateHolder> holders = contributions.stream()
                    .map(e -> new DebateHolder((Debate) e, lang)).collect(Collectors.toList());

            Html html;

            switch (responseType){
                default:
                    html = defaultContributionAsHtml(holders);
            }

            return new AsHtmlResponse(holders, html);
        }

        private AsHtmlResponse sendTextsAsHtml(List<? extends Contribution> contributions){

            List<TextHolder> holders = contributions.stream()
                    .map(e -> new TextHolder((Text) e, user.getId(), lang)).collect(Collectors.toList());

            Html html;

            switch (responseType){
                case AS_HTML_TEMPLATE_1:
                    html = folderTextsTextBox.render(holders, user);
                    break;
                default:
                    html = defaultContributionAsHtml(holders);
            }

            return new AsHtmlResponse(holders, html);
        }

        private Html defaultContributionAsHtml(List<? extends ContributionHolder> holders){
            return searchResult.render(null, holders, false, true, null, false, false);
        }

        private class AsHtmlResponse {
            public List<? extends ContributionHolder> holders;
            public Html html;

            public AsHtmlResponse(List<? extends ContributionHolder> holders, Html html) {
                this.holders = holders;
                this.html = html;
            }
        }
    }

}
