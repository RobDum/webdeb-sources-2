/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.argument;

import be.webdeb.core.api.argument.EArgumentLinkShade;
import play.data.validation.ValidationError;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArgumentContextSimpleForm {

    protected Long argumentId = -1L;
    protected Long contextId = -1L;

    protected Long linkToArgumentId = -1L;
    protected int linkShadeType = -1;

    /**
     * Play / JSON compliant constructor
     */
    public ArgumentContextSimpleForm() {
    }

    /**
     * Create a contextualized argument simple form from a given context
     *
     * @param argumentId the argument
     * @param contextId the context contribution
     * @param linkToArgumentId the contextualized argument id that will be linked (justification link) to this new one.
     * @param linkShadeType the link shade of the link between linkToArgumentId and this one.
     */
    public ArgumentContextSimpleForm(Long argumentId, Long contextId, Long linkToArgumentId, int linkShadeType) {
        this.argumentId = argumentId;
        this.contextId = contextId;
        this.linkToArgumentId = linkToArgumentId;
        this.linkShadeType = linkShadeType;
    }

    /**
     * Form validation (implicit call from form submit)
     *
     * @return a map of ValidationError if any error in form was found, null otherwise
     */
    public Map<String, List<ValidationError>> validate() {
        Map<String, List<ValidationError>> errors = new HashMap<>();

        if (contextId != null && contextId != -1L) {
            errors.put("contextId", Collections.singletonList(new ValidationError("contextId", "argument.context.error.contextId")));
        }

        if (argumentId != null) {
            errors.put("argumentId", Collections.singletonList(new ValidationError("argumentId", "argument.context.error.argumentId")));
        }

        if (linkToArgumentId != null && linkToArgumentId != -1L) {
            errors.put("linkToArgumentId", Collections.singletonList(new ValidationError("linkToArgumentId", "argument.context.error.linkToArgumentId")));
        }

        if (linkShadeType != -1L && EArgumentLinkShade.value(linkShadeType) != null) {
            errors.put("linkShadeType", Collections.singletonList(new ValidationError("linkShadeType", "argument.context.error.linkShadeType")));
        }

        // must return null if errors is empty
        return errors.isEmpty() ? null : errors;
    }

    /*
     * Getter and Setter
     */

    public Long getArgumentId() {
        return argumentId;
    }

    public void setArgumentId(Long argumentId) {
        this.argumentId = argumentId;
    }

    public Long getContextId() {
        return contextId;
    }

    public void setContextId(Long contextId) {
        this.contextId = contextId;
    }

    public Long getLinkToArgumentId() {
        return linkToArgumentId;
    }

    public void setLinkToArgumentId(Long linkToArgumentId) {
        this.linkToArgumentId = linkToArgumentId;
    }

    public int getLinkShadeType() {
        return linkShadeType;
    }

    public void setLinkShadeType(int linkShadeType) {
        this.linkShadeType = linkShadeType;
    }
}
