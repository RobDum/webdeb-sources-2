/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.folder;

import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.folder.Folder;
import be.webdeb.core.api.folder.FolderLink;
import be.webdeb.presentation.web.controllers.entry.ContributionHolder;

/**
 * This class holds concrete values of a FolderLink (i.e. no type/data IDs, but their descriptions, as
 * defined in the database)
 *
 * @author Martin Rouffiange
 */
public class FolderLinkHolder extends ContributionHolder {

  // custom logger
  protected static final org.slf4j.Logger logger = play.Logger.underlying();

  // Note: as for all wrappers, all fields MUST hold empty values for proper form validation
  protected Long parentId = -1L;
  protected Long childId = -1L;

  // for lazy loading of hierarchy
  private Folder parent;
  private Folder child;

  /**
   * Play / JSON compliant constructor
   */
  public FolderLinkHolder() {
    super();
    type = EContributionType.FOLDERLINK;
  }

  /**
   * Construct a folder link wrapper with a given parent and child
   *
   * @param parent a parent folder
   * @param child a child folder
   * @param lang 2-char ISO code of context language (among play accepted languages)
   */
  public FolderLinkHolder(Folder parent, Folder child, String lang) {
    this.parent = parent;
    parentId = parent.getId();
    this.child = child;
    childId = child.getId();
    this.lang = lang;
  }

  /**
   * Construct a folder link wrapper with a given FolderLink
   *
   * @param link an existing FolderLink
   * @param lang 2-char ISO code of context language (among play accepted languages)
   */
  public FolderLinkHolder(FolderLink link, String lang) {
    super(link, lang);
    parent = link.getParent();
    parentId = parent.getId();
    child = link.getChild();
    childId = child.getId();
    this.lang = lang;
  }

  /**
   * Construct a folder link wrapper with a given parent and child id
   *
   * @param parent a parent folder id
   * @param child a child folder id
   * @param lang 2-char ISO code of context language (among play accepted languages)
   */
  public FolderLinkHolder(Long parent, Long child, String lang) {
    this.parentId = parent;
    this.childId = child;
    this.lang = lang;
  }

  /**
   * Check if all fields are empty
   *
   * @return true if all fields of this are considered as empty (@see Values.isBlank)
   */
  public boolean empty() {
    return (values.isBlank(parentId) || parentId.equals(-1L))
        && (values.isBlank(childId) || childId.equals(-1L));
  }

  @Override
  public MediaSharedData getMediaSharedData() {
    return null;
  }

  @Override
  public String getContributionDescription() {
    return null;
  }

  @Override
  public String getDefaultAvatar(){
    return "";
  }

  @Override
  public String toString() {
    return "folder link : parent [" + parentId + "] " + (parent != null ? parent.getDefaultName() : "")
         + " parent [" + childId + "] " + (child != null ? child.getDefaultName() : "");
  }

  /*
   * GETTERS
   */

  /**
   * Get the id of the parent folder
   *
   * @return a folder id
   */
  public Long getParentId() {
    return parentId;
  }

  /**
   * Get the id of the child folder
   *
   * @return a folder id
   */
  public Long getChildId() {
    return childId;
  }

  /**
   * Get the the parent folder
   *
   * @return a folder
   */
  public Folder getParent() {
    return parent;
  }

  /**
   * Get the id of the child folder
   *
   * @return a folder
   */
  public Folder getChild() {
    return child;
  }
}
