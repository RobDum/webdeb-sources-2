/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.folder;

import be.objectify.deadbolt.java.actions.Restrict;
import be.webdeb.application.query.BadQueryException;
import be.webdeb.application.query.EQueryKey;
import be.webdeb.application.query.QueryExecutor;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.folder.*;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.infra.fs.FileSystem;
import be.webdeb.infra.persistence.model.Group;
import be.webdeb.infra.ws.nlp.RequestProxy;
import be.webdeb.presentation.web.controllers.CommonController;
import be.webdeb.presentation.web.controllers.EDataSendType;
import be.webdeb.presentation.web.controllers.entry.*;
import be.webdeb.presentation.web.controllers.entry.routes;
import be.webdeb.presentation.web.controllers.permission.WebdebRole;
import be.webdeb.presentation.web.controllers.SessionHelper;
import be.webdeb.presentation.web.controllers.permission.WebdebUser;
import be.webdeb.presentation.web.controllers.viz.EVizPane;
import be.webdeb.presentation.web.views.html.entry.folder.editFolder;
import be.webdeb.presentation.web.views.html.entry.folder.editFolderModal;
import be.webdeb.presentation.web.views.html.entry.folder.editFolderFields;
import be.webdeb.presentation.web.views.html.util.message;
import be.webdeb.presentation.web.views.html.util.handleFolderNameMatches;
import play.data.Form;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;
import play.twirl.api.Html;

import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;


/**
 * Folder-related actions, ie controller of all pages to edit folders
 *
 * @author Martin Rouffiange
 */
public class FolderActions extends CommonController {

  @Inject
  protected FileSystem files;

  @Inject
  protected RequestProxy proxy;

  @Inject
  private QueryExecutor executor;

  private static final String NOTFOUND = "folder.notfound";

  /**
   * Get the folder edition page for given text id
   *
   * @param id a folder id (-1 or null for a new folder)
   * @return the folder form to either add a new folder or modify an existing one
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> edit(Long id) {
    logger.debug("GET edit folder page " + id);
    Map<String, String> messages = new HashMap<>();
    WebdebUser user = sessionHelper.getUser(ctx());
    Folder folder = folderFactory.retrieve(id);
    FolderForm form;

    if (folder != null) {
      form = new FolderForm(folder, user.getId(), ctx().lang().code());
      form.setInGroup(Group.getPublicGroup().getIdGroup());
    } else {
      if (id != -1L) {
        messages.put(SessionHelper.ERROR, i18n.get(ctx().lang(), NOTFOUND));
      }
      form = new FolderForm();
      // set default group
      form.setInGroup(Group.getPublicGroup().getIdGroup());
      // set current language
      form.setLang(ctx().lang().code());
    }

    return CompletableFuture.supplyAsync(() ->
        ok(editFolder.render(formFactory.form(FolderForm.class).fill(form), helper, user, messages)), context.current());
  }

  /**
   * Update or save a folder into the database.
   *
   * @param id the folder id (-1 of null for a new folder)
   * @return either the addFolder page if form contains error, if it appears to already exist
   * its visualisation page if the action succeeded, or redirect to main entry page if an error occurred.
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public synchronized CompletionStage<Result> save(Long id) {
    logger.debug("POST save folder " + id);
    Form<FolderForm> form = formFactory.form(FolderForm.class).bindFromRequest();
    try {
      FolderForm result = saveFolder(form);

      // all good -> go to this folder's page (and remove session key_goto if any)
      sessionHelper.remove(ctx(), SessionHelper.KEY_GOTO);
      flash(SessionHelper.SUCCESS, i18n.get(ctx().lang(),"entry.folder.save") + " " + result.getFolderName());
      return CompletableFuture.supplyAsync(() ->
              redirect(be.webdeb.presentation.web.controllers.viz.routes.VizActions.folder(result.getId(), EVizPane.CARTO.id(), 0)),
          context.current());

    } catch (FolderNotSavedException e) {
      return handleFolderError(e, false);
    }
  }

  /**
   * Update or create a folder from a modal form
   *
   * @return either the addFolderFields form if the form contains errors (code 400),
   * the modal page with next (autocreated) folder to be added or a confirmation message if the action succeeded
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> saveFromModal() {
    logger.debug("POST save folder from modal");

    Map<String, String> messages = new HashMap<>();
    Form<FolderForm> form = formFactory.form(FolderForm.class).bindFromRequest();

    try {
      FolderForm folderForm = saveFolder(form);
      messages.put(SessionHelper.SUCCESS, i18n.get(ctx().lang(),"entry.folder.save") + " " + folderForm.getFolderName());

      // remove folder from session (new ones have been added in saveFolder)
      sessionHelper.removeValue(ctx(), SessionHelper.KEY_NEWFOLDER, String.valueOf(folderForm.getId()));

      // get next modal, if any
      Html modal = getAutoCreatedFolderModal(ctx(), messages);
      return CompletableFuture.supplyAsync(() ->
          modal != null ? ok(modal) : ok(message.render(messages)), context.current());

    } catch (FolderNotSavedException e) {
      return handleFolderError(e, true);
    }
  }

  /**
   * Save a link between two folders
   *
   * @param parent the parent folder
   * @param child the child folder
   *
   * @return redirect to either the cached goto key or the referer page
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> saveFolderLink(Long parent, Long child) {
    logger.info("GET save folder link direct between " + parent + " and " + child);

    String goTo = sessionHelper.get(ctx(), SessionHelper.KEY_GOTO);
    if (goTo == null) {
      goTo = sessionHelper.getReferer(ctx());
    }
    String finalGoto = goTo;
    sessionHelper.remove(ctx(), SessionHelper.KEY_GOTO);
    sessionHelper.remove(ctx(), SessionHelper.KEY_NEWFOLDERLINK);

    Folder p = folderFactory.retrieve(parent);
    Folder c = folderFactory.retrieve(child);
    if (p == null || c == null) {
      logger.error("a given reference is null or unfound");
      flash(SessionHelper.ERROR, i18n.get(ctx().lang(),NOTFOUND));
      return CompletableFuture.supplyAsync(() -> redirect(finalGoto), context.current());
    }

    // check if no such link exists
    if (folderFactory.retrieveLink(parent, child) != null) {
      logger.warn("such folder link already exist between " + parent + " " + child);
      flash(SessionHelper.WARNING, i18n.get(ctx().lang(),"folder.link.alreadyexist"));
      return CompletableFuture.supplyAsync(() -> redirect(finalGoto), context.current());
    }

    try {
      // set up link fields and save
      FolderLink link = folderFactory.getFolderLink();
      int group = Group.getPublicGroup().getIdGroup();
      link.setParent(p);
      link.setChild(c);
      link.addInGroup(group);
      link.save(sessionHelper.getUser(ctx()).getContributor().getId(), group);
      flash(SessionHelper.SUCCESS, i18n.get(ctx().lang(),"folder.link.added"));
    } catch (FormatException | PersistenceException | PermissionException e) {

      flash(SessionHelper.ERROR, i18n.get(ctx().lang(),"error.crash"));
      logger.error("unable to save new folder link", e);
    }

    // redirect to final goto page (as stored in session)
    return CompletableFuture.supplyAsync(() -> redirect(finalGoto), context.current());
  }

  /**
   * Ask to work with a randomly chosen folder
   *
   * @return redirect to "work with folder page" with a randomly chosen folder
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> randomText() {
    logger.debug("GET random folder");

    Folder random = folderFactory.random();
    if(random != null) {
      return CompletableFuture.supplyAsync(() ->
              redirect(be.webdeb.presentation.web.controllers.viz.routes.VizActions
                      .dispatch(random.getId(), EContributionType.FOLDER.id(), 0)), context.current());
    }

    return CompletableFuture.completedFuture(Results.badRequest());
  }

  /**
   * Add a contribution in given folder
   *
   * @param folderId a folder id
   * @return undefined yet
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> addContribution(Long folderId) {
    logger.debug("POST add contribution to folder " + folderId);
    return null;
  }

  /**
   * Remove given contribution from fiven folder
   *
   * @param folderId a folder id
   * @param contributionId a contribution id
   * @return undefined yet
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> removeContribution(Long folderId, Long contributionId) {
    return null;
  }

  /*
   * AJAX calls
   */

  /**
   * Search for a folder based on given query.
   *
   * @param term the values to search for
   * @param idToIgnore the folder id to ignore, or null
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return a jsonified list of folder holders
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> searchFolder(String term, Long idToIgnore, int fromIndex, int toIndex) {
    List<SimpleFolderForm> result = new ArrayList<>();
    List<Map.Entry<EQueryKey, String>> query = new ArrayList<>();
    query.add(new AbstractMap.SimpleEntry<>(EQueryKey.CONTRIBUTION_TYPE, String.valueOf(EContributionType.FOLDER.id())));
    query.add(new AbstractMap.SimpleEntry<>(EQueryKey.FOLDER_NAME, term));
    if(idToIgnore != null)
      query.add(new AbstractMap.SimpleEntry<>(EQueryKey.ID_IGNORE, idToIgnore+""));

    try {
      WebdebUser user = sessionHelper.getUser(ctx());
      String lang = ctx().lang().code();
      for(Contribution c : executor.searchContributions(query, fromIndex, toIndex).stream().filter(sessionHelper.getUser(ctx())::mayView).collect(Collectors.toList())) {
        result.add(new SimpleFolderForm((Folder) c, user, lang));
      }
    } catch (BadQueryException e) {
      logger.warn("unable to search for folders with given term " + term, e);
    }
    return CompletableFuture.supplyAsync(() -> ok(Json.toJson(result)), context.current());
  }


  /**
   * Build a list of FolderForm with all folders present in session (auto-created folders from other contributions)
   *
   * @return the list of FolderForm containing all folders stored in session or an empty bad request (400) if none present
   */
  public CompletionStage<Result> getAutoCreatedFolders() {
    if (!values.isBlank(sessionHelper.getUser(ctx()).getId())) {
      Html modal = getAutoCreatedFolderModal(ctx(), null);
      return CompletableFuture.supplyAsync(() -> modal != null ? ok(modal) : badRequest(Json.toJson("")), context.current());
    }
    return CompletableFuture.completedFuture(badRequest(Json.toJson("")));
  }

  /**
   * Find the list of contextualized arguments that are contained in a given folder limited by index
   *
   * @param folder a folder id
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return a jsonified list of holders or bad request if folder is unknown
   */
  public CompletionStage<Result> findFolderArguments(Long folder, int fromIndex, int toIndex) {
    logger.debug("Find folder arguments for folder " + folder + " limited : " + fromIndex + ", " + toIndex);
    Folder f = folderFactory.retrieve(folder);

    if(f != null) {
      return sendPartialLoadedContributions(f.getContextualizedArguments(fromIndex, toIndex),
              fromIndex, toIndex, folder, EContributionType.ARGUMENT_CONTEXTUALIZED, EDataSendType.AS_HTML_TEMPLATE_1);
    }
    return CompletableFuture.supplyAsync(Results::badRequest, context.current());
  }

  /**
   * Find the list of excerpts that are contained in a given folder limited by index
   *
   * @param folder a folder id
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return a jsonified list of holders or bad request if folder is unknown
   */
  public CompletionStage<Result> findFolderExcerpts(Long folder, int fromIndex, int toIndex) {
    logger.debug("Find folder excerpts for folder " + folder + " limited : " + fromIndex + ", " + toIndex);
    Folder f = folderFactory.retrieve(folder);

    if(f != null) {
      return sendPartialLoadedContributions(f.getExcerpts(fromIndex, toIndex),
              fromIndex, toIndex, folder, EContributionType.EXCERPT, EDataSendType.AS_HTML_TEMPLATE_1);
    }
    return CompletableFuture.supplyAsync(Results::badRequest, context.current());
  }

  /**
   * Find the list of debates that are contained in a given folder limited by index
   *
   * @param folder a folder id
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return a jsonified list of holders or bad request if folder is unknown
   */
  public CompletionStage<Result> findFolderDebates(Long folder, int fromIndex, int toIndex) {
    logger.debug("Find folder debates for folder " + folder + " limited : " + fromIndex + ", " + toIndex);
    Folder f = folderFactory.retrieve(folder);

    if(f != null) {
      return sendPartialLoadedContributions(f.getDebates(fromIndex, toIndex),
              fromIndex, toIndex, folder, EContributionType.DEBATE, EDataSendType.AS_HTML_TEMPLATE_1);
    }
    return CompletableFuture.supplyAsync(Results::badRequest, context.current());
  }

  /**
   * Find the list of texts that are contained in a given folder limited by index
   *
   * @param folder a folder id
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return a jsonified list of holders or bad request if folder is unknown
   */
  public CompletionStage<Result> findFolderTexts(Long folder, int fromIndex, int toIndex) {
    logger.debug("Find folder texts for folder " + folder + " limited : " + fromIndex + ", " + toIndex);
    Folder f = folderFactory.retrieve(folder);

    if(f != null) {
      return sendPartialLoadedContributions(f.getTexts(fromIndex, toIndex),
              fromIndex, toIndex, folder, EContributionType.TEXT, EDataSendType.AS_HTML_TEMPLATE_1);
    }
    return CompletableFuture.supplyAsync(Results::badRequest, context.current());
  }

  /*
   * PRIVATE HELPERS
   */

  /**
   * Helper method to build a folder modal frame
   *
   * @param context the HTTP request context
   * @param messages messages to be shown on modal frame
   * @return the add folder modal frame if there are still auto-created folders, null otherwise
   */
  private Html getAutoCreatedFolderModal(Http.Context context, Map<String, String> messages) {
    // check session cache for possible folders to be filled by user (automatic creation)
    List<String> ids = sessionHelper.getValues(context, SessionHelper.KEY_NEWFOLDER);
    WebdebUser user = sessionHelper.getUser(ctx());

    if (ids != null && !ids.isEmpty()) {
      logger.debug("will ask details about " + ids.get(0));
      try {
        Folder f = folderFactory.retrieve(Long.parseLong(ids.get(0)));
        if (f != null) {
          FolderForm form = new FolderForm(f, user.getId(), context.lang().code());
          form.setInGroup(Group.getPublicGroup().getIdGroup());
          return editFolderModal.render(
              formFactory.form(FolderForm.class).fill(form), helper, sessionHelper.getUser(ctx()), messages);

        } else {
          logger.error("unable to retrieve folder " + ids.get(0));
        }
      } catch (NumberFormatException e) {
        logger.error("unaparsable folder id " + ids.get(0));
      }
      sessionHelper.removeValue(ctx(), SessionHelper.KEY_NEWFOLDER, ids.get(0));
    }
    // return null if nothing to show
    return null;
  }

  /**
   * Handle error on folder form submission and returns the folder form view (depending on the switch).
   * If an unknown error occurred, either a "goto" page or the general entry view is returned.
   *
   * @param exception the exception raised from unsuccessful save
   * @param onlyFields switch to know if the addFolderFields or addFolder views must be returned
   * @return if the form contains error, a bad request (400) response is returned with, if onlyfield, the
   * editFolderFields template or the editFolder full form otherwise. In case of possible name matches, a
   * 409 response is returned with the modal frame to select among possible matches. If another error occurred,
   * a redirect to either a "goto" session-cached url or the main entry page.
   */
  private CompletionStage<Result> handleFolderError(FolderNotSavedException exception, boolean onlyFields) {
    Map<String, String> messages = new HashMap<>();
    WebdebUser user = sessionHelper.getUser(ctx());
    Form<FolderForm> form = exception.form;
    FolderForm folder = form.get();
    switch (exception.error) {
      case FolderNotSavedException.NAME_MATCH:
        if (onlyFields) {
          // we are handling a folder from a modal, only send modal with 409 status code
          // must find the right list of name matches
          NameMatch<FolderHolder> namematch = new ConcreteNameMatch<>(0, folder.getCandidates());
          return CompletableFuture.supplyAsync(() ->
              status(409, handleFolderNameMatches.render(namematch.getNameMatches(), namematch.getIndex())), context.current());
        } else {
          // full folder form => send full page that will trigger modal
          return CompletableFuture.supplyAsync(() ->
              badRequest(editFolder.render(form, helper, sessionHelper.getUser(ctx()), messages)), context.current());
        }
      case FolderNotSavedException.HIERARCHY_NOMATCH:
        if (onlyFields && (folder.getParentThatNameMatch() != null || folder.getChildThatNameMatch() != null)) {
          boolean parentMatch = folder.getParentThatNameMatch() != null;
          NameMatch<FolderHolder> namematch = new ConcreteNameMatch<>(0, (parentMatch ? folder.getParentThatNameMatch() : folder.getChildThatNameMatch()).getNameMatches());
          return CompletableFuture.supplyAsync(() -> status(parentMatch ? 409 : 410,
                  handleFolderNameMatches.render(namematch.getNameMatches(), namematch.getIndex())), context.current());
        }
        return CompletableFuture.supplyAsync(() -> badRequest(editFolder.render(form, helper, sessionHelper.getUser(ctx()), messages)), context.current());

      case FolderNotSavedException.ERROR_FORM:
        // error in form, just resend it
        messages.put(SessionHelper.WARNING, i18n.get(ctx().lang(), "error.form"));

        return CompletableFuture.supplyAsync(() -> onlyFields ?
            badRequest(editFolderFields.render(form, helper, user, messages))
            : badRequest(editFolder.render(form, helper, sessionHelper.getUser(ctx()), messages)), context.current());

      default:
        // any other error, check where do we have to go after and show message in exception
        String goTo = sessionHelper.get(ctx(), SessionHelper.KEY_GOTO);
        if (goTo == null) {
          goTo = routes.EntryActions.contribute().url();
        } else {
          // will consume next page
          sessionHelper.remove(ctx(), SessionHelper.KEY_GOTO);
        }
        String finalGoTo = goTo;
        flash(SessionHelper.ERROR, exception.getMessage());
        messages.put(SessionHelper.ERROR, i18n.get(ctx().lang(), "error.crash"));
        return CompletableFuture.supplyAsync(() -> onlyFields ?
            internalServerError(message.render(messages))
            : redirect(finalGoTo), context.current());
    }
  }

  /**
   * Save a folder from a given form and add in session cookie the list of auto-created folders' id if any
   *
   * @param form an folder form object that may contain errors
   * @return given (updated) folder form
   * @throws FolderNotSavedException if an error exist in passed form or any error arisen from save action
   */
  private synchronized FolderForm saveFolder(Form<FolderForm> form) throws FolderNotSavedException {
    // sends back form if there are some errors
    if (form.hasErrors()) {
      logger.debug("form has errors " + form.errors().toString());
      throw new FolderNotSavedException(form, FolderNotSavedException.ERROR_FORM);
    }
    FolderForm folder = form.get();
    // check if matches exists for this folder, if any, return badRequest that will construct a pop-up form to ask what to do
    if (values.isBlank(folder.getId()) && !folder.getFolderNames().isEmpty() && !folder.getIsNotSame()) {
      List<Folder> match = folderFactory.findByName(folder.getFolderNames().get(0).getName(), EFolderType.NONE);
      if (!match.isEmpty()) {
        // send back form to ask if the found texts are not this one
        folder.setCandidates(match);
        throw new FolderNotSavedException(form.fill(folder), FolderNotSavedException.NAME_MATCH);
      }
    }

    // check name matches for folders hierarchy
    if (!helper.searchForFolderNameMatches(folder.getParents(), ctx().lang().code(), folder, true).isEmpty() ||
        !helper.searchForFolderNameMatches(folder.getChildren(), ctx().lang().code(), folder, false).isEmpty()) {
      throw new FolderNotSavedException(form.fill(folder), FolderNotSavedException.HIERARCHY_NOMATCH);
    }

    // all good, let's save validated actor
    Map<Integer, List<Contribution>> toBeProposed;
    try {
      // save will return actors that have been automatically created
      toBeProposed = folder.save(sessionHelper.getUser(ctx()).getContributor().getId());
      if(toBeProposed.containsKey(EContributionType.FOLDER.id()))
        toBeProposed.get(EContributionType.FOLDER.id()).removeIf(e -> e.getId().equals(folder.getId()));
      // prepend this
    } catch (FormatException | PersistenceException | PermissionException e) {
      logger.error("unable to save folder", e);
      // both exceptions are sending message keys
      throw new FolderNotSavedException(form, FolderNotSavedException.ERROR_DB, i18n.get(ctx().lang(),e.getMessage()));
    }

    // add to session the auto-created folders
    if(toBeProposed.containsKey(EContributionType.FOLDER.id()))
      toBeProposed.get(EContributionType.FOLDER.id())
          .forEach(a -> sessionHelper.addValue(ctx(), SessionHelper.KEY_NEWFOLDER, a.getId().toString()));
    return folder;
  }

  /*
   * INNER CLASS
   */

  /**
   * Inner class to handle folder exception when a folder cannot be saved from private save execution
   */
  private class FolderNotSavedException extends Exception {

    private static final long serialVersionUID = 1L;
    final Form<FolderForm> form;
    final int error;

    static final int ERROR_FORM = 0;
    static final int NAME_MATCH = 1;
    static final int ERROR_DB = 2;
    static final int HIERARCHY_NOMATCH = 3;

    FolderNotSavedException(Form<FolderForm> form, int error) {
      this.error = error;
      this.form = form;
    }

    FolderNotSavedException(Form<FolderForm> form, int error, String message) {
      super(message);
      this.error = error;
      this.form = form;
    }
  }
}
