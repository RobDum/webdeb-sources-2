/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.argument;

import be.webdeb.core.api.argument.Argument;
import be.webdeb.core.api.argument.ArgumentFactory;
import be.webdeb.core.api.argument.ArgumentType;
import be.webdeb.core.api.argument.EArgumentLinkShade;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.presentation.web.controllers.entry.EFilterName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import play.api.Play;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * This class holds concrete values of an Argument Holder (no IDs, but their description, as defined in the
 * database). Except by using a constructor, no value can be edited outside of this package or by
 * subclassing.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public class ArgumentHolder extends BaseArgumentHolder {

  static final int TITLE_MAX_LENGTH = 512;

  @Inject
  protected ArgumentFactory argumentFactory = Play.current().injector().instanceOf(ArgumentFactory.class);

  // used for lazy loading of argument and text title
  protected Argument argument;
  // the argument link id where the argument come from (form similarity map)
  protected Long argumentLinkId = -1L;

  // type, subtype, etc are initialized with -1 instead of empty values
  protected String argtype = "-1";
  protected String shadeterm;

  // all similarity links with this argument
  protected List<SimilarityLinkForm> similarityLinks;

  /**
   * Play / JSON compliant constructor
   */
  public ArgumentHolder() {
    super();
    type = EContributionType.ARGUMENT;
  }

  /**
   * Construct a ReadOnlyArgument from a given argument with descriptions in the given language
   *
   * @param argument an Argument
   * @param lang 2-char ISO code of context language (among play accepted languages)
   */
  public ArgumentHolder(Argument argument, String lang) {
    super(argument, lang);
    this.argument = argument;

    title = argument.getDictionary().getTitle();
    fullTitle = argument.getFullTitle();
    language = argument.getDictionary().getLanguage().getName(lang);
    languageCode = argument.getDictionary().getLanguage().getCode();
    addFilterable(EFilterName.LANGUAGE, language);

    ArgumentType argumentType = argument.getArgumentType();
    argtype = argumentType.getTypeName(languageCode);
    shade = String.valueOf(argumentType.getArgumentShade());
    shadeterm = argumentType.getShadeName(lang);
    addFilterable(EFilterName.ARGTYPE, shadeterm);
  }


  /**
   * Construct a ReadOnlyArgument from a given argument with descriptions in the given language from a similarity map
   *
   * @param argument an ARgument
   * @param argumentLink an argument link id where the argument come from (form similarity map)
   * @param lang 2-char ISO code of context language (among play accepted languages)
   */
  public ArgumentHolder(Argument argument, Long argumentLink, String lang) {
    this(argument, lang);
    this.argumentLinkId = argumentLink;
  }

  /**
   * Get the amount of argument similarity links linked with this argument that a given contributor can see by given shade
   *
   * @param contributor a contributor
   * @return a map of shade / amount
   */
  public Map<EArgumentLinkShade, Integer> getArgumentSimilarityShadesAmount(Long contributor) {
    return argument.getArgumentShadesAmount(contributor,
            Arrays.asList(EArgumentLinkShade.SIMILAR, EArgumentLinkShade.OPPOSES));
  }

  /**
   * Lazy loading of similarity links
   */
  protected void initSimilarityLinks() {
    similarityLinks = new ArrayList<>();
    argument.getSimilarityLinks().forEach(l -> similarityLinks.add(new SimilarityLinkForm(l, lang)));
  }

  @Override
  public String toString() {
    return "argument [" + id + "] of shade " + shade + "; title: " + title;
  }

  @Override
  public String getContributionDescription(){
    List<String> descriptions = new ArrayList<>();
    descriptions.add(title);

    return String.join(", ", descriptions);
  }

  @Override
  public MediaSharedData getMediaSharedData(){
    if(mediaSharedData == null){
      mediaSharedData = new MediaSharedData(title, "argument");
    }
    return mediaSharedData;
  }

  @Override
  public String getDefaultAvatar(){
    return "";
  }

  /*
   * GETTERS
   */

  /**
   * Get the argument type id (see ArgumentType interface)
   *
   * @return an argument type label (language-specific)
   */
  public String getArgtype() {
    return argtype;
  }

  /**
   * Get the argument shade term (language specific)
   *
   * @return the shade term
   */
  public String getShadeterm() {
    return shadeterm;
  }

  /**
   * Get the list of similarity links of this argument
   *
   * @return a list of similarity links
   */
  @JsonIgnore
  public List<SimilarityLinkForm> getSimilarityLinks() {
    if (similarityLinks == null) {
      initSimilarityLinks();
    }
    return similarityLinks;
  }

  /**
   * Get the argument link id where the argument come from (form similarity map)
   *
   * @return the argument link id
   */
  public Long getArgumentLinkId() {
    return argumentLinkId;
  }
}
