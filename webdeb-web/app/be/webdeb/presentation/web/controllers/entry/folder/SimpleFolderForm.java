/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.folder;

import be.webdeb.core.api.folder.EFolderType;
import be.webdeb.core.api.folder.Folder;
import be.webdeb.core.api.folder.FolderFactory;
import be.webdeb.core.api.folder.FolderName;
import be.webdeb.core.exception.FormatException;
import be.webdeb.infra.persistence.model.Group;
import be.webdeb.presentation.web.controllers.permission.WebdebUser;
import be.webdeb.util.ValuesHelper;
import org.apache.commons.lang3.StringUtils;
import play.api.Play;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Simple form for folder name
 *
 * @author Martin Rouffiange
 */
public class SimpleFolderForm {

  private Long folderId = -1L;
  private String completeName;
  private String name;
  private int folderType;
  private int nbParents = 0;
  private int nbContributions = 0;

  // will be put to true if user explicitly disambiguated this folder
  // if this id is -1, the folder will be created (whatever this folder is a namesake or not)
  // if this folder has an id, we will not check for name matches
  private boolean isDisambiguated = false;
  // list of folders whith the same name as this folder (used when folder has no id and we possibly found existing folders)
  private List<FolderHolder> nameMatches = new ArrayList<>();

  @Inject
  private ValuesHelper values = Play.current().injector().instanceOf(ValuesHelper.class);
  @Inject
  protected FolderFactory folderFactory = Play.current().injector().instanceOf(FolderFactory.class);


  // custom logger
  protected static final org.slf4j.Logger logger = play.Logger.underlying();

  /**
   * Play / JSON compliant constructor
   */
  public SimpleFolderForm () {
    // needed by play
  }

  /**
   * Creates a simple folder form from an id, a name and a folder type
   *
   * @param folder a folder
   * @param lang the user lang
   */
  public SimpleFolderForm(Folder folder, String lang) {
    this(folder, null, lang);
  }

  /**
   * Creates a simple folder form from an id, a name and a folder type
   *
   * @param folder a folder
   * @param user a WebDeb user
   * @param lang the user lang
   */
  public SimpleFolderForm(Folder folder, WebdebUser user, String lang) {
    this.folderId = folder.getId();
    this.name = folder.getName(lang);
    this.completeName = folder.getRewordingNamesByLang(lang).isEmpty() ? folder.getName(lang) :
            folder.getName(lang) + " (" + folder.getRewordingNamesByLang(lang).stream().map(FolderName::getName)
                    .collect(Collectors.joining(" ,")) + ")";
    this.folderType = folder.getFolderType().getType();
    if(folderType == EFolderType.COMPOSED.id())
      this.nbParents = StringUtils.countMatches("name", " / ");
    if(user != null)
      this.nbContributions = folder.getNbContributions(user.getId(), user.getGroup().getGroupId());
  }

  /**
   * Cast this form to an API Folder object
   *
   * @param lang the user lang
   * @return the Form holding all values of this form
   */
  public Folder toFolder(String lang) {
    logger.debug("transform folder " + toString());
    Folder folder = folderFactory.getFolder();

    // check folder name
    if (!values.isBlank(name)) {
      folder.addName(lang, name.trim());
      try {
        folder.setFolderType(folderFactory.getFolderType(EFolderType.TAG.id()));
        folder.addInGroup(Group.getPublicGroup().getIdGroup());
        return folder;
      } catch (FormatException e) {
        logger.debug("Folder type problem : " + e);
      }
    }
    return null;
  }

  /**
   * Helper method to check whether the name and id are empty (contains empty or null values)
   *
   * @return true if all fields are empty
   */
  public boolean isEmpty() {
    return values.isBlank(name);
  }

  @Override
  public String toString() {
    return "SimpleFolderForm{" +
        "folderId=" + folderId +
        ", completename='" + completeName + '\'' +
        ", name='" + name + '\'' +
        ", type='" + folderType + '\'' +
        '}';
  }

  /*
   * GETTERS / SETTERS
   */

  public Long getFolderId() {
    return folderId;
  }

  public void setFolderId(Long folderId) {
    this.folderId = folderId;
  }

  public String getCompleteName() {
    return completeName;
  }

  public void setCompleteName(String completeName) {
    this.completeName = completeName;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getFolderType() {
    return folderType;
  }

  public void setFolderType(int folderType) {
    this.folderType = folderType;
  }

  public int getNbParents() {
    return nbParents;
  }

  public void setNbParents(int nbParents) {
    this.nbParents = nbParents;
  }

  /*
   * NAME MATCHES HANDLING
   */

  /**
   * Get the isDisambiguated flag, ie, if this flag is false and this folder has no id, a search by name (full match)
   * must be performed at form validation time to try to retrieve a possible match in database.
   * Otherwise (this flag is true), no check must be performed whatever the id value is
   *
   * @return true if this folder has been explicitly disambiguated
   */
  public boolean getIsDisambiguated() {
    return isDisambiguated;
  }

  /**
   * Set the isDisambiguated flag, ie, if this flag is false and this folder has no id, a search by name (full match)
   * must be performed at form validation time to try to retrieve a possible match in database.
   * Otherwise (this flag is true), no check must be performed whatever the id value is
   *
   * @param isDisambiguated true if this folder has been explicitly disambiguated
   */
  public void setIsDisambiguated(boolean isDisambiguated) {
    this.isDisambiguated = isDisambiguated;
  }

  /**
   * Get the list  possible matches on this folder's name
   *
   * @return a (possibly empty) list of folder holder having the same name as this folder
   */
  public List<FolderHolder> getNameMatches() {
    return nameMatches;
  }

  /**
   * Set the list of possible matches on this folder's full name
   *
   * @param nameMatches a list of folder holder having the same name as this folder
   */
  public void setNameMatches(List<FolderHolder> nameMatches) {
    this.nameMatches = nameMatches;
  }

  /**
   * Get the number of contributions contained in this folder
   *
   * @return the number of contributions contained in this folder
   */
  public int getNbContributions(){
    return nbContributions;
  }
}
