/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.argument;

import be.objectify.deadbolt.java.actions.Restrict;
import be.webdeb.application.query.BadQueryException;
import be.webdeb.application.query.EQueryKey;
import be.webdeb.core.api.actor.Actor;
import be.webdeb.core.api.argument.*;
import be.webdeb.core.api.contribution.ContextContribution;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.debate.Debate;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.core.api.excerpt.ExcerptFactory;
import be.webdeb.core.api.text.Text;
import be.webdeb.core.api.text.TextFactory;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.infra.fs.FileSystem;
import be.webdeb.infra.ws.nlp.RequestProxy;
import be.webdeb.presentation.web.controllers.CommonController;
import be.webdeb.presentation.web.controllers.browse.SearchForm;
import be.webdeb.presentation.web.controllers.entry.*;
import be.webdeb.presentation.web.controllers.entry.actor.ActorHolder;
import be.webdeb.presentation.web.controllers.entry.actor.ActorSimpleForm;
import be.webdeb.presentation.web.controllers.entry.debate.DebateHolder;
import be.webdeb.presentation.web.controllers.entry.excerpt.ExcerptForm;
import be.webdeb.presentation.web.controllers.entry.excerpt.ExcerptHolder;
import be.webdeb.presentation.web.controllers.entry.folder.FolderHolder;
import be.webdeb.presentation.web.controllers.permission.WebdebRole;
import be.webdeb.presentation.web.controllers.SessionHelper;
import be.webdeb.presentation.web.controllers.permission.WebdebUser;
import be.webdeb.presentation.web.controllers.viz.EVizPane;
import be.webdeb.presentation.web.controllers.viz.EVizPaneName;
import be.webdeb.presentation.web.controllers.viz.argument.*;
import be.webdeb.presentation.web.controllers.viz.excerpt.ExcerptSociography;
import be.webdeb.presentation.web.controllers.viz.excerpt.EExcerptGroupKey;
import be.webdeb.presentation.web.views.html.entry.argument.editArgumentDictionaryModal;
import be.webdeb.presentation.web.views.html.entry.argumentcontext.editArgumentContext;
import be.webdeb.presentation.web.views.html.entry.argumentcontext.editArgumentContextModal;
import be.webdeb.presentation.web.views.html.entry.argumentcontext.argumentContextSelection;
import be.webdeb.presentation.web.views.html.util.*;
import be.webdeb.presentation.web.views.html.viz.argument.*;
import be.webdeb.presentation.web.views.html.viz.privateContribution;
import be.webdeb.presentation.web.views.html.viz.argumentExcerptsListDiv;
import play.data.Form;
import play.libs.Json;
import play.mvc.*;

import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

/**
 * Argument-related actions, ie controller of all pages to edit arguments
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public class ArgumentActions extends CommonController {

  @Inject
  protected FileSystem files;

  @Inject
  protected RequestProxy proxy;

  private ArgumentContextVizHolder viz = null;

  // custom logger
  private static final org.slf4j.Logger logger = play.Logger.underlying();

  private static final String OOPS = "argument.properties.oops";

  /**
   * Ask page to add a new contextualized argument
   *
   * @return the form page where users may add details regarding a selected argument, or redirect to
   * to the session "goto" page if passed form contains error (should not happen since form is created
   * implicitly from request and not by user)
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> newContextualizedArgument() {
    logger.debug("POST new contextualized argument");
    Form<ArgumentContextSimpleForm> form = formFactory.form(ArgumentContextSimpleForm.class).bindFromRequest();

    if (form.hasErrors()) {
      // should definitely not happen
      logger.debug("form has error (should not happen here) " + form.errors().toString());
      flash(SessionHelper.ERROR, i18n.get(ctx().lang(), "argument.text.notfound"));
      return redirectToGoTo(null);
    }

    ArgumentContextSimpleForm newArg = form.get();

    // does context contribution exists and contribution type is debate or text ?
    Contribution contributionContext = textFactory.retrieveContribution(newArg.getContextId());
    if (contributionContext == null || contributionContext.getContributionType().getEContributionType() != EContributionType.DEBATE ||
            contributionContext.getContributionType().getEContributionType() != EContributionType.TEXT) {
      flash(SessionHelper.ERROR, i18n.get(ctx().lang(),"argument.context.context.notfound"));
      sessionHelper.remove(ctx(), SessionHelper.KEY_GOTO);
      return redirectToGoTo(contributionContext == null ? null :
              be.webdeb.presentation.web.controllers.viz.routes.VizActions.dispatch(
                      newArg.getContextId(), contributionContext.getContributionType().getContributionType(), 0));
    }

    // does linked argument exists ?
    ArgumentContext argumentContext = argumentFactory.retrieveContextualized(newArg.getLinkToArgumentId());
    if (argumentContext == null) {
      flash(SessionHelper.ERROR, i18n.get(ctx().lang(),"argument.context.argumentLinked.notfound"));
      sessionHelper.remove(ctx(), SessionHelper.KEY_GOTO);
      return redirectToGoTo(be.webdeb.presentation.web.controllers.viz.routes.VizActions.dispatch(
                      newArg.getContextId(), contributionContext.getContributionType().getContributionType(), 0));
    }

    // pre-fill argument and current group
    ArgumentContextForm argForm = new ArgumentContextForm(newArg.getArgumentId(), newArg.getContextId(), ctx().lang().code());

    argForm.setInGroup(sessionHelper.getCurrentGroup(ctx()));
    argForm.setLang(ctx().lang().code());

    // display first page of argument edition
    return CompletableFuture.supplyAsync(() ->
                    ok(editArgumentContext.render(
                            formFactory.form(ArgumentContextForm.class).fill(argForm),
                            helper,
                            sessionHelper.getUser(ctx()),
                            null)),
            context.current());
  }

  /**
   * Display the page to edit the contextualized argument properties
   *
   * @param argId the contextualized argument id to edit
   * @return the property page with given argument data, or redirect to the session "goto" page with an error message
   * to display (flash) if contextualized argument is not found
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> editContextualized(Long argId) {
    logger.debug("GET argument properties of " + argId);
    return editArgumentContext(argId, false);
  }


  /**
   * Display the modal to edit the contextualized argument properties
   *
   * @param argId the contextualized argument id to edit
   * @return the edit modal page with given argument data, or redirect to the session "goto" page with an error message
   * to display (flash) if contextualized argument is not found
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> editContextualizedFromModal(Long argId) {
    logger.debug("GET argument properties of " + argId);
    return editArgumentContext(argId, true);
  }

  /**
   * Cancel edition of given argument and either redirect to session-cached "goto", or argument viz page
   *
   * @param argId an argument (or contextualized argument) id
   * @return either the session "goto" page or argument viz page
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> cancel(Long argId) {
    logger.debug("GET cancel edit argument for " + argId);
    Contribution c = argumentFactory.retrieveContribution(argId);
    Call defaultCall;
    if(c == null || (c.getType() != EContributionType.ARGUMENT && c.getType() != EContributionType.ARGUMENT_CONTEXTUALIZED)){
      defaultCall = null;
    }else{
      defaultCall = be.webdeb.presentation.web.controllers.viz.routes.VizActions.dispatch(argId, c.getType().id(), 0);
    }
    // check where we have to go after in case of cancellation or happy ending
    return redirectToGoTo(defaultCall);
  }

  /**
   * Update or create a contextualized argument
   *
   * @param argId the contextualized argument id (-1 if new one)
   * @return the argument context form object if it contains errors, or the context contribution page if this new
   * contextualized argument has been created to be linked to another one, or in case of success or DB crash,
   * redirect to "goto" session key url.
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> saveContextualized(Long argId) {
    logger.debug("POST save properties of " + argId);
    Form<ArgumentContextForm> form = formFactory.form(ArgumentContextForm.class).bindFromRequest();
    try {
      ArgumentContextForm result = saveArgumentContext(form);

      // all good -> go to this actor's page (and remove session key_goto if any)
      sessionHelper.remove(ctx(), SessionHelper.KEY_GOTO);
      ctx().flash().put(SessionHelper.SUCCESS, i18n.get(ctx().lang(),"argument.properties.added"));
      return CompletableFuture.supplyAsync(() ->
                      redirect(be.webdeb.presentation.web.controllers.viz.routes.VizActions.argumentContext(result.getId(), EVizPane.CARTO.id(), 0)),
              context.current());

    } catch (ArgumentContextNotSavedException e) {
      return handleArgumentContextError(e, false);
    }
  }

  /**
   * Update or create a contextualized argument
   *
   * @param argId the contextualized argument id (-1 if new one)
   * @return the argument context form object if it contains errors, or the context contribution page if this new
   * contextualized argument has been created to be linked to another one, or in case of success or DB crash,
   * redirect to "goto" session key url.
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> saveContextualizedFromModal(Long argId) {
    logger.debug("POST save properties of " + argId);

    Map<String, String> messages = new HashMap<>();
    Form<ArgumentContextForm> form = formFactory.form(ArgumentContextForm.class).bindFromRequest();

    try {
      saveArgumentContext(form);
      return CompletableFuture.supplyAsync(() -> ok(message.render(messages)), context.current());

    } catch (ArgumentContextNotSavedException e) {
      return handleArgumentContextError(e, true);
    }
  }

  /**
   * Update or create a contextualized argument
   *
   * @param argId the contextualized argument id (-1 if new one)
   * @return the argument context form object if it contains errors, or the context contribution page if this new
   * contextualized argument has been created to be linked to another one, or in case of success or DB crash,
   * redirect to "goto" session key url.
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> editDictionary(Long argId) {
    logger.debug("GET edit properties of " + argId);

    Map<String, String> messages = new HashMap<>();
    ArgumentDictionary dictionary = argumentFactory.retrieveDictionary(argId);
    if (dictionary == null) {
      return CompletableFuture.supplyAsync(Results::badRequest, context.current());
    }

    ArgumentDictionaryForm form = new ArgumentDictionaryForm(dictionary);

    return CompletableFuture.supplyAsync(() -> ok(editArgumentDictionaryModal.render(
            formFactory.form(ArgumentDictionaryForm.class).fill(form),
            helper,
            sessionHelper.getUser(ctx()),
            messages
    )), context.current());
  }

  /**
   * Update or create a contextualized argument
   *
   * @param argId the contextualized argument id (-1 if new one)
   * @return the argument context form object if it contains errors, or the context contribution page if this new
   * contextualized argument has been created to be linked to another one, or in case of success or DB crash,
   * redirect to "goto" session key url.
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> saveDictionary(Long argId) {
    logger.debug("POST save properties of " + argId);

    Map<String, String> messages = new HashMap<>();
    Form<ArgumentDictionaryForm> form = formFactory.form(ArgumentDictionaryForm.class).bindFromRequest();

    // check errors, sends back whole form if any
    if (form.hasErrors()) {
      logger.debug("form has errors " + form.errors());
      return CompletableFuture.supplyAsync(Results::badRequest, context.current());
    }

    // try to save in DB
    try {
      ArgumentDictionaryForm dictionary = form.get();
      dictionary.save();
    } catch (FormatException | PersistenceException e) {
      logger.error("unable to save argument dictionary", e);
      return CompletableFuture.supplyAsync(Results::internalServerError, context.current());
    }
    return CompletableFuture.supplyAsync(() -> ok(message.render(messages)), context.current());
  }


  /**
  /**
   * Get argument selection modal to select a contextualized argument to be bound to given contextualized argument or excerpt with given shade
   *
   * @param contextId the context contribution where add this link
   * @param argExcId a contextualized argument id
   * @param shade a contextualized argument link shade, if any
   * @return the argumentSelection page to look for a text to start with
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> argumentContextSelection(Long contextId, Long argExcId, int shade) {
    logger.debug("GET contextualized argument selection modal to add link from " + argExcId + " with shade " + shade + " in the context " + contextId);

    String lang = ctx().lang().code();

    ContextContribution contextContribution = textFactory.retrieveContextContribution(contextId);
    if( contextContribution == null){
      logger.error("called properties with unknown context contribution " + contextId);
      return CompletableFuture.supplyAsync(Results::badRequest, context.current());
    }

    EArgumentLinkShade type = EArgumentLinkShade.value(shade);
    Contribution argOrExc = argumentFactory.retrieveContribution(argExcId);
    EContributionType contributionType;
    ArgumentContext argument;
    Excerpt excerpt;

    if(argOrExc != null && argOrExc.getType() == EContributionType.EXCERPT){
      excerpt = excerptFactory.retrieve(argExcId);
      if (excerpt == null || !excerpt.getText().getId().equals(contextContribution.getId())) {
        logger.error("called properties with unknown excerpt " + argExcId + " or excerpt not in context " + contextId);
        return CompletableFuture.supplyAsync(Results::badRequest, context.current());
      }
      argument = null;
      contributionType = EContributionType.EXCERPT;
    }else {
      argument = argumentFactory.retrieveContextualized(argExcId == -1 ? contextContribution.getFirstArgumentId() : argExcId);
      if ((shade != -1 && type == null) || (argExcId != -1L && (argument == null
              || (!contextContribution.getFirstArgumentId().equals(argExcId) && !argument.getContextId().equals(contextContribution.getId()))))) {
        logger.error("called properties with unknown contextualized argument " + argExcId + " or shade " + shade + " or argument not in context " + contextId);
        return CompletableFuture.supplyAsync(Results::badRequest, context.current());
      }
      excerpt = null;
      contributionType = EContributionType.ARGUMENT_CONTEXTUALIZED;
    }

    // store link in user session with shade
    sessionHelper.setValues(ctx(),
            (contributionType == EContributionType.ARGUMENT_CONTEXTUALIZED ? SessionHelper.KEY_NEW_JUS_LINK : SessionHelper.KEY_NEW_ILLU_LINK),
            Arrays.asList(argExcId.toString(), String.valueOf(shade), String.valueOf(contextId)));
    sessionHelper.set(ctx(), SessionHelper.KEY_GOTO, sessionHelper.getReferer(ctx()));

    WebdebUser user = sessionHelper.getUser(ctx());

    // init search query (reuse query from search form)
    SearchForm query = new SearchForm();
    query.setAllContributionsFlags(false);
    query.setIsArgumentContext(true);
    query.setIsStrict(true);
    query.setContextToIgnore(contextId);
    query.setInGroup(user.getGroup().getGroupId());

    // init argument context form (in case of new argument)
    ArgumentContextForm argForm = new ArgumentContextForm(contextContribution.getId(), lang);
    argForm.setInGroup(user.getGroup().getGroupId());

    if(excerpt != null){
      ExcerptForm excerpForm = new ExcerptForm(excerpt, lang);
      argForm.setFolders(excerpForm.getFolders());
      argForm.setPlaces(excerpForm.getPlaces());
      argForm.setCitedactors(excerpForm.getCitedactors());
      type = EArgumentLinkShade.ILLUSTRATE;
    }
    argForm.setShadeLink(type == null ? null : String.valueOf(type.id()));

    ContributionHolder contextHolder = helper.toHolder(contextContribution, user, lang);

    List<ArgumentContextForm> candidates = contributionType == EContributionType.EXCERPT || argument == null ?
            new ArrayList<>() :
            new ArgumentContextHolder(argument, lang).getPotentialJustificationLinks(contextId);

    String title = contributionType == EContributionType.ARGUMENT_CONTEXTUALIZED ?
            (contextContribution.getFirstArgumentId().equals(argExcId) || argExcId.equals(-1L) ?
              (contextContribution.getFirstArgument() != null && contextContribution.getFirstArgument().getArgument() != null
                    && contextContribution.getFirstArgument().getArgument().getFullTitle().length() > 0 ?
                    contextContribution.getFirstArgument().getArgument().getFullTitle(EArgumentType.value(shade)) : null)
            : argument.getArgument().getFullTitle()) : excerpt.getTechWorkingExcerpt();
    final EArgumentLinkShade shadeType = type;

    return CompletableFuture.supplyAsync(() ->
            ok(argumentContextSelection.render(
                    title,
                    argExcId,
                    contributionType,
                    contextHolder,
                    shadeType,
                    formFactory.form(SearchForm.class).fill(query),
                    formFactory.form(ArgumentContextForm.class).fill(argForm),
                    candidates,
                    helper, user)),
            context.current());
  }

  /**
   * Save links from argument viz
   *
   * @param fromArgument the argument where the save is performed
   * @param type the name of the viz pane type
   * @param linkType the linkType from carto
   * @return the changed tab
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> saveLinks(Long fromArgument, String type, int linkType) {
    logger.debug("POST save links from " + type);
    WebdebUser user = sessionHelper.getUser(ctx());
    boolean error = false;

    // for each edited links from similarity links, check if new shade is -1. Then delete it. Otherwise, apply the new shade
    for (ArgumentLinksRequest link : Json.fromJson(request().body().asJson().get("links"), ArgumentLinksRequest[].class)) {
      if (link.currentShade != link.newShade) {
        try {
          Contribution l = argumentFactory.retrieveContribution(link.linkId);
          if (l != null) {
            if (link.newShade > -1) {
              logger.debug("Update link [" + l.toString() + "] with shade " + link.newShade);
              EArgumentLinkShade shade = EArgumentLinkShade.value(link.newShade);
              if (shade != null) {
                switch(l.getContributionType().getEContributionType()){
                  case ARG_JUSTIFICATION:
                  case ARG_SIMILARITY:
                  case ARG_ILLUSTRATION:
                    ((ArgumentShadedLink) l).getArgumentLinkType().setOppositeType(shade);
                    break;
                  default:
                    throw new FormatException(FormatException.Key.WRONG_CONTRIBUTION_TYPE);
                }
                l.save(user.getId(), user.getGroup().getGroupId());
              }
            } else {
              l.remove(user.getId());
            }
          }
        } catch (PersistenceException | PermissionException | FormatException e) {
          logger.error("unable to update or delete link " + link.linkId, e);
          error = true;
        }
      }
    }
    // resend error if an exception occured
    if(error){
      return CompletableFuture.supplyAsync(Results::unauthorized, context.current());
    }
    // if everything is ok, return the updated viz pane
    return CompletableFuture.supplyAsync(Results::ok, context.current());
  }

  /**
   * Save a link between two contextualized or simple arguments or between a contextualized argument and an excerpt
   *
   * @param from an (contextualized) argument id
   * @param to an other (contextualized) argument or an excerpt id
   * @param contextContributionId a context contribution or -1 if not needed
   * @param shade the shade id that will be used to bind both argument
   * @param group the group where save this link
   *
   * @return redirect to either the cached goto key or the referer page
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> saveLink(Long from, Long to, Long contextContributionId, int shade, int group) {
    logger.info("POST save link direct between " + from + " and " + to + " and context " + context + " with shade " + shade + " in group " + group);

    sessionHelper.remove(ctx(), SessionHelper.KEY_NEW_JUS_LINK);
    sessionHelper.remove(ctx(), SessionHelper.KEY_NEW_ILLU_LINK);
    sessionHelper.remove(ctx(), SessionHelper.KEY_NEW_SIM_LINK);

    Contribution fromC = argumentFactory.retrieveContribution(from);
    Contribution toC = argumentFactory.retrieveContribution(to);
    Contribution contextContribution = argumentFactory.retrieveContribution(contextContributionId);
    EArgumentLinkShade type = EArgumentLinkShade.value(shade);
    if ((from != -1L && fromC == null) || toC == null || (shade != -1 && type == null)) {
      logger.error("a given reference is null or unfound");
      return CompletableFuture.supplyAsync(Results::badRequest, context.current());
    }

    // check if no such link exists
    if (argumentFactory.linkAlreadyExists(from, to)) {
      logger.warn("such link already exist " + from + " " + to + " " + shade);
      return CompletableFuture.supplyAsync(Results::badRequest, context.current());
    }
    try {
      switch(toC.getType()){
        case ARGUMENT:
          saveSimilarityLink(fromC, toC, type, group);
          break;
        case ARGUMENT_CONTEXTUALIZED:
          saveJustificationLink(fromC, toC, contextContribution, type, group);
          break;
        case EXCERPT:
          saveIllustrationLink(fromC, toC, type, group);
          break;
        default :
          logger.warn("unexpected contribution type " + fromC.getId() + " " + toC.getId() + " types : " +
                  fromC.getContributionType() + " " + toC.getContributionType());
          flash(SessionHelper.WARNING, i18n.get(ctx().lang(),"argument.links.wrongcontributiontype"));
          return CompletableFuture.supplyAsync(Results::badRequest, context.current());
      }
    } catch (FormatException | PersistenceException | PermissionException e) {
      logger.error("unable to save new link", e);
      return CompletableFuture.supplyAsync(Results::internalServerError, context.current());
    }

    // Send ok
    return CompletableFuture.supplyAsync(Results::ok, context.current());
  }

  /*
   * AJAX calls
   */

  /**
   * Search for an argument title in argument dictionnary.
   *
   * @param term the values to search for
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return a jsonified list of argument holders
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> searchArgument(String term, int fromIndex, int toIndex) {
    String lang = ctx().lang().code();
    List<ArgumentDictionarySimpleForm> result = argumentFactory.findByTitle(term, null, fromIndex, toIndex).stream()
            .map(ArgumentDictionarySimpleForm::new).collect(Collectors.toList());
    return CompletableFuture.supplyAsync(() -> ok(Json.toJson(result)), context.current());
  }

  /**
   * Search for a contextualized argument title.
   *
   * @param term the values to search for
   * @param contribution the context contribution to ignore
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return a jsonified list of argument context holders
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> searchArgumentContext(String term, Long contribution, int fromIndex, int toIndex) {
    List<ArgumentContextHolder> result = new ArrayList<>();
    List<Map.Entry<EQueryKey, String>> query = new ArrayList<>();
    query.add(new AbstractMap.SimpleEntry<>(EQueryKey.CONTRIBUTION_TYPE, String.valueOf(EContributionType.ARGUMENT_CONTEXTUALIZED.id())));
    query.add(new AbstractMap.SimpleEntry<>(EQueryKey.ARGUMENT_CONTEXT_TITLE, term));
    query.add(new AbstractMap.SimpleEntry<>(EQueryKey.CONTEXT_TO_IGNORE, String.valueOf(contribution)));
    try {
      String lang = ctx().lang().code();
      executor.searchContributions(query, fromIndex, toIndex).stream().filter(
              sessionHelper.getUser(ctx())::mayView).forEach(a ->
              result.add(new ArgumentContextHolder((ArgumentContext) a, lang)));
    } catch (BadQueryException e) {
      logger.warn("unable to search for contextualized arguments with given term " + term, e);
    }
    return CompletableFuture.supplyAsync(() -> ok(Json.toJson(result)), context.current());
  }

  /**
   * Cancel link creation request, i.e. remove cached new link from user session
   *
   * @return true in a json structure
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> abortLinkCreation() {
    logger.debug("GET abort link creation");
    deleteNewLinkKeys();
    sessionHelper.remove(ctx(), SessionHelper.KEY_GOTO);
    return CompletableFuture.supplyAsync(() -> ok(Json.toJson(true)), context.current());
  }

    /**
   * Retrieve all shades for a given argument type
   *
   * @param type an argument type id
   * @return a list of ShadeHolder objects that are valid shades for the given argument (type, timing) (Json)
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> getArgumentShades(int type, String lang) {
    return CompletableFuture.supplyAsync(() ->
        ok(Json.toJson(helper.getArgumentShades(type, lang).entrySet().stream().map(s ->
            new ShadeHolder(s.getKey(), s.getValue())).collect(Collectors.toList()))),
        context.current());
  }

  /**
   * Retrieve the shades for the given link type
   *
   * @param type a argument link type
   * @return the list of LinkShade for the given type
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> getShadeLinkTypes(int type) {
    String lang = ctx().lang().code();
    return CompletableFuture.supplyAsync(() ->
                    ok(Json.toJson(helper.getShadeLinkTypes(String.valueOf(type), lang).entrySet().stream().map(entry ->
                            new ShadeHolder(entry.getKey(), entry.getValue())).collect(Collectors.toList()))),
            context.current());
  }

  /**
   * Reload all linked contributions by type
   *
   * @param argument the argument id
   * @return a jsonified list of holders or bad request if actor is unknown or pov is unknown
   */
  public CompletionStage<Result> reloadLinkedContributions(Long argument) {
    return reloadLinkedContributions(argument, true);
  }

  /**
   * Reload all linked contributions by type
   *
   * @param argument the argument id
   * @param allLinks true if we need similars too
   * @return a jsonified list of holders or bad request if actor is unknown or pov is unknown
   */
  public CompletionStage<Result> reloadLinkedContributions(Long argument, boolean allLinks) {
    ArgumentContext a = argumentFactory.retrieveContextualized(argument);

    if(a != null) {
      viz = new ArgumentContextVizHolder(a, ctx().lang().code(), allLinks);
      return CompletableFuture.supplyAsync(Results::ok, context.current());
    }
    return CompletableFuture.supplyAsync(Results::badRequest, context.current());
  }

  /**
   * Get all linked contributions by type
   *
   * @param argument the argument id
   * @param type the name of the viz pane type
   * @return a jsonified list of holders or bad request if actor is unknown or pov is unknown
   */
  public CompletionStage<Result> getLinkedContributions(Long argument, String type) {
    return getLinkedContributions(argument, type, 0, null);
  }

  /**
   * Get all linked contributions by type
   *
   * @param argument the argument id
   * @param type the name of the viz pane type
   * @param kind 0 for argument, 1 and 2 for debate
   * @return a jsonified list of holders or bad request if actor is unknown or pov is unknown
   */
  public CompletionStage<Result> getLinkedContributions(Long argument, String type, int kind, Long contextId) {
    logger.debug("GET linked contributions with contextualized argument " + argument + " for " + type + " contextid" + contextId);
    ArgumentContext a = argumentFactory.retrieveContextualized(argument);
    WebdebUser user = sessionHelper.getUser(ctx());
    String lang = ctx().lang().code();

    if(a != null) {
      List<ContributionHolder> holders = new ArrayList<>();
      if(viz == null) {
        viz = new ArgumentContextVizHolder(a, ctx().lang().code(), kind == 0);
      }

      switch(EVizPaneName.value(type)){
        case CARTO :
          viz.viewCartography(user).values().forEach(holders::addAll);
          return CompletableFuture.supplyAsync(() ->
                    ok(argumentCartoContent.render(viz, helper.buildFilters(holders, lang), user, contextId, kind > 0)), context.current());
        case CITATION :
        case RADIO :
          // add all arguments from cited viz to holders for build filters
          viz.viewCited(user).values().forEach(cited -> cited.forEach(e -> holders.add(e.getActor())));
          return CompletableFuture.supplyAsync(() ->
                  ok(argumentCitedContent.render(viz, helper.buildFilters(holders, lang), user, kind, contextId)), context.current());
        case SOCIO :
          ExcerptSociography viewed = viz.viewSociography(user);
          return CompletableFuture.supplyAsync(() -> ok(argumentSociographyContent.render(
                  viewed.getViewed(), helper.buildFilters(viewed.getActors(), lang),  user)), context.current());
        case TEXTS:
          return CompletableFuture.supplyAsync(() -> ok(argumentTextsContent.render(
                  viz, null,  user)), context.current());
      }
    }
    Map<String, String> messages = new HashMap<>();
    messages.put(SessionHelper.ERROR, i18n.get(ctx().lang(),"argument"));
    return CompletableFuture.supplyAsync(() -> badRequest(message.render(messages)), context.current());
  }

    /**
     * See all excerpts that are linked with the given argument
     *
     * @param argId a contextualized argument id
     * @param contributionId the id of the contribution to be focused on if any
     * @return the argumentRadioContent fill with excerpts or bad request if the given argument is not found.
     */
    public CompletionStage<Result> seeArgumentExcerpts(Long argId, Long contributionId) {
      logger.debug("GET excerpts related with arg " + argId);
      ArgumentContext a = argumentFactory.retrieveContextualized(argId);
      if(a == null){
        return CompletableFuture.supplyAsync(Results::badRequest, context.current());
      }

      Contribution contribution = actorFactory.retrieveContribution(contributionId);

      return CompletableFuture.supplyAsync(() -> ok(argumentExcerptsModal.render(
              new ArgumentContextVizHolder(a, ctx().lang().code()),
              sessionHelper.getUser(ctx()),
              a.getContextId(),
              contribution != null ? contribution.getId() : null,
              contribution != null ? contribution.getContributionType().getEContributionType() : null,
              0)), context.current());
    }

  /*
   * PRIVATE HELPERS
   */

  /**
   * Display the modal or page to edit a contextualized argument
   *
   * @param argId the contextualized argument id to edit
   * @param fromModal true if the editing is for a modal
   * @return the edit modal or page with given argument data, or redirect to the session "goto" page with an error message
   * to display (flash) if contextualized argument is not found or bad request
   */
  private CompletionStage<Result> editArgumentContext(Long argId, boolean fromModal){
    WebdebUser user = sessionHelper.getUser(ctx());
    deleteNewLinkKeys();

    // prepare wrapper for argument
    ArgumentContext argument = argumentFactory.retrieveContextualized(argId);
    if (argument == null) {
      // leave if we couldn't find contextualized argument
      flash(SessionHelper.ERROR, i18n.get(ctx().lang(), OOPS));
      return fromModal ? CompletableFuture.completedFuture(Results.badRequest()) : redirectToGoTo(null);
    }
    else{
      if(!user.mayView(argument)){
        return CompletableFuture.completedFuture(Results.unauthorized(
                privateContribution.render(user)));
      }
      sessionHelper.set(ctx(), SessionHelper.KEY_GOTO, sessionHelper.getReferer(ctx()));
    }

    ArgumentContextForm argForm = new ArgumentContextForm(argument, ctx().lang().code());
    argForm.setInGroup(sessionHelper.getCurrentGroup(ctx()));
    // display first page of argument edition
    Form<ArgumentContextForm> form = formFactory.form(ArgumentContextForm.class).fill(argForm);
    return CompletableFuture.supplyAsync(() ->
            fromModal ? ok(editArgumentContextModal.render(form, helper, sessionHelper.getUser(ctx()), null)) :
                    ok(editArgumentContext.render(form, helper, sessionHelper.getUser(ctx()), null)), context.current());
  }

  /**
   * Save a justification link from contextualized arguments and a link shade type and a context contribution.
   *
   * @param from the contextualized argument that is the source of the link
   * @param to the contextualized argument that is the destination of the link
   * @param context a context contribution for this link
   * @param type the link shade type that said if the justification is supported or rejected
   * @param group the group where the link must be save
   */
  private void saveJustificationLink(Contribution from, Contribution to, Contribution context, EArgumentLinkShade type, int group) throws FormatException, PersistenceException, PermissionException {
      // only verify if the source contribution is in the good contribution type because
      // we fall here with the type of the destination contribution.
      if(context != null) {
        ArgumentJustification link = argumentFactory.getArgumentJustificationLink();
        link.setOrigin(from == null ? ((ContextContribution) context).getFirstArgument() : (ArgumentContext) from);
        link.setDestination((ArgumentContext) to);
        link.setContext(context);
        link.setArgumentLinkType(argumentFactory.getArgumentLinkType(type == null ? EArgumentLinkShade.SUPPORTS : type));
        link.addInGroup(group);
        link.save(sessionHelper.getUser(ctx()).getContributor().getId(), group);
      }
  }

  /**
   * Save a similarity link from arguments and a link shade type.
   *
   * @param from the argument that is the source of the link
   * @param to the argument that is the destination of the link
   * @param type the link shade type that said if the similarity is similar or opposed
   * @param group the group where the link must be save
   */
  private void saveSimilarityLink(Contribution from, Contribution to, EArgumentLinkShade type, int group) throws FormatException, PersistenceException, PermissionException {
    // only verify if the source contribution is in the good contribution type because
    // we fall here with the type of the destination contribution.
    if(from.getContributionType().getEContributionType() == EContributionType.ARGUMENT) {
      ArgumentSimilarity link = argumentFactory.getArgumentSimilarityLink();
      link.setOrigin((Argument) from);
      link.setDestination((Argument) to);
      link.setArgumentLinkType(argumentFactory.getArgumentLinkType(type == null ? EArgumentLinkShade.SIMILAR : type));
      link.addInGroup(group);
      link.save(sessionHelper.getUser(ctx()).getContributor().getId(), group);
    }
  }

  /**
   * Save a justification link from contextualized arguments and a link shade type and a context contribution.
   *
   * @param from the contextualized argument that is the source of the link
   * @param to the contextualized argument that is the destination of the link
   * @param type the link shade type that said if the illustration is an example, a shaded example or a counter example
   * @param group the group where the link must be save
   */
  public void saveIllustrationLink(Contribution from, Contribution to, EArgumentLinkShade type, int group) throws FormatException, PersistenceException, PermissionException {
      // only verify if the source contribution is in the good contribution type because
      // we fall here with the type of the destination contribution.
    if(from.getContributionType().getEContributionType() == EContributionType.ARGUMENT_CONTEXTUALIZED) {
      ArgumentIllustration link = argumentFactory.getArgumentIllustrationLink();
      link.setArgument((ArgumentContext) from);
      link.setExcerpt((Excerpt) to);
      link.setArgumentLinkType(argumentFactory.getArgumentLinkType(type == null ? EArgumentLinkShade.ILLUSTRATE : type));
      link.addInGroup(group);
      link.save(sessionHelper.getUser(ctx()).getContributor().getId(), group);
    }
  }

  /**
   * Save a contextualized argument from a given form.
   *
   * @param form a contextualized argument form object that may contain errors
   * @return given (updated) actor form
   * @throws ArgumentContextNotSavedException if an error exist in passed form or any error arisen from save action
   */
  private synchronized ArgumentContextForm saveArgumentContext(Form<ArgumentContextForm> form) throws ArgumentContextNotSavedException {
    // check errors, sends back whole form if any
    if (form.hasErrors()) {
      logger.debug("form has errors " + form.errors() + "\nData:" + form.data());
      throw new ArgumentContextNotSavedException(form, ArgumentContextNotSavedException.ERROR_FORM);
    }

    ArgumentContextForm argument = form.get();

    if (!helper.searchForNameMatches(argument.getCitedactors(), ctx().lang().code()).isEmpty())  {
      throw new ArgumentContextNotSavedException(form.fill(argument), ArgumentContextNotSavedException.AUTHOR_NAME_MATCH);
    }

    // check folder name matches
    if (!helper.searchForFolderNameMatches(argument.getFolders(), ctx().lang().code()).isEmpty()) {
      throw new ArgumentContextNotSavedException(form.fill(argument), ArgumentContextNotSavedException.FOLDER_NAME_MATCH);
    }

    // get argument language if none
    if(values.isBlank(argument.getLanguage())) {
      argument.setLanguage(values.detectTextLanguage(argument.getFullTitle(), getLanguageDetectionApiKey(), ctx().lang().code()));
    }

    argument.setPlaces(savePlaces(argument.getPlaces(), true));

    // try to save in DB
    try {
      treatSaveContribution(argument.save(sessionHelper.getUser(ctx()).getContributor().getId()));

    } catch (FormatException | PersistenceException | PermissionException e) {
      logger.error("unable to save argument", e);
      throw new ArgumentContextNotSavedException(form, ArgumentContextNotSavedException.ERROR_DB, i18n.get(ctx().lang(),e.getMessage()));
    }

    // check if this contextualized argument was created to be linked to another one (from visualization page)
    List<String> cachedLink = sessionHelper.getValues(ctx(), SessionHelper.KEY_NEW_JUS_LINK);
    if (cachedLink != null && cachedLink.size() == 3 && values.isNumeric(cachedLink.get(0), 0, false)
            && values.isNumeric(cachedLink.get(2)) && values.isNumeric(cachedLink.get(1), 0, false)) {
      logger.info("ask confirmation for previously new link request to new contextualized argument " + argument.getId());
      sessionHelper.remove(ctx(), SessionHelper.KEY_NEW_JUS_LINK);

      Long originId = Long.parseLong(cachedLink.get(0));
      ArgumentContext origin = argumentFactory.retrieveContextualized(originId);
      ArgumentContext destination = argumentFactory.retrieveContextualized(argument.getId());
      Contribution context = argumentFactory.retrieveContribution(Long.parseLong(cachedLink.get(2)));

      // form that will be passed is reconstructed from cached contextualized argument since it was
      // selected as the origin of the new link creation request
      if (destination != null && context != null) {
        // check if no such link exists
        if (argumentFactory.linkAlreadyExists(originId, argument.getId())) {
          logger.warn("such link already exist " + originId + " " + argument.getId() + " " +  cachedLink.get(1));
          throw new ArgumentContextNotSavedException(form, ArgumentContextNotSavedException.LINK_ALREADY_EXISTS , i18n.get(ctx().lang(), ""));
        }

        try{
          int shade;
          try{
            shade = Integer.parseInt(cachedLink.get(1).equals("-1") ? argument.getShade() : cachedLink.get(1));
          } catch (NumberFormatException e) {
            shade = EArgumentLinkShade.SUPPORTS.id();
          }
          saveJustificationLink(origin, destination, context, EArgumentLinkShade.value(shade), argument.getInGroup());
        } catch (FormatException | PersistenceException | PermissionException e) {
          logger.error("unable to save new justification link", e);
          throw new ArgumentContextNotSavedException(form, ArgumentContextNotSavedException.ERROR_DB, i18n.get(ctx().lang(),e.getMessage()));
        }
      }
    }
    // check if this contextualized argument was created to be linked to an excerpt
    cachedLink = sessionHelper.getValues(ctx(), SessionHelper.KEY_NEW_ILLU_LINK);
    if (cachedLink != null && cachedLink.size() >= 2 && values.isNumeric(cachedLink.get(0), 0, false)
            && values.isNumeric(cachedLink.get(1), 0, false)) {
      logger.info("ask confirmation for previously new link request to new contextualized argument " + argument.getId());
      sessionHelper.remove(ctx(), SessionHelper.KEY_NEW_ILLU_LINK);

      Long excerptId = Long.parseLong(cachedLink.get(0));
      ArgumentContext origin = argumentFactory.retrieveContextualized(argument.getId());
      Excerpt destination = excerptFactory.retrieve(excerptId);

      // form that will be passed is reconstructed from cached contextualized argument since it was
      // selected as the origin of the new link creation request
      if (origin != null && destination != null) {
        // check if no such link exists
        if (argumentFactory.linkAlreadyExists(argument.getId(), excerptId)) {
          logger.warn("such link already exist between " + argument.getId() + " and " + excerptId);
          throw new ArgumentContextNotSavedException(form, ArgumentContextNotSavedException.LINK_ALREADY_EXISTS , i18n.get(ctx().lang(), ""));
        }

        try{
          int shade;
          try{
            shade = Integer.parseInt(cachedLink.get(1).equals("-1") ? argument.getShadeLink() : cachedLink.get(1));
          } catch (NumberFormatException e) {
            shade = EArgumentLinkShade.ILLUSTRATE.id();
          }
          saveIllustrationLink(origin, destination, EArgumentLinkShade.value(shade), argument.getInGroup());
        } catch (FormatException | PersistenceException | PermissionException e) {
          logger.error("unable to save new illustration link", e);
          throw new ArgumentContextNotSavedException(form, ArgumentContextNotSavedException.ERROR_DB, i18n.get(ctx().lang(),e.getMessage()));
        }
      }
    }

    return argument;
  }


  /**
   * Handle error on contextualized argument form submission and returns the actor form view (depending on the switch).
   * If an unknown error occurred, either a "goto" page or the general entry view is returned.
   *
   * @param exception the exception raised from unsuccessful save
   * @param onlyFields switch to know if the argumentContextDiv views must be returned
   * @return if the form contains error, a bad request (400) response is returned with, if onlyfield, the
   * argumentContextDiv template or the editArgumentContext full form otherwise. In case of possible folder name matches, 
   * a 409 response is returned with the modal frame to select among possible matches. If another error occurred,
   * a redirect to either a "goto" session-cached url or the main entry page.
   */
  private CompletionStage<Result> handleArgumentContextError(ArgumentContextNotSavedException exception, boolean onlyFields) {
    Map<String, String> messages = new HashMap<>();
    Form<ArgumentContextForm> form = exception.form;
    ArgumentContextForm argument;

    switch (exception.error) {
      case ArgumentContextNotSavedException.AUTHOR_NAME_MATCH:
        // check author name matches
        argument = form.get();
        NameMatch<ActorHolder> actorsMatches = helper.searchForNameMatches(argument.getCitedactors(), ctx().lang().code());

        if(!actorsMatches.isEmpty()) {
          return CompletableFuture.supplyAsync(() -> onlyFields ?
                          status(409, handleNameMatches.render(actorsMatches.getNameMatches(), true, actorsMatches.getIndex())) :
                          badRequest(editArgumentContext.render(form, helper, sessionHelper.getUser(ctx()), messages))
                  , context.current());
        }

      case ArgumentContextNotSavedException.FOLDER_NAME_MATCH:
        // check folder name matches
        argument = form.get();
        NameMatch<FolderHolder> folderMatches = helper.searchForFolderNameMatches(argument.getFolders(), ctx().lang().code());
        if (!folderMatches.isEmpty()) {
            return CompletableFuture.supplyAsync(() -> onlyFields ?
                            status(409, handleFolderNameMatches.render(folderMatches.getNameMatches(), folderMatches .getIndex())) :
                            badRequest(editArgumentContext.render(form, helper, sessionHelper.getUser(ctx()), messages))
                    , context.current());
        }

      case ArgumentContextNotSavedException.ERROR_FORM:
        // error in form, just resend it
        messages.put(SessionHelper.WARNING, i18n.get(ctx().lang(), "error.form"));

        return CompletableFuture.supplyAsync(() -> onlyFields ?
                badRequest(argumentcontextdiv.render(form, helper, "argument", EArgumentType.NOT_FIRST_ARGUMENT))
                : badRequest(editArgumentContext.render(form, helper, sessionHelper.getUser(ctx()), messages)), context.current());

      case ArgumentContextNotSavedException.LINK_ALREADY_EXISTS:
        return CompletableFuture.supplyAsync(() -> onlyFields ?
              status(406)
              : status(406, editArgumentContext.render(form, helper, sessionHelper.getUser(ctx()), messages)), context.current());

      default:
        // any other error, check where do we have to go after and show message in exception
        messages.put(SessionHelper.ERROR, i18n.get(ctx().lang(), "error.crash"));
        return onlyFields ? CompletableFuture.supplyAsync(() ->
                internalServerError(message.render(messages)), context.current())
                : redirectToGoTo(null);
    }
  }

  /**
   * Inner class to handle contextualized argument exception when a contextualized argument cannot be saved
   * from private save execution
   */
  private class ArgumentContextNotSavedException extends Exception {

    private static final long serialVersionUID = 1L;
    final Form<ArgumentContextForm> form;
    final int error;

    static final int ERROR_FORM = 0;
    static final int AUTHOR_NAME_MATCH = 1;
    static final int FOLDER_NAME_MATCH = 2;
    static final int ERROR_DB = 3;
    static final int LINK_ALREADY_EXISTS = 4;

    ArgumentContextNotSavedException(Form<ArgumentContextForm> form, int error) {
      this.error = error;
      this.form = form;
    }

    ArgumentContextNotSavedException(Form<ArgumentContextForm> form, int error, String message) {
      super(message);
      this.error = error;
      this.form = form;
    }
  }
}
