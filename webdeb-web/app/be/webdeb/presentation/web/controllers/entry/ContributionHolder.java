/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry;

import be.webdeb.core.api.actor.*;
import be.webdeb.core.api.contribution.*;
import be.webdeb.core.api.contributor.Group;
import be.webdeb.core.api.folder.EFolderType;
import be.webdeb.core.api.folder.Folder;
import be.webdeb.infra.fs.FileSystem;
import be.webdeb.infra.persistence.model.Excerpt;
import be.webdeb.presentation.web.controllers.entry.actor.ActorHolder;
import be.webdeb.presentation.web.controllers.entry.actor.ActorSimpleForm;
import be.webdeb.presentation.web.controllers.entry.actor.AffiliationForm;
import be.webdeb.presentation.web.controllers.entry.argument.ArgumentContextHolder;
import be.webdeb.presentation.web.controllers.entry.argument.ArgumentHolder;
import be.webdeb.presentation.web.controllers.entry.argument.JustificationLinkForm;
import be.webdeb.presentation.web.controllers.entry.debate.DebateHolder;
import be.webdeb.presentation.web.controllers.entry.excerpt.ExcerptHolder;
import be.webdeb.presentation.web.controllers.entry.folder.FolderHolder;
import be.webdeb.presentation.web.controllers.entry.folder.SimpleFolderForm;
import be.webdeb.presentation.web.controllers.entry.text.TextHolder;
import be.webdeb.presentation.web.controllers.permission.WebdebUser;
import be.webdeb.util.ValuesHelper;
import com.fasterxml.jackson.annotation.JsonIgnore;
import play.api.Play;
import play.data.validation.ValidationError;
import play.i18n.Lang;
import play.i18n.MessagesApi;
import play.libs.Json;

import javax.inject.Inject;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This class is used as a supertype wrapper for any contribution holder
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public abstract class ContributionHolder implements Comparable<ContributionHolder> {

  @Inject
  protected ValuesHelper values = Play.current().injector().instanceOf(ValuesHelper.class);
  @Inject
  protected FileSystem files = Play.current().injector().instanceOf(FileSystem.class);
  @Inject
  protected ContributionHelper helper = Play.current().injector().instanceOf(ContributionHelper.class);
  @Inject
  protected ActorFactory actorFactory = Play.current().injector().instanceOf(ActorFactory.class);
  @Inject
  protected MessagesApi i18n = Play.current().injector().instanceOf(MessagesApi.class);


  // custom logger
  protected static final org.slf4j.Logger logger = play.Logger.underlying();

  // common fields
  protected Long id;
  // only set for "new elements" not already persisted at controller's level
  protected Integer inGroup;
  protected List<Integer> groups;
  protected Contribution contribution;
  protected long version;
  protected EContributionType type;
  protected String lang;
  protected EValidationState validated;
  protected Map<EFilterName, List<String>> filterable = new EnumMap<>(EFilterName.class);
  protected List<SimpleFolderForm> folders = new ArrayList<>();
  protected List<PlaceForm> places = new ArrayList<>();

  // for comma-separated list of topics
  protected static final String DELIM = ",";

  // used for some group-based visualization
  protected Long creator;
  protected String creatorName;
  protected boolean indecision = false;
  protected Map<EContributionType, Integer> countRelationsMap = null;

  protected MediaSharedData mediaSharedData = null;

  /**
   * Default constructor, simply initialize id, version and group resp. to -1, 0 and 0 (default public group).
   */
  public ContributionHolder() {
    id = -1L;
    version = 0L;
    inGroup = 0;
  }

  /**
   * Constructor from a given contribution
   *
   * @param contribution a contribution
   * @param lang 2-char ISO code of context language (among play accepted languages)
   */
  public ContributionHolder(Contribution contribution, String lang) {
    this.contribution = contribution;

    if(contribution != null) {
      id = contribution.getId();
      version = contribution.getVersion();
      type = contribution.getType();
      validated = contribution.getValidated().getEState();

      groups = contribution.getInGroups().stream().map(Group::getGroupId).collect(Collectors.toList());
      if (!values.isBlank(id)) {
        creator = contribution.getCreator().getId();
        creatorName = contribution.getCreator().getPseudo();
      }
      addFilterable(EFilterName.CTYPE, i18n.get(Lang.forCode(lang), "general.filter.ctype." + contribution.getType().id()));
    }

    this.lang = lang;
  }

  /**
   * Init actor filters
   *
   * @param actor a linked actor
   */
  protected void initActorFilter(ActorSimpleForm actor){
    addFilterable(EFilterName.NAME, actor.getFullname());
    if(!values.isBlank(actor.getBirthOrCreation())) {
      addDateFilterable(EFilterName.BIRTHDATE, actor.getBirthOrCreation());
    }
    if (!values.isBlank(actor.getFunction())) {
      addFilterable(EFilterName.FUNCTION, actor.getFunction());
    }
    if (!values.isBlank(actor.getAffname())) {
      addFilterable(EFilterName.AFFILIATION, actor.getAffname());
    }
    if (!values.isBlank(actor.getResidence())) {
      addFilterable(EFilterName.COUNTRY, actor.getResidence());
    }
    if (actor.getActortype() == EActorType.PERSON.id()){
      addFilterable(EFilterName.GENDER, actor.getGender());
    }else if (actor.getActortype() == EActorType.ORGANIZATION.id()){
      addFilterable(EFilterName.LEGAL, actor.getLegal());
      actor.getSectors().forEach(s -> addFilterable(EFilterName.SECTOR, s));
    }
  }

  /*
   * GETTERS / SETTERS
   */

  /**
   * Get this contribution id
   *
   * @return an id
   */
  public Long getId() {
    return id;
  }

  /**
   * Set this contribution id
   *
   * @param id an id
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * Get the list of group ids where this contribution is visible
   *
   * @return a list of group id
   */
  public List<Integer> getGroups() {
    return groups;
  }

  /**
   * Get this contribution version
   *
   * @return a version (timestamp)
   */
  public long getVersion() {
    return version;
  }

  /**
   * Set the version (timestamp) of this contribution
   *
   * @param version a timestamp value
   */
  public void setVersion(long version) {
    this.version = version;
  }

  /**
   * Get the concrete type of contribution
   *
   * @return the EContributionType of this contribution
   * @see be.webdeb.core.api.contribution.EContributionType
   */
  public EContributionType getType() {
    return type;
  }

  /**
   * Get the concrete type of contribution
   *
   * @return the EContributionType of this contribution
   */
  public int getTypeNum() {
    return type.id();
  }

  /**
   * Set the concrete type of contribution
   *
   * @param type the EContributionType of this contribution
   * @see be.webdeb.core.api.contribution.EContributionType
   */
  public void setType(EContributionType type) {
    this.type = type;
  }

  /**
   * Get the group (visibility) of this contribution
   *
   * @return a group id in which this contribution is visible (-1 denotes publicly visible contributions)
   */
  public Integer getInGroup() {
    return inGroup;
  }

  /**
   * Set the group (visibility) of this contribution
   *
   * @param group a group id in which this contribution is visible (-1 denotes publicly visible contributions)
   */
  public void setInGroup(Integer group) {
    this.inGroup = group;
  }

  /**
   * Get the id of the contributor that created this contribution, may be unset (for performance reason)
   *
   * @return the creator id, or null of unset
   */
  public Long getCreator() {
    return creator;
  }

  /**
   * Set the id of the contributor that created this contribution
   *
   * @param creator the id of the contributor that created this contribution
   */
  public void setCreator(Long creator) {
    this.creator = creator;
  }

  /**
   * Get the name of the contributor that created this contribution, may be unset (for performance reason)
   *
   * @return the creator name, or null of unset
   */
  public String getCreatorName() {
    return creatorName;
  }

  /**
   * Set the name of the contributor that created this contribution
   *
   * @param creatorName the name of the contributor that created this contribution
   */
  public void setCreatorName(String creatorName) {
    this.creatorName = creatorName;
  }

  /**
   * Check whether this contribution is validated
   *
   * @return the validation stateof this contribution
   */
  public EValidationState isValidated() {
    return validated;
  }

  /**
   * Set whether this contribution is validated
   *
   * @param validated the validation state
   */
  public void setValidated(EValidationState validated) {
    this.validated = validated;
  }

  /**
   * Get the current language 2-char ISO code of the user's UI
   *
   * @return a two char iso code as defined in accepted languages for the webdeb system
   */
  public String getLang() {
    return lang;
  }

  /**
   * Set the current language 2-char ISO code of the user's UI
   *
   * @param lang a two char iso code as defined in accepted languages for the webdeb system
   */
  public void setLang(String lang) {
    this.lang = lang;
  }

  /**
   * Get the list of folders linked with this contribution
   *
   * @return a list of folders
   */
  public List<SimpleFolderForm> getFolders() {
    return folders;
  }

  /**
   * Get the list of folders ignored composed folders
   *
   * @return a list of tag folders
   */
  public List<SimpleFolderForm> getFolderTags() {
    return folders.stream().filter(e -> e.getFolderType() != EFolderType.COMPOSED.id()).collect(Collectors.toList());
  }

  /**
   * Get the places concerned by this contribution
   *
   * @return a possibly empty list of places
   */
  public List<PlaceForm> getPlaces() {
    return places;
  }

  /**
   * True if an actor has ambiguous position about a contribution in a context
   *
   * @return the ambiguous flag
   */
  public boolean isIndecision() {
    return indecision;
  }

  /**
   * True if an actor has ambiguous position about a contribution in a context
   *
   * @param indecision the ambiguous flag
   */
  public void setIndecision(boolean indecision) {
    this.indecision = indecision;
  }

  /**
   * Get an image that represent this contribution
   *
   * @return the path for the image
   */
  public abstract String getDefaultAvatar();

  /**
   * Get the map of number of linked elements with this contribution
   *
   * @param user the user that need that stats
   * @return the map of number of linked elements with this contribution by contribution type
   */
  public Map<EContributionType, Integer> getCountRelationsMap(WebdebUser user) {
    if(countRelationsMap == null){
      countRelationsMap = contribution.getCountRelationsMap(user.getId(), user.getGroupId());
    }
    return countRelationsMap;
  }

  /**
   * Get the related elements for the given contribution type
   *
   * @param user the user that need that stats
   * @param type the type of contribution to show the count
   * @return the number of related elements
   */
  public int getNbRelatedElements(EContributionType type, WebdebUser user) {

    getCountRelationsMap(user);

    if(countRelationsMap.containsKey(type)){
      return countRelationsMap.get(type);
    }
    return 0;
  }

  /**
   * Get the filterable string, ie, a stringified map of key : [array of values] separated by commas "," containing
   * all values that may be subject to filter contributions on. Mainly used when displaying results of search queries.
   *
   * Looks like a stringified javascript map, to be handled as such by the view.
   *
   * @return a stringified list of key, value pairs.
   * @see EFilterName
   */
  @JsonIgnore
  public String getFilterable() {
    return Json.toJson(filterable).toString();
  }

  /**
   * Get a string to describe all linked actor to this contribution.
   *
   * @param actors the list of actors to describe
   * @param withLink true if folder must be linked to its viz page
   * @return a description of actors linked to this contribution
   */
  public String getActorDescription(List<ActorSimpleForm> actors, boolean withLink){
    List<String> actorsName = new ArrayList<>();

    for(ActorSimpleForm actor : actors){
      actorsName.add(withLink ? writeContributionHtmlLink(actor.getId(), EContributionType.ACTOR, actor.getFullname()) : actor.getFullname());
    }
    return  String.join(", ", actorsName);
  }

  /**
   * Get a string to describe all linked folders to this contribution. It will return the name of the
   * composed folder that have the most parents, or if this one has no composed folder, the name of the first folder
   * found.
   *
   * @param withLink true if folder must be linked to its viz page
   * @return a description of the folders linked to this contextualized argument
   */
  public String getFolderDescription(boolean withLink){
    SimpleFolderForm folder;
    List<SimpleFolderForm> sub = folders.stream()
            .filter(e -> e.getFolderType() == EFolderType.COMPOSED.id()).collect(Collectors.toList());
    if(!sub.isEmpty()){
      folder = sub.stream().max((Comparator.comparing(SimpleFolderForm::getNbParents))).get();
    }else{
      folder = folders.isEmpty() ? null : folders.get(0);
    }

    if(folder != null){
      return withLink ? writeContributionHtmlLink(folder.getFolderId(), EContributionType.FOLDER, folder.getName()) : folder.getName();
    }
    return "";
  }

  /**
   * Initialize folders linked with this contribution
   *
   * @param folders a list of folders
   * @param lang two-char ISO code used for function names
   * @param fromForm true is folders must be initialized for form
   */
  protected void initFolders(List<Folder> folders, String lang, boolean fromForm) {
    List<Folder> f = fromForm ? folders.stream()
            .filter(e -> e.getFolderType().getEFolderType() != EFolderType.COMPOSED).collect(Collectors.toList()) : folders;
    f.forEach(e -> {
      this.folders.add(new SimpleFolderForm(e, lang));
      addFilterable(EFilterName.FOLDER, e.getName(lang));
    });
  }

  /**
   * Get a string to describe all places linked to this .
   *
   * @return a description of the places linked to this contribution
   */
  public String getPlaceDescription(){
    return getPlaceDescription(false);
  }

  /**
   * Get a string to describe all places linked to this. All places will have a redirection link to viz them.
   *
   * @return a description of the places linked to this contribution
   */
  public String getPlaceDescription(boolean withLink){
    if(places != null){
      return places.stream().map(e ->  withLink ? writePlaceHtmlLink(e.getId(), e.getName()) : e.getName())
              .collect(Collectors.joining(" / "));
    }
    return "";
  }


  /**
   * Initialize places linked with this contribution
   *
   * @param places a list of places
   * @param lang two-char ISO code used for function names
   */
  protected void initPlaces(List<Place> places, String lang) {
    this.places = new ArrayList<>();
    places.forEach(p -> {
      this.places.add(new PlaceForm(p, lang));
      addFilterable(EFilterName.PLACE, p.getName(lang));
    });
  }

  /**
   * Add given filter name and value to filterable map
   *
   * @param filter the filter name
   * @param value a value for this filter
   */
  protected void addFilterable(EFilterName filter, String value) {
    if (!values.isBlank(value)) {
      if (!filterable.containsKey(filter)) {
        filterable.put(filter, new ArrayList<>());
      }
      filterable.get(filter).add(value);
    }
  }

  /**
   * Set filterable field of this contribution with given author
   *
   * @param author an author of this contribution
   */
  protected void setAuthorFilterable(Actor author) {
    author.getAffiliations().forEach(aff -> {
      if (aff.getFunction() != null) {
        addFilterable(EFilterName.FUNCTION, aff.getFunction().getSubstitute() != null ?
            aff.getFunction().getSubstitute().getName(lang) : aff.getFunction().getName(lang));
      }
      if (aff.getActor() != null) {
        addFilterable(EFilterName.AFFILIATION, aff.getActor().getName(lang).getFullName(aff.getActor().getActorType()));
      }
    });

    switch (author.getActorType()) {
      case PERSON:
        Person p = (Person) author;
        addFilterable(EFilterName.BIRTHDATE, p.getBirthdate());
        addFilterable(EFilterName.BIRTHDATE, p.getDeathdate());
        addFilterable(EFilterName.GENDER, p.getGender() != null ? p.getGender().getName(lang) : null);
        addFilterable(EFilterName.COUNTRY, p.getResidence() != null ? p.getResidence().getName(lang) : null);
        break;
      case ORGANIZATION:
        Organization o = (Organization) author;
        addFilterable(EFilterName.BIRTHDATE, o.getCreationDate());
        addFilterable(EFilterName.BIRTHDATE, o.getTerminationDate());
        addFilterable(EFilterName.LEGAL, o.getLegalStatus() != null ? o.getLegalStatus().getName(lang) : null);
        o.getPlaces().forEach(p1 -> addFilterable(EFilterName.PLACE, p1.getName(lang)));
        o.getBusinessSectors().forEach(s -> addFilterable(EFilterName.SECTOR, s.getName(lang)));
        break;
      default:
        // ignore
    }

  }

  /**
   * Check a list of folders
   *
   * @param folders the list of folders to check
   * @param field the field name in which the folders are present (for proper error binding)
   * @return a map of field name - list of validation errors if any, null otherwise
   */
  protected Map<String, List<ValidationError>> checkFolders(List<SimpleFolderForm> folders, String field) {
    Map<String, List<ValidationError>> errors = new HashMap<>();

    for (int i = 0; i < folders.size(); i++){
      if(!values.isBlank(folders.get(i).getName())){
        String fieldName = field + "[" + i + "].name";
        if(values.isNotValidFolderName(folders.get(i).getName())) {
          errors.put(fieldName, Collections.singletonList(new ValidationError(fieldName, "folder.error.name.name.chars")));
        }
        /*if(values.checkFolderNameSize(folders.get(i).getName())){
          errors.put(fieldName, Collections.singletonList(new ValidationError(fieldName, "folder.error.name.name.size")));
        }*/
      }
    }

    return errors.isEmpty() ? null : errors;
  }

  /**
   * Check a list of places
   *
   * @param places the list of places to check
   * @return a map of field name - list of validation errors if any, null otherwise
   */
  protected Map<String, List<ValidationError>> checkPlaces(List<PlaceForm> places) {
    Map<String, List<ValidationError>> errors = new HashMap<>();

    for (int i = 0; i < places.size(); i++) {
      PlaceForm place = places.get(i);
      if (!place.isEmpty() &&
              places.stream().filter(p -> !p.isEmpty()).anyMatch(p -> (p != place &&
                      (p.getGeonameId() == null ? p.getId() != null && p.getId().equals(place.getId())
                              : p.getGeonameId().equals(place.getGeonameId()))))) {
        errors.put("places[" + i + "].placename", Collections.singletonList(new ValidationError("places[" + i + "].placename", "argument.error.placename.twice")));
      }
    }

    return errors.isEmpty() ? null : errors;
  }

  /**
   * Check a list of affiliation objects and return a map of (key, list of validation errors)
   *
   * @param fullname the name of the contributor/actor that will hold those affiliations
   * @param birthdate the holder's date of birth or creation date in (DD/MM/)YYYY format (may be null)
   * @param deathdate the holder's date of death or termination date in (DD/MM/)YYYY format (may be null)
   * @param person true if the holder of this affiliation is a person, false otherwise
   * @param affiliations the affiliations to check
   * @param field the field name in which the affiliations are present (for proper error binding)
   * @return a map of field name - list of validation errors if any, null otherwise
   */
  protected Map<String, List<ValidationError>> checkAffiliations(String fullname, String birthdate, String deathdate, boolean person,
                                                                 List<AffiliationForm> affiliations, String field) {
    Map<String, List<ValidationError>> errors = new HashMap<>();

    // if we have an id, must check that dumb user didn't change it and is trying to affiliate this actor to previous name
    String oldname = fullname;
    if (!values.isBlank(id)) {
      Actor actor = actorFactory.retrieve(id);
      oldname = actor != null ? actor.getFullname(lang) : oldname;
    }
    for (int i = 0; i < affiliations.size(); i++){
      AffiliationForm current = affiliations.get(i);
      Map<String, String> afferrors = current.checkAffiliation();
      Actor affiliationActor = (!values.isBlank(current.getAffid()) ? actorFactory.retrieve(current.getAffid()) : null);

      for (Map.Entry<String, String> entry : afferrors.entrySet()) {
        String fieldname = field + "[" + i + "]." + entry.getKey();
        errors.put(fieldname, Collections.singletonList(new ValidationError(fieldname, entry.getValue())));
      }

      // changed name and put old name as affiliation (avoid cyclic ref)
      if (oldname.equals(affiliations.get(i).getAffname())) {
        String fieldname = field + "[" + i + "].affname";
        errors.put(fieldname, Collections.singletonList(new ValidationError(fieldname, "affiliation.error.oldself")));
      }

      // same id (except -1L) or same name
      if (!values.isBlank(current.getId()) && id.equals(current.getId())
          || !values.isBlank(current.getAffname()) && oldname.equals(current.getAffname())) {
        String fieldname = field + "[" + i + "].affname";
        errors.put(fieldname, Collections.singletonList(new ValidationError(fieldname, "affiliation.error.notself")));
      }

      // dates may not be prior birth and later than death
      if (!values.isBlank(current.getStartDate()) || !values.isBlank(current.getEndDate())) {

        if (!values.isBlank(birthdate) && !values.isBefore(birthdate, current.getStartDate())) {
          String fieldname = field + "[" + i + "].start";
          errors.put(fieldname, Collections.singletonList(
              new ValidationError(fieldname, "affiliation.error." + (person ? "person" : "org") + ".start")));
        }
        if (!values.isBlank(deathdate) && !values.isBefore(current.getEndDate(), deathdate)) {
          String fieldname = field + "[" + i + "].end";
          errors.put(fieldname, Collections.singletonList(
              new ValidationError(fieldname, "affiliation.error." + (person ? "person" : "org") + ".end")));
        }
        if (!values.isBlank(current.getStartDate()) && values.isBlank(current.getStartDateType())) {
          String fieldname = field + "[" + i + "].starttype";
          errors.put(fieldname, Collections.singletonList(
                  new ValidationError(fieldname, "affiliation.error.datetype")));
        }
        if (!values.isBlank(current.getEndDate()) && values.isBlank(current.getEndDateType())) {
          String fieldname = field + "[" + i + "].endtype";
          errors.put(fieldname, Collections.singletonList(
                  new ValidationError(fieldname, "affiliation.error.datetype")));
        }

        // affiliation dates are not included in affiliation birth-creation/death-termination dates
        if (!values.isBlank(current.getAffid())) {
          // because of previous call to checkAffiliation, we know that if we have an id, we have an affiliation actor
          switch (affiliationActor.getActorType()) {
            case PERSON:
              Person p = (Person) affiliationActor;
              // in case of any date is null or empty, isBefore returns true
              if (!values.isBefore(p.getBirthdate(), current.getStartDate())) {
                String fieldname = field + "[" + i + "].start";
                errors.put(fieldname, Collections.singletonList(new ValidationError(fieldname, "affiliation.error.before.born.start")));
              }
              if (!values.isBefore(current.getEndDate(), p.getDeathdate())) {
                String fieldname = field + "[" + i + "].end";
                errors.put(fieldname, Collections.singletonList(new ValidationError(fieldname, "affiliation.error.after.death.end")));
              }
              break;

            case ORGANIZATION:
              Organization o = (Organization) affiliationActor;
              // in case of any date is null or empty, isBefore returns true
              if (!values.isBefore(o.getCreationDate(), current.getStartDate())) {
                logger.debug(current.getStartDate() + " " + o.getCreationDate());
                String fieldname = field + "[" + i + "].start";
                errors.put(fieldname, Collections.singletonList(new ValidationError(fieldname, "affiliation.error.before.creation.start")));
              }
              if (!values.isBefore(current.getEndDate(), o.getTerminationDate())) {
                logger.debug(current.getEndDate() + " " + o.getTerminationDate());
                String fieldname = field + "[" + i + "].end";
                errors.put(fieldname, Collections.singletonList(new ValidationError(fieldname, "affiliation.error.after.termination.end")));
              }
              break;

            default:
              // ignore, nothing to do
          }
        }
      }

      // check type of affiliation if holder is not a person
      if (!person) {
        String fieldname = field + "[" + i + "].afftype";
        if (!values.isBlank(current.getAfftype())) {
          // valid value
          if (!values.isNumeric(current.getAfftype())) {
            errors.put(fieldname, Collections.singletonList(new ValidationError(fieldname, "affiliation.error.type.unset")));
          } else {
            EAffiliationType parsedType = EAffiliationType.value(Integer.parseInt(current.getAfftype()));
            if (parsedType == null) {
              errors.put(fieldname, Collections.singletonList(new ValidationError(fieldname, "affiliation.error.type.wrong")));
            }
          }
        } else {
          errors.put(fieldname, Collections.singletonList(new ValidationError(fieldname, "affiliation.error.type.unset")));
        }
      }
    }
    return errors.isEmpty() ? null : errors;
  }

  /**
   * Redefine equals at this level since all contributions may have an id. Hashcode method must be overridden
   * in concrete classes
   *
   * @param obj an object to compare to this
   * @return true if given object is considered as equal to this object
   */
  @Override
  public boolean equals(Object obj) {

    if (obj == null || !(obj instanceof ContributionHolder)) {
      return false;
    }

    ContributionHolder c = (ContributionHolder) obj;
    if (!id.equals(-1L) && !c.getId().equals(-1L)) {
      return id.equals(c.getId());
    }

    return hashCode() == obj.hashCode();
  }

  @Override
  public int hashCode() {
    return 59 * Long.hashCode(id) * Long.hashCode(version);
  }

  @Override
  public int compareTo(ContributionHolder o) {
    String name = getNameToCompare(this);
    String otherName = getNameToCompare(o);

    if (name == null) {
      return 1;
    }

    if (otherName == null) {
      return -1;
    }

    return name.compareToIgnoreCase(otherName);
  }

  /**
   * Get a comparable name for given contribution holder (depending on concrete implementation
   *
   * @param holder a contribution holder
   * @return a name, depending on the concrete type
   */
  protected String getNameToCompare(ContributionHolder holder) {
    switch (holder.getType()) {
      case ACTOR:
        return ((ActorHolder) holder).getFullname();
      case DEBATE:
        return ((DebateHolder) holder).getTitle();
      case ARGUMENT:
        return ((ArgumentHolder) holder).getTitle();
      case ARGUMENT_CONTEXTUALIZED:
        return ((ArgumentContextHolder) holder).getArgument().getTitle();
      case EXCERPT:
        return ((ExcerptHolder) holder).getOriginalExcerpt();
      case TEXT:
        return  ((TextHolder) holder).getTitle();
      default:
        logger.warn("wrong type passed as holder or should not be used " + holder.getId() + " with type " + holder.getType());
        return null;
    }
  }

  /**
   * Create Place with place form
   *
   * @param place the place form
   * @return the created place
   */
  protected Place createPlaceFromForm(PlaceForm place){
    Place p = null;
    if(place != null) {
      Long placeId = actorFactory.retrievePlaceByGeonameIdOrPlaceId(place.getGeonameId(), place.getId());
      if (placeId == null) {
        PlaceType t = actorFactory.findPlaceTypeByCode(place.getPlaceType());
        if (t != null) {
          Map<String, String> names = new HashMap<>();
          // Add names from form
          place.getNames().forEach(n -> names.put(n.getLang(), n.getName()));
          // Create the place
          p = actorFactory.createPlace(place.getId(), place.getGeonameId(),
              place.getCode(), place.getLatitude(), place.getLongitude(), names);
          p.setPlaceType(t);
          p.setContinent(createPlaceFromForm(place.getContinent()));
          p.setCountry(createPlaceFromForm(place.getCountry()));
          p.setRegion(createPlaceFromForm(place.getRegion()));
          p.setSubregion(createPlaceFromForm(place.getSubregion()));
        }
      } else {
        p = actorFactory.createSimplePlace(placeId);
      }
    }
    return p;
  }

    /*
   * PRIVATE HELPER
   */

  /**
   * Add filterable value for given date with given filterable id
   *
   * @param filter the filter id
   * @param date a stringified date (in DD/MM/YYYY format with DD and MM optional)
   */
  protected void addDateFilterable(EFilterName filter, String date) {
    if (!values.isBlank(date)) {
      String[] temp = date.split("/");
      addFilterable(filter, temp[temp.length - 1]);
    }
  }

  public String getContributionTile(){
    switch (type) {
      case ACTOR:
        return this instanceof ActorHolder ? ((ActorHolder) this).getFullname() : "";
      case TEXT:
        return this instanceof TextHolder ? ((TextHolder) this).getTitle() : "";
      case DEBATE:
        return this instanceof DebateHolder ? ((DebateHolder) this).getFullTitle() : "";
      case ARGUMENT_CONTEXTUALIZED:
        return this instanceof ArgumentContextHolder ? ((ArgumentContextHolder) this).getFullTitle() : "";
      case EXCERPT:
        return this instanceof ExcerptHolder ? ((ExcerptHolder) this).getWorkingExcerpt() : "";
      case FOLDER:
        return this instanceof FolderHolder ? ((FolderHolder) this).getFolderName() : "";
      default:
        return "";
    }
  }

  /**
   * Get the data about this contribution for media sharing
   *
   * @return the media shared data object
   */
  abstract public MediaSharedData getMediaSharedData();

  /**
   * Get the description of the contribution as string
   *
   * @return the string contribution description
   */
  abstract public String getContributionDescription();

  /**
   * Inner class that represents data needed for social media sharing
   */
  public class MediaSharedData {
    private String title;
    private String description;
    private String image;

    public MediaSharedData(String title, String image){
      this.title = title;
      this.description = getContributionDescription();
      this.image = (image.startsWith("/") ? image : "https://webdeb.be/assets/images/picto/" + image + ".png");
    }

    public String getTitle() {
      return title;
    }

    public String getDescription() {
      return description;
    }

    public String getImage() {
      return image;
    }
  }

  /*
   * OTHERS
   */

  /**
   * Create a link to redirect to the given contribution
   *
   * @return a html link to redirect to contribution
   */
  protected String writeContributionHtmlLink(Long id, EContributionType type, String name){
    return "<a class=\"primary\" href=\"" + be.webdeb.presentation.web.controllers.viz.routes.VizActions.dispatch(id, type.id(), 0) + "\">" + name + "</a>";
  }

  /**
   * Create an action button to do something with a place
   *
   * @return a button for a place
   */
  protected String writePlaceHtmlLink(Long id, String name){
    return "<button type=\"button\" class=\"btn btn-link btn-link-simple primary btn-place-viz\" data-id =\"" + this.id + "\" data-place-id=\"" + id + "\">" + name + "</button>";
  }



}
