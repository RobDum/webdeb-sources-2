/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.folder;

import be.webdeb.core.api.folder.Folder;
import be.webdeb.core.api.folder.FolderFactory;
import be.webdeb.core.api.folder.FolderLink;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import javax.inject.Inject;

import play.api.Play;
import play.data.validation.ValidationError;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Wrap all fields of a folder link.
 *
 * @author Martin Rouffiange
 */
public class FolderLinkForm extends FolderLinkHolder {

  @Inject
  private FolderFactory factory = Play.current().injector().instanceOf(FolderFactory.class);

  /**
   * JSON - Play compliant constructor
   */
  public FolderLinkForm() {
    super();
  }

  /**
   * Construct a link form with given folder link
   *
   * @param link a folder link
   * @param lang the language code (2-char ISO) of the user interface
   */
  public FolderLinkForm(FolderLink link, String lang) {
    super(link, lang);
  }

  /**
   * Construct a link form with given folder parent and child
   *
   * @param parent a parent folder
   * @param child a child folder
   * @param lang the language code (2-char ISO) of the user interface
   */
  public FolderLinkForm(Folder parent, Folder child, String lang) {
    super(parent, child, lang);
  }

  /**
   * Construct a link form with given folder parent and child id
   *
   * @param parent a parent folder id
   * @param child a child folder id
   * @param lang the language code (2-char ISO) of the user interface
   */
  public FolderLinkForm(Long parent, Long child, String lang) {
    super(parent, child, lang);
  }

  /**
   * Form validation (implicit call from form submit)
   *
   * @return a map of ValidationError if any error in form was found, null otherwise
   */
  public Map<String, List<ValidationError>> validate() {
    Map<String, List<ValidationError>> errors = new HashMap<>();
    /*if (!empty()) {
      if (parentId.equals(childId)) {
        List<ValidationError> list = new ArrayList<>();
        list.add(new ValidationError("destinationArgument", "argument.links.error.toarg"));
        errors.put("destinationArgument", list);
      }
    }*/

    // must return null if errors is empty
    return errors.isEmpty() ? null : errors;
  }

  /**
   * Save this FolderLinkForm in persistence layer
   *
   * @param contributor the contributor id that created this link
   *
   * @throws FormatException if any field contains error
   * @throws PermissionException if given contributor may not perform this action or if such action would cause
   * integrity problems
   * @throws PersistenceException if any error occurred at the persistence layer (concrete error is wrapped)
   *
   */
  public void save(Long contributor) throws FormatException, PermissionException, PersistenceException {
    logger.debug("try to save link from folder " + toString() + " with version " + version + " in group " + inGroup);
    toLink().save(contributor, inGroup);
  }

  /**
   * Transform this form into an API form link
   *
   * @return an API link corresponding to this form link form
   */
  public FolderLink toLink() {
    FolderLink link = factory.getFolderLink();
    link.setId(id);
    link.setVersion(version);
    link.setParent(factory.retrieve(parentId));
    link.setChild(factory.retrieve(childId));
    link.addInGroup(inGroup);
    return link;
  }

  /**
   * Check if parent or child are empty
   *
   * @return true if all fields are considered as empty (null or "")
   */
  public boolean isEmpty() {
    return parentId == null || parentId == -1 || childId == null || childId == -1;
  }

  /*
   * SETTERS
   */

  /**
   * Set the id of the parent folder
   *
   * @param parent a folder id
   */
  public void setParentId(Long parent) {
    parentId = parent;
  }

  /**
   * Set the id of the child folder
   *
   * @param child a folder id
   */
  public void setChildId(Long child) {
    childId = child;
  }
}
