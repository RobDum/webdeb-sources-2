/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.excerpt;

import be.webdeb.core.api.argument.EArgumentLinkShade;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.presentation.web.controllers.entry.ContributionHolder;

import java.util.Map;

/**
 * This simple wrapper is used to contain only excerpt's id, parent's text id, original and working excerpt.
 * Mainly used to create a new excerpt or to manipulate excerpts with minimum data
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public class ExcerptSimpleForm extends ContributionHolder {

    protected Long textId;
    protected String originalExcerpt;
    protected String workingExcerpt;
    protected String shade = "";
    protected Map<Integer, Integer> shadesAmount = null;


    /**
     * Constructor (Play / Json compliant)
     */
    public ExcerptSimpleForm() {
        type = EContributionType.EXCERPT;
    }

    /**
     * Constructor for an empty form referencing a text
     *
     * @param textId the text id to which this new excerpt will belong to
     */
    public ExcerptSimpleForm(Long textId) {
        this();
        this.textId = textId;
    }

    /**
     * Constructor with a given excerpt
     *
     * @param excerpt an API excerpt to be wrapped
     */
    public ExcerptSimpleForm(Excerpt excerpt, String lang) {
        this(excerpt.getTextId());
        id = excerpt.getId();
        version = excerpt.getVersion();
        originalExcerpt = excerpt.getOriginalExcerpt();
        workingExcerpt = values.isBlank(excerpt.getOriginalExcerpt()) ? excerpt.getOriginalExcerpt() : excerpt.getWorkingExcerpt(lang, true);
    }

    @Override
    public MediaSharedData getMediaSharedData() {
        return null;
    }

    @Override
    public String getContributionDescription() {
        return null;
    }

    @Override
    public String getDefaultAvatar(){
        return "";
    }

    /**
     * Get the text id from which this excerpt has been extracted
     *
     * @return an id
     */
    public Long getTextId() {
        return textId;
    }

    /**
     * Set the text id from which this excerpt has been extracted
     *
     * @param textId a text id
     */
    public void setTextId(Long textId) {
        this.textId = textId;
    }

    /**
     * Get the original excerpt from which this excerpt has been extracted
     *
     * @return the original excerpt
     */
    public String getOriginalExcerpt() {
        return originalExcerpt;
    }

    /**
     * Set the original excerpt from which this excerpt has been extracted
     *
     * @param excerpt an original excerpt
     */
    public void setOriginalExcerpt(String excerpt) {
        this.originalExcerpt = excerpt;
    }

    /**
     * Get the working excerpt(free-form part) corresponding to the original excerpt
     *
     * @return a working excerpt
     */
    public String getWorkingExcerpt() {
        return workingExcerpt;
    }

    /**
     * Set the working excerpt(free-form part) corresponding to the original excerpt
     *
     * @param excerpt a working excerpt
     */
    public void setWorkingExcerpt(String excerpt) {
        this.workingExcerpt = excerpt;
    }

    /**
     * Get the shade if we want to link this Excerpt
     *
     * @return a argument link shade
     */
    public String getShade() {
        return shade;
    }

    /**
     * Set the shade if we want to link this Excerpt
     *
     * @param shade a argument link shade
     */
    public void setShade(String shade) {
        this.shade = shade;
    }
}