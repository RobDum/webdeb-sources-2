/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.excerpt;

import be.webdeb.core.api.actor.ActorRole;
import be.webdeb.core.api.argument.EArgumentLinkShade;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.excerpt.*;
import be.webdeb.core.api.folder.Folder;
import be.webdeb.core.api.text.Text;
import be.webdeb.presentation.web.controllers.entry.ContributionHolder;
import be.webdeb.presentation.web.controllers.entry.EFilterName;
import be.webdeb.presentation.web.controllers.entry.PlaceForm;
import be.webdeb.presentation.web.controllers.entry.actor.ActorSimpleForm;
import be.webdeb.presentation.web.controllers.entry.argument.ArgumentContextHolder;
import be.webdeb.presentation.web.controllers.entry.folder.SimpleFolderForm;
import com.fasterxml.jackson.annotation.JsonIgnore;
import play.api.Play;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This class holds concrete values of an Excerpt (no IDs, but their description, as defined in the
 * database). Except by using a constructor, no value can be edited outside of this package or by
 * subclassing.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public class ExcerptHolder extends ContributionHolder {

    static final int ORIGINAL_EXCERPT_MAX_LENGTH = 600;
    static final int WORKING_EXCERPT_MAX_LENGTH = 1000;

    @Inject
    protected ExcerptFactory excerptFactory = Play.current().injector().instanceOf(ExcerptFactory.class);

    // used for lazy loading of excerpt and text title
    protected Excerpt excerpt;

    // Note: as for all wrappers, all fields MUST hold empty values for proper form validation
    protected Long textId = -1L;
    protected String textTitle = "";
    protected String textUrl = "";

    protected EArgumentLinkShade linkShade;

    protected String originalExcerpt = "";
    protected String workingExcerpt = "";
    protected String techWorkingExcerpt = "";

    protected List<ActorSimpleForm> authors = new ArrayList<>();
    protected List<ActorSimpleForm> reporters = new ArrayList<>();
    protected List<ActorSimpleForm> citedactors = new ArrayList<>();

    // text-related properties
    protected String publicationDate;
    protected String source;
    protected String excLang = "";
    protected String textType = "";

    protected ECommentsType commentsType;

    /**
     * Play / JSON compliant constructor
     */
    public ExcerptHolder() {
        super();
        type = EContributionType.EXCERPT;
    }

    /**
     * Construct a ReadOnlyExcerpt from a given excerpt with descriptions in the given language
     *
     * @param excerpt an Excerpt
     * @param lang 2-char ISO code of context language (among play accepted languages)
     */
    public ExcerptHolder(Excerpt excerpt, String lang) {
        super(excerpt, lang);
        this.excerpt = excerpt;

        Text text = excerpt.getText();
        textId = text.getId();
        textTitle = text.getTitle(lang);
        textUrl = text.getUrl();

        // set original and working excerpt
        originalExcerpt = values.transformExcerpt(excerpt.getOriginalExcerpt());
        workingExcerpt = excerpt.getWorkingExcerpt(lang, true);
        techWorkingExcerpt = excerpt.getTechWorkingExcerpt();

        publicationDate = excerpt.getText().getPublicationDate();
        addDateFilterable(EFilterName.PUBLIDATE, publicationDate);
        source = excerpt.getText().getSourceTitle();
        addFilterable(EFilterName.SOURCE, source);
        excLang = excerpt.getText().getLanguage().getName(lang);
        addFilterable(EFilterName.LANGUAGE, excLang);
        textType = excerpt.getText().getTextType().getName(lang);
        addFilterable(EFilterName.TTYPE, textType);

        initActors(excerpt.getActors(), lang);
        initPlaces(excerpt.getPlaces(), lang);
        initFolders(excerpt.getFoldersAsList(), lang, false);
    }

    /**
     * Initialize authors, reporters and cited actors fields with given actor roles
     *
     * @param actors a list of actor roles
     * @param lang two-char ISO code used for function names
     */
    protected void initActors(List<ActorRole> actors, String lang) {
        actors.forEach(r -> {
            ActorSimpleForm a = new ActorSimpleForm(r, lang);
            if (r.isAuthor()) {
                initActorFilter(a);
                authors.add(a);
            } else if (r.isReporter()) {
                reporters.add(a);
            } else {
                citedactors.add(a);
            }
        });

        if (!reporters.isEmpty()) {
            // at least one reporter => reported
            commentsType = ECommentsType.REPORTED;
        } else {
            // no reporter => signed or co-signed
            if (authors.size() > 1) {
                commentsType = ECommentsType.COSIGNED;
            } else {
                // actor is alone and author => signed
                commentsType = ECommentsType.SIGNED;
            }
        }
    }

    /**
     * Initialize folders linked to this excerpt, by default all folders linked with the text excerpt
     *
     * @param folders a list of folders
     * @param lang two-char ISO code used for function names
     */
    protected void initFolders(List<Folder> folders, String lang) {
        folders.forEach(f -> {
            this.folders.add(new SimpleFolderForm(f, lang));
            addFilterable(EFilterName.FOLDER, f.getName(lang));
        });
    }

    @Override
    public String toString() {
        StringBuilder actors = new StringBuilder();
        if (getActors() != null) {
            for (ActorSimpleForm a : getActors()) {
                actors.append(a.getFullname() + " (" + a.getId() + ") " + a.getFullFunction() + " (" + a.getAha()
                        + ") author/reporter: " + a.getAuthor() + "-" + a.getReporter() + ", ");
            }
        }
        return "excerpt [" + id + "] with original excerpt : " + originalExcerpt;
    }

    @Override
    public String getContributionDescription(){
        List<String> descriptions = new ArrayList<>();
        descriptions.add(originalExcerpt);
        descriptions.add(textTitle);


        return String.join(", ", descriptions);
    }

    @Override
    public MediaSharedData getMediaSharedData(){
        if(mediaSharedData == null){
            mediaSharedData = new MediaSharedData(originalExcerpt, "excerpt");
        }
        return mediaSharedData;
    }

    @Override
    public String getDefaultAvatar(){
        return "";
    }

    /**
     * Get a complete description of this excerpt
     *
     * @return the description
     */
    public String getDescription() {
        return " (" + getFolderDescription(false) +  (places.size() > 0 ? ", "
            + getPlaceDescription() : "") + ", " + getActorDescription(getActors(3), false) + ")";
    }

    /**
     * Get a complete description of this excerpt with redirection link for context elements (like folders, places)
     *
     * @return the description with link
     */
    public String getDescriptionWithLink() {
        return " (" + getFolderDescription(true) + (places.size() > 0 ? ", "
            + getPlaceDescription(true) : "") + ", " + getActorDescription(getActors(3), true) + ")";
    }

    /*
     * GETTERS
     */

    /**
     * Get the text id from which this excerpt originates from
     *
     * @return a text id
     */
    public Long getTextId() {
        return textId;
    }

    /**
     * Get the text url where it comes, if any
     *
     * @return the text source url
     */
    public String getTextUrl() {
        if (values.isBlank(textUrl)) {
            textUrl = excerpt.getText().getUrl();
        }
        return textUrl;
    }

    /**
     * Get the text title where this excerpt originating from
     *
     * @return a text title
     */
    public String getTextTitle() {
        if (values.isBlank(textTitle) && excerpt != null) {
            textTitle = excerpt.getText().getTitle(lang);
        }
        return textTitle;
    }

    /**
     * Get the original excerpt
     *
     * @return an original excerpt of this.textId
     */
    public String getOriginalExcerpt() {
        return originalExcerpt;
    }

    /**
     * Get the working excerpt
     *
     * @return a working excerpt of this.textId
     */
    public String getWorkingExcerpt() {
        return workingExcerpt;
    }

    /**
     * Get the technical working excerpt for construct the workingExcerpt api
     *
     * @return a technical working excerpt
     */
    public String getTechWorkingExcerpt() {
        return techWorkingExcerpt;
    }

    /**
     * Get the list of folders linked to this excerpt
     *
     * @return a list of folders
     */
    public List<SimpleFolderForm> getFolders() {
        return folders;
    }

    /**
     * Get the list of authors of this excerpt
     *
     * @return a list of authors
     */
    public List<ActorSimpleForm> getAuthors() {
        return authors;
    }

    /**
     * Get the list of actors being the thinker of this text
     *
     * @return the list of actors
     */
    public List<ActorSimpleForm> getReporters() {
        return reporters.isEmpty() ? authors : reporters;
    }

    /**
     * Get the list of actors being the authors of this excerpt limited by max size
     *
     * @return the list of actors
     */
    public List<ActorSimpleForm> getAuthors(int maxSize) {
        return authors.stream().limit(maxSize).collect(Collectors.toList());
    }

    /**
     * Get the list of actors being the thinker  of this excerpt limited by max size
     *
     * @return the list of actors
     */
    public List<ActorSimpleForm> getReporters(int maxSize) {
        return getReporters().stream().limit(maxSize).collect(Collectors.toList());
    }

    /**
     * Get the list of cited actors of this excerpt
     *
     * @return a list of cited actors
     */
    public List<ActorSimpleForm> getCitedactors() {
        return citedactors;
    }

    /**
     * Get the list of all directly involved actors as authors or reporters
     *
     * @return a list of actors
     */
    @JsonIgnore
    public List<ActorSimpleForm> getActors() {
        return Stream.concat(Stream.concat(authors.stream(), reporters.stream()), citedactors.stream()).collect(Collectors.toList());
    }

    /**
     * Get the list of all directly involved actors as authors or reporters limited by size
     *
     * @param maxSize the limit of actors to get
     * @return a list of actors
     */
    @JsonIgnore
    public List<ActorSimpleForm> getActors(int maxSize) {
        return getActors().stream().limit(maxSize).collect(Collectors.toList());
    }

    /**
     * Get the source name of the text from which this excerpt has been extracted
     *
     * @return the owning text source name
     */
    public String getSource() {
        return source;
    }

    /**
     * Get the publication date of the text from which this excerpt has been extracted
     *
     * @return a string representation of date of the form DD/MM/YYYY (D and M optional)
     */
    public String getPublicationDate() {
        return publicationDate;
    }

    /**
     * Get the language of this excerpt (by default, same as owning text language)
     *
     * @return two char ISO code of the excerpt's language
     */
    public String getExcLang() {
        return excLang;
    }

    /**
     * Get the text type from which this excerpt originates from
     *
     * @return the text type
     */
    public String getTextType() {
        return textType;
    }

    public ECommentsType getCommentsType() {
        return commentsType;
    }

    public EArgumentLinkShade getLinkShade() {
        return linkShade;
    }

    public void setLinkShade(EArgumentLinkShade linkShade) {
        this.linkShade = linkShade;
    }

    public boolean isThinker(Long actorId){
        return excerpt.actorIsThinker(actorId);
    }
}
