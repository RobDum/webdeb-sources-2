/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.argument;

import be.webdeb.core.api.actor.ActorRole;
import be.webdeb.core.api.argument.*;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.debate.Debate;
import be.webdeb.core.api.text.Text;
import be.webdeb.presentation.web.controllers.entry.EFilterName;
import be.webdeb.presentation.web.controllers.entry.actor.ActorSimpleForm;
import be.webdeb.presentation.web.controllers.entry.excerpt.ExcerptForm;
import be.webdeb.presentation.web.controllers.permission.WebdebUser;
import com.fasterxml.jackson.annotation.JsonIgnore;
import play.api.Play;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This class holds concrete values of a contextualized Argument (no IDs, but their description, as defined in the
 * database). Except by using a constructor, no value can be edited outside of this package or by
 * subclassing.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public class ArgumentContextHolder extends BaseArgumentHolder {

    @Inject
    protected ArgumentFactory argumentFactory = Play.current().injector().instanceOf(ArgumentFactory.class);

    protected EContributionType contextType;
    protected ArgumentContext argumentContext;

    protected EArgumentLinkShade linkShade;

    protected Long contextId = -1L;
    protected Long contextFirstArgumentId = -1L;
    protected String contextFirstArgumentTitle = "";
    protected String contextTitle = "";
    protected Long argumentId = -1L;
    protected ArgumentHolder argument;

    protected Map<Integer, Integer> excerptMetric = null;

    protected List<IllustrationLinkForm> excerpts = new ArrayList<>();

    protected List<ActorSimpleForm> citedactors = new ArrayList<>();

    // all justification links this argument is the source, i.e this ---> fromlinks
    protected List<JustificationLinkForm> fromlinks;
    // all justification links this argument is the target, i.e tolinks ---> this
    protected List<JustificationLinkForm> tolinks;

    // all justification links that could justify this argument
    protected List<ArgumentContextForm> potentialJustificationLinks;
    // all justification links that could be linked to this argument
    protected List<ExcerptForm> potentialIllustrationLinks;

    protected boolean isForDebate = false;

    /**
     * Play / JSON compliant constructor
     */
    public ArgumentContextHolder() {
        super();
        type = EContributionType.ARGUMENT_CONTEXTUALIZED;
    }

    /**
     * Construct a ReadOnlyArgumentContext from a given contextualized argument with descriptions in the given language
     *
     * @param argumentContext an ArgumentContext
     * @param lang 2-char ISO code of context language (among play accepted languages)
     */
    public ArgumentContextHolder(ArgumentContext argumentContext, String lang, boolean fromForm) {
        super(argumentContext, lang);
        if(fromForm){
            initArgumentContext(argumentContext, lang);
        }
    }

    /**
     * Construct a ReadOnlyArgumentContext from a given contextualized argument with descriptions in the given language
     *
     * @param argumentContext an ArgumentContext
     * @param lang 2-char ISO code of context language (among play accepted languages)
     */
    public ArgumentContextHolder(ArgumentContext argumentContext, String lang) {
        super(argumentContext, lang);
        initArgumentContext(argumentContext, lang);
        initFolders(argumentContext.getFoldersAsList(), lang, false);
    }

    /**
     * Construct a ArgumentContext from a given contextualized argument  in the given language
     *
     * @param argumentContext an ArgumentContext
     * @param lang 2-char ISO code of context language (among play accepted languages)
     */
    public void initArgumentContext(ArgumentContext argumentContext, String lang){
        this.argumentContext = argumentContext;

        argument = new ArgumentHolder(argumentContext.getArgument(), lang);
        addFilterable(EFilterName.ARGTYPE, argument.getShadeterm());

        argumentId = argument.getId();
        title = argument.getTitle();
        fullTitle = argument.getFullTitle();
        shade = argument.getShade();
        language = argument.getLanguage();
        languageCode = argument.getLanguageCode();
        addFilterable(EFilterName.LANGUAGE, language);

        contextFirstArgumentId = argumentContext.getContextFirstArgument() == null ? -1L : argumentContext.getContextFirstArgument().getId();
        contextFirstArgumentTitle = argumentContext.getContextFirstArgument() == null ? "" : argumentContext.getContextFirstArgument().getTitle(lang);

        initPlaces(argumentContext.getPlaces(), lang);
        initContext(argumentContext.getContext(), lang);
        initActors(argumentContext.getActors(), lang);
    }

    /**
     * Initialize cited actors fields with given actor roles
     *
     * @param actors a list of actor roles
     * @param lang two-char ISO code used for function names
     */
    protected void initActors(List<ActorRole> actors, String lang) {
        actors.forEach(r -> {
            ActorSimpleForm a = new ActorSimpleForm(r, lang);
            if (!r.isAuthor() && !r.isReporter()) {
                citedactors.add(a);
            }
        });
    }

    /**
     * Get the amount of argument illustration links linked with this contextualized argument that a given contributor can see by given shade
     *
     * @param contributor a contributor
     * @return a map of shade / amount
     */
    public Map<EArgumentLinkShade, Integer> getArgumentIllustrationShadesAmount(Long contributor) {
        return argumentContext.getArgumentShadesAmount(contributor,
                Arrays.asList(EArgumentLinkShade.ILLUSTRATE, EArgumentLinkShade.SHADED_EXAMPLE, EArgumentLinkShade.COUNTER_EXAMPLE));
    }

    /**
     * Initialize the context contribution linked with this contextualized argument
     *
     * @param context the context contribution
     * @param lang 2-char ISO code of context language (among play accepted languages)
     */
    protected void initContext(Contribution context, String lang) {
        contextId = context.getId();
        contextType = context.getContributionType().getEContributionType();
        switch (contextType){
            case DEBATE:
                contextTitle = ((Debate) context).getTitle();
                break;
            case TEXT:
                contextTitle = ((Text) context).getTitle(lang);
                break;
            default:
                logger.debug("The context contribution must be a debate or a text.");
        }
    }

    /**
     * Lazy loading of justification links
     */
    protected void initJustificationLinks() {
        tolinks = new ArrayList<>();
        fromlinks = new ArrayList<>();

        argumentContext.getJustificationLinksThatAreJustified().forEach(l -> tolinks.add(new JustificationLinkForm(l, lang)));
        argumentContext.getJustificationLinksThatJustify().forEach(l -> fromlinks.add(new JustificationLinkForm(l, lang)));
    }

    /**
     * Get the list of potential justification links for this argument
     *
     * @param contextId the context where whe need to found candidates
     * @return a list of justification links that could be linked to this argument
     */
    @JsonIgnore
    public List<ArgumentContextForm> getPotentialJustificationLinks(Long contextId) {
        if (potentialJustificationLinks == null) {
            potentialJustificationLinks = argumentContext.getPotentialJustificationLinks(contextId).stream()
                    .map(e -> new ArgumentContextForm(e, lang)).collect(Collectors.toList());
        }
        return potentialJustificationLinks;
    }

    /**
     * Get the list of potential illustration links for this argument
     *
     * @param contextId the context where whe need to found candidates
     * @return a list of illustration links that could be linked to this argument
     */
    @JsonIgnore
    public List<ExcerptForm> getPotentialIllustrationLinksLinks(Long contextId) {
        if (potentialIllustrationLinks == null) {
            potentialIllustrationLinks = argumentContext.getPotentialIllustrationLinks(contextId).stream()
                    .map(e -> new ExcerptForm(e, lang)).collect(Collectors.toList());
        }
        return potentialIllustrationLinks;
    }

    @Override
    public String getDefaultAvatar(){
        return "";
    }

    /**
     * Get a complete description of this contextualized argument
     *
     * @return the description
     */
    public String getDescription() {
        return places.size() > 0 ? " (" + getPlaceDescription() + ")" : "";
    }

    /**
     * Get a complete description of this contextualized argument with redirection link for context elements (like folders, places)
     *
     * @return the description with link
     */
    public String getDescriptionWithLink() {
        return places.size() > 0 ? " (" + getPlaceDescription(true) + ")" : "";
    }

    @Override
    public String toString() {
        return "argument context [" + id + "] with title " + title + ", argument " + argumentId +
                " and context " + contextId;
    }

    @Override
    public String getContributionDescription(){
        List<String> descriptions = new ArrayList<>();
        descriptions.add(fullTitle);

        return String.join(", ", descriptions);
    }

    @Override
    public MediaSharedData getMediaSharedData(){
        if(mediaSharedData == null){
            mediaSharedData = new MediaSharedData(fullTitle, "argument");
        }
        return mediaSharedData;
    }

    /*
     * GETTERS
     */

    /**
     * Get the argument holder
     *
     * @return an argument holder
     */
    @JsonIgnore
    public ArgumentHolder getArgument() {
        return argument;
    }

    /**
     * Get the context contribution id
     *
     * @return a contribution id
     */
    public Long getContextId() {
        return contextId;
    }

    /**
     * Get the context contribution title
     *
     * @return a contribution title
     */
    public String getContextTitle() {
        return contextTitle;
    }

    /**
     * Get the argument id of this contextualized argument
     *
     * @return an argument id
     */
    public Long getArgumentId() {
        return argumentId;
    }

    /**
     * Get the context contribution id where this contextualized argument is the first argument.
     *
     * @return a context contribution id
     */
    public Long getContextFirstArgumentId() {
        return contextFirstArgumentId;
    }

    /**
     * Get the context contribution title where this contextualized argument is the first argument.
     *
     * @return a context contribution title
     */
    public String getContextFirstArgumentTitle() {
        return contextFirstArgumentTitle;
    }

    /**
     * Get the context contribution type
     *
     * @return a contribution type
     * @see EContributionType
     */
    public EContributionType getContextType(){
        return contextType;
    }

    /**
     * Get the list of cited actors of this argument
     *
     * @return a list of cited actors
     */
    public List<ActorSimpleForm> getCitedactors() {
        return citedactors;
    }

    public EArgumentLinkShade getLinkShade() {
        return linkShade;
    }

    public void setLinkShade(EArgumentLinkShade linkShade) {
        this.linkShade = linkShade;
    }

    public boolean isFirstArgument() {
        return contextFirstArgumentId != -1L;
    }

    /**
     * Retrieve the amount of excerpts linked to this argument by shade as integer map
     *
     * @return user the current user
     * @return the amount of excerpts that are connected to this argument by shade as integer map
     */
    public synchronized Map<Integer, Integer> getExcerptMetrics(WebdebUser user) {
        if(excerptMetric == null) {
            excerptMetric = argumentContext.getSimpleExcerptMetrics(user.getId(), user.getGroupId());
        }
        return excerptMetric;
    }

    /**
     * Get the list of justification links where this contextualized argument is the origin
     *
     * @return a list of justification links having this contextualized argument as their origin
     */
    @JsonIgnore
    public List<JustificationLinkForm> getFromlinks() {
        if (fromlinks == null) {
            initJustificationLinks();
        }
        return fromlinks;
    }

    /**
     * Get the list of justification links where this contextualized argument is the destination
     *
     * @return a list of justification links having this contextualized argument as their destination
     */
    @JsonIgnore
    public List<JustificationLinkForm> getTolinks() {
        if (tolinks == null) {
            initJustificationLinks();
        }
        return tolinks;
    }
}