/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.debate;

import be.objectify.deadbolt.java.actions.Restrict;
import be.webdeb.application.query.BadQueryException;
import be.webdeb.application.query.EQueryKey;
import be.webdeb.core.api.argument.*;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.debate.*;

import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.presentation.web.controllers.SessionHelper;
import be.webdeb.presentation.web.controllers.entry.argument.ArgumentActions;
import be.webdeb.presentation.web.controllers.permission.WebdebRole;
import be.webdeb.presentation.web.controllers.permission.WebdebUser;

import be.webdeb.presentation.web.controllers.CommonController;
import be.webdeb.presentation.web.controllers.viz.EVizPane;
import be.webdeb.presentation.web.views.html.entry.debate.editDebate;

import be.webdeb.presentation.web.views.html.util.message;
import play.data.Form;
import play.libs.Json;
import play.mvc.Call;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;

import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

/**
 * Debate-related actions, ie controller of all pages to edit debates
 *
 * @author Martin Rouffiange
 */
public class DebateActions extends CommonController {

    @Inject
    private ArgumentActions argumentActions;

    /**
     * Display the page to edit the debate properties
     *
     * @param debateId the debate id to which this debate comes from
     * @return the property page with given debate data
     */
    @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
    public CompletionStage<Result> edit(Long debateId) {
        logger.debug("GET debate properties of " + debateId);
        WebdebUser user = sessionHelper.getUser(ctx());

        // prepare wrapper for debate
        Debate debate = debateFactory.retrieve(debateId);
        DebateForm form;
        Map<String, String> messages = new HashMap<>();

        if (debate == null) {
            form = new DebateForm();
            // set current language
            form.setLang(ctx().lang().code());
        }
        else{
            form = new DebateForm(debate, ctx().lang().code());
        }
        // set default group
        form.setInGroup(sessionHelper.getCurrentGroup(ctx()));

        return CompletableFuture.supplyAsync(() ->
                ok(editDebate.render(formFactory.form(DebateForm.class).fill(form), helper, user, messages)), context.current());
    }

    /**
     * Update or create a debate
     *
     * @param debateId a debate id (-1 if new one)
     * @return the debate form object if it contains errors or debate data page.
     */
    @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
    public CompletionStage<Result> save(Long debateId) {
        logger.debug("POST save properties of " + debateId);

        Form<DebateForm> form = formFactory.form(DebateForm.class).bindFromRequest();
        Http.Context ctx = ctx();
        Map<String, String> messages = new HashMap<>();

        // check errors, sends back whole form if any
        if (form.hasErrors()) {
            logger.debug("form has errors " + form.errors() + "\nData:" + form.data());
            messages.put(SessionHelper.WARNING, i18n.get(ctx.lang(),SessionHelper.ERROR_FORM));
            return CompletableFuture.supplyAsync(() ->
                            badRequest(editDebate.render(form, helper, sessionHelper.getUser(ctx), messages)),
                    context.current());
        }

        DebateForm debate = form.get();

        ArgumentContext argumentContext = argumentFactory.retrieveContextualized(debate.getFirstArgumentId());
        // check if argument exists or if we do not have a contextualized argument with same title
        if (values.isBlank(debate.getId()) && !debate.getIsNotSame()) {
            Set<Debate> same = new HashSet<>(searchDebates(debate.getTitle(), 0, 10));

            if (!same.isEmpty()) {
                // send back form to ask if the found debates are not this one
                debate.setCandidates(new ArrayList<>(same));
                return CompletableFuture.supplyAsync(() ->
                        badRequest(editDebate.render(form, helper, sessionHelper.getUser(ctx()), messages)), context.current());
            }
        }

        if(argumentContext == null || argumentContext.getArgument() == null){
            Argument argument = argumentFactory.findUniqueByTitle(debate.getTitle());
            debate.setFirstArgumentId(argument == null ? -1L : argument.getId());
        }

        // check folder name matches
        if (!helper.searchForFolderNameMatches(debate.getFolders(), ctx().lang().code()).isEmpty()) {
            return CompletableFuture.supplyAsync(() ->
                    badRequest(editDebate.render(form, helper, sessionHelper.getUser(ctx()), messages)), context.current());
        }

        // get debate first argument language if none
        if(values.isBlank(debate.getLanguage())) {
            debate.setLanguage(values.detectTextLanguage(debate.getFullTitle(), getLanguageDetectionApiKey(), ctx().lang().code()));
        }

        debate.setPlaces(savePlaces(debate.getPlaces(), true));

        // try to save in DB
        try {
            treatSaveContribution(debate.save(sessionHelper.getUser(ctx).getContributor().getId()));
        } catch (Exception e) {
            logger.error("unable to save argument", e);
            ctx.flash().put(SessionHelper.ERROR, i18n.get(ctx.lang(), e.getMessage()));
            if(debate.getTmpDebate() != null) debate.getTmpDebate().deleteContextContribution(sessionHelper.getUser(ctx).getContributor().getId());
            return CompletableFuture.supplyAsync(() ->
                            badRequest(editDebate.render(form, helper, sessionHelper.getUser(ctx), messages)),
                    context.current());
        }

        // return success state
        ctx.flash().put(SessionHelper.SUCCESS, i18n.get(ctx.lang(),"argument.properties.added"));
        logger.info("REDIRECT to debate " + debateId);
        return CompletableFuture.supplyAsync(() ->
                        redirect(be.webdeb.presentation.web.controllers.viz.routes.VizActions.debate(debate.getId(), EVizPane.CARTO.id(), 0)),
                context.current());
    }

    /**
     * Create a debate from an existing contextualized argument
     *
     * @param argId a contextualized argument id
     * @return the debate page if all is ok, badrequest if the given argument is not found or internalServer error if
     * an error occurs.
     */
    @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
    public CompletionStage<Result> createFromArgumentContext(Long argId) {
        logger.debug("POST create debate from argument " + argId);
        WebdebUser user = sessionHelper.getUser(ctx());

        ArgumentContext argument = argumentFactory.retrieveContextualized(argId);
        // check if given contextualized argument exists
        if (argument == null) {
            logger.debug("given argument context not found");
            return CompletableFuture.supplyAsync(Results::badRequest, context.current());
        }

        Debate debate;
        try {
            debate = debateFactory.getDebate();
            debate.setInGroups(argument.getInGroups());
            debate.setFirstArgumentId(argument.getId());
            debate.setFirstArgument(argument);
            debate.setFirstArgumentType(
                    argumentFactory.getArgumentType(argument.getArgument().getArgumentType().getEType().convertToDebateShade().id()));
            // try to save in DB
            treatSaveContribution(debate.save(user.getId(), user.getGroupId()));
        } catch (Exception e) {
            logger.error("unable to save debate from given argument", e);
            return CompletableFuture.supplyAsync(Results::internalServerError, context.current());
        }

        // return success state
        logger.info("REDIRECT to debate " + debate.getId());
        ctx().flash().put(SessionHelper.SUCCESS, i18n.get(ctx().lang(),"argument.properties.added"));
        return CompletableFuture.supplyAsync(() ->
                        ok(be.webdeb.presentation.web.controllers.viz.routes.VizActions.debate(debate.getId(), EVizPane.CARTO.id(), 0).url()),
                context.current());
    }

    /**
     * Save justification links for given debate
     *
     * @param id a debate id
     * @return simple ok / badRequest messages depending on saving action result
     */
    @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
    public CompletionStage<Result> saveJustificationLinks(Long id, int group) {
        logger.debug("POST save links for debate " + id + " in group " + group);
        Debate debate = debateFactory.retrieve(id);

        if (debate == null) {
            logger.error("debate could not be retrieved " + id);
            return CompletableFuture.supplyAsync(() -> badRequest(""), context.current());
        }

        // convert to argument justification links and set current group
        List<ArgumentJustification> tosave = convertLinksFormToApi();
        // save links now
        try {
            debate.saveJustificationLinks(tosave, group, sessionHelper.getUser(ctx()).getId());
        }catch(PersistenceException  | PermissionException e){
            logger.debug("Error while save debate links : " + e);
            return CompletableFuture.supplyAsync(() -> internalServerError(""), context.current());
        }

        // all good, just return empty json
        logger.debug("all justification links have been updated for debate " + id);
        return CompletableFuture.supplyAsync(() -> ok(""), context.current());
    }

    /**
     * Cancel edition of given debate and either redirect to session-cached "goto" or to the context page
     *
     * @param contextId the context where back
     * @return either the session "goto" page or the context page where a debate creation was asked
     */
    @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
    public CompletionStage<Result> cancel(Long contextId) {
        logger.debug("GET cancel edit debate from " + contextId);
        Contribution c = debateFactory.retrieveContribution(contextId);
        Call defaultCall = c != null ? be.webdeb.presentation.web.controllers.viz.routes.VizActions.dispatch(contextId, c.getType().id(), 0) : null;
        // check where we have to go after in case of cancellation or happy ending
        return redirectToGoTo(defaultCall);
    }

    /*
     * AJAX calls
     */

    /**
     * Search for a debate based on given query. Will look in first contextualized argument title, country
     *
     * @param term the values to search for
     * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
     * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
     * @return a jsonified list of debate holders
     */
    @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
    public CompletionStage<Result> searchDebate(String term, int fromIndex, int toIndex) {
        List<DebateHolder> result = new ArrayList<>();
        searchDebates(term, fromIndex, toIndex).forEach(d ->
                result.add(new DebateHolder((Debate) d, ctx().lang().code())));

        return CompletableFuture.supplyAsync(() -> ok(Json.toJson(result)), context.current());
    }

    /**
     * Reload all linked contributions by type
     *
     * @param debate the debate id
     * @return a jsonified list of holders or bad request if actor is unknown or pov is unknown
     */
    public CompletionStage<Result> reloadLinkedContributions(Long debate) {
        Debate d = debateFactory.retrieve(debate);
        if(d != null) {
            return argumentActions.reloadLinkedContributions(d.getFirstArgumentId(), false);
        }
        return CompletableFuture.supplyAsync(Results::badRequest, context.current());
    }

    /**
     * Get all linked contributions by type
     *
     * @param debate the debate id
     * @param type the name of the viz pane type
     * @return a jsonified list of debate holders
     */
    public CompletionStage<Result> getLinkedContributions(Long debate, String type) {
        Debate d = debateFactory.retrieve(debate);
        if(d != null) {
            return argumentActions.getLinkedContributions(d.getFirstArgumentId(), type, EArgumentShade.value(d.getFirstArgumentType().getArgumentShade()) == EArgumentShade.DEBATE_NECESSARY ? 1 : 2, debate);
        }
        Map<String, String> messages = new HashMap<>();
        messages.put(SessionHelper.ERROR, i18n.get(ctx().lang(),"viz.debate.error"));
        return CompletableFuture.supplyAsync(() -> badRequest(message.render(messages)), context.current());
    }

    /*
     * PRIVATE HELPERS
     */

    /**
     * Search for debate for the given term
     *
     * @param term a debate title
     * @return a list of api debate corresponding to the given term, or an empty list of nothing was found
     */
    private List<Debate> searchDebates(String term, int fromIndex, int toIndex){
        List<Map.Entry<EQueryKey, String>> query = new ArrayList<>();
        query.add(new AbstractMap.SimpleEntry<>(EQueryKey.CONTRIBUTION_TYPE, String.valueOf(EContributionType.DEBATE.id())));
        query.add(new AbstractMap.SimpleEntry<>(EQueryKey.DEBATE_TITLE, term));
        try {
            return executor.searchContributions(query, fromIndex, toIndex).stream()
                    .filter(sessionHelper.getUser(ctx())::mayView).map(e -> (Debate) e).collect(Collectors.toList());
        } catch (BadQueryException e) {
            logger.warn("unable to search for debates with given term " + term, e);
        }

        return new ArrayList<>();
    }
}
