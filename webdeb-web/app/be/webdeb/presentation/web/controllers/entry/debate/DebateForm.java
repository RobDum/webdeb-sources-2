/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.debate;

import be.webdeb.core.api.argument.*;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.Place;
import be.webdeb.core.api.debate.Debate;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.presentation.web.controllers.entry.PlaceForm;
import be.webdeb.presentation.web.controllers.entry.argument.ArgumentContextForm;
import be.webdeb.presentation.web.controllers.entry.folder.SimpleFolderForm;
import play.data.validation.ValidationError;

import java.util.*;
import java.util.stream.Collectors;

public class DebateForm extends DebateHolder {

    // avoid calling valueAccessor too much
    private String shadeterm;

    protected List<DebateHolder> candidates = new ArrayList<>();
    private Debate tmpDebate;
    ArgumentContextForm firstArgumentContext;

    /**
     * Play / JSON compliant constructor
     */
    public DebateForm() {
        super();
    }

    /**
     * Create a new debate wrapper for a given first contextualized argument id and user lang
     *
     * @param argumentContext first contextualized argument
     * @param lang the user language
     */
    public DebateForm(ArgumentContext argumentContext, String lang) {
        super();
        this.argumentId = argumentContext.getArgumentId();
        this.title = argumentContext.getArgument().getDictionary().getTitle();
        language = argumentContext.getArgument().getDictionary().getLanguage().getName(lang);
        languageCode = argumentContext.getArgument().getDictionary().getLanguage().getCode();

        initFolders(argumentContext.getFoldersAsList(), lang, true);
        initPlaces(argumentContext.getPlaces(), lang);
    }

    /**
     * Create a new debate wrapper from a debate api and user lang
     *
     * @param debate the debate api
     * @param lang the user language
     */
    public DebateForm(Debate debate, String lang) {
        super(debate, lang, true);

        initFolders(argumentContext.getFoldersAsList(), lang, true);
        initPlaces(argumentContext.getPlaces(), lang);
    }

    /**
     * Form validation (implicit call from form submit)
     *
     * @return a map of ValidationError if any error in form was found, null otherwise
     */
    public Map<String, List<ValidationError>> validate() {
        Map<String, List<ValidationError>> errors = new HashMap<>();

        firstArgumentContext = new ArgumentContextForm(argumentId, id, shade, title, folders, places, lang);
        Map<String, List<ValidationError>> subErrors = firstArgumentContext.validate(false);
        languageCode = firstArgumentContext.getLanguageCode();
        if(subErrors != null)
            errors.putAll(subErrors);

        // must return null if errors is empty
        return errors.isEmpty() ? null : errors;
    }

    /**
     * Save a debate into the database. This id is updated if it was not set before.
     *
     * @param contributor the contributor id that ask to save this contribution
     * @return the map of Contribution type and a list of contribution (actors or folders) that have been created during
     * this insertion(for all unknown contributions), an empty list if none had been created
     *
     * @throws FormatException if this contribution has invalid field values (should be pre-checked before-hand)
     * @throws PermissionException if given contributor may not perform this action or if such action would cause
     * integrity problems
     * @throws PersistenceException if any error occurred at the persistence layer (concrete error is wrapped)
     */
    public Map<Integer, List<Contribution>> save(Long contributor) throws FormatException, PermissionException, PersistenceException {
        logger.debug("try to save debate " + id + " " + toString() + " with version " + version + " in group " + inGroup);
        Map<Integer, List<Contribution>> result = new HashMap<>();

        Debate debate = debateFactory.getDebate();
        debate.setId(id != null ? id : -1L);
        debate.setVersion(version);
        debate.addInGroup(inGroup);
        debate.setFirstArgumentId(firstArgumentId != null ? firstArgumentId : -1L);
        EArgumentShade eShade;
        try {
            eShade = EArgumentShade.value(Integer.parseInt(shade));
        } catch (NumberFormatException e) {
            eShade = EArgumentShade.DEBATE_NECESSARY;
        }

        // save debate first
        if(debate.getId() == -1L) {
            debate.saveContextContribution(contributor, inGroup);
            tmpDebate = debate;
        }

        if(debate.getId() != -1L) {
            // attached the newly created debate to argument
            Argument argument = getArgument(argumentId, contributor, eShade.convertToArgumentShade(), shade);
            ArgumentContext argumentContext = argumentFactory.retrieveContextualized(firstArgumentId);
            if(argumentContext == null){
                argumentContext = argumentFactory.getContextualizedArgument();
                argumentContext.setId(-1L);
                argumentContext.setContextId(debate.getId());
            }
            argumentContext.addInGroup(inGroup);
            argumentContext.setArgumentId(argument.getId());

            // folders
            argumentContext.initFolders();
            for (SimpleFolderForm f : folders.stream().filter(s -> !s.isEmpty()).collect(Collectors.toList())) {
                try {
                    argumentContext.addFolder(helper.toFolder(f, lang));
                } catch (FormatException e) {
                    logger.error("unparsable folder " + f, e);
                }
            }

            // bound places
            argumentContext.initPlaces();
            for (PlaceForm place : places) {
                if (!place.isEmpty()) {
                    Place placeToAdd = createPlaceFromForm(place);
                    if(placeToAdd != null)
                        argumentContext.addPlace(placeToAdd);
                }
            }

            argumentContext.save(contributor, inGroup);
            debate = debateFactory.retrieve(debate.getId());
            debate.setFirstArgumentId(argumentContext.getId());
            debate.setFirstArgument(argumentContext);
            debate.setFirstArgumentType(argumentFactory.getArgumentType(eShade.id()));
            result = debate.save(contributor, inGroup);
            id = debate.getId();
        }
        return result;
    }

    /**
     * Get the argument id
     *
     * @return a argument id
     */
    public Long getArgumentId() {
        return argumentId;
    }

    /**
     * Set the argument id
     *
     * @param argumentId an argument id
     */
    public void setArgumentId(Long argumentId) {
        this.argumentId = argumentId;
    }

    /**
     * Set the first debate first argument id
     *
     * @param argumentId a contextualized argument id
     */
    public void setFirstArgumentId(Long argumentId) {
        this.firstArgumentId = argumentId;
    }

    /**
     * Set the title of this argument
     *
     * @param title a argument title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Set the argument shade type (see ArgumentType interface)
     *
     * @param shade an argument shade id
     */
    public void setShade(String shade) {
        this.shade = shade;
    }

    public void setFolders(List<SimpleFolderForm> folders) {
        this.folders = folders;
    }

    public List<PlaceForm> getPlaces() {
        return places;
    }

    public void setPlaces(List<PlaceForm> places) {
        this.places = places;
    }

    public Long getArgumentDictionaryId() {
        return argumentDictionaryId;
    }

    public void setArgumentDictionaryId(Long argumentDictionaryId) {
        this.argumentDictionaryId = argumentDictionaryId;
    }

    public void setLanguage(String lang){
        this.language = lang;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public Debate getTmpDebate() {
        return tmpDebate;
    }

    /**
     * Get the list of candidate debates having the same data as this debate
     *
     * @return a (possibly empty) list of debate holders
     */
    public List<DebateHolder> getCandidates() {
        return candidates;
    }

    /**
     * Set the list of candidate debates having the same data as this debate
     *
     * @param candidates a list of api Debates
     */
    public void setCandidates(List<Debate> candidates) {
        // may not use this.lang since it may not have been set
        this.candidates = candidates.stream().map(t -> new DebateHolder(t, lang)).collect(Collectors.toList());
    }
}
