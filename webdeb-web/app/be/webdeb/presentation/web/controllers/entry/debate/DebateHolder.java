/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.debate;

import be.webdeb.core.api.argument.ArgumentContext;
import be.webdeb.core.api.argument.ArgumentFactory;
import be.webdeb.core.api.argument.ArgumentType;
import be.webdeb.core.api.argument.EArgumentShade;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.contribution.Language;
import be.webdeb.core.api.debate.Debate;
import be.webdeb.core.api.debate.DebateFactory;
import be.webdeb.core.api.folder.Folder;
import be.webdeb.core.exception.FormatException;
import be.webdeb.presentation.web.controllers.entry.ContributionHolder;
import be.webdeb.presentation.web.controllers.entry.EFilterName;
import be.webdeb.presentation.web.controllers.entry.PlaceForm;
import be.webdeb.presentation.web.controllers.entry.argument.ArgumentContextForm;
import be.webdeb.presentation.web.controllers.entry.argument.ArgumentContextHolder;
import be.webdeb.presentation.web.controllers.entry.argument.BaseArgumentHolder;
import be.webdeb.presentation.web.controllers.entry.folder.SimpleFolderForm;
import play.api.Play;
import play.i18n.Lang;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DebateHolder extends BaseArgumentHolder {

    @Inject
    protected DebateFactory debateFactory = Play.current().injector().instanceOf(DebateFactory.class);
    @Inject
    protected ArgumentFactory argumentFactory = Play.current().injector().instanceOf(ArgumentFactory.class);

    // used for lazy loading of debate justification
    protected Debate debate;
    protected ArgumentContext argumentContext;
    protected ArgumentContextHolder argumentContextHolder;
    protected Long firstArgumentId = -1L;
    protected Long argumentId = -1L;

    /**
     * Play / JSON compliant constructor
     */
    public DebateHolder() {
        super();
        type = EContributionType.DEBATE;
    }

    /**
     * Construct a debate from a given api debate in the given language
     *
     * @param debate a debate
     * @param lang 2-char ISO code of context language (among play accepted languages)
     * @param fromForm true if a form call this constructor
     */
    public DebateHolder(Debate debate, String lang, boolean fromForm) {
        super(debate, lang);
        if(fromForm){
            initDebate(debate, lang);
        }
    }

    /**
     * Construct a debate from a given api debate in the given language
     *
     * @param debate a debate
     * @param lang 2-char ISO code of context language (among play accepted languages)
     */
    public DebateHolder(Debate debate, String lang) {
        super(debate, lang);
        initDebate(debate, lang);
        argumentContextHolder = new ArgumentContextHolder(argumentContext, lang);
        initFolders(argumentContext.getFoldersAsList(), lang, false);
    }

    /**
     * Initialize a debate form from an api debate and user lang
     *
     * @param debate a debate
     * @param lang 2-char ISO code of context language (among play accepted languages)
     */
    public void initDebate(Debate debate, String lang){
        this.debate = debate;
        firstArgumentId = debate.getFirstArgumentId();
        argumentContext = debate.getFirstArgument();

        argumentId = argumentContext.getArgumentId();
        title = argumentContext.getArgument().getDictionary().getTitle();
        fullTitle = debate.getFullTitle();
        shade = String.valueOf(debate.getFirstArgumentType().getArgumentShade());
        language = argumentContext.getArgument().getDictionary().getLanguage().getName(lang);
        addFilterable(EFilterName.LANGUAGE, language);
    }

    @Override
    public String toString() {
        return "debate [" + id + "] with first argument [" + argumentId + "] shade " + shade + " and title: " + title;
    }

    @Override
    public MediaSharedData getMediaSharedData() {
        if(mediaSharedData == null){
            mediaSharedData = new MediaSharedData(title, "debate");
        }
        return mediaSharedData;
    }

    @Override
    public String getDefaultAvatar(){
        return "";
    }

    @Override
    public String getContributionDescription() {
        List<String> descriptions = new ArrayList<>();
        descriptions.add(title);
        descriptions.addAll(argumentContextHolder.getFolders().stream().map(SimpleFolderForm::getName).collect(Collectors.toList()));
        descriptions.addAll(argumentContextHolder.getPlaces().stream().map(PlaceForm::getName).collect(Collectors.toList()));

        return String.join(", ", descriptions);
    }

    /*
     * GETTERS
     */

    /**
     * Get the first debate argument id
     *
     * @return a contextualized argument id
     */
    public Long getFirstArgumentId() {
        return firstArgumentId;
    }

    /**
     * Get the first contextualized argument of the debate
     *
     * @return a contextualized argument
     */
    public ArgumentContextHolder getFirstArgumentContext() {
        return argumentContextHolder;
    }

    /**
     * Get the places concerned by the first argument of this debate
     *
     * @return a possibly empty list of places
     */
    public List<PlaceForm> getPlaces() {
        return argumentContextHolder != null ? argumentContextHolder.getPlaces() : new ArrayList<>();
    }

    /**
     * Get the positive declination of the debate title
     *
     * @return a positive declination of the title
     */
    public String getPositiveDebateTitle() {
        return toStatement("label.yes", debate.getFirstArgumentType().getEType().getPositive().id());
    }

    /**
     * Get the negative declination of the debate title
     *
     * @return a negative declination of the title
     */
    public String getNegativeDebateTitle() {
        return toStatement("label.no", debate.getFirstArgumentType().getEType().getNegative().id());
    }

    /**
     * A positive or negative version of the debate title
     *
     * @param label the begin label
     * @param shade the new shade for the title
     * @return the declination
     */
    private String toStatement(String label, int shade){
        ArgumentType argType;
        try {
            argType = argumentFactory.getArgumentType(shade);
            return i18n.get(Lang.forCode(lang), label) + ", " + argType.getShadeName(lang).toLowerCase() + " " + title;
        } catch (FormatException e) {
            return i18n.get(Lang.forCode(lang), label) + ", " + fullTitle;
        }
    }



}
