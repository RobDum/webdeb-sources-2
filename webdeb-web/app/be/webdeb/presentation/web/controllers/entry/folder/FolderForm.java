/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.folder;

import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.folder.Folder;
import be.webdeb.core.api.folder.FolderFactory;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.infra.persistence.model.Group;
import be.webdeb.presentation.web.controllers.entry.PlaceForm;
import javax.inject.Inject;

import play.api.Play;
import play.data.validation.ValidationError;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Wrapper class for forms used to encode a new folder into the database.
 *
 * Note that all supertype getters corresponding to predefined values (ie, types) are sending
 * ids instead of language-dependent descriptions.
 *
 * @author Martin Rouffiange
 */
public class FolderForm extends FolderHolder {

  // list of folders having the same name
  private boolean isNotSame = false;
  private List<FolderHolder> candidates = new ArrayList<>();

  private SimpleFolderForm parentThatNameMatch = null;
  private SimpleFolderForm childThatNameMatch = null;

  /**
   * Play / JSON compliant constructor
   */
  public FolderForm() {
    super();
  }

  /**
   * Constructor. Create a new form object for given folder. Calls super then init beforehand.
   *
   * @param folder a Folder
   * @param contributor a contributor id for whom the folder contributions content must be displayed
   * @param lang 2-char ISO code of context language (among play accepted languages)
   */
  public FolderForm(Folder folder, Long contributor, String lang) {
    super(folder, contributor, lang);
  }

  /**
   * Validator (called from form submit)
   *
   * @return null if no error has been found, otherwise the list of found errors
   */
  public Map<String, List<ValidationError>> validate() {
    Map<String, List<ValidationError>> errors = new HashMap<>();

    if (values.isBlank(folderType)) {
      errors.put("folderType", Collections.singletonList(new ValidationError("folderType", "folder.error.folderType")));
    }

    boolean noName = true;
    for (int i = 0; i < folderNames.size(); i++) {
      FolderNameForm nameForm = folderNames.get(i);
      String fieldNames = "folderNames[" + i + "].";
      String fieldName = "";
      String message = "";

      if (!values.isBlank(nameForm.getLang()) || !values.isBlank(nameForm.getName())) {
        noName = false;
        if (values.isBlank(nameForm.getLang())) {
          if(i == 0){
            folderNames.get(i).setLang(lang);
          }else {
            fieldName = fieldNames + "lang";
            message = "folder.error.name.lang";
            errors.put(fieldName, Collections.singletonList(new ValidationError(fieldName, message)));
          }
        }
        if (values.isBlank(nameForm.getName())) {
          fieldName = fieldNames + "name";
          message = "folder.error.name.name";
          errors.put(fieldName, Collections.singletonList(new ValidationError(fieldName, message)));
        }else if(values.isNotValidFolderName(nameForm.getName())){
          fieldName = fieldNames + "name";
          message = "folder.error.name.name.chars";
          errors.put(fieldName, Collections.singletonList(new ValidationError(fieldName, message)));
        }
      }
      // Search if there is multiple names in the same language
      if (folderNames.stream().filter(n -> !n.isEmpty()).anyMatch(n -> (n != nameForm && n.getLang().equals(nameForm.getLang())))) {
        errors.put(fieldNames + "lang", Collections.singletonList(new ValidationError("folderNames", "actor.error.lang.twice")));
      }
    }
    if (noName){
      errors.put("folderNames[0].name", Collections.singletonList(new ValidationError("folderNames[0].name", "folder.error.name")));
    }

    for (int i = 0; i < folderRewordingNames.size(); i++) {
      FolderNameForm nameForm = folderRewordingNames.get(i);
      String fieldNames = "folderRewordingNames[" + i + "].";
      String fieldName = "";
      String message = "";

      if (!values.isBlank(nameForm.getLang()) || !values.isBlank(nameForm.getName())) {
        if (values.isBlank(nameForm.getLang())) {
          fieldName = fieldNames + "lang";
          message = "folder.error.name.lang";
          errors.put(fieldName, Collections.singletonList(new ValidationError(fieldName, message)));
        }
        if (values.isBlank(nameForm.getName())) {
          fieldName = fieldNames + "name";
          message = "folder.error.name.name";
          errors.put(fieldName, Collections.singletonList(new ValidationError(fieldName, message)));
        }
      }
      // Search if there is multiple names in the same language
      if (folderRewordingNames.stream().filter(n -> !n.isEmpty()).anyMatch(n -> (n != nameForm && n.getLang().equals(nameForm.getLang()) && n.getName().equals(nameForm.getName())))) {
        errors.put(fieldNames+"name", Collections.singletonList(new ValidationError(fieldNames+"name", "folder.error.rewordingname.twice")));
      }
    }

    // Analyse parents hierarchy
    /*if(folderType != EFolderType.ROOT.id()) {
      list = helper.checkFolderLinkFromSimpleForm(parents, id, lang, "parents", true);
      if (list != null) {
        list.forEach(e -> errors.put(e.key(), Collections.singletonList(new ValidationError(e.key(), e.message()))));
      }
    }*/

    // Analyse children hierarchy
    /*if(folderType != EFolderType.LEAF.id()) {
      list = helper.checkFolderLinkFromSimpleForm(children, id, lang, "children", false);
      if (list != null) {
        list.forEach(e -> errors.put(e.key(), Collections.singletonList(new ValidationError(e.key(), e.message()))));
      }
    }*/

    return errors.isEmpty() ? null : errors;
  }

  /**
   * Save a folder into the database. This id is updated if it was not set before.
   *
   * @param contributor the contributor id that ask to save this contribution
   * @return the map of Contribution type and a list of folders (as Contribution) that have been created during
   * this insertion (for all unknown folders), a empty list if none had been created
   *
   * @throws FormatException if this contribution has invalid field values (should be pre-checked before-hand)
   * @throws PermissionException if given contributor may not perform this action or if such action would cause
   * integrity problems
   * @throws PersistenceException if any error occurred at the persistence layer (concrete error is wrapped)
   */
  public Map<Integer, List<Contribution>> save(Long contributor) throws FormatException, PermissionException, PersistenceException {
    logger.debug("try to save " + toString() + " with version " + version + " in group " + inGroup);

    Folder folder = folderFactory.getFolder();
    folder.setId(id != null ? id : -1L);
    folder.setVersion(version);
    folder.addInGroup(Group.getPublicGroup().getIdGroup());
    folderNames.stream().filter(FolderNameForm::isValid).forEach(n -> folder.addName(n.getLang(), n.getName()));
    folderRewordingNames.stream().filter(FolderNameForm::isValid).forEach(n -> folder.addRewordingName(n.getLang(), n.getName()));
    try {
      folder.setFolderType(folderFactory.getFolderType(folderType));
    } catch (NumberFormatException | FormatException e) {
      logger.error("unknown folder type id" + folderType, e);
    }

    folder.initParents();
    for (SimpleFolderForm f : normalParents) {
      if(f != null) {
        if(f.getFolderId() != null && f.getFolderId() != -1L) {
          try {
            folder.addParent(folderFactory.retrieve(f.getFolderId()));
          } catch (FormatException e) {
            logger.debug("error while adding parent to folder ", e);
            throw new PersistenceException(PersistenceException.Key.SAVE_FOLDER, e);
          }
        } else {
          folder.addParent(f.toFolder(lang));
        }
      }
    }

    folder.initChildren();
    for (SimpleFolderForm f : normalChildren) {
      if(f != null){
        if(f.getFolderId() != null && f.getFolderId() != -1L) {
          try {
            folder.addChild(folderFactory.retrieve(f.getFolderId()));
          } catch (FormatException e) {
            logger.debug("error while adding child to folder ", e);
            throw new PersistenceException(PersistenceException.Key.SAVE_FOLDER, e);
          }
        }else{
          folder.addChild(f.toFolder(lang));
        }
      }
    }

    Map<Integer, List<Contribution>> folders = folder.save(contributor, inGroup);
    // do not forget to update the id, since the controller needs it for redirection
    id = folder.getId();
    return folders;
  }

  /*
   * SETTERS
   */

  /**
   * Set the folder name
   *
   * @param name the folder name
   */
  public void setFolderName(String name) {
    this.name = name;
  }

  /**
   * Set the list of parent folder links
   *
   * @param parents a list of folder links
   */
  public void setParents(List<SimpleFolderForm> parents) {
    this.parents = parents;
  }

  /**
   * Set the list of child folder links
   *
   * @param children a list of folder links
   */
  public void setChildren(List<SimpleFolderForm> children) {
    this.children = children;
  }

  /**
   * Set the list of normal (not composed) parent folder links
   *
   * @param normalParents a list of folder links
   */
  public void setNormalParents(List<SimpleFolderForm> normalParents) {
    this.normalParents = normalParents;
  }

  /**
   * Set the list of normal (not composed) child folder links
   *
   * @param normalChildren a list of folder links
   */
  public void setNormalChildren(List<SimpleFolderForm> normalChildren) {
    this.normalChildren = normalChildren;
  }

  /**
   * Set the folder type from which this folder originates from
   *
   * @param type the folder type
   */
  public void setFolderType(int type) {
    this.folderType = type;
  }

  /**
   * Set the folder names
   *
   * @param names the possibly empty list of folder names
   */
  public void setFolderNames(List<FolderNameForm> names) {
    folderNames = names;
  }

  /**
   * Set the rewording folder names
   *
   * @param names the possibly empty list of rewording folder names
   */
  public void setFolderRewordingNames(List<FolderNameForm> names) {
   folderRewordingNames = names;
  }

  /**
   * Get the list of candidate folders having the same name as this folder
   *
   * @return a (possibly empty) list of folder holders
   */
  public List<FolderHolder> getCandidates() {
    return candidates;
  }

  /**
   * Set he list of candidate folders having the same name as this folder
   *
   * @param candidates a list of api Folders
   */
  public void setCandidates(List<Folder> candidates) {
    this.candidates = candidates.stream().map(t -> new FolderHolder(t, contributor, lang)).collect(Collectors.toList());
  }

  /**
   * Check whether this folder has been explicitly flagged as a new one (used when other folders exists with same name)
   *
   * @return true if this folder is a new one despite it has the same folder as another one in db
   */
  public boolean getIsNotSame() {
    return isNotSame;
  }

  /**
   * Set whether this folder has been explicitly flagged as a new one (used when other folders exists withsame name)
   *
   * @param notSame true if this folder is a new one despite it has the same name as another one in db
   */
  public void setIsNotSame(boolean notSame) {
    isNotSame = notSame;
  }

  /**
   * Get the parent that name match
   *
   * @return a (possibly null) parent as simple folder form
   */
  public SimpleFolderForm getParentThatNameMatch() {
    return parentThatNameMatch;
  }

  /**
   * Set the parent that name match
   *
   * @param parentThatNameMatch a parent as simple folder form
   */
  public void setParentThatNameMatch(SimpleFolderForm parentThatNameMatch) {
    this.parentThatNameMatch = parentThatNameMatch;
  }

  /**
   * Get the child that name match (if any)
   *
   * @return a (possibly null) child as simple folder form
   */
  public SimpleFolderForm getChildThatNameMatch() {
    return childThatNameMatch;
  }

  /**
   * Set the child that name match
   *
   * @param childThatNameMatch a child as simple folder form
   */
  public void setChildThatNameMatch(SimpleFolderForm childThatNameMatch) {
    this.childThatNameMatch = childThatNameMatch;
  }
}
