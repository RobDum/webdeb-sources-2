/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry;

import be.webdeb.core.api.actor.*;
import be.webdeb.core.api.argument.*;
import be.webdeb.core.api.contribution.*;
import be.webdeb.core.api.contributor.ContributorFactory;
import be.webdeb.core.api.contributor.EContributorRole;
import be.webdeb.core.api.contributor.GroupColor;
import be.webdeb.core.api.debate.Debate;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.core.api.folder.Folder;
import be.webdeb.core.api.folder.FolderFactory;
import be.webdeb.core.api.contribution.Language;
import be.webdeb.core.api.text.Text;
import be.webdeb.core.api.text.TextFactory;
import be.webdeb.core.api.text.WordGender;
import be.webdeb.core.exception.FormatException;
import be.webdeb.infra.ws.nlp.WikiAffiliation;
import be.webdeb.presentation.web.controllers.entry.actor.*;
import be.webdeb.presentation.web.controllers.entry.argument.*;
import be.webdeb.presentation.web.controllers.entry.debate.DebateForm;
import be.webdeb.presentation.web.controllers.entry.debate.DebateHolder;
import be.webdeb.presentation.web.controllers.entry.excerpt.ExcerptForm;
import be.webdeb.presentation.web.controllers.entry.excerpt.ExcerptHolder;
import be.webdeb.presentation.web.controllers.entry.folder.*;
import be.webdeb.presentation.web.controllers.entry.text.TextHolder;
import be.webdeb.presentation.web.controllers.permission.WebdebUser;
import be.webdeb.util.ValuesHelper;
import play.Configuration;
import play.data.validation.ValidationError;
import play.i18n.Lang;
import play.i18n.MessagesApi;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.nio.charset.Charset;
import java.text.Collator;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


/**
 * This helper class (injected) offers a set of functions to manipulate API objects and convert them to
 * Presentation-layer ones.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
@Singleton
public class ContributionHelper {

  // custom logger
  private static final org.slf4j.Logger logger = play.Logger.underlying();

  private ValuesHelper values;
  private ArgumentFactory argumentFactory;
  private ActorFactory actorFactory;
  private TextFactory textFactory;
  private FolderFactory folderFactory;
  private ContributorFactory contributorFactory;
  private MessagesApi i18n;

  private static int maxWarningSizeStandardForm;
  private static int maxDangerSizeStandardForm;

  private static final String FILTER_TREE = "three";

  /**
   * Injected constructor
   *
   * @param actorFactory the actor factory
   * @param textFactory the text factory
   * @param argumentFactory the argument factory
   * @param folderFactory the folder factory
   * @param contributorFactory the contributor factory
   * @param configuration play configuration module (to get predefined values)
   * @param i18n the play messages API
   * @param values the values helper singleton
     */
  @Inject
  public ContributionHelper(ActorFactory actorFactory, TextFactory textFactory, ArgumentFactory argumentFactory,
        FolderFactory folderFactory,ContributorFactory contributorFactory, Configuration configuration,
                            MessagesApi i18n, ValuesHelper values) {
    this.values = values;
    this.actorFactory = actorFactory;
    this.textFactory = textFactory;
    this.argumentFactory = argumentFactory;
    this.folderFactory = folderFactory;
    this.contributorFactory = contributorFactory;
    this.i18n = i18n;
  }

  /**
   * Helper method to convert API ContributionToExplore into Presentation objects (into their concrete types
   * depending on their API type)
   *
   * @param contributions a list of API contributionsToExplore
   * @param user a WebDeb user
   * @param lang 2-char ISO code of context language (among play accepted languages)
   * @return the list of converted contributions to explore into ContributionHolder concrete classes
   */
  public final List<ContributionHolder> fromExploreToHolders(List<ContributionToExplore> contributions, WebdebUser user, String lang) {
    return toHolders(contributions.stream().map(ContributionToExplore::getContribution).collect(Collectors.toList()), user, lang);
  }

  /**
   * Helper method to convert API Contribution objects into Presentation objects (into their concrete types
   * depending on their API type)
   *
   * @param contributions a list of API contributions
   * @param user a WebDeb user
   * @param lang 2-char ISO code of context language (among play accepted languages)
   * @return the list of converted contributions into ContributionHolder concrete classes
   */
  public final List<ContributionHolder> toHolders(Collection<? extends Contribution> contributions, WebdebUser user, String lang) {
    return contributions.parallelStream().map(c -> toHolder(c, user, lang)).collect(Collectors.toList());
  }

  /**
   * Helper method to convert API Contribution object into a Presentation object
   *
   * @param contribution an API contribution
   * @param user a WebDeb user
   * @param lang 2-char ISO code of context language (among play accepted languages)
   * @return the converted contribution into a ContributionHolder concrete class
   */
  public final ContributionHolder toHolder(Contribution contribution, WebdebUser user, String lang) {
    switch (contribution.getType()) {
      case ACTOR:
        return new ActorHolder((Actor) contribution, lang);
      case TEXT:
        return new TextHolder((Text) contribution, user.getId(), lang);
      case DEBATE:
        return new DebateHolder((Debate) contribution, lang);
      case ARGUMENT:
        return new ArgumentHolder((Argument) contribution, lang);
      case ARGUMENT_CONTEXTUALIZED:
        return new ArgumentContextHolder((ArgumentContext) contribution, lang);
      case EXCERPT:
        return new ExcerptHolder((Excerpt) contribution, lang);
      case ARG_JUSTIFICATION:
        return new JustificationLinkForm((ArgumentJustification) contribution, lang, true);
      case ARG_SIMILARITY:
        return new SimilarityLinkForm((ArgumentSimilarity) contribution, lang, true);
      case ARG_ILLUSTRATION:
        return new IllustrationLinkForm((ArgumentIllustration) contribution, lang, true);
      case FOLDER:
        return new FolderHolder((Folder) contribution, user, lang);
      default:
        logger.error("unknown or unhandled type for contribution " + contribution.getType().name());
        return null;
    }
  }

  /**
   * Convert a String fullname to an ActorName
   *
   * @param actor the actor concerned
   * @param fullname the fullname to transform
   * @param lang the language used to fill in the function (profession) field
   * @return an API ActorName object containing the given fullname
   */
  public final ActorName toActorName(Actor actor, String fullname, String lang) {
    ActorName name = null;

    if(fullname != null) {
      name = actorFactory.getActorName(lang);
      String [] names = fullname.split(" ");

      switch (actor.getActorType()) {
        case PERSON:
          if(names.length <= 1){
            name.setFirst(fullname);
            name.setLast(fullname);
          }else{
            name.setFirst(names[0]);
            name.setLast(fullname.substring(name.getFirst().length()));
          }
          break;
        default:
          name.setLast(fullname);
      }
    }

    return name;
  }

  /**
   * Convert an ActorSimpleForm to an ActorRole
   *
   * @param actorname the actor name to, all ids are considered correct (must be validated by checkActors)
   * @param contribution the contribution to be bound to the actor role
   * @param lang the language used to fill in the function (profession) field
   * @return an API ActorRole object containing all fields in actorname
   */
  public final ActorRole toActorRole(ActorSimpleForm actorname, Contribution contribution, String lang) {
    logger.debug("convert " + actorname.toString() + " to actor role");
    Actor actor = actorFactory.retrieve(actorname.getId());

    // create a new one
    if (actor == null) {
      actor = actorFactory.getActor();
      actor.setId(-1L);
      ActorName name = actorFactory.getActorName(lang);
      name.setLast(actorname.getFullname().trim());
      actor.setNames(Collections.singletonList(name));
    }

    // create the role
    ActorRole role = actorFactory.getActorRole(actor, contribution);
    role.setIsAuthor(actorname.getAuthor());
    role.setIsReporter(actorname.getReporter());
    role.setIsJustCited(actorname.getCited());
    // handle actor affiliation
    if (actorname.hasAffiliation()) {
      logger.debug("bind affiliation " + actorname.getFullFunction() + " with id " + actorname.getAha());
      // look for selected affiliation, if any (id has been checked in checkActors)
      Affiliation affiliation;
      if (!values.isBlank(actorname.getAha())) {
        affiliation = actorFactory.getAffiliation(actorname.getAha());
      } else {
        // create a new affiliation
        affiliation = actorFactory.getAffiliation();
        affiliation.setAffiliated(actor);
        // affiliation actor ?
        if (!values.isBlank(actorname.getAffname())) {
          Actor affActor;
          if (!values.isBlank(actorname.getAffid())) {
            affActor = actorFactory.retrieve(actorname.getAffid());
          } else {
            affActor = actorFactory.getActor();
            ActorName name = actorFactory.getActorName(lang);
            name.setLast(actorname.getAffname().trim());
            affActor.setNames(Collections.singletonList(name));
          }
          affiliation.setActor(affActor);
        }

        // function ?
        if (!values.isBlank(actorname.getFunction())) {
          Profession profession;
          if (!values.isBlank(actorname.getFunctionid())) {
            // id must be valid (checked in checkActors)
            try {
              profession = actorFactory.getProfession(actorname.getFunctionid());
            } catch (FormatException e) {
              logger.error("unable to retrieve profession " + actorname.getFunctionid() + " " + actorname.getFunction(), e);
              profession = actorFactory.createProfession(-1,
                  values.fillProfessionName(lang, actorname.getFunctionGender(), actorname.getFunction()));
            }
          } else {
            profession = actorFactory.createProfession(-1,
                values.fillProfessionName(lang, actorname.getFunctionGender(), actorname.getFunction()));
          }
          affiliation.setFunction(profession);
        }
      }
      role.setAffiliation(affiliation);
    }
    return role;
  }

  /**
   * Convert an SimpleFolderForm to a Folder
   *
   * @param folderForm the folderForm
   * @param lang the language used to fill the folder name
   * @return an API Folder object named
   */
  public final Folder toFolder(SimpleFolderForm folderForm, String lang) {
    logger.debug("convert " + folderForm.toString() + " to folder");
    Folder folder = folderFactory.retrieve(folderForm.getFolderId());

    // create a new one
    if (folder == null) {
      folder = folderFactory.getFolder();
      folder.setId(-1L);
      folder.addName(lang, folderForm.getName());
    }

    return folder;
  }

  /**
   * Check a given url if it is valid, return a validation error list if it is malformed
   *
   * @param url a url
   * @param field the field name in which the url is present (for proper error binding)
   * @return a (possibly empty) list of validation error if the url is malformed
   */
  public List<ValidationError> checkUrl(String url, String field) {
    List<ValidationError> errors = new ArrayList<>();
    if (!values.isURL(url)) {
      errors.add(new ValidationError(field, "entry.error.url.format"));
    }
    return errors;
  }


  /**
   * Check a list of actors, return a validation error list if their content is not valid.
   * Also, check consistency between field and hidden values regarding actor, function and
   * affiliation ids.
   *
   * @param tocheck a list of ActorSimpleForm objects
   * @param field the field name in which the actors are present (for proper error binding)
   * @return a list of validation error if any, null otherwise
   */
  public List<ValidationError> checkActors(List<ActorSimpleForm> tocheck, String field) {
    List<ValidationError> errors = new ArrayList<>();
    if (tocheck == null) {
      errors.add(new ValidationError(field + "[0].fullname", "entry.error.actor.noname"));
      return errors;
    }

    tocheck.forEach(a -> {
      // avoid filter collect (otherwise, a is another reference)
      if (!a.isEmpty()) {
        String fieldname = field + "[" + tocheck.indexOf(a) + "]";
        // do we have a name
        if (values.isBlank(a.getFullname())) {
          errors.add(new ValidationError(fieldname + ".fullname", "entry.error.actor.noname"));
        }

        // now check and update all fields (mainly checking actor/affiliation/function ids)
        a.checkFieldsIntegrity();

        // check if affiliated to self, same id (except -1L) or same name
        if (!values.isBlank(a.getAffname()) && a.getFullname().equals(a.getAffname())) {
          errors.add(new ValidationError(fieldname + ".fullname", "affiliation.error.notself"));
        }

        // check if affiliated to an person
        if (!values.isBlank(a.getAffid())) {
          Actor actor = actorFactory.retrieve(a.getAffid());
          if(actor != null && actor.getActorType().id() == EActorType.PERSON.id())
            errors.add(new ValidationError(fieldname + ".affname", "affiliation.error.notperson"));
        }

      }
    });
    return !errors.isEmpty() ? errors : null;
  }

  /**
   * Will remove (and rebind) duplicates in bound actors' affiliation actors, ie, it is possible that the same
   * unknown affiliation actor is specified more than once in actor's affiliations. Must bind to same instance,
   * otherwise, multiple actors will be created for the same unknown actor
   *
   * @param contribution a textual contribution to be checked for duplicates in actors' affiliations
   * @param lang 2-char ISO code for the UI language
   */
  public void removeDuplicateUnknownActors(TextualContribution contribution, String lang) {
    // post process actors since new affiliations may be put more than once
    for (int i = 0; i < contribution.getActors().size(); i++) {
      // only actors with one and only one affiliation with actor's id = -1 are of interest
      Affiliation current = contribution.getActors().get(i).getAffiliation();
      if (current != null && current.getActor() != null && values.isBlank(current.getActor().getId())) {

        // check all previous elements if such an affiliation does not yet exist, if so, replace current instance with other one
        // replace current affiliation actor with already created one
        Optional<ActorRole> role = contribution.getActors().subList(0, i).stream().filter(r ->
            r.getAffiliation() != null && r.getAffiliation().getActor() != null
                && values.isBlank(r.getAffiliation().getActor().getId())
                && r.getAffiliation().getActor().getFullname(lang).equalsIgnoreCase(current.getActor().getFullname(lang)))
            .findFirst();
        if (role.isPresent()) {
          // replace current affiliation actor with already created one
          logger.debug("bind unknown actor " + role.get().getAffiliation().getActor().getFullname(lang) + " to unique instance");
          current.setActor(role.get().getAffiliation().getActor());
        }
      }
    }
  }

  /**
   * Compute default avatar picture name
   *
   * @param type an actortype id (-1, 0 or 1), use 0 for contributor
   * @param gender a gender type id (F, M or null)
   * @return the default picture name for an actor or contributor's avatar
   */
  public String computeAvatar(int type, String gender) {
    switch (type) {
      case 0:
        if (gender != null) {
          switch (gender) {
            case "M":
              return "man.png";
            case "F":
              return "woman.png";
            default:
              return "person.png";
          }
        }
        return "person.png";
      case 1:
        return "org.png";
      default:
        return "unknown.png";
    }
  }


  /**
   * Compute filters from given list of contributions. Returned filters are a map of the form:
   * <ul>
   *   <li>key: the unique key of the filter name field, eg, publication.date, language, folder, etc.</li>
   *   <li>map (value, object-type)</li>
   *   <ul>
   *     <li>value is the actual value associated to this filtering element. For ranges, two values must be passed,
   *     separated by a comma (",")</li>
   *     <li>the filter object type is either "checkbox" or "range", depicting the type of filter input</li>
   *   </ul>
   * </ul>
   *
   * Note that all ContributionHolder (and forms subtypes not being constructed by explicit super() call)
   * have their filterable property set up in their constructor.
   *
   * @param contributions a list of contributions to build a filter pane for
   * @param lang the UI language code (2-char iso 639-1)
   * @return a map of filters of the form (key, List(objecttype, value)) with the key being the filter key,
   * the objecttype as a key for the html object containing the values for the filter)
   * @see EFilterName for all available filters
   * @see EFilterType for all type of filters (boxes or ranges)
   * @see FilterTree that manage the filter
   */
  public final Map<EFilterName, List<FilterTree>> buildFilters(Collection<? extends ContributionHolder> contributions, String lang) {
    Map<EFilterName, Map<String, FilterTree>> filters = new EnumMap<>(EFilterName.class);
    Map<EFilterName, List<FilterTree>> filtersReturn = new LinkedHashMap<>();

    FilterTree placeTree = new FilterTree(EFilterType.TREE, true);
    FilterTree functionTree = new FilterTree(EFilterType.TREE);

    // initialize filter list with all available filter values
    for (EFilterName e : EFilterName.all()) {
      filters.put(e, new HashMap<>());
    }

    // dates used to create the ranges for birthdate/deathhdate and publication date
    final Map<EDateType, String> birthdateMap = new LinkedHashMap<>();
    final Map<EDateType, String> pubdateMap = new LinkedHashMap<>();
    final SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy", Locale.FRENCH);

    birthdateMap.put(EDateType.LOW_DATE, yearFormat.format(new Date()));
    birthdateMap.put(EDateType.HIGHER_DATE, yearFormat.format(new Date()));
    pubdateMap.put(EDateType.LOW_DATE, yearFormat.format(new Date()));
    pubdateMap.put(EDateType.HIGHER_DATE, yearFormat.format(new Date()));

    contributions.forEach(contribution -> {
      putFilterElementInMap(filters.get(EFilterName.CTYPE), i18n.get(Lang.forCode(lang), "general.filter.ctype."
          + contribution.getType().id()), EFilterType.BOX);
      // object type keys
      switch (contribution.getType()) {
        case ACTOR:

          ActorHolder actor = (ActorHolder) contribution;
          putFilterElementInMap(filters.get(EFilterName.ATYPE), i18n.get(Lang.forCode(lang),
                  "actor.label.actortype." + actor.getActortype()),EFilterType.BOX );
          switch (EActorType.value(actor.getActortype())) {
            case PERSON:
              buildPersonFilters(actor, filters, birthdateMap, functionTree);
              break;
            case ORGANIZATION:
              buildOrganizationFilters(actor, filters, birthdateMap, functionTree, placeTree);
              break;
            default:
              // ignore
          }
          break;

        case DEBATE:
          DebateHolder debate = (DebateHolder) contribution;
          putFilterElementInMap(filters.get(EFilterName.LANGUAGE), debate.getLanguage(), EFilterType.BOX);
          if(debate.getFirstArgumentContext() != null)
            buildArgumentContextFilters(debate.getFirstArgumentContext(), filters, birthdateMap, placeTree);
          break;

        case ARGUMENT_CONTEXTUALIZED:
          buildArgumentContextFilters((ArgumentContextHolder) contribution, filters, birthdateMap, placeTree);
          break;

        case EXCERPT:
          buildExcerptFilters((ExcerptHolder) contribution, filters, birthdateMap, pubdateMap, lang, placeTree);
          break;

        case TEXT:
          buildTextFilters((TextHolder) contribution, filters, birthdateMap, pubdateMap);
          break;
        case FOLDER:
          FolderHolder folder = (FolderHolder) contribution;
          putFilterElementInMap(filters.get(EFilterName.FOLDER), folder.getFolderName(), EFilterType.BOX);
          break;
        default:
          logger.warn("unsupported contribution type to build filters with " + contribution.getType().name());
      }
    });

    // now add birth date and publication date filters if needed
    if (!birthdateMap.get(EDateType.LOW_DATE).equals(birthdateMap.get(EDateType.HIGHER_DATE))) {
      String date = birthdateMap.get(EDateType.LOW_DATE) + "," + birthdateMap.get(EDateType.HIGHER_DATE);
      filters.get(EFilterName.BIRTHDATE).put(date, new FilterTree(EFilterType.RANGE, date));
    }
    if (!pubdateMap.get(EDateType.LOW_DATE).equals(pubdateMap.get(EDateType.HIGHER_DATE))) {
      String date = pubdateMap.get(EDateType.LOW_DATE) + "," + pubdateMap.get(EDateType.HIGHER_DATE);
      filters.get(EFilterName.PUBLIDATE).put(date, new FilterTree(EFilterType.RANGE, date));
    }

    // add filter places trees
    if(!placeTree.isEmpty()) {
      filters.get(EFilterName.PLACE).put(FILTER_TREE, placeTree);
    }

    // add filter functions trees
    if(!functionTree.isEmpty()) {
      filters.get(EFilterName.FUNCTION).put(FILTER_TREE, functionTree);
    }

    for(Map.Entry<EFilterName, Map<String, FilterTree>> entry : filters.entrySet()){
      List<FilterTree> subFilters = new ArrayList<>(entry.getValue().values());
      Collections.sort(subFilters);
      subFilters.forEach(FilterTree::sortTree);
      filtersReturn.put(entry.getKey(), subFilters);
    }
    filtersReturn.put(EFilterName.FILTERID, Collections.singletonList(new FilterTree(EFilterType.RANGE, generatedRandomItemId())));

    return filtersReturn;
  }

  /**
   * Update filters map with given person
   */
  private void buildPersonFilters(ActorHolder actor, Map<EFilterName, Map<String, FilterTree>> filters, Map<EDateType, String> birthdateMap, FilterTree functionTree) {
    if (!values.isBlank(actor.getResidence())) {
      putFilterElementInMap(filters.get(EFilterName.COUNTRY), actor.getResidence(), EFilterType.BOX);
    }

    buildActorAffiliationsFilters(actor.getAffiliations(), filters, functionTree);

    if (!values.isBlank(actor.getGender())) {
      putFilterElementInMap(filters.get(EFilterName.GENDER), actor.getGender(), EFilterType.BOX);
    }

    if (!values.isBlank(actor.getBirthdate())) {
      updateDateMap(birthdateMap, actor.getBirthdate().split("/"));
    }
  }

  /**
   * Update filters map with given organization
   */
  private void buildOrganizationFilters(ActorHolder actor, Map<EFilterName, Map<String, FilterTree>> filters, Map<EDateType, String> birthdateMap, FilterTree functionTree, FilterTree placeTree) {
    if (!values.isBlank(actor.getHeadOffice())) {
      putFilterElementInMap(filters.get(EFilterName.COUNTRY), actor.getHeadOffice(), EFilterType.BOX);
    }

    buildActorAffiliationsFilters(actor.getOrgaffiliations(), filters, functionTree);

    actor.getBusinessSectors().forEach(s -> filters.get(EFilterName.SECTOR).put(s, new FilterTree(EFilterType.BOX, s)));
    actor.getPlaces().forEach(p -> placeTree.addListToTree(p.getPlaceComposition(), true));

    if (!values.isBlank(actor.getLegalStatus())) {
      putFilterElementInMap(filters.get(EFilterName.LEGAL), actor.getLegalStatus(), EFilterType.BOX);
    }
    if (!values.isBlank(actor.getCreationDate())) {
      updateDateMap(birthdateMap, actor.getCreationDate().split("/"));
    }
  }

  /**
   * Update filters map with given actor affiliations
   */
  private void buildActorAffiliationsFilters(List<? extends AffiliationHolder> affiliations, Map<EFilterName, Map<String, FilterTree>> filters, FilterTree functionTree){

    Set<String> affTypeMap = new HashSet<>();
    Set<String> functionMap = new HashSet<>();
    Set<String> affMap = new HashSet<>();

    for(AffiliationHolder a : affiliations){
      if (!values.isBlank(a.getAfftype())) {
        putFilterElementInMap(filters.get(EFilterName.AFFTYPE), a.getAfftype(), EFilterType.BOX, !affTypeMap.contains(a.getAfftype()));
        affTypeMap.add(a.getAfftype());
      }
      if (!values.isBlank(a.getFunction())) {
        functionTree.addListToTree(a.getFunctionComposition(), !functionMap.contains(a.getFunction()));
        functionMap.add(a.getFunction());
      }
      if (!values.isBlank(a.getAffname())) {
        putFilterElementInMap(filters.get(EFilterName.AFFILIATION), a.getAffname(), EFilterType.BOX, !affMap.contains(a.getAffname()));
        affMap.add(a.getAffname());
      }
    }
  }

  /**
   * Update filters map with given argument context
   */
  private void buildArgumentContextFilters(ArgumentContextHolder argumentContext, Map<EFilterName, Map<String, FilterTree>> filters, Map<EDateType, String> birthdateMap, FilterTree placeTree) {
    // add authors' filters and update birthdate filter based on authors dates
    //updateDateMap(birthdateMap, filters, argumentContext);
    putFilterElementInMap(filters.get(EFilterName.LANGUAGE), argumentContext.getArgument().getLanguage(), EFilterType.BOX);
    putFilterElementInMap(filters.get(EFilterName.ARGTYPE), argumentContext.getArgument().getShadeterm(), EFilterType.BOX);

    //EFilterName filterName = argumentContext.getContextType() == EContributionType.DEBATE ? EFilterName.DEBATE : EFilterName.TEXT;
    //putFilterElementInMap(filters.get(filterName), argumentContext.getContextTitle(), EFilterType.BOX);

    argumentContext.getPlaces().forEach(p -> placeTree.addListToTree(p.getPlaceComposition(), true));
    argumentContext.getFolderTags().forEach(e ->
            putFilterElementInMap(filters.get(EFilterName.FOLDER), e.getName(), EFilterType.BOX));
  }

  /**
   * Update filters map with given excerpt
   */
  private void buildExcerptFilters(ExcerptHolder excerpt, Map<EFilterName, Map<String, FilterTree>> filters, Map<EDateType, String> birthdateMap, Map<EDateType, String> pubdateMap, String lang, FilterTree placeTree ) {
    // add authors' filters and update birthdate filter based on authors dates
    updateDateMap(birthdateMap, filters, excerpt);

    if (excerpt instanceof ExcerptForm) {
      putFilterElementInMap(filters.get(EFilterName.LANGUAGE), getLanguageName(excerpt.getExcLang(), lang), EFilterType.BOX);
      if (excerpt.getTextType() != null && !"".equals(excerpt.getTextType())) {
        putFilterElementInMap(filters.get(EFilterName.TTYPE), getTextTypes(lang).get(excerpt.getTextType()), EFilterType.BOX);
      }
    } else {
      putFilterElementInMap(filters.get(EFilterName.LANGUAGE), excerpt.getExcLang(), EFilterType.BOX);
      putFilterElementInMap(filters.get(EFilterName.TTYPE), excerpt.getTextType(), EFilterType.BOX);
    }
    if (!values.isBlank(excerpt.getSource())) {
      putFilterElementInMap(filters.get(EFilterName.SOURCE), excerpt.getSource(), EFilterType.BOX);
    }

    if (!values.isBlank(excerpt.getPublicationDate())) {
      updateDateMap(pubdateMap, excerpt.getPublicationDate().split("/"));
    }

    excerpt.getPlaces().forEach(p -> placeTree.addListToTree(p.getPlaceComposition(), true));
    excerpt.getFolderTags().forEach(e ->
            putFilterElementInMap(filters.get(EFilterName.FOLDER), e.getName(), EFilterType.BOX));
  }

  /**
   * Update filters map with given text
   */
  private void buildTextFilters(TextHolder text, Map<EFilterName, Map<String, FilterTree>> filters, Map<EDateType, String> birthdateMap, Map<EDateType, String> pubdateMap) {
    // add authors' filters and update birthdate filter based on authors dates
    updateDateMap(birthdateMap, filters, text);

    putFilterElementInMap(filters.get(EFilterName.TTYPE), text.getTextType(), EFilterType.BOX);
    if (!values.isBlank(text.getSourceTitle())) {
      putFilterElementInMap(filters.get(EFilterName.SOURCE), text.getSourceTitle(), EFilterType.BOX);
    }
    putFilterElementInMap(filters.get(EFilterName.LANGUAGE), text.getLanguage(), EFilterType.BOX);

    if (!values.isBlank(text.getPublicationDate())) {
      updateDateMap(pubdateMap, text.getPublicationDate().split("/"));
    }

    text.getFolderTags().forEach(e ->
            putFilterElementInMap(filters.get(EFilterName.FOLDER), e.getName(), EFilterType.BOX));
  }

  /**
   * Update the given map date with given data
   *
   * @param dateMap the map to update
   * @param toAdd the date to put in the map
   */
  private void updateDateMap(Map<EDateType, String> dateMap, String[] toAdd){
    String formerLower = dateMap.get(EDateType.LOW_DATE);
    String lower = getEarlierOrLater( toAdd[ toAdd.length - 1], formerLower, true);
    if(!lower.equals(formerLower))
      dateMap.put(EDateType.LOW_DATE, lower);

    String formerHigher = dateMap.get(EDateType.HIGHER_DATE);
    String higher = getEarlierOrLater( toAdd[ toAdd.length - 1], formerHigher, false);
    if(!higher.equals(formerHigher))
      dateMap.put(EDateType.HIGHER_DATE, higher);
  }

  /**
   * Update the given map date with given data
   *
   * @param dateMap the map to update
   * @param filters needed to update the map date
   * @param contribution the contribution concerned
   */
  private void updateDateMap(Map<EDateType, String> dateMap, Map<EFilterName, Map<String, FilterTree>> filters, ContributionHolder contribution){
    String formerLower = dateMap.get(EDateType.LOW_DATE);
    String formerHigher = dateMap.get(EDateType.HIGHER_DATE);
    String[] authorDates = setAuthorsFilters(filters, contribution, formerLower, formerHigher);

    if(!authorDates[0].equals(formerLower))
      dateMap.put(EDateType.LOW_DATE, authorDates[0]);
    if(!authorDates[1].equals(formerHigher))
      dateMap.put(EDateType.HIGHER_DATE, authorDates[1]);
  }

  /**
   * Add an item and count to the filter map and count the number of occurences
   *
   * @param elementMap the map where add new item or add a new occurences
   * @param key the element key in the map
   * @param type the type of element
   */
  private void putFilterElementInMap(Map<String, FilterTree> elementMap, String key, EFilterType type){
    putFilterElementInMap(elementMap, key, type, true);
  }

  /**
   * Add an item and count to the filter map and count the number of occurences
   *
   * @param elementMap the map where add new item or add a new occurences
   * @param key the element key in the map
   * @param type the type of element
   * @param addToOccurs true if the element must be counted
   */
  private void putFilterElementInMap(Map<String, FilterTree> elementMap, String key, EFilterType type, boolean addToOccurs){
    if(key != null && !key.equals("")) {
      FilterTree element = elementMap.get(key);
      if (element != null && addToOccurs) {
        element.addToOccurs();
      } else {
        elementMap.put(key, new FilterTree(type, key));
      }
    }
  }

  /**
   * Get a random string as an html unique id
   *
   * @return a string
   */
  private String generatedRandomItemId(){
    String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    StringBuilder salt = new StringBuilder();
    Random rnd = new Random();
    while (salt.length() < 10) { // length of the random string.
      int index = (int) (rnd.nextFloat() * SALTCHARS.length());
      salt.append(SALTCHARS.charAt(index));
    }
    return salt.toString();
  }

  /**
   * Helper method to check if given actors names (e.g contribution authors, reporters or source authors) must be
   * matched to existing actors in database. First identified possible match (even for an actor's name or its
   * affiliation name) will have its nameMatches list updated and returned. The remaining given actors will not be checked
   * further. Only actors with
   *
   * @param actors a list of actors (being part of the actor's list of a textual contribution)
   * @param lang 2-char iso-639-1 language code
   * @return a NameMatch object that contains the index of the match and
   *         an empty list if no matches exists, the list of namesake actors if any of given actors has namesakes
   */
  public NameMatch<ActorHolder> searchForNameMatches(List<ActorSimpleForm> actors, String lang) {
    for (int i = 0; i < actors.size(); i++) {
      ActorSimpleForm actor = actors.get(i);
      if(!actor.isEmpty()) {
        logger.debug("search for matches for " + actor.toString());
        List<ActorHolder> matches = searchForNameMatches(actor, lang);
        if (!matches.isEmpty()) {
          logger.debug("send modal page to choose what to do with matches for actor " + actor.getFullname()
              + " with " + actor.getNameMatches().toString());
          return new ConcreteNameMatch<>(i, matches, true);
        }
      }
    }
    return new ConcreteNameMatch<>();
  }

  /**
   * Helper method to check if matches can be found for given affiliations' names (full form objects as displayed
   * in ActorForm). This helper method is not used to disambiguate in author forms (aka ActorSimpleForms).
   *
   * Given AffiliationForm nameMatches list will be updated as a side effect.
   *
   * @param affiliations a list of affiliations
   * @param lang 2-char iso-639-1 language code
   * @return a NameMatch object that contains the index of the match and
   *         an empty list if no match exists, the list of namesake actors if any of given affiliation actors has matches
   */
  public NameMatch<ActorHolder> findAffiliationsNameMatches(List<AffiliationForm> affiliations, String lang){
    for (int i = 0; i < affiliations.size(); i++) {
      AffiliationForm aff = affiliations.get(i);
      if(!aff.isEmpty()) {
        List<ActorHolder> matches = searchForNameMatches(aff, lang);
        if (!matches.isEmpty()) {
          logger.debug("send modal page to choose what to do with matches for affiliation " + aff.getAffname()
              + " with " + matches.toString());
          return new ConcreteNameMatch<>(i, matches, false);
        }
      }
    }
    return new ConcreteNameMatch<>();
  }


  /**
   * Retrieve and update the list of possible matches for actor's name (actor being author/reporter/source author of
   * a contribution). If given actor has no id and is not yet disambiguated, a search on his name is performed, so the
   * id present in the form must be valid and match the DB actor corresponding to this actor form.
   *
   * This method will also check for possible matches in actor's affiliation name, if actor.affid is blank and the
   * affiliation actor is not yet disambiguated, an analogous search is performed for the affiliation actor.
   *
   * This helper method may not be used for disambiguation in full ActorForms (use
   * {@link #findAffiliationsNameMatches(List, String) affiliation matches disovery} for affiliations instead}.
   *
   * @param actor an actor simple form
   * @param lang 2-char ISO 639-1 code of UI
   * @return a (possibly empty) list of ActorHolders that have the same name as given actor's name or affiliation
   */
  public List<ActorHolder> searchForNameMatches(ActorSimpleForm actor, String lang) {
    List<ActorHolder> matchNames = simpleSearchForNameMatches(actor, lang);

    if (values.isBlank(actor.getAffid()) && !actor.getIsAffDisambiguated()) {
      actor.setAffNameMatches(searchForNameMatches(actor.getAffname().trim(), actor.getAffid(), EActorType.UNKNOWN, lang));
      logger.info("found affiliation matches:" + actor.getAffNameMatches());
    }

    return matchNames.isEmpty() ? actor.getAffNameMatches() : matchNames;
  }

  /**
   * Retrieve and update the list of possible matches for actor person's name.
   *
   * @param actor an actor simple form
   * @param lang 2-char ISO 639-1 code of UI
   * @return a (possibly empty) list of ActorHolders that have the same name as given actor' persons name
   */
  private List<ActorHolder> simpleSearchForNameMatches(ActorSimpleForm actor, String lang) {
    if (values.isBlank(actor.getId()) && !actor.getIsDisambiguated()) {
      actor.setNameMatches(searchForNameMatches(actor.getFullname().trim(), actor.getId(), EActorType.value(actor.getActortype()), lang));
      logger.info("found matches :" + actor.getNameMatches());
    }
    return actor.getNameMatches();
  }

  /*
   * **************************************
   *
   * HELPER METHODS FOR HTML SELECT OPTIONS
   *
   * **************************************
   */

  /*
   * ACTORS
   */

  /**
   * Get all actor affiliation types (for organizations)
   *
   * @param lang two char ISO code of interface language
   * @param actorType 0 org and person, 1 org, 2 person
   * @param subtype -1 all, 0 affiliation, 1 affiliated, 2 filiation
   * @return a map of actor affiliation types id-value pairs
   */
  public Map<String, String> getAffiliationTypes(String lang, int actorType, int subtype) {
    return actorFactory.getAffiliationTypes().stream()
            .filter(t -> ((t.getActorId() == 0 && t.getActorId() < 2) || t.getActorId() == actorType || (actorType == 0 && t.getActorId() > -1))
                    && (t.getSubId() == -1 || t.getSubId() == subtype))
            .collect(Collectors.toMap(e -> String.valueOf(e.getId()), e -> e.getName(lang)));
  }

  /**
   * Get profession types
   *
   * @param lang two char ISO code of interface language
   * @param subType the subtype of profession concerned
   * @return a map of precision date type
   */
  public Map<String, String> getProfessionTypes(String lang, int subType) {
    return actorFactory.getProfessionTypes().stream().filter(t -> t.getSubId() == subType)
            .collect(Collectors.toMap(e -> String.valueOf(e.getId()), e -> e.getName(lang)));
  }

  /**
   * Get date precision types
   *
   * @param lang two char ISO code of interface language
   * @param inPast true if it concerns dates in the past
   * @return a map of precision date type
   */
  public Map<String, String> getPrecisionDateTypes(String lang, boolean inPast) {
    return actorFactory.getPrecisionDateTypes().stream().filter(t -> t.isInPast() == inPast)
            .collect(Collectors.toMap(e -> String.valueOf(e.getId()), e -> e.getName(lang)));
  }

  /**
   * Get all countries
   *
   * @param lang two char ISO code of interface language
   * @return a map of countries types id-value pairs
   */
  public Map<String, String> getCountries(String lang) {
    Map<String, String> result = actorFactory.getCountries().stream()
        .collect(Collectors.toMap(Country::getCode, e -> e.getName(lang)
    ));
    return values.sortByValue(result);
  }

  /**
   * Get country by country name
   *
   * @param query the name or the iso code of the country to get
   * @return the corresponding Country
   */
  public Country getCountryByNameOrCode(String query) {
    Country result = null;
    if(!values.isBlank(query)) {
      Optional<Country> r = actorFactory.getCountries().stream()
          .filter(c -> c.getNames().entrySet().stream().anyMatch(n -> n.getValue().equalsIgnoreCase(query))
              || c.getCode().equalsIgnoreCase(query)).findFirst();
      if (r.isPresent()) result = r.get();
    }
    return result;
  }

  /**
   * Get all business sectors (for organizations)
   *
   * @param lang two char ISO code of interface language
   * @return a map of sectors id-value pairs
   */
  public Map<String, String> getBusinessSectors(String lang) {
    Map<String, String> result = new LinkedHashMap<>();
    // avoid using collectors, otherwise order if fucked up because of int key
    actorFactory.getBusinessSectors().stream().sorted((e1, e2) -> e1.getName(lang).compareTo(e2.getName(lang)))
            .forEach(s -> result.put(String.valueOf(s.getId()), s.getName(lang)));
    return result;
  }

  /**
   * Get all legal statuses (for organizations)
   *
   * @param lang two char ISO code of interface language
   * @return a map statuses id-value pairs
   */
  public Map<String, String> getLegalStatuses(String lang) {
    Map<String, String> result = new LinkedHashMap<>();
    // avoid using collectors, otherwise order if fucked up because of int key
    actorFactory.getLegalStatuses().forEach(l -> result.put(String.valueOf(l.getId()), l.getName(lang)));
    return result;
  }

  /**
   * Get all actor genders (for persons)
   *
   * @param lang two char ISO code of interface language
   * @return a map of genders id-value pairs
   */
  public Map<String, String> getGenders(String lang) {
    Map<String, String> genders = new LinkedHashMap<>();
    actorFactory.getGenders().stream().sorted(Comparator.comparing(Gender::getId)).forEach(e -> genders.put(e.getId(), e.getName(lang)));
    return genders;
  }

  /*
   * ARGUMENTS
   */

  /**
   * Get all types of arguments
   *
   * @param lang two char ISO code of interface language
   * @return a map of argument types id-value pairs
   */
  public Map<String, String> getArgumentTypes(String lang) {
    Map<String, String> result = new LinkedHashMap<>();
    argumentFactory.getArgumentTypes().forEach(t -> {
      if (!result.containsKey(String.valueOf(t.getArgumentType()))) {
        result.put(String.valueOf(t.getArgumentType()), t.getTypeName(lang));
      }
    });
    return result;
  }

  /**
   * Get all argument shades
   *
   * @param lang two char ISO code of interface language
   * @return a map of argument shade id-value pairs for given lang
   */
  public Map<String, String> getArgumentShades(String lang) {
    Map<String, String> result = new LinkedHashMap<>();
    argumentFactory.getArgumentTypes().forEach(t ->
            result.put(String.valueOf(t.getArgumentShade()), t.getShadeName(lang)));
    return result;
  }

  /**
   * Get all argument shades for given type, subtype
   *
   * @param type an argument type (int value in string format)
   * @param lang two char ISO code of interface language
   * @return a map of argument shade id-value pairs for given arguments
   */
  public Map<String, String> getArgumentShades(int type, String lang) {
    Map<String, String> result = new LinkedHashMap<>();
    try {
      argumentFactory.getArgumentShades(type).forEach(t ->
          result.put(String.valueOf(t.getArgumentShade()), t.getShadeNameWithDots(lang)));
    } catch (NumberFormatException e) {
      logger.error("unable to parse type " + type);
    }
    return result;
  }

  /**
   * Get justification form
   *
   * @param lang two char ISO code of interface language
   * @return a map of argument shade id-value pairs for justification
   */
  public Map<String, String> getJutificationForm(Lang lang){
    Map<String, String> map = new LinkedHashMap<>();
    for(EArgumentLinkShade shade : EArgumentLinkShade.getJustifications()) {
      map.put(String.valueOf(shade.id()), i18n.get(lang, "argument.links.shade." + shade.id() + ".form"));
    }
    return map;
  }

  /**
   * Get illustration form
   *
   * @param lang two char ISO code of interface language
   * @return a map of argument shade id-value pairs for illustration
   */
  public Map<String, String> getIllustrationForm(Lang lang){
    Map<String, String> map = new LinkedHashMap<>();
    for(EArgumentLinkShade shade : EArgumentLinkShade.getIllustrations()) {
      map.put(String.valueOf(shade.id()), i18n.get(lang, "argument.links.shade." + shade.id() + ".form"));
    }
    return map;
  }

  /**
   * Get all argument link types
   *
   * @param lang two char ISO 639-1 code of interface language
   * @return a map of argument link types id-value pairs
   */
  public Map<String, String> getArgumentLinktypes(String lang) {
    return argumentFactory.getArgumentLinkTypes().stream().collect(Collectors.toMap(e ->
        String.valueOf(e.getLinkType()), e -> e.getLinkTypeName(lang)));
  }

  /**
   * Get all argument link shades
   *
   * @param type an argument link type (int value in string format)
   * @param lang two char ISO 639-1 code of interface language
   * @return a map of argument link shades for given type id-value pairs
   */
  public Map<String, String> getShadeLinkTypes(String type, String lang) {
    try {
      return argumentFactory.getArgumentLinkTypes().stream().filter(t ->
          type.equals(String.valueOf(t.getLinkType()))).collect(Collectors.toMap(e ->
              String.valueOf(e.getLinkShade()), e -> e.getLinkShadeName(lang)));

    } catch (Exception e) {
      logger.error("unable to retrieve link shades of " + type, e);
      return Collections.emptyMap();
    }
  }

  /*
   * CONTRIBUTORS
   */

  /**
   * Get all group color codes
   *
   * @return a list of group color codes
   */
  public List<String> getGroupColorCodes() {
    return contributorFactory.getGroupColors().stream().map(GroupColor::getColorCode).collect(Collectors.toList());
  }

  /*
   * TEXTS
   */

  /**
   * Get all genders
   *
   * @param lang two char ISO code of interface language
   * @return a map of genders id-value pairs
   */
  public Map<String, String> getWordGenders(String lang) {
    return textFactory.getWordGenders().stream().collect(Collectors.toMap(e ->
            String.valueOf(e.getId()), e -> e.getName(lang)));
  }

  /**
   * Get all genders code
   *
   * @return a map of genders one-char code
   */
  public List<String> getWordGenders() {
    return textFactory.getWordGenders().stream().map(WordGender::getId).collect(Collectors.toList());
  }

  /**
   * Get all languages
   *
   * @param lang two char ISO 639-1 code of interface language
   * @return a map of languages id-value pairs
   */
  public Map<String, String> getLanguages(String lang) {

    Map<String, String> temp = textFactory.getLanguages().stream().collect(Collectors.toMap(e ->
        String.valueOf(e.getCode()), e -> e.getName(lang) + " (" + e.getName("own") + ")"));

    temp = values.sortByValue(temp);
    Map<String, String> result = new LinkedHashMap<>();
    // must add first french, dutch and english, then the rest
    // id of french
    result.put("fr", temp.get("fr"));
    // id of dutch
    result.put("nl", temp.get("nl"));
    // id of english
    result.put("en", temp.get("en"));
    result.put("optgroup", "---");
    temp.forEach(result::put);
    return result;
  }

  /**
   * Get language from given (lang) code
   *
   * @param lang two char ISO-639-1 code of the language label to retrieve
   * @param inlang two char ISO-639-1 code of the language to use to retrieve given lang
   * @return a language label in given (inlang) language, null if not found
   */
  public String getLanguage(String lang, String inlang) {
    Optional<Language> tofind = textFactory.getLanguages().stream().filter(l -> lang.equals(l.getCode())).findFirst();
    return tofind.isPresent() ? tofind.get().getName(inlang) : null;
  }

  /**
   * Get all types of text
   *
   * @param lang two char ISO 639-1 code of interface language
   * @return a map of types of text id-value pairs
   */
  public Map<String, String> getTextTypes(String lang) {
    return textFactory.getTextTypes().stream().collect(Collectors.toMap(e ->
        String.valueOf(e.getType()), e -> e.getName(lang)));
  }

  /**
   * Get all type of visibility for texts
   *
   * @param lang two char ISO 639-1 code of interface language
   * @return a map of text visibility id-value pairs
   */
  public Map<String, String> getTextVisibilities(String lang) {
    return textFactory.getTextVisibilities().stream().limit(2).collect(Collectors.toMap(e ->
        String.valueOf(e.getVisibility().id()), e -> e.getName(lang)));
  }

  /*
   * FOLDERS
   */

  /**
   * Get all types of folder
   *
   * @param lang two char ISO 639-1 code of interface language
   * @return a map of types of folder id-value pairs
   */
  public Map<String, String> getFolderTypes(String lang) {
    return folderFactory.getFolderTypes().stream().collect(Collectors.toMap(e ->
        String.valueOf(e.getType()), e -> e.getName(lang)));
  }

  /**
   * Check a list of folder links, return a validation error list if their content is not valid.
   * Also, check folders complete hierarchy
   *
   * @param tocheck a list of FolderLinkForm objects
   * @param lang the user language
   * @param linkedFolder the id of the folder to link
   * @param field the field name in which the folder links are present (for proper error binding)
   * @param parent true if the parents hierarchy must analysed, false if it is the children one.
   * @return a list of validation error if any, null otherwise
   */
  public List<ValidationError> checkFolderLinkFromSimpleForm(List<SimpleFolderForm> tocheck, Long linkedFolder, String lang, String field, boolean parent) {
    List<FolderLinkForm> links = tocheck.stream().map(f ->
        (parent ? new FolderLinkForm(linkedFolder, f.getFolderId(), lang) : new FolderLinkForm(f.getFolderId(), linkedFolder, lang)))
        .collect(Collectors.toList());
    return checkFolderLink(links, field, parent);
  }

  /**
   * Check a list of folder links, return a validation error list if their content is not valid.
   * Also, check folders complete hierarchy
   *
   * @param tocheck a list of FolderLinkForm objects
   * @param field the field name in which the folder links are present (for proper error binding)
   * @param parent true if the parents hierarchy must analysed, false if it is the children one.
   * @return a list of validation error if any, null otherwise
   */
  public List<ValidationError> checkFolderLink(List<FolderLinkForm> tocheck, String field, boolean parent) {
    List<ValidationError> errors = new ArrayList<>();
    boolean noHierarchy = true;

    if (tocheck == null) {
      return errors;
    }

    for(int i = 0; i < tocheck.size(); i++){
      FolderLinkForm link = tocheck.get(i);
      if (!link.isEmpty()) {
        noHierarchy = false;
        String fieldname = field + "[" + tocheck.indexOf(link) + "]";

        if (parent && folderFactory.retrieve(link.getParentId()) == null) {
          errors.add(new ValidationError(fieldname + ".folderName", "folder.hierarchy.notfound"));
        }

        if (!parent && folderFactory.retrieve(link.getChildId()) == null) {
          errors.add(new ValidationError(fieldname + ".folderName", "folder.hierarchy.notfound"));
        }

      }
    }

    if(parent && noHierarchy){
      errors.add(new ValidationError(field + "[0].folderName", "folder.hierarchy.noparent"));
    }

    return !errors.isEmpty() ? errors : null;
  }

  /*
   * ALL TYPES OF CONTRIBUTIONS
   */

  /**
   * Get the amount of contribution types for given type
   *
   * @param type a contribution type
   * @param group a group id
   * @return the amount of given contribution type
   */
  public int getAmountOf(EContributionType type, int group) {
    return textFactory.getAmountOf(type, group);
  }


  /*
   * PRIVATE HELPERS
   */

  /**
   * Get the language name for given language code in given language
   *
   * @param code a 2 char iso-639-1 code for the language name to retrieve
   * @param lang a 2 char iso-639-1 code for the language in which given code must be retrieved
   * @return a language name for given code in given lang, null if not found
   */
  private String getLanguageName(String code, String lang) {
    try {
      return textFactory.getLanguage(code).getName(lang);
    } catch (FormatException e) {
      logger.error("unable to get language name for " + code + " in lang " + lang, e);
      return null;
    }
  }

  /**
   * Retrieve and update the list of possible matches for given affiliation actor's name. If the affiliation actor id in
   * given affiliation form (aka affid) is blank and not yet disambiguated, a search on full name is performed,
   * so the affid present in given affiliation form must be valid and match the DB actor corresponding to this
   * affiliation actor.
   *
   * @param affiliation an affiliation form
   * @param lang 2-char ISO 639-1 code of UI displayed language
   * @return a (possibly empty) list of ActorHolders that have the same name as this wrapper
   */
  private List<ActorHolder> searchForNameMatches(AffiliationForm affiliation, String lang) {
    if (values.isBlank(affiliation.getAffid()) && !affiliation.getIsDisambiguated()) {
      // will search for any spelling in fetched value
      WikiAffiliation wikiAffiliation = affiliation.deserializeFetched();
      List<String> spellings = new ArrayList<>();
      if (wikiAffiliation != null && wikiAffiliation.getOrganization() != null) {
        // must ensure that affname is still a value in the fetched organizations, if not,
        // clear fetched values since user has changed visible name from fetched values
        // and we do not know if they still correspond
        Map<String, String> unfetched = affiliation.getDeserializedFetched(wikiAffiliation.getOrganization());
        if (unfetched.values().stream().anyMatch(s -> s.equals(affiliation.getAffname()))) {
          spellings.addAll(unfetched.values());
        } else {
          // only add affname and clear fetched
          spellings.add(affiliation.getAffname());
          affiliation.setFetched("");
        }
      } else {
        spellings.add(affiliation.getAffname());
      }

      // add all possible name matches to this affiliation
      affiliation.setNameMatches(spellings.stream().flatMap(v ->
          // search by fullname for all possible spellings
          actorFactory.findByFullname(v, EActorType.UNKNOWN).stream()).collect(Collectors.toSet()).stream().map(a ->
          // wrap them into actorHolders
          new ActorHolder(a, lang)).collect(Collectors.toList()));
    }

    logger.debug("found matches: " + affiliation.getNameMatches().toString());
    return affiliation.getNameMatches();
  }

  /**
   * Retrieve and update the list of matches for given name, actor id and actor type
   *
   * @param name a name to search for matches in database
   * @param id an actor id (may be null)
   * @param type the actor type to look for
   * @param lang 2-char ISO 639-1 code of UI
   * @return a list of ActorHolders that have the same name as this wrapper, may be empty
   */
  private List<ActorHolder> searchForNameMatches(String name, Long id, EActorType type, String lang) {
    // search for all matches by full name (exact matches)
    if (!values.isBlank(name)) {
      return actorFactory.findByName(name, type)
          // remove this one from list
          .stream().filter(a -> !a.getId().equals(id) && a.getActorType() != EActorType.UNKNOWN)
          // map to actor holders
          .map(a -> new ActorHolder(a, lang)).collect(Collectors.toList());
    }
    return new ArrayList<>();
  }

  /**
   * Compare given stringified YYYY date return either the earlier one (before = true) or the later
   *
   * @param newOne the string date to compare
   * @param oldOne the date to be compared to
   * @param earlier true if given newOne must be before, false otherwise
   * @return the newOne date if it is earlier (or later if before is false) than given oldOne
   */
  private String getEarlierOrLater(String newOne, String oldOne, boolean earlier) {
    return ((earlier && !values.isBlank(newOne) && oldOne.compareTo(newOne) > 0)
            || (!earlier && !values.isBlank(newOne) && oldOne.compareTo(newOne) < 0) ? newOne : oldOne);
  }

  /**
   * Set the author-related filter values in given filter from given list of actors
   *
   * @param filters the filters map to update
   * @param contribution a contribution holder (argument or text) to retrieve the authors from
   * @param low the lowest date for birthdate at present time
   * @param high the highest date for birthdate at present time
   * @return array of two possibly empty dates to be added checked against existing BIRTHDATE filter values
   */
  private String[] setAuthorsFilters(Map<EFilterName, Map<String, FilterTree>> filters, ContributionHolder contribution, String low, String high) {
    List<ActorSimpleForm> actors;
    String[] result = new String[] {low, high};
    switch (contribution.getType()) {
      case ARGUMENT_CONTEXTUALIZED:
        actors = ((ArgumentContextHolder) contribution).getCitedactors();
        break;
      case EXCERPT:
        actors = ((ExcerptHolder) contribution).getAuthors();
        break;
      case TEXT:
        actors = ((TextHolder) contribution).getAuthors();
        break;
      default:
        // ignore, just return
        return result;
    }

    actors.forEach(a -> {
      // compute lowest and highest dates for RANGE filter
      String[] temp = a.getBirthOrCreation().split("/");
      result[0] = getEarlierOrLater(temp[temp.length - 1], result[0], true);
      result[1] = getEarlierOrLater(temp[temp.length - 1], result[1], false);

      // create BOX filters
      putFilterElementInMap(filters.get(EFilterName.NAME), a.getFullname(), EFilterType.BOX);
      putFilterElementInMap(filters.get(EFilterName.FUNCTION), a.getFunction(), EFilterType.BOX);
      if (!values.isBlank(a.getAffname())) {
        putFilterElementInMap(filters.get(EFilterName.AFFILIATION), a.getAffname(), EFilterType.BOX);
      }
      if (!values.isBlank(a.getResidence())) {
        putFilterElementInMap(filters.get(EFilterName.COUNTRY), a.getResidence(), EFilterType.BOX);
      }

      switch (EActorType.value(a.getActortype())) {
        case PERSON:
          putFilterElementInMap(filters.get(EFilterName.GENDER), a.getGender(), EFilterType.BOX);
          break;
        case ORGANIZATION:
          putFilterElementInMap(filters.get(EFilterName.LEGAL), a.getLegal(), EFilterType.BOX);
          a.getSectors().forEach(s -> putFilterElementInMap(filters.get(EFilterName.SECTOR), s, EFilterType.BOX));
          break;
        default:
          // ignore
      }
    });
    return result;
  }

  /**
   * Helper method to check if given folders names must be matched to existing folders in database.
   * First identified possible match will have its nameMatches list updated and returned.
   * The remaining given folders will not be checked further.
   *
   * @param folders a list of folders (being part of the folder's list of a textual contribution)
   * @param lang 2-char iso-639-1 language code
   * @param folderForm not null if the search comes from a folderForm
   * @param hierarchy true if we seek in parent hierarchy, false if it is children and null if is not in hierarchy
   * @return a NameMatch object that contains the index of the match and
   *         an empty list if no matches exists, the list of namesake folders if any of given folders has namesakes
   */
  public NameMatch<FolderHolder> searchForFolderNameMatches(List<SimpleFolderForm> folders, String lang, FolderForm folderForm, Boolean hierarchy) {
    for (int i = 0; i < folders.size(); i++) {
      SimpleFolderForm folder = folders.get(i);
      if(!folder.isEmpty()) {
        logger.debug("search for matches for " + folder.toString());
        List<FolderHolder> matches = simpleSearchForFolderNameMatches(folder, lang);
        if (!matches.isEmpty()) {
          logger.debug("send modal page to choose what to do with matches for folder " + folder.getName()
              + " with " + folder.getNameMatches().toString());
          folder.setNameMatches(matches);
          if(folderForm != null && hierarchy != null) {
            if(hierarchy){
              folderForm.setParentThatNameMatch(folder);
            }else{
              folderForm.setChildThatNameMatch(folder);
            }
          }
          return new ConcreteNameMatch<>(i, matches);
        }
      }
    }
    return new ConcreteNameMatch<>();
  }

  /**
   * Helper method to check if given folders names must be matched to existing folders in database.
   * First identified possible match will have its nameMatches list updated and returned.
   * The remaining given folders will not be checked further.
   *
   * @param folders a list of folders (being part of the folder's list of a textual contribution)
   * @param lang 2-char iso-639-1 language code
   * @return a NameMatch object that contains the index of the match and
   *         an empty list if no matches exists, the list of namesake folders if any of given folders has namesakes
   */
  public NameMatch<FolderHolder> searchForFolderNameMatches(List<SimpleFolderForm> folders, String lang) {
    return searchForFolderNameMatches(folders, lang, null, null);
  }

  /**
   * Retrieve and update the list of possible matches for folder's name.
   *
   * @param folder an folder simple form
   * @param lang 2-char ISO 639-1 code of UI
   * @return a (possibly empty) list of FolderHolders that have the same name as given folder's name
   */
  private List<FolderHolder> simpleSearchForFolderNameMatches(SimpleFolderForm folder, String lang) {
    if (values.isBlank(folder.getFolderId()) && !folder.getIsDisambiguated()) {
      folder.setNameMatches(searchForFolderNameMatches(folder.getName(), folder.getFolderId(), lang));
      logger.info("found matches :" + folder.getNameMatches());
    }
    return folder.getNameMatches();
  }

  /**
   * Retrieve and update the list of matches for given name and folder id
   *
   * @param name a name to search for matches in database
   * @param id a folder id (may be null)
   * @param lang 2-char ISO 639-1 code of UI
   * @return a list of FolderHolders that have the same name as this wrapper, may be empty
   */
  private List<FolderHolder> searchForFolderNameMatches(String name, Long id, String lang) {
    // search for all matches by name (exact matches)
    if (!values.isBlank(name)) {
      return folderFactory.findByName(name)
          // remove this one from list
          .stream().filter(f -> !f.getId().equals(id))
          // map to folder holders
          .map(f -> new FolderHolder(f, -1L, lang)).collect(Collectors.toList());
    }
    return new ArrayList<>();
  }

  /**
   * Convert an ActorSimpleForm to ProfessionForm
   *
   * @param form an ActorSimpleForm
   * @param lang a 2-char iso code that represents a language
   * @return list ProfessionForm
   */
  public List<ProfessionForm> convertActorSimpleFormToProfessionForm(List<ActorSimpleForm> form, String lang) {
    return form.stream().filter(f -> f != null && f.getFunctionid() != null)
        .map(f -> new ProfessionForm(f.getFunctionid(), f.getFunction(), lang, f.getFunctionGender()))
        .collect(Collectors.toList());
  }


  /**
   * Convert an AffiliationForm to ProfessionForm
   *
   * @param form an AffiliationForm
   * @param lang a 2-char iso code that represents a language
   * @return list ProfessionForm
   */
  public List<ProfessionForm> convertAffiliationFormToProfessionForm(List<AffiliationForm> form, String lang){
    return form.stream().map(a -> new ProfessionForm(a.getFunctionid(), a.getFunction(), lang, a.getGender()))
        .collect(Collectors.toList());
  }

  /**
   * check if new professions are mentioned in affiliation forms and create them if they are new
   *
   * @param forms the affiliations forms
   * @return list of Integer (new profession ids), may be empty
   */
  public List<Integer> checkIfNewProfessionsMustBeCreated(List<ProfessionForm> forms){
    List<Integer> professionIds = new ArrayList<>();
    for(ProfessionForm form : forms) {
      Integer idP = checkIfNewProfessionMustBeCreated(form.getId(), form.getName(), form.getLang(), form.getGender());
      if (idP != -1) professionIds.add(idP);
    }
    return professionIds;
  }

  /**
   * check if a given functionId and given functionName are a new profession, create it if it is a new one
   *
   * @param functionId the id of the profession, -1 if it is a new one
   * @param functionName the name of a function to look for
   * @param lang the lang of the function name
   * @param gender the gender of the function name
   * @return a Integer (new profession id), may be null
   */
  public Integer checkIfNewProfessionMustBeCreated(int functionId, String functionName, String lang, String gender){
    if(functionId == -1 && functionName != null
            && !functionName.equals("") && actorFactory.findProfession(functionName, true) == null) {
      Profession p = actorFactory.createProfession(-1, values.fillProfessionName(lang, gender, functionName));
      return actorFactory.saveProfession(p);
    }
    return -1;
  }

  /**
   * check if a given profession name is valid
   *
   * @param name the string corresponding to the profession's name
   * @param lang a two char iso-639-1 code
   * @return true if the name is valid
   */
  public boolean verifyProfessionName(String name, String lang){
    logger.debug("Verify profession name "+name);
    if(name != null && !name.equals("")){
      //Check for begin warned words match())
      if(textFactory.getWarnedWords(EWarnedWordType.PROFESSION.id(), 0).stream()
          .anyMatch(w -> matchWarnedNames(w.getNames(), true, name, false, null))){
        return false;
      }
      //Check for other warned words match
      if(textFactory.getWarnedWords(EWarnedWordType.PROFESSION.id(), 1).stream().
          anyMatch(w -> matchWarnedWords(w.getNames(), false, name, lang))){
        return false;
      }
      //Check for countries names
      if(actorFactory.getCountries().stream().anyMatch(t -> matchWarnedWords(t.getNames(), false, name, lang))){
        return false;
      }
      //Check for organization names
      /*List<Actor> organizations = actorFactory.getAllActorOrganizations();
      if(organizations.stream().anyMatch(o -> matchWarnedWords(
          o.getNames().stream().collect(Collectors.toMap(ActorName::getLang, ActorName::getLast)), false, name, null))){
        return false;
      }*/
    }
    return true;
  }

  /**
   * check if a given folder name is valid
   *
   * @param name the folderName
   * @param lang a two char iso-639-1 code
   * @return true if it is valid, false otherwise
   */
  public boolean verifyFolder(String name, String lang){
    if(name != null && name.length() > 0){
      logger.debug("Verify folder name "+name);

      //Check for other warned words match
      if(argumentFactory.getWarnedWords(EWarnedWordType.FOLDER.id(), 1).stream()
          .anyMatch(w -> matchWarnedNames(w.getNames(), false, name, true, lang))) {
        return false;
      }
    }
    return true;
  }

  /**
   * check if a given name is in the warning list
   *
   * @param mapToMatch a map of names
   * @param beginWord true if the word is to match at the begin of the word
   * @param name word to match
   * @param strict true if all word have to corresponding
   * @param lang a two char iso-639-1 code
   * @return true if the name is valid
   */
  private boolean matchWarnedNames(Map<String, String> mapToMatch, boolean beginWord, String name, boolean strict, String lang){
    name = name.toLowerCase();
    boolean match = false;
    String toCompare;

    for(Map.Entry<String ,String> m : mapToMatch.entrySet()){
      if(m.getValue() != null && (lang == null || m.getKey().equals(lang))){
        toCompare = m.getValue().toLowerCase();
        if(strict){
          match = name.equals(toCompare);
        }else{
          match = (beginWord ? name.indexOf(toCompare) == 0 : name.contains(toCompare.toLowerCase()));
        }
        if(match) return true;
      }
    }
    return false;
  }

  /**
   * check if a given sentence has words that are the warning list
   *
   * @param mapToMatch a map of names
   * @param firstWord true if the word is the first of the sentence
   * @param name word to match
   * @param lang a two char iso-639-1 code
   * @return true if the name is valid
   */
  private boolean matchWarnedWords(Map<String, String> mapToMatch, boolean firstWord, String name, String lang){
    name = name.toLowerCase();
    String[] words = name.replaceAll("[^a-zA-Z ]", "").toLowerCase().split("\\s+");

    if(lang != null && mapToMatch.containsKey(lang)){
      return compareWordsToWord(words, mapToMatch.get(lang), firstWord);
    }else {
      for (Map.Entry<String, String> m : mapToMatch.entrySet()) {
        if (compareWordsToWord(words, m.getValue(), firstWord)) return true;
      }
    }
    return false;
  }

  /**
   * Compare a word with a list of words
   *
   * @param words the list of words
   * @param word the word to compare
   * @param firstWord only compare with the first word of the list of words
   * @return true if two words are the same
   */
  private boolean compareWordsToWord(String[] words, String word, boolean firstWord){
    boolean matched = false;
    if (word != null && words.length > 0) {
      String toCompare = word.toLowerCase();
      if (firstWord) {
        matched = words[0].equals(toCompare);
      } else {
        matched = Arrays.stream(words).parallel().anyMatch(toCompare::equals);
      }
    }
    return matched;
  }

  /**
   * Get all actor roles
   *
   * @param lang a 2-char iso code that represents a language
   * @return the list of actor roles
   */
  public Map<String, String> getAllContributorRoles(String lang){
    Map<String, String> roles = new HashMap<>();
    for(int i = 1; i <= EContributorRole.OWNER.id(); i++){
      roles.put(i+"", i18n.get(Lang.forCode(lang), "contributor.role."+i));
    }
    return roles;
  }

  /**
   * Get all actor roles until the current contributor role
   *
   * @param lang a 2-char iso code that represents a language
   * @param contributorRole the current contributor role
   * @return the list of actor roles
   */
  public Map<String, String> getAllContributorRoles(String lang, int contributorRole){
    Map<String, String> roles = new HashMap<>();
    for(int i = 1; i <= (contributorRole > EContributorRole.OWNER.id() ? EContributorRole.OWNER.id() : contributorRole); i++){
      roles.put(i+"", i18n.get(Lang.forCode(lang), "contributor.role."+i));
    }
    return roles;
  }

  /**
   * Get contributor group role : contributor, owner, admin and all
   *
   * @param lang a 2-char iso code that represents a language
   * @return the list of contributor roles
   */
  public Map<String, String> getContributorGroupRoles(String lang){
    Map<String, String> roles = new HashMap<>();
    for(int i =  EContributorRole.CONTRIBUTOR.id(); i <= EContributorRole.MYSLEF.id() && i <= EContributorRole.ADMIN.id(); i++){
      roles.put(i+"", i18n.get(Lang.forCode(lang), "admin.mail.usergroup."+i));
    }
    return roles;
  }

  /**
   * Check if a given domain name is a free source or not. Free means free of rights.
   *
   * @param domain the domain name to check
   * @return true if the given domain name is a free source
   */
  public boolean isFreeSource(String domain){
    return textFactory.sourceIsCopyrightfree(domain);
  }

  /**
   * Check if a given domain name is a free source or not. Free means free of rights.
   *
   * @param domain the domain name to check
   * @param contributor the contributor id
   * @return true if the given domain name is a free source
   */
  public boolean isFreeSource(String domain, Long contributor){
    return textFactory.sourceIsCopyrightfree(domain, contributor);
  }

  /**
   * This enumeration holds values for lower and higher date in the the corresponding map used for filters.
   *
   * @author Martin Rouffiange
   */
  private enum EDateType {

    /**
     * Lower date
     */
    LOW_DATE(0),
    /**
     * Higher date
     */
    HIGHER_DATE(1);

    private int id;
    private static Map<Integer, EDateType> map = new LinkedHashMap<>();

    static {
      for (EDateType type : EDateType.values()) {
        map.put(type.id, type);
      }
    }

    /**
     * Constructor
     *
     * @param id an int representing a date type
     */
    EDateType(int id) {
      this.id = id;
    }

    /**
     * Get the enum value for a given type
     *
     * @param id an int representing an EDateType
     * @return the EDateType enum value corresponding to the given id, null otherwise.
     */
    public static EDateType value(int id) {
      return map.get(id);
    }

    /**
     * Get this type
     *
     * @return an int representation of this EDateType
     */
    public int id() {
      return id;
    }
  }
}


