/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.excerpt;

import be.objectify.deadbolt.java.actions.Restrict;
import be.webdeb.application.query.BadQueryException;
import be.webdeb.application.query.EQueryKey;
import be.webdeb.core.api.argument.ArgumentContext;
import be.webdeb.core.api.argument.ArgumentFactory;
import be.webdeb.core.api.argument.ArgumentIllustration;
import be.webdeb.core.api.argument.EArgumentLinkShade;
import be.webdeb.core.api.contribution.ContextContribution;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.contributor.Contributor;
import be.webdeb.core.api.excerpt.*;
import be.webdeb.core.api.text.ETextType;
import be.webdeb.core.api.text.Text;
import be.webdeb.core.api.text.TextFactory;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.infra.fs.FileSystem;
import be.webdeb.infra.ws.external.excerpt.ExcerptRequest;
import be.webdeb.infra.ws.nlp.RequestProxy;
import be.webdeb.presentation.web.controllers.CommonController;
import be.webdeb.presentation.web.controllers.browse.SearchForm;
import be.webdeb.presentation.web.controllers.entry.ContributionHolder;
import be.webdeb.presentation.web.controllers.entry.NameMatch;
import be.webdeb.presentation.web.controllers.entry.actor.ActorHolder;
import be.webdeb.presentation.web.controllers.entry.actor.ActorSimpleForm;
import be.webdeb.presentation.web.controllers.entry.argument.ArgumentActions;
import be.webdeb.presentation.web.controllers.entry.argument.ArgumentContextForm;
import be.webdeb.presentation.web.controllers.entry.argument.ArgumentContextHolder;
import be.webdeb.presentation.web.controllers.entry.folder.FolderHolder;
import be.webdeb.presentation.web.controllers.permission.WebdebRole;
import be.webdeb.presentation.web.controllers.SessionHelper;
import be.webdeb.presentation.web.controllers.permission.WebdebUser;
import be.webdeb.presentation.web.views.html.entry.excerpt.editExcerpt;
import be.webdeb.presentation.web.views.html.entry.excerpt.editExcerptFields;
import be.webdeb.presentation.web.views.html.entry.excerpt.editExcerptModal;
import be.webdeb.presentation.web.views.html.entry.excerpt.excerptSelection;
import be.webdeb.presentation.web.views.html.util.handleFolderNameMatches;
import be.webdeb.presentation.web.views.html.util.handleNameMatches;
import be.webdeb.presentation.web.views.html.util.message;
import be.webdeb.presentation.web.views.html.viz.privateContribution;
import be.webdeb.presentation.web.views.html.viz.excerptArgumentsListDiv;
import play.data.Form;
import play.libs.Json;
import play.mvc.Result;
import play.mvc.Results;

import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

/**
 * Excerpt-related actions, ie controller of all pages to edit excerpts
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public class ExcerptActions extends CommonController {

    @Inject
    protected FileSystem files;

    @Inject
    protected RequestProxy proxy;

    @Inject
    private ArgumentActions argumentActions;

    private static final String NOACTION = "argument.properties.noaction";
    private static final String OOPS = "argument.properties.oops";

    /**
     * Ask page to add a new excerpt
     *
     * @return the form page where users may add details regarding a selected excerpt, or redirect to
     * the "work with text" page if passed form contains error (should not happen since form is created
     * implicitly from request and not by user)
     */
    @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
    public CompletionStage<Result> newExcerpt() {
        logger.debug("POST new excerpt");
        Form<ExcerptSimpleForm> form = formFactory.form(ExcerptSimpleForm.class).bindFromRequest();
        sessionHelper.set(ctx(), SessionHelper.KEY_GOTO, sessionHelper.getReferer(ctx()));
        String lang = ctx().lang().code();

        if (form.hasErrors()) {
            // should definitely not happen
            logger.debug("form has error (should not happen here) " + form.errors().toString());
            flash(SessionHelper.ERROR, i18n.get(ctx().lang(), "argument.text.notfound"));
            return gotoWorkWithText(-1L, context.current());
        }

        ExcerptSimpleForm newExc = form.get();

        // does text exist ?
        Text text = textFactory.retrieve(newExc.getTextId());
        if (text == null) {
            flash(SessionHelper.ERROR, i18n.get(ctx().lang(),"argument.text.notfound"));
            sessionHelper.remove(ctx(), SessionHelper.KEY_GOTO);
            return gotoWorkWithText(-1L, context.current());
        }

        // do we have an original excerpt
        if (values.isBlank(newExc.getOriginalExcerpt())) {
            flash(SessionHelper.ERROR, i18n.get(ctx().lang(),"argument.route.noarg"));
            sessionHelper.remove(ctx(), SessionHelper.KEY_GOTO);
            return gotoWorkWithText(newExc.getTextId(), context.current());
        }

        if(newExc.getShade() != null && !newExc.getShade().equals("") && EArgumentLinkShade.value(Integer.parseInt(newExc.getShade())) != null){
            List<String> toAdd = sessionHelper.getValues(ctx(), SessionHelper.KEY_NEW_ILLU_LINK);
            if(toAdd != null && toAdd.size() == 2) {
                toAdd.set(1, newExc.getShade());
                sessionHelper.setValues(ctx(), SessionHelper.KEY_NEW_ILLU_LINK, toAdd);
            }
        }

        // pre-fill excerpt with text's actors, folders and current group
        ExcerptForm excForm = new ExcerptForm(newExc.getTextId(), text.getLanguage().getCode(), lang, newExc.getOriginalExcerpt());
        addAuthorsToForm(excForm, text);

        //text.getFoldersAsList().forEach(f -> excForm.folders.add(new SimpleFolderForm(f.getId(), f.getName(ctx().lang().code()))));
        excForm.setInGroup(sessionHelper.getCurrentGroup(ctx()));
        excForm.setLang(ctx().lang().code());

        // check if this excerpt was created to be linked to an argument, if it is the case add every context
        // elements like folders, places and cited actors.
        List<String> cachedLink = sessionHelper.getValues(ctx(), SessionHelper.KEY_NEW_ILLU_LINK);
        if (cachedLink != null && values.isNumeric(cachedLink.get(0))){
            ArgumentContext arg = argumentFactory.retrieveContextualized(Long.parseLong(cachedLink.get(0)));
            if(arg != null){
                ArgumentContextHolder argHolder = new ArgumentContextForm(arg, lang);
                excForm.setFolders(argHolder.getFolders());
                excForm.setPlaces(argHolder.getPlaces());
                excForm.setCitedactors(argHolder.getCitedactors());
            }
        }

        // display first page of argument edition
        return CompletableFuture.supplyAsync(() ->
                        ok(editExcerpt.render(
                                formFactory.form(ExcerptForm.class).fill(excForm),
                                excForm.getTextId(),
                                -1L,
                                helper,
                                sessionHelper.getUser(ctx()),
                                null)),
                context.current());
    }

    /**
     * Display the page to edit the excerpt properties
     *
     * @param textId the text id to which this excerpt comes from
     * @param excId the excerpt id to edit
     * @return the property page with given excerpt data, or redirect to "text excerpt" page
     * of given text with an, error message to display (flash) if excerpt is not found
     */
    @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
    public CompletionStage<Result> edit(Long textId, Long excId) {
        logger.debug("GET excerpt properties of " + excId + " from text " + textId);
        return editExcerpt(textId, excId, false);
    }

    /**
     * Display the modal to edit the excerpt properties
     *
     * @param textId the text id to which this excerpt comes from
     * @param excId the excerpt id to edit
     * @return the edit modal page with given excerpt data, or redirect to the session "goto" page with an error message
     * to display (flash) if excerpt or text are not found
     */
    @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
    public CompletionStage<Result> editFromModal(Long textId, Long excId) {
        logger.debug("GET excerpt modal properties of " + excId + " from text " + textId);
        return editExcerpt(textId, excId, true);
    }

    /**
     * Update or create an excerpt
     *
     * @param textId the text id
     * @param excId the excerpt id (-1 if new one)
     * @return the excerpt form object if it contains errors or actor names must be disambiguated (400 response),
     * or in case of success or DB crash, redirect to either TextActions.textExcerpts if no "goto" session key has
     * been set, or to that "goto" url.
     */
    @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
    public CompletionStage<Result> save(Long textId, Long excId) {
        logger.debug("POST save properties of " + excId + " from text " + textId);
        Form<ExcerptForm> form = formFactory.form(ExcerptForm.class).bindFromRequest();
        try {
            ExcerptForm result = saveExcerpt(form);

            // all good -> go to this actor's page (and remove session key_goto if any)
            ctx().flash().put(SessionHelper.SUCCESS, i18n.get(ctx().lang(),"argument.properties.added"));
            return redirectToGoTo(be.webdeb.presentation.web.controllers.entry.text.routes.TextActions.textExcerpts(result.getTextId()));

        } catch (ExcerptNotSavedException e) {
            return handleExcerptError(e, textId, excId, false);
        }
    }

    /**
     * Update or create an excerpt from a modal
     *
     * @param textId the text id
     * @param excId the excerpt id (-1 if new one)
     * @return the excerpt form object if it contains errors, or the context contribution page if this new excerpt
     * has been created to be linked to another one, or in case of success or DB crash, redirect to "goto" session key url.
     */
    @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
    public CompletionStage<Result> saveFromModal(Long textId, Long excId) {
        logger.debug("POST save properties of " + excId + " from text " + textId);

        Map<String, String> messages = new HashMap<>();
        Form<ExcerptForm> form = formFactory.form(ExcerptForm.class).bindFromRequest();

        try {
            saveExcerpt(form);
            return CompletableFuture.supplyAsync(() -> ok(message.render(messages)), context.current());

        } catch (ExcerptNotSavedException e) {
            return handleExcerptError(e, textId, excId,true);
        }
    }

    /**
     * Cancel edition of given excerpt and either redirect to session-cached "goto" or to textExcerpt page
     *
     * @param textId a text id
     * @param excId an excerpt id
     * @return either the session "goto" page or the textExcerpt page of given textId
     */
    @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
    public CompletionStage<Result> cancel(Long textId, Long excId) {
        logger.debug("GET cancel edit excerpt for " + excId + " from text " + textId);
        // check where we have to go after in case of cancellation or happy ending
        return redirectToGoTo(be.webdeb.presentation.web.controllers.entry.text.routes.TextActions.textExcerpts(textId));
    }

    /*
     * AJAX calls
     */

    /**
     * See all arguments that are linked with the given excerpt
     *
     * @param excId an excerpt id
     * @return the excerptArgumentsListDiv fill with arguments or bad request if the given excerpt is not found.
     */
    public CompletionStage<Result> seeExcerptArguments(Long excId) {
        logger.debug("GET arguments related to exc " + excId);
        Excerpt e = excerptFactory.retrieve(excId);
        if(e == null){
            return CompletableFuture.supplyAsync(Results::badRequest, context.current());
        }

        Map<EArgumentLinkShade, List<ArgumentContextHolder>> argsMap = new LinkedHashMap<>();
        EArgumentLinkShade.getIllustrations().forEach(shade -> argsMap.put(shade, new ArrayList<>()));

        e.getArguments().forEach(illustration ->
                argsMap.get(illustration.getArgumentLinkType().getEType())
                .add(new ArgumentContextHolder(illustration.getArgument(), ctx().lang().code())));

        return CompletableFuture.supplyAsync(() ->
                ok(excerptArgumentsListDiv.render(
                    new ExcerptHolder(e, ctx().lang().code()),
                    argsMap,
                    sessionHelper.getUser(ctx()))
                ), context.current());
    }

    /**
     * Search for an excerpt based on given query. Will look in original excerpt, authors and text title
     *
     * @param term the values to search for
     * @param argIdToIgnore the contextualized argument id to ignore if searched to linked, if any
     * @param textToLook the text id where to search, if any
     * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
     * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
     * @return a jsonified list of excerpt holders
     */
    @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
    public CompletionStage<Result> searchExcerpt(String term, Long argIdToIgnore, Long textToLook, int fromIndex, int toIndex) {
        List<ExcerptHolder> result = new ArrayList<>();
        List<Map.Entry<EQueryKey, String>> query = new ArrayList<>();
        query.add(new AbstractMap.SimpleEntry<>(EQueryKey.CONTRIBUTION_TYPE, String.valueOf(EContributionType.EXCERPT.id())));
        query.add(new AbstractMap.SimpleEntry<>(EQueryKey.EXCERPT_TITLE, term));
        query.add(new AbstractMap.SimpleEntry<>(EQueryKey.CONTEXT_TO_IGNORE, argIdToIgnore.toString()));

        if(textToLook == -1L) {
            query.add(new AbstractMap.SimpleEntry<>(EQueryKey.AUTHOR, term));
            query.add(new AbstractMap.SimpleEntry<>(EQueryKey.TEXT_TITLE, term));
        }else{
            query.add(new AbstractMap.SimpleEntry<>(EQueryKey.TEXT_TO_LOOK, textToLook.toString()));
        }

        try {
            String lang = ctx().lang().code();
            executor.searchContributions(query, fromIndex, toIndex).stream().filter(
                    sessionHelper.getUser(ctx())::mayView).forEach(e ->
                    result.add(new ExcerptHolder((Excerpt) e, lang)));
        } catch (BadQueryException e) {
            logger.warn("unable to search for excerpts with given term " + term, e);
        }
        return CompletableFuture.supplyAsync(() -> ok(Json.toJson(result)), context.current());
    }

    /**
     * Get text selection modal to select an excerpt to be bound to given contextualized argument with given link shade
     *
     * @param argId an argument id
     * @param contextId the context contribution where select an excerpt
     * @param shade an argument link shade, if any
     * @return the argumentSelection page to look for a text to start with
     */
    @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
    public CompletionStage<Result> excerptSelection(Long argId, Long contextId, int shade) {
        logger.debug("GET argument selection modal to add illustration link from " + argId + " with shade " + shade);
        WebdebUser user = sessionHelper.getUser(ctx());

        ContextContribution contextContribution = textFactory.retrieveContextContribution(contextId);

        EArgumentLinkShade type = EArgumentLinkShade.value(shade);
        ArgumentContext argumentContext = argumentFactory.retrieveContextualized(argId);

        if ((shade != -1 && type == null) || argumentContext == null) {
            logger.error("called properties with unknown contextualized argument " + argId + " or shade " + shade);
            return CompletableFuture.supplyAsync(Results::badRequest, context.current());
        }

        // store link in user session with shade
        sessionHelper.setValues(ctx(), SessionHelper.KEY_NEW_ILLU_LINK,
                Arrays.asList(argId.toString(), String.valueOf(shade)));
        sessionHelper.set(ctx(), SessionHelper.KEY_GOTO, sessionHelper.getReferer(ctx()));

        // init search query (reuse query from search form)
        SearchForm query = new SearchForm();
        query.setAllContributionsFlags(false);
        query.setIsExcerpt(true);
        query.setIsStrict(true);
        //query.setContextToIgnore(argId);
        query.setInGroup(user.getGroup().getGroupId());

        ExcerptSimpleForm excForm = new ExcerptSimpleForm();

        if(contextContribution != null && contextContribution.getType() == EContributionType.TEXT) {
            query.setTextToLook(contextContribution.getId());
            excForm.setTextId(contextContribution.getId());
        }

        return CompletableFuture.supplyAsync(() ->
                        ok(excerptSelection.render(argumentContext.getArgument().getFullTitle(),
                                argumentContext.getId(),
                                contextContribution != null ? helper.toHolder(contextContribution, user, ctx().lang().code()) : null,
                                formFactory.form(ExcerptSimpleForm.class).fill(excForm),
                                type,
                                formFactory.form(SearchForm.class).fill(query),
                                null, helper, user)),
                context.current());
    }

    /**
     * Add selected shade in session on the excerptSelection process.
     *
     * @param argId an argument id
     * @param shade an argument link shade, if any
     * @return ok if given argId and shade are good
     */
    @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
    public CompletionStage<Result> excerptSelectionShade(Long argId, int shade) {
        logger.debug("GET selected shade");
        EArgumentLinkShade type = EArgumentLinkShade.value(shade);
        ArgumentContext argumentContext = argumentFactory.retrieveContextualized(argId);
        if (type == null || argumentContext == null) {
            logger.error("called properties with unknown contextualized argument " + argId + " or shade " + shade);
            return CompletableFuture.supplyAsync(Results::badRequest, context.current());
        }
        // store link in user session with shade
        sessionHelper.setValues(ctx(), SessionHelper.KEY_NEW_ILLU_LINK,
                Arrays.asList(argId.toString(), String.valueOf(shade)));

        return CompletableFuture.supplyAsync(Results::ok, context.current());
    }

    /**
     * Search for an illustration link between an argument and a excerpt
     *
     * @param argId contextualized argument id
     * @param excId an excerpt id
     * @return not found if form has errors, unauthorized if contributor or text are not defined or a db crash, or the
     * excerpt edit page if everything is ok.
     */
    public CompletionStage<Result> findIllustrationLink( Long argId, Long excId) {
        logger.debug("Find illustration link with argId " + argId + " and excId " + excId);

        ArgumentIllustration illustration = excerptFactory.retrieveIllustrationLink(argId, excId);

        if(illustration == null){
            logger.debug("Illustration not found");
            return CompletableFuture.supplyAsync(Results::badRequest, context.current());
        }
        return CompletableFuture.supplyAsync(() -> ok(Json.toJson(illustration.getId())), context.current());
    }

    /**
     * Save an excerpt from external service into the database
     *
     * @return not found if form has errors, unauthorized if contributor or text are not defined or a db crash, or the
     * excerpt edit page if everything is ok.
     */
    public CompletionStage<Result> saveFromExternal() {
        logger.debug("POST save external excerpt");
        Form<ExcerptRequest> form = formFactory.form(ExcerptRequest.class).bindFromRequest();

        if (!form.hasErrors()) {
            ExcerptRequest excerptForm = form.get();
            Contributor contributor = excerptForm.getUser().getContributor();
            Text text = textFactory.retrieve(excerptForm.getTextId());

            if(contributor != null && text != null){
                session(SessionHelper.KEY_USERMAILORPSEUDO, contributor.getEmail());
                sessionHelper.getUser(ctx());
                try{
                    Long excerptId;
                    ExternalExcerpt excerpt = excerptFactory.findExistingExternalExcerpt(contributor.getId(),
                            excerptForm.getOriginalExcerpt(), excerptForm.getTextId());
                    if(excerpt == null) {
                        excerptForm.save(contributor.getId());
                        excerptId = excerptForm.getId();
                    }else{
                        excerptId = excerpt.getId();
                    }
                    return CompletableFuture.supplyAsync(() ->
                            ok(be.webdeb.presentation.web.controllers.entry.excerpt.routes.ExcerptActions.edit(text.getId(), excerptId)
                            .toString()), context.current());
                } catch (FormatException | PersistenceException | PermissionException e) {
                    logger.error("unable to save external excerpt", e);
                }
            }else{
                return CompletableFuture.supplyAsync(Results::unauthorized, context.current());
            }
        }
        return CompletableFuture.supplyAsync(Results::badRequest, context.current());
    }

    /*
     * PRIVATE HELPERS
     */

    /**
     * Convenience private method to redirect to the work with text page
     *
     * @param id a text id
     * @return the work with text page
     */
    private CompletionStage<Result> gotoWorkWithText(Long id, Executor executor) {
        return CompletableFuture.supplyAsync(() ->
                redirect(be.webdeb.presentation.web.controllers.entry.text.routes.TextActions.textExcerpts(id)), executor);
    }

    /**
     * Add authors to form, from text
     *
     * @param excForm the excForm where add authors
     * @param text the text linked to the excerpt
     */
    private void addAuthorsToForm(ExcerptForm excForm, Text text){
        if(text != null) {
            text.getActors().forEach(r -> excForm.authors.add(new ActorSimpleForm(r, ctx().lang().code())));
        }
    }

    /**
     * Edit an excerpt from a given text id, from a modal or not.
     *
     * @param textId a text id
     * @param excId an excerpt id
     * @return bad request if excerpt not found, unauthorize if user must not see this excerpt, the edit form if all is ok.
     */
    public CompletionStage<Result> editExcerpt(Long textId, Long excId, boolean fromModal) {
        WebdebUser user = sessionHelper.getUser(ctx());
        deleteNewLinkKeys();
        String lang = ctx().lang().code();

        // prepare wrapper for excerpt
        Excerpt excerpt = excerptFactory.retrieve(excId);
        Text text = textFactory.retrieve(textId);
        ExternalExcerpt externalExcerpt = null;
        if (excerpt == null) {
            externalExcerpt = excerptFactory.retrieveExternal(excId);
            if(externalExcerpt == null || (externalExcerpt.getInternalContribution() != null && externalExcerpt.getInternalContribution() != -1)) {
                if(fromModal){
                    return CompletableFuture.completedFuture(Results.badRequest());
                }
                // leave if we couldn't find argument
                flash(SessionHelper.ERROR, i18n.get(ctx().lang(), OOPS));
                sessionHelper.remove(ctx(), SessionHelper.KEY_GOTO);
                return CompletableFuture.supplyAsync(() ->
                                redirect(be.webdeb.presentation.web.controllers.entry.text.routes.TextActions.textExcerpts(textId).url()),
                        context.current());
            }
        }
        else{
            if(!user.mayView(excerpt)){
                return CompletableFuture.completedFuture(Results.unauthorized(
                        privateContribution.render(user)));
            }

            sessionHelper.set(ctx(), SessionHelper.KEY_GOTO, sessionHelper.getReferer(ctx()));
        }
        ExcerptForm excForm;
        if(excerpt != null){
            excForm = new ExcerptForm(excerpt, lang);
        }else{
            excForm = new ExcerptForm(externalExcerpt, textId);
            addAuthorsToForm(excForm, text);
        }
        excForm.setInGroup(sessionHelper.getCurrentGroup(ctx()));
        // display the page of excerpt edition
        Form<ExcerptForm> form = formFactory.form(ExcerptForm.class).fill(excForm);
        return CompletableFuture.supplyAsync(() ->
                ok(fromModal ? editExcerptModal.render(form, textId, excId, helper, sessionHelper.getUser(ctx()), null) :
                        editExcerpt.render(form, textId, excId, helper, sessionHelper.getUser(ctx()), null)), context.current());
    }

    /*
     * PRIVATE HELPERS
     */

    /**
     * Save an excerpt from a given form.
     *
     * @param form an excerpt form object that may contain errors
     * @return given (updated) actor form
     * @throws ExcerptNotSavedException if an error exist in passed form or any error arisen from save action
     */
    private synchronized ExcerptForm saveExcerpt(Form<ExcerptForm> form) throws ExcerptNotSavedException {
        // check errors, sends back whole form if any
        if (form.hasErrors()) {
            logger.debug("form has errors " + form.errors() + "\nData:" + form.data());
            throw new ExcerptNotSavedException(form, ExcerptNotSavedException.ERROR_FORM);
        }

        ExcerptForm excerpt = form.get();

        if (!helper.searchForNameMatches(excerpt.getActors(), ctx().lang().code()).isEmpty())  {
            throw new ExcerptNotSavedException(form.fill(excerpt), ExcerptNotSavedException.AUTHOR_NAME_MATCH);
        }

        List<Integer> newProfessions = helper.checkIfNewProfessionsMustBeCreated(
                helper.convertActorSimpleFormToProfessionForm(excerpt.getActors(), ctx().lang().code()));
        newProfessions.forEach(id -> sessionHelper.addValue(ctx(), SessionHelper.KEY_NEWPROFESSION, id+""));

        // check folder name matches
        if (!helper.searchForFolderNameMatches(excerpt.getFolders(), ctx().lang().code()).isEmpty()) {
            throw new ExcerptNotSavedException(form.fill(excerpt), ExcerptNotSavedException.FOLDER_NAME_MATCH);
        }

        excerpt.setPlaces(savePlaces(excerpt.getPlaces(), true));

        // try to save in DB
        try {
            treatSaveContribution(excerpt.save(sessionHelper.getUser(ctx()).getContributor().getId()));
        } catch (FormatException | PersistenceException | PermissionException e) {
            logger.error("unable to save excerpt", e);
            throw new ExcerptNotSavedException(form, ExcerptNotSavedException.ERROR_DB, i18n.get(ctx().lang(),e.getMessage()));
        }

        // check if this excerpt was created to be linked to another one (from visualization page)
        List<String> cachedLink = sessionHelper.getValues(ctx(), SessionHelper.KEY_NEW_ILLU_LINK);
        if (cachedLink != null && cachedLink.size() == 2 && values.isNumeric(cachedLink.get(0))
                && values.isNumeric(cachedLink.get(1), 0, false)) {
            logger.info("ask confirmation for previously new link request to new excerpt " + excerpt.getId());
            sessionHelper.remove(ctx(), SessionHelper.KEY_NEW_ILLU_LINK);

            ArgumentContext origin = argumentFactory.retrieveContextualized(Long.parseLong(cachedLink.get(0)));
            Excerpt destination = excerptFactory.retrieve(excerpt.getId());

            if (origin != null && destination != null) {
                // form that will be passed is reconstructed from cached contextualized argument since it was
                // selected as the origin of the new illustration link creation request
                try{
                    argumentActions.saveIllustrationLink(origin, destination,
                            EArgumentLinkShade.value(Integer.parseInt(cachedLink.get(1))), excerpt.getInGroup());
                } catch (FormatException | PersistenceException | PermissionException e) {
                    logger.error("unable to save new illustration link", e);
                }
            }
        }

        return excerpt;
    }


    /**
     * Handle error on excerpt form submission and returns the actor form view (depending on the switch).
     * If an unknown error occurred, either a "goto" page or the general entry view is returned.
     *
     * @param exception the exception raised from unsuccessful save
     * @param textId the text id
     * @param excId the excerpt id (-1 if new one)
     * @param onlyFields switch to know if the argumentContextDiv views must be returned
     * @return if the form contains error, a bad request (400) response is returned with, if onlyfield, the
     * editExcerptFields template or the editExcerpt full form otherwise. In case of possible author name matches,
     * a 409 response is returned with the modal frame to select among possible matches.In case of possible
     * folder name matches, a 410 response is returned with the modal frame to select among possible matches.
     * If another error occurred, a redirect to either a "goto" session-cached url or the main entry page.
     */
    private CompletionStage<Result> handleExcerptError(ExcerptNotSavedException exception, Long textId, Long excId, boolean onlyFields) {
        Map<String, String> messages = new HashMap<>();
        Form<ExcerptForm> form = exception.form;
        ExcerptForm excerpt;

        switch (exception.error) {
            case ExcerptNotSavedException.AUTHOR_NAME_MATCH:
                // check auhtor name matches
                excerpt = form.get();
                NameMatch<ActorHolder> actorsMatches = helper.searchForNameMatches(excerpt.getActors(), ctx().lang().code());

                if(!actorsMatches.isEmpty()) {
                    return CompletableFuture.supplyAsync(() -> onlyFields ?
                                    status(409, handleNameMatches.render(actorsMatches.getNameMatches(), actorsMatches.isActor(), actorsMatches.getIndex())) :
                                    badRequest(editExcerpt.render(form, textId, excId, helper, sessionHelper.getUser(ctx()), messages))
                            , context.current());
                }

            case ExcerptNotSavedException.FOLDER_NAME_MATCH:
                // check folder name matches
                excerpt = form.get();
                NameMatch<FolderHolder> folderMatches = helper.searchForFolderNameMatches(excerpt.getFolders(), ctx().lang().code());
                if (!folderMatches.isEmpty()) {
                    return CompletableFuture.supplyAsync(() -> onlyFields ?
                                    status(409, handleFolderNameMatches.render(folderMatches.getNameMatches(), folderMatches.getIndex())) :
                                    badRequest(editExcerpt.render(form, textId, excId, helper, sessionHelper.getUser(ctx()), messages))
                            , context.current());
                }
            case ExcerptNotSavedException.ERROR_FORM:
                // error in form, just resend it
                messages.put(SessionHelper.WARNING, i18n.get(ctx().lang(), "error.form"));

                return CompletableFuture.supplyAsync(() -> onlyFields ?
                        badRequest(editExcerptFields.render(form, textId, excId, helper, sessionHelper.getUser(ctx()), messages))
                        : badRequest(editExcerpt.render(form, textId, excId, helper, sessionHelper.getUser(ctx()), messages)), context.current());
            default:
                // any other error, check where do we have to go after and show message in exception
                messages.put(SessionHelper.ERROR, i18n.get(ctx().lang(), "error.crash"));
                return onlyFields ? CompletableFuture.supplyAsync(() ->
                        internalServerError(message.render(messages)), context.current())
                        : redirectToGoTo(null);
        }
    }

    /**
     * Inner class to handle excerpt exception when an excerpt cannot be saved from private save execution
     */
    private class ExcerptNotSavedException extends Exception {

        private static final long serialVersionUID = 1L;
        final Form<ExcerptForm> form;
        final int error;

        static final int ERROR_FORM = 0;
        static final int AUTHOR_NAME_MATCH = 1;
        static final int FOLDER_NAME_MATCH = 2;
        static final int ERROR_DB = 3;

        ExcerptNotSavedException(Form<ExcerptForm> form, int error) {
            this.error = error;
            this.form = form;
        }

        ExcerptNotSavedException(Form<ExcerptForm> form, int error, String message) {
            super(message);
            this.error = error;
            this.form = form;
        }
    }
}
