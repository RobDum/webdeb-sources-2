/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.argument;

import be.webdeb.core.api.argument.ArgumentContext;
import be.webdeb.core.api.argument.ArgumentJustification;
import be.webdeb.core.api.argument.EArgumentLinkShade;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.exception.PersistenceException;

/**
 * This class holds concrete values of an ArgumentJustification link (i.e. no type/data IDs, but their descriptions, as
 * defined in the database)
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public class JustificationLinkForm extends BaseLinkForm {

    // for lazy loading of link
    private ArgumentContext origin;
    private ArgumentContext destination;

    /**
     * Play / JSON compliant constructor
     */
    public JustificationLinkForm() {
        super();
        type = EContributionType.ARG_JUSTIFICATION;
    }

    /**
     * Construct a link wrapper with a given ArgumentJustification link (full initialization)
     *
     * @param link an existing ArgumentJustification
     * @param lang 2-char ISO code of context language (among play accepted languages)
     */
    public JustificationLinkForm(ArgumentJustification link, String lang) {
        this(link, lang, true);
    }

    /**
     * Construct a link wrapper with a given ArgumentJustification link
     *
     * @param link an existing ArgumentJustification
     * @param lang 2-char ISO code of context language (among play accepted languages)
     * @param withType true if the link type must be initialized too
     */
    public JustificationLinkForm(ArgumentJustification link, String lang, boolean withType) {
        super(link, lang);
        origin = link.getOrigin();
        originId = origin.getId();
        originTitle = origin.getArgument().getDictionary().getTitle();
        destination = link.getDestination();
        destinationTitle = destination.getArgument().getDictionary().getTitle();
        destinationId = destination.getId();
        this.lang = lang;
        if (withType) {
            linktype = link.getArgumentLinkType().getLinkTypeName(lang);
            //String.valueOf(link.getArgumentLinkType().getLinkType());
            linkshade = link.getArgumentLinkType().getLinkShadeName(lang);
            shade = link.getArgumentLinkType().getEType();
            linkshadeId = link.getArgumentLinkType().getLinkShade();
        }
    }

    /**
     * Transform this form into an API justification link
     *
     * @return an API justification link corresponding to this justification link form
     * @throws PersistenceException if given linkshade could not be casted into an int value
     */
    public ArgumentJustification toLink() throws PersistenceException {
        return toLink(true);
    }

    /**
     * Transform this form into an API justification link, maybe setting the link type
     *
     * @param withType true if type must also be set
     *
     * @return an API justification link corresponding to this link form
     * @throws PersistenceException if given linkshade could not be casted into an int value
     */
    public ArgumentJustification toLink(boolean withType) throws PersistenceException {
        ArgumentJustification link = factory.getArgumentJustificationLink();
        link.setId(id);
        link.setVersion(version);
        if (withType) {
            try {
                link.setArgumentLinkType(factory.getArgumentLinkType(EArgumentLinkShade.value(Integer.parseInt(linkshade))));
            } catch (NumberFormatException e) {
                logger.error("unknown linkshade " + linkshade);
                throw new PersistenceException(PersistenceException.Key.SAVE_JUSTIFICATION_LINK, e);
            }
        }
        link.setOrigin(factory.retrieveContextualized(originId));
        link.setDestination(factory.retrieveContextualized(destinationId));
        link.addInGroup(inGroup);
        return link;
    }
}
