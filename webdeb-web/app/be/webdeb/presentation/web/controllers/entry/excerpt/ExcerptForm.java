/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.excerpt;

import be.webdeb.core.api.actor.ExternalAuthor;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.core.api.excerpt.ExternalExcerpt;
import be.webdeb.core.api.contribution.*;
import be.webdeb.core.api.contributor.Group;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.presentation.web.controllers.entry.EFilterName;
import be.webdeb.presentation.web.controllers.entry.PlaceForm;
import be.webdeb.presentation.web.controllers.entry.actor.ActorSimpleForm;
import be.webdeb.presentation.web.controllers.entry.folder.SimpleFolderForm;
import play.data.validation.ValidationError;
import play.i18n.Lang;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Wrapper class for forms used to encode a new excerpt into the database.
 *
 * Note that all supertype getters corresponding to predefined values (ie, types) are sending
 * ids instead of language-dependent descriptions.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public class ExcerptForm extends ExcerptHolder {

    private Long externalExcerptId = -1L;

    /**
     * Play / JSON compliant constructor
     */
    public ExcerptForm() {
        super();
    }

    /**
     * Constructor. Create a new form object for given external excerpt.
     *
     * @param excerpt an ExternalExcerpt
     * @param textId the text to which this excerpt will belong to
     */
    public ExcerptForm(ExternalExcerpt excerpt, Long textId) {
        super();
        if(excerpt != null) {
            originalExcerpt = excerpt.getOriginalExcerpt();
            workingExcerpt = originalExcerpt;
            techWorkingExcerpt = originalExcerpt;
            externalExcerptId  = excerpt.getId();
            this.textId = textId;
            excLang = excerpt.getLanguage().getCode();
            for(ExternalAuthor a : excerpt.getAuthors()){
                authors.add(new ActorSimpleForm(a.getId(), a.getName()));
            }
        }
    }

    /**
     * Create a new excerpt wrapper for a given text id
     *
     * @param textId the text to which this excerpt will belong to
     */
    public ExcerptForm(Long textId) {
        super();
        this.textId = textId;
    }

    /**
     * Create a new excerpt wrapper for a given original excerpt from a given text (excerpt is empty)
     *
     * @param textId the text to which this excerpt will belong to
     * @param excLang the language of the original excerpt
     * @param lang the user language
     * @param originalExcerpt the selected original excerpt from the given text
     */
    public ExcerptForm(Long textId, String excLang, String lang, String originalExcerpt) {
        super();
        this.lang = lang;
        this.textId = textId;
        this.originalExcerpt = originalExcerpt;
        this.workingExcerpt = originalExcerpt;
        this.techWorkingExcerpt = originalExcerpt;
        this.excLang = excLang;
    }

    /**
     * Create an excerpt form from a given excerpt
     *
     * @param excerpt an excerpt
     * @param lang 2-char ISO code of context language (among play accepted languages)
     */
    public ExcerptForm(Excerpt excerpt, String lang) {
        // must be set manually, may not call super(argument, lang) constructor because their handling is too different
        id = excerpt.getId();
        type = EContributionType.ARGUMENT;
        addFilterable(EFilterName.CTYPE, i18n.get(Lang.forCode(lang), "general.filter.ctype."+ type.id()));
        version = excerpt.getVersion();
        validated = excerpt.getValidated().getEState();
        groups = excerpt.getInGroups().stream().map(Group::getGroupId).collect(Collectors.toList());

        this.excerpt= excerpt;
        this.excLang = excerpt.getText().getLanguage().getCode();
        this.lang = lang;

        textId = excerpt.getTextId();

        originalExcerpt = excerpt.getOriginalExcerpt();
        workingExcerpt = excerpt.getWorkingExcerpt(lang, false);
        techWorkingExcerpt = excerpt.getTechWorkingExcerpt();

        initActors(excerpt.getActors(), lang);
        initPlaces(excerpt.getPlaces(), lang);
        initFolders(excerpt.getFoldersAsList(), lang, true);
    }

    /**
     * Form validation (implicit call from form submit)
     *
     * @return a map of ValidationError if any error in form was found, null otherwise
     */
    public Map<String, List<ValidationError>> validate() {
        Map<String, List<ValidationError>> errors = new HashMap<>();
        Map<String, List<ValidationError>> subErrors = new HashMap<>();
        List<ValidationError> list;

        if (authors == null || authors.stream().allMatch(ActorSimpleForm::isEmpty)) {
            errors.put("authors[0].fullname", Collections.singletonList(new ValidationError("authors[0].fullname", "argument.error.noauthor")));
        } else {
            authors.stream().filter(a -> values.isBlank(a.getLang())).forEach(a -> a.setLang(lang));
            list = helper.checkActors(authors, "authors");
            if (list != null) {
                list.forEach(e -> errors.put(e.key(), Collections.singletonList(new ValidationError(e.key(), e.message()))));
            }
        }

        // if we have reporter object, must have a name
        if (reporters != null) {
            reporters.stream().filter(a -> values.isBlank(a.getLang())).forEach(a -> a.setLang(lang));
            list = helper.checkActors(reporters, "reporters");
            if (list != null) {
                list.forEach(e -> errors.put(e.key(), Collections.singletonList(new ValidationError(e.key(), e.message()))));
            }
        }

        // reporters and authors may not be the same
        if (authors != null && reporters != null) {
            reporters.stream().filter(r -> !r.isEmpty()).forEach(r -> {
                if (authors.stream().anyMatch(a -> r.getFullname().equals(a.getFullname()))) {
                    int index = reporters.indexOf(r);
                    errors.put("reporters[" + index + "].fullname", Collections.singletonList(
                            new ValidationError("reporters[" + index + "].fullname", "excerpt.error.authorisreporter")));
                }
            });
        }

        if (values.isBlank(originalExcerpt)) {
            errors.put("originalExcerpt", Collections.singletonList(new ValidationError("originalExcerpt", "excerpt.error.original.blank")));
        }else if(originalExcerpt.length() > ORIGINAL_EXCERPT_MAX_LENGTH){
            errors.put("originalExcerpt", Collections.singletonList(new ValidationError("originalExcerpt", "excerpt.error.original.length")));
        }

        if(techWorkingExcerpt.length() > WORKING_EXCERPT_MAX_LENGTH){
            errors.put("techWorkingExcerpt", Collections.singletonList(new ValidationError("techWorkingExcerpt", "excerpt.error.working.length")));
            //errors.put("workingExcerpt", Collections.singletonList(new ValidationError("workingExcerpt", "excerpt.error.working.length")));
        }

        // Should not append intentionally
        /*if(!values.correspondingExcerptAndTechExcerpt(originalExcerpt, techWorkingExcerpt)){
            errors.put("workingExcerpt", Collections.singletonList(new ValidationError("workingExcerpt", "excerpt.error.working.notcorresponding")));
        }*/

        if (folders == null || folders.isEmpty() || values.isBlank(folders.get(0).getName())) {
            String fieldName = "folders[0].name";
            errors.put(fieldName, Collections.singletonList(new ValidationError(fieldName, "text.error.folder.name")));
        }else{
            subErrors = checkFolders(folders, "folders");
            if(subErrors != null) errors.putAll(subErrors);
        }


        // check if there is not the same place
        if(places != null){
            subErrors = checkPlaces(places);
            if(subErrors != null) errors.putAll(subErrors);
        }

        // if we have reporter object, must have a name
        if (citedactors != null) {
            citedactors.stream().filter(a -> values.isBlank(a.getLang())).forEach(a -> a.setLang(lang));
            list = helper.checkActors(citedactors, "cited");
            if (list != null) {
                list.forEach(e -> errors.put(e.key(), Collections.singletonList(new ValidationError(e.key(), e.message()))));
            }

            citedactors.stream().filter(r -> !r.isEmpty()).forEach(r -> {
                if (authors.stream().anyMatch(a -> r.getFullname().equals(a.getFullname()))
                        || (reporters != null && reporters.stream().anyMatch(a -> r.getFullname().equals(a.getFullname())))) {
                    int index = citedactors.indexOf(r);
                    errors.put("citedactors[" + index + "].fullname", Collections.singletonList(
                            new ValidationError("citedactors[" + index + "].fullname", "excerpt.error.authoriscited")));
                }
            });
        }

        // must return null if errors is empty
        return errors.isEmpty() ? null : errors;
    }

    /**
     * Save an excerpt into the database. This id is updated if it was not set before.
     *
     * @param contributor the contributor id that ask to save this contribution
     * @return the map of Contribution type and a list of contribution (actors or folders) that have been created during
     * this insertion(for all unknown contributions), an empty list if none had been created
     *
     * @throws FormatException if this contribution has invalid field values (should be pre-checked before-hand)
     * @throws PermissionException if given contributor may not perform this action or if such action would cause
     * integrity problems
     * @throws PersistenceException if any error occurred at the persistence layer (concrete error is wrapped)
     */
    public Map<Integer, List<Contribution>> save(Long contributor) throws FormatException, PermissionException, PersistenceException {
        logger.debug("try to save excerpt " + id + " " + toString() + " with version " + version + " in group " + inGroup);

        Excerpt excerpt = excerptFactory.getExcerpt();
        excerpt.setVersion(version);
        excerpt.addInGroup(inGroup);

        excerpt.setTextId(textId);
        excerpt.setOriginalExcerpt(originalExcerpt.trim());
        excerpt.setTechWorkingExcerpt(techWorkingExcerpt.trim());
        excerpt.setExternalExcerptId(externalExcerptId);

        // folders
        excerpt.initFolders();
        for (SimpleFolderForm f : folders.stream().filter(s -> !s.isEmpty()).collect(Collectors.toList()))  {
            try {
                excerpt.addFolder(helper.toFolder(f, lang));
            } catch (FormatException e) {
                logger.error("unparsable folder " + f, e);
            }
        }

        // bound places
        excerpt.initPlaces();
        for (PlaceForm place : places) {
            if(!place.isEmpty()) {
                Place placeToAdd = createPlaceFromForm(place);
                if(placeToAdd != null)
                    excerpt.addPlace(placeToAdd);
            }
        }

        // bound actors
        try{
            excerpt.getActors();
            boundActors(excerpt, authors, true, false);
            boundActors(excerpt, reporters, false, true);
            boundActors(excerpt, citedactors, false, false);
        }catch (FormatException e) {
            logger.debug("error while adding list of authors", e);
            throw new PersistenceException(PersistenceException.Key.SAVE_TEXT, e);
        }


        // handle possible duplicates in unknown affiliation actors
        helper.removeDuplicateUnknownActors(excerpt, lang);

        excerpt.setId(id != null ? id : -1L);
        // save this excerpt
        Map<Integer, List<Contribution>> result = excerpt.save(contributor, inGroup);
        // update this id (in case of creation
        id = excerpt.getId();
        return result;
    }

    private void boundActors(Excerpt excerpt, List<ActorSimpleForm> actors, boolean isAuthor, boolean isReporter) throws FormatException{
        for (ActorSimpleForm a : actors.stream().filter(n -> !n.isEmpty()).collect(Collectors.toList())) {
            if(isAuthor) a.setAuthor(true);
            if(isReporter) a.setReporter(true);
            if(!isAuthor && !isReporter) a.setCited(true);
            excerpt.addActor(helper.toActorRole(a, excerpt, lang));
        }
    }

    /*
     * GETTERS / SETTERS
     */

    /*
     * supertype fields
     */

    /**
     * Get the external excerpt id if this excerpt come from an external excerpt
     *
     * return an external excerpt id
     */
    public Long getExternalExcerptId() {
        return externalExcerptId;
    }

    /**
     * Set the external excerpt id if this excerpt come from an external excerpt
     *
     * @param externalExcerptId an external excerpt id
     */
    public void setExternalExcerptId(Long externalExcerptId) {
        this.externalExcerptId = externalExcerptId;
    }

    /**
     * Set the text id from which this excerpt originates from
     *
     * @param textId a text id
     */
    public void setTextId(Long textId) {
        this.textId = textId;
    }

    /**
     * Set the original excerpt of this excerpt
     *
     * @param originalExcerpt a text excerpt
     */
    public void setOriginalExcerpt(String originalExcerpt) {
        this.originalExcerpt = originalExcerpt;
    }

    /**
     * Set the working excerpt of this excerpt
     *
     * @param techWorkingExcerpt a reformulated text excerpt
     */
    public void setTechWorkingExcerpt(String techWorkingExcerpt) {
        this.techWorkingExcerpt = techWorkingExcerpt;
    }

    /**
     * Set the list of folders linked to this texts
     *
     * @param folders a list of folders
     */
    public void setFolders(List<SimpleFolderForm> folders) {
        this.folders = folders;
    }

    /**
     * Set the places concerned by this contextualized argument
     *
     * @param places a possibly empty list of places
     */
    public void setPlaces(List<PlaceForm> places) {
        this.places = places;
    }

    /**
     * Set the list of authors of this excerpt
     *
     * @param authors a list of authors
     */
    public void setAuthors(List<ActorSimpleForm> authors) {
        this.authors = authors;
    }

    /**
     * Get the list of actors being the thinker of this text
     *
     * @return the list of actors
     */
    @Override
    public List<ActorSimpleForm> getReporters() {
        return reporters;
    }

    /**
     * Set the list of thinker of this excerpt
     *
     * @param reporters a list of reporters
     */
    public void setReporters(List<ActorSimpleForm> reporters) {
        this.reporters = reporters;
    }

    /**
     * Set the list of cited actors of this excerpt
     *
     * @param cited a list of actors being cited in this excerpt
     */
    public void setCitedactors(List<ActorSimpleForm> cited) {
        this.citedactors = cited;
    }

    /**
     * Set the language of this excerpt (by default, same as owning text language)
     *
     * @param excLang a two char ISO code of the excerpt's language
     */
    public void setExcLang(String excLang) {
        this.excLang = excLang;
    }

}
