/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.validate;

import be.objectify.deadbolt.java.actions.Restrict;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.contribution.EExternalSource;
import be.webdeb.core.api.excerpt.ExcerptFactory;
import be.webdeb.core.api.excerpt.ExternalExcerpt;
import be.webdeb.core.api.text.ETextVisibility;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.infra.ws.nlp.RequestProxy;
import be.webdeb.presentation.web.controllers.CommonController;
import be.webdeb.presentation.web.controllers.entry.*;
import be.webdeb.presentation.web.controllers.entry.actor.ActorHolder;
import be.webdeb.presentation.web.controllers.entry.excerpt.ExcerptForm;
import be.webdeb.presentation.web.controllers.entry.text.TextForm;
import be.webdeb.presentation.web.controllers.permission.WebdebRole;
import be.webdeb.presentation.web.controllers.SessionHelper;
import be.webdeb.presentation.web.controllers.permission.WebdebUser;
import be.webdeb.presentation.web.views.html.util.handleNameMatches;
import be.webdeb.presentation.web.views.html.util.message;
import be.webdeb.presentation.web.views.html.validate.validateExcerpts;
import be.webdeb.presentation.web.views.html.entry.excerpt.editExcerptFields;
import play.data.Form;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

/**
 * This controller class handles validation-related actions, ie, users may validate things that have been
 * automatically discovered/analyzed/retrieved by WDTAL services
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public class ValidateActions extends CommonController {

  @Inject
  private RequestProxy proxy;

  private static final String INVISIBLE_TEXT_TITLE = "";
  private static final String OPINION_TEXT_TYPE = "5";

  /**
   * Display the validate excerpts form (for users to validate discovered excerpts from WDTAL-twitter service)
   *
   * @param maxResults the maximum of excerpts to get
   * @return the excerpts validation page
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> excerpts(int maxResults) {
    logger.debug("GET validate excerpts page");
    String lang = ctx().lang().code();

    List<ContributionHolder> holders = excerptFactory.getExternalExcerptsByExternalSource(EExternalSource.TWITTER.id(), maxResults).parallelStream().map(e ->
        new ExcerptForm(e, e.getTextId())).collect(Collectors.toList());

    return CompletableFuture.supplyAsync(() ->
        ok(validateExcerpts.render(holders.stream().map(h ->
                formFactory.form(ExcerptForm.class).fill((ExcerptForm) h)).collect(Collectors.toList()),
            helper,
            helper.buildFilters(holders, ctx().lang().code()),
            sessionHelper.getUser(ctx()))),
        context.current());
  }

  /**
   * Ajax call to get html template to edit an external excerpt (to manually validate all fields before saving it into DB)
   *
   * @param tempId the id of the external excerpt to be edited
   * @return the editExcerptContent page with given external excerpt filled in or a message template with the error
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> editExternalExcerpt(Long tempId) {
    logger.debug("GET edit partial page for external excerpt " + tempId);

    // retrieve excerpt and sends error message if needed
    ExternalExcerpt excerpt = excerptFactory.retrieveExternal(tempId);
    if (excerpt == null) {
      logger.debug("cannot find external excerpt " + tempId);
      Map<String, String> messages = new HashMap<>();
      messages.put(SessionHelper.ERROR, i18n.get(ctx().lang(), "excerpt.not.found"));
      return CompletableFuture.completedFuture(badRequest(message.render(messages)));
    }

    // create excerpt form, set group as default one and sends template back
    ExcerptForm excForm = new ExcerptForm(excerpt, -1L);
    excForm.setInGroup(contributorFactory.getDefaultGroup().getGroup().getGroupId());
    Form<ExcerptForm> form = formFactory.form(ExcerptForm.class).fill(excForm);
    return CompletableFuture.supplyAsync(() ->
        ok(editExcerptFields.render(form, -1L, excerpt.getId(), helper, sessionHelper.getUser(ctx()), null)), context.current());
  }

  /**
   * Set the rejected state of given external contribution (identified by given id) to given state
   *
   * @param id an external contribution id
   * @param rejected true if this external contribution must be rejected, false if this external contribution must be re-shown later
   * @return a message template with the result of this action
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> setExternalContributionState(Long id, boolean rejected) {
    logger.debug("GET external contribution with id " + id + " will be " + (rejected ? " rejected " : " re-proposed"));
    Map<String, String> messages = new HashMap<>();
    try {
      excerptFactory.updateDiscoveredExternalContributionState(id, rejected);
    } catch (PersistenceException e) {
      logger.error("unable to change deleted state of " + id + " with rejected status " + rejected, e);
      messages.put(SessionHelper.ERROR, i18n.get(ctx().lang(), "validate.error"));
      return CompletableFuture.supplyAsync(() -> badRequest(message.render(messages)), context.current());
    }
    // all good
    messages.put(SessionHelper.SUCCESS, i18n.get(ctx().lang(), "validate.success"));
    return CompletableFuture.supplyAsync(() -> ok(message.render(messages)), context.current());
  }

  /**
   * Send to WDTAL that the Tweet has been rejected
   *
   * @param id the WDTAL Tweet ID
   * @return a message template with the result of this action
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> rejectTweet(Long id) {
    Map<String, String> messages = new HashMap<>();
    proxy.rejectTweet(id.toString());
    messages.put("Success", "OK");
    return CompletableFuture.supplyAsync(() -> ok(message.render(messages)), context.current());
  }

  /**
   * Submit validation of a discovered excerpt to be added into the database.
   *
   * @param excId current external excerpt id
   * @return a message template with the result of the save action
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> submitExternalExcerpt(Long excId) {
    logger.debug("POST save external excerpt from validation");
    Map<String, String> messages = new HashMap<>();

    Form<ExcerptForm> form = formFactory.form(ExcerptForm.class).bindFromRequest();
    Http.Context ctx = ctx();

    // check errors, sends back whole form if any
    if (form.hasErrors()) {
      logger.debug("form has errors " + form.errors().toString());
      logger.debug(form.data().toString());
      messages.put(SessionHelper.WARNING, i18n.get(ctx.lang(),SessionHelper.ERROR_FORM));
      return CompletableFuture.supplyAsync(() ->
              badRequest(editExcerptFields.render(form, -1L, excId, helper, sessionHelper.getUser(ctx), messages)),
          context.current());
    }

    // check possible matches in involved actors and return handleNameMatches modal frame if needed
    ExcerptForm excerpt = form.get();
    NameMatch<ActorHolder> nameMatch = helper.searchForNameMatches(excerpt.getActors(), ctx().lang().code());
    if (!nameMatch.isEmpty()) {
      // check if we have a possible match on actors or affiliations (ie, which list is non-empty)
      boolean isActor = excerpt.getActors().stream().anyMatch(a -> !a.getNameMatches().isEmpty());
      return CompletableFuture.supplyAsync(() ->
          status(409, handleNameMatches.render(nameMatch.getNameMatches(), isActor, nameMatch.getIndex())),
          context.current());
    }

    // check external excerpt
   ExternalExcerpt externalExcerpt = excerptFactory.retrieveExternal(excerpt.getId());
    if (externalExcerpt == null) {
      logger.error("unable to find external excerpt " + excId);
      messages.put(SessionHelper.ERROR, i18n.get(ctx.lang(), "persistence.not.found.object"));
      return CompletableFuture.supplyAsync(() -> internalServerError(message.render(messages)), context.current());
    }

    List<Integer> newProfessions = helper.checkIfNewProfessionsMustBeCreated(
        helper.convertActorSimpleFormToProfessionForm(excerpt.getActors(), ctx().lang().code()));
    newProfessions.forEach(id -> sessionHelper.addValue(ctx(), SessionHelper.KEY_NEWPROFESSION, id+""));

    // try to save in DB
    Map<Integer, List<Contribution>> toBeProposed;
    try {
      WebdebUser user = sessionHelper.getUser(ctx);
      // if no text linked, must first create a default (invisible) one
      if (excerpt.getTextId() == -1L) {
        TextForm text = new TextForm();
        text.setInGroup(excerpt.getInGroup());
        text.setLanguage(excerpt.getExcLang());
        text.setTitle(INVISIBLE_TEXT_TITLE);
        text.setTextVisibility(String.valueOf(ETextVisibility.PRIVATE.id()));
        text.setTextType(OPINION_TEXT_TYPE);
        text.setIsHidden(true);
        text.setAuthors(excerpt.getAuthors());

        // uneditable fields that must be retrieved from external excerpt
        text.setSourceTitle(excerpt.getSource());
        text.setPublicationDate(excerpt.getPublicationDate());

        // save this new (empty) text
        logger.debug("will create invisible text for " + excerpt.getOriginalExcerpt());
        text.save(user.getId());

        // set linked text id
        excerpt.setTextId(text.getId());
      }

      // must clear this excerpt id since it is a temporary one
      // otherwise, DB-layer will try to find it and crash
      excerpt.setId(-1L);
      toBeProposed = excerpt.save(user.getId());

      // mark as "rejected" this tweet
      excerptFactory.updateDiscoveredExternalContributionState(excId, true);

      // call validation service
      proxy.validateTweet(String.valueOf(externalExcerpt.getId()), String.valueOf(excerpt.getId()), excerpt.getOriginalExcerpt())
          .thenAcceptAsync(r -> logger.info("response from wdtal validate tweet : " + r))
          .exceptionally(t -> {
            logger.debug("unable to get response from wdtal validate tweet, reason: " + t.getMessage());
            return null;
          });

    } catch (FormatException | PersistenceException | PermissionException e) {
      logger.error("unable to save excerpt", e);
      messages.put(SessionHelper.ERROR, i18n.get(ctx.lang(), e.getMessage()));
      return CompletableFuture.supplyAsync(() -> internalServerError(message.render(messages)), context.current());
    }

    // add to session the auto-created actors
    if(toBeProposed.containsKey(EContributionType.ACTOR.id()))
      toBeProposed.get(EContributionType.ACTOR.id())
          .forEach(a -> sessionHelper.addValue(ctx(), SessionHelper.KEY_NEWACTOR, a.getId().toString()));
    if(toBeProposed.containsKey(EContributionType.FOLDER.id()))
      toBeProposed.get(EContributionType.FOLDER.id())
          .forEach(a -> sessionHelper.addValue(ctx(), SessionHelper.KEY_NEWFOLDER, a.getId().toString()));

    // all good, return standard form to be displayed
    return CompletableFuture.supplyAsync(() -> ok(excerpt.getOriginalExcerpt()), context.current());
  }

}
