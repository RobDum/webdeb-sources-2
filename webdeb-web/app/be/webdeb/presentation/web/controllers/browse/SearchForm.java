/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.browse;

import be.webdeb.application.query.EQueryKey;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.presentation.web.controllers.entry.ContributionHolder;
import be.webdeb.presentation.web.controllers.SessionHelper;
import be.webdeb.util.ValuesHelper;
import play.api.Play;

import javax.inject.Inject;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Query form (browse page)
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public class SearchForm {

  // custom logger
  protected static final org.slf4j.Logger logger = play.Logger.underlying();

  @Inject
  private ValuesHelper values = Play.current().injector().instanceOf(ValuesHelper.class);

  // the query string
  private Long idContribution = -1L;
  private Long ignore = -1L;
  private Long contextToIgnore = -1L;
  private Long textToLook = -1L;
  private String query = "";
  private String queryString = "";
  private int inGroup = -1;
  private int amongGroup = -1;

  // type of contributions
  private boolean isArgument = false;
  private boolean isArgumentContext = false;
  private boolean isDebate = false;
  private boolean isExcerpt = false;
  private boolean isText = false;
  private boolean isActor = false;
  private boolean isFolder = false;

  // boolean saying if the search must be restrictive, ie, if multiple token are passed, they may not be decoupled
  private boolean isStrict = false;

  protected Collection<ContributionHolder> result = new ArrayList<>();

  /**
   * Default constructor.
   */
  public SearchForm() {
    // do nothing (default constructor is needed for proper json handling by play)
  }

  /**
   * Constructor from a contribution type
   *
   * @param contributionType a contribution type
   * @see EContributionType
   */
  public SearchForm(EContributionType contributionType) {
    switch(contributionType){
      case ALL:
        isArgument = true;
        isArgumentContext = true;
        isDebate = true;
        isExcerpt = true;
        isText = true;
        isActor = true;
        isFolder = true;
        break;
      case TEXT:
        isText = true;
        break;
      case ACTOR:
        isActor = true;
        break;
      case ARGUMENT:
        isArgument = true;
        break;
      case ARGUMENT_CONTEXTUALIZED:
        isArgumentContext = true;
        break;
      case EXCERPT:
        isExcerpt = true;
        break;
      case DEBATE:
        isDebate = true;
        break;
      case FOLDER:
        isFolder = true;
        break;
    }
  }

  /**
   * Constructor. Build a query object from given query string
   *
   * @param query a query string (as valid for the Executor)
   * @param inGroup the current group, if not passed in query string
   */
  public SearchForm(String query, int inGroup) {
    for (String criterion : query.split("\\+")) {
      if (!criterion.contains("=") || criterion.split("=").length < SessionHelper.MIN_QUERY_SIZE) {
        // don't handle this criterion since it is badly formed
        continue;
      }

      String[] pair = criterion.split("=");
      switch (EQueryKey.value(pair[0])) {
        case CONTRIBUTOR:
          // shorthand to get all contributions of a contributor
          this.query = ""; // do not show anything in query
          this.isArgumentContext = true;
          this.isExcerpt = true;
          this.isText = true;
          this.isActor = true;
          this.isFolder = true;
          break;
        case GROUP:
          // shorthand to get all contributions of a group
          if (values.isNumeric(pair[1])) {
            this.inGroup = Integer.parseInt(pair[1]);
          }
          break;
        case AMONG_GROUP:
          // shorthand to get all contributions of a group
          if (values.isNumeric(pair[1])) {
            this.amongGroup = Integer.parseInt(pair[1]);
          }
          break;
        case QUERY:
          // shorthand to make simple queries
          this.query = pair[1];
          this.isArgument = true;
          this.isText = true;
          this.isActor = true;
          this.isFolder = true;
          this.isArgumentContext = true;
          this.isExcerpt = true;
          this.isDebate = true;
          break;
        case CONTRIBUTION_TYPE:
          if (values.isNumeric(pair[1])) {
            switch (EContributionType.value(Integer.parseInt(pair[1]))) {
              case ACTOR:
                isActor = true;
                break;
              case TEXT:
                isText = true;
                break;
              case ARGUMENT:
                isArgument = true;
                break;
              case FOLDER:
                isFolder = true;
                break;
              case ARGUMENT_CONTEXTUALIZED:
                isArgumentContext = true;
                break;
              case DEBATE:
                isDebate = true;
                break;
              case EXCERPT:
                isExcerpt = true;
                break;
              default:
                logger.warn("wrong contribution type id passed " + pair[1]);
                break;
            }
          } else {
            logger.warn("wrong data type for contribution type passed " + pair[1]);
          }
          break;
        case ALL:
          this.isText = true;
          this.isActor = true;
          this.isFolder = true;
          this.isArgumentContext = true;
          this.isExcerpt = true;
          this.isDebate = true;
          break;
        case ID_CONTRIBUTION:
          // shorthand to make simple queries
          try {
            this.idContribution = Long.parseLong(pair[1], 10);
          }catch(NumberFormatException e){
            logger.debug(pair[1] + " is not a number");
          }
          break;
          // arguments
        case ARGUMENT_TITLE:
          isArgument = true;
          this.query = pair[1];
          break;
        // arguments
          case ARGUMENT_CONTEXT_TITLE:
          isArgumentContext = true;
          this.query = pair[1];
          break;
        // excerpts
        case EXCERPT_TITLE:
          isExcerpt = true;
          this.query = pair[1];
          break;
          // texts
        case TEXT_TITLE:
          isText = true;
          this.query = pair[1];
          break;
        // folders
        case FOLDER_NAME:
          isFolder = true;
          this.query = pair[1];
          break;
          // texts and arguments

        case ACTOR:
        case AUTHOR:
        case REPORTER:
        case SOURCE_AUTHOR:
          this.query = pair[1];
          break;

          // actors
        case ACTOR_NAME:
          isActor = true;
          this.query = pair[1];
          break;
        case STRICT:
          isStrict = true;
          break;
        default:
          logger.warn("unknown key given " + pair[0]);
      }
    }

    // finalize query string
    queryString = query;
    if (this.inGroup == -1) {
      this.inGroup = inGroup;
    }
  }

  /*
   * GETTERS / SETTERS
   */

  public void setAllContributionsFlags(boolean isContribution) {
    isArgument = isActor = isText = isContribution;
  }

  /**
   * Get the contribution id to browse first
   *
   * @return a contribution id
   */
  public Long getIdContribution() {
    return idContribution;
  }

  /**
   * Set the search query as entered by user
   *
   * @param idContribution a contribution id
   */
  public void setIdContribution(Long idContribution) {
    this.idContribution = idContribution;
  }

  /**
   * Get the contribution id to ignore if any
   *
   * @return a contribution id to ignore
   */
  public Long getIgnore() {
    return ignore;
  }

  /**
   * Set the contribution id to ignore
   *
   * @param ignore a contribution id to ignore
   */
  public void setIgnore(Long ignore) {
    this.ignore = ignore;
  }

  /**
   * Get the context contribution id to ignore if any
   *
   * @return a context contribution id to ignore
   */
  public Long getContextToIgnore() {
    return contextToIgnore ;
  }

  /**
   * Set the context contribution id to ignore
   *
   * @param ignore a context contribution id to ignore
   */
  public void setContextToIgnore(Long ignore) {
    this.contextToIgnore = ignore;
  }

  /**
   * Get the text id where to look, if any
   *
   * @return a text id where to look
   */
  public Long getTextToLook() {
    return textToLook;
  }

  /**
   * Set the text id where to look
   *
   * @param toLook a text id where to look
   */
  public void setTextToLook(Long toLook) {
    this.textToLook = toLook;
  }

  /**
   * Get the search query only (what user entered in input field)
   *
   * @return a query
   */
  public String getQuery() {
    return query;
  }

  /**
   * Set the search query as entered by user
   *
   * @param query a query
   */
  public void setQuery(String query) {
    this.query = query;
  }

  /**
   * Get the full string query of "key=value" pairs separated by "+" signs, built from user query and filters
   *
   * All keys should comply to EQueryKey values
   *
   * @return a query
   */
  public String getQueryString() {
    return queryString;
  }

  /**
   * Get the search query only (what user entered in input field) and the group where perform the search
   *
   * @return a query
   */
  public String getCompleteQueryString() {
    return queryString + (amongGroup != -1 ? "+" + EQueryKey.AMONG_GROUP.id() + "=" + amongGroup :
            (inGroup != -1 ? "+" + EQueryKey.GROUP.id() + "=" + inGroup : ""));
  }

  /**
   * Set the full string query of "key=value" pairs separated by "+" signs, built from query and filters
   *
   * All keys should comply to EQueryKey values.
   *
   * @param queryString a query
   */
  public void setQueryString(String queryString) {
    this.queryString = queryString;
  }

  /**
   * Get group id, if any (aka, current scope)
   *
   * @return a group id
   */
  public int getInGroup() {
    return inGroup;
  }

  /**
   * Set group id (of current scope)
   *
   * @param inGroup a group id
   */
  public void setInGroup(int inGroup) {
    this.inGroup = inGroup;
  }

  /**
   * Get group id to browse among it and public groups, if any
   *
   * @return a group id or null
   */
  public int getAmongGroup() {
    return amongGroup;
  }

  /**
   * Set group id to browse among it and public groups (of current scope)
   *
   * @param group a group id
   */
  public void setAmongGroup(int group) {
    this.amongGroup = group;
  }

  /**
   * Get the "search for argument" flag
   *
   * @return true if the argument filter is "on"
   */
  public boolean getIsArgument() {
    return isArgument;
  }

  /**
   * Set the "search for argument" flag
   *
   * @param isArgument true if the argument filter is "on"
   */
  public void setIsArgument(boolean isArgument) {
    this.isArgument = isArgument;
  }

  /**
   * Get the "search for text" flag
   *
   * @return true if the text filter is "on"
   */
  public boolean getIsText() {
    return isText;
  }

  /**
   * Set the "search for text" flag
   *
   * @param isText true if the text filter is "on"
   */
  public void setIsText(boolean isText) {
    this.isText = isText;
  }

  /**
   * Get the "search for actor" flag
   *
   * @return true if the actor filter is "on"
   */
  public boolean getIsActor() {
    return isActor;
  }

  /**
   * Set the "search for actor" flag
   *
   * @param isActor true if the actor filter is "on"
   */
  public void setIsActor(boolean isActor) {
    this.isActor = isActor;
  }

  /**
   * Get the "search for folder" flag
   *
   * @return true if the folder filter is "on"
   */
  public boolean getIsFolder() {
    return isFolder;
  }

  /**
   * Set the "search for folder" flag
   *
   * @param isFolder true if the folder filter is "on"
   */
  public void setIsFolder(boolean isFolder) {
    this.isFolder = isFolder;
  }

  /**
   * Get the "search for argument" flag
   *
   * @return true if the argument filter is "on"
   */
  public boolean getIsArgumentContext() {
    return isArgumentContext;
  }

  /**
   * Set the "search for contextualized argument" flag
   *
   * @param isArgumentContext true if the contextualized argument filter is "on"
   */
  public void setIsArgumentContext(boolean isArgumentContext) {
    this.isArgumentContext = isArgumentContext;
  }

  /**
   * Get the "search for excerpt" flag
   *
   * @return true if the excerpt filter is "on"
   */
  public boolean getIsExcerpt() {
    return isExcerpt;
  }

  /**
   * Set the "search for excerpt" flag
   *
   * @param isExcerpt true if the excerpt filter is "on"
   */
  public void setIsExcerpt(boolean isExcerpt) {
    this.isExcerpt = isExcerpt;
  }

  /**
   * Get the "search for debate" flag
   *
   * @return true if the debate filter is "on"
   */
  public boolean getIsDebate() {
    return isDebate;
  }

  /**
   * Set the "search for debate" flag
   *
   * @param isDebate true if the debate filter is "on"
   */
  public void setIsDebate(boolean isDebate) {
    this.isDebate = isDebate;
  }

  /**
   * Check whether a strict search must be performed, ie, all tokens must be treated as is without splitting them
   *
   * @return true if a strict search must be performed
   */
  public boolean getIsStrict() {
    return isStrict;
  }

  /**
   * Set whether a strict search must be performed, ie, all tokens must be treated as is without splitting them
   *
   * @param strict true if a strict search must be performed
   */
  public void setIsStrict(boolean strict) {
    isStrict = strict;
  }

  /**
   * Get the collection of contributions matching the search query and filters
   *
   * @return a collection of contributions
   */
  public Collection<ContributionHolder> getResult() {
    return result;
  }

  /**
   * Set the collection of contributions matching the search query and filters
   *
   * @param result a collection of contributions that matches the query and filters set in this object
   */
  public void setResult(Collection<ContributionHolder> result) {
    this.result = result;
  }

  @Override
  public String toString() {
    return getQuery() + " text:" + isText + " actor:" + isActor + " argument:" + isArgument  + " folder:" + isFolder +
            " contextualized argument:" + isArgumentContext + " debate:" + isDebate + " excerpt:" + isExcerpt +
            " in group: " + inGroup + " strict:" + isStrict;
  }
}
