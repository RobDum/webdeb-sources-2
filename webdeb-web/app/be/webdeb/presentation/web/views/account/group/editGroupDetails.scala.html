@*
* WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
*
* List of the contributors to the development of WebDeb: see AUTHORS file.
* Description and complete License: see LICENSE file.
*
* This program (WebDeb) is free software:
* you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License along with this program (see COPYING file).
* If not, see <http://www.gnu.org/licenses/>.
*@

@*
* Modal frame to create or edit group data
*
* @param group the group form (with pre-loaded data if existing)
* @param map the messages map
*
* @author Fabian Gilson
* @author Martin Rouffiange
*@


@import be.webdeb.core.api.contributor.EContributionVisibility
@import be.webdeb.presentation.web.controllers.entry.ContributionHelper
@import be.webdeb.presentation.web.controllers.account.group.GroupForm
@import be.webdeb.presentation.web.views.html.util.helpButton
@import be.webdeb.presentation.web.views.html.util.{inlineform, bootstrapInput, emptyModalframe, funkyCheckbox, hiddenFormField, message}
@import helper._

@(
    group: Form[GroupForm],
    helper : ContributionHelper,
    map : java.util.Map[String, String]
)

@implicitFieldConstructor = @{ FieldConstructor(bootstrapInput.render) }

<link rel="stylesheet" media="screen" href="@routes.Assets.at("stylesheets/colorpicker.css")">
<script src="@routes.Assets.at("javascripts/colorPicker.js")" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function() {
    addEditGroupListeners();
    initColorPicker($("#colorSelector"), $("#groupColor"), $("#create-group-modal") );
  });

  /**
   * Add listeners to the "edit group details" form
   */
  function addEditGroupListeners() {
    manageExclusiveCheckboxes($('#contribution-viz').find('input[type="checkbox"]'), $('#contributionVisibility'), true);
    manageExclusiveCheckboxes($('#member-viz').find('input[type="checkbox"]'), $('#memberVisibility'), true);
    // register event on add btn
    $('#submit-btn').on('click', function () {
      var timeout = startWaitingModal();
      saveGroup($('#group-form')).done(function (data) {
        stopWaitingModal(timeout);
        $('#manage-groups').empty().append(data);
        addManageGroupListeners();
        hideAndDestroyModal('#create-group-modal');
      }).fail(function (jqXHR) {
        stopWaitingModal(timeout);
        if (jqXHR.status === 400) {
          // we got the form to amend because it contains errors
          replaceContent('#group-form', jqXHR.responseText, 'form');
          addEditGroupListeners();
        } else {
          // internal server error, show error message
          hideAndDestroyModal('#create-group-modal');
          showErrorMessage(jqXHR);
        }
      });
    });
    $("#cancel-btn").on('click', function () {
      hideAndDestroyModal($("#create-group-modal"));
    });
  }
</script>

@emptyModalframe("create-group-modal", true, "default", "", false) {
  <div class="modal-header">
    <h3 class="modal-title small-caps text-muted">
      @if(group("id").value != "-1") {
        @Messages("group.manage.edit.title") <span class="text-primary">@group("name").value</span>
      } else {
        @Messages("group.manage.create.title")
      }
    </h3>
  </div>

  <div class="modal-body row">
    <div id="form-content" class="col-xs-12">
      <div class="row">
        <div class="col-xs-12 col-sm-6 instructions">@Messages("general.required.fields")</div>
        <div class="col-xs-12 col-sm-6">@helpButton("group", false)</div>
        <br>
        <br>
      </div>
      <form id="group-form">
        <fieldset>
          @hiddenFormField(group("id"))
          @hiddenFormField(group("version"))
          @hiddenFormField(group("groupColor"))

          <div class="col-xs-12">
            <div class="col-sm-6 col-xs-12 padding-field">
              @inputText(
                group("name"),
                '_label -> Messages("group.manage.label.name"),
                'class -> "form-control",
                'autocomplete -> "off"
              )
            </div>

            <div class="col-sm-6 col-xs-12 padding-field">
              <div class="funkyradio">
                <label class="control-label"><b>@Messages("group.manage.toplabel.isopen")</b></label>
                @checkbox(
                  group("isOpen"),
                  '_label -> Messages("group.manage.label.isopen")
                )(handler = funkyCheckbox, implicitly[Messages])
              </div>
            </div>

          </div>

          <div class="col-sm-9 col-xs-12">
          @inputText(
            group("description"),
            '_label -> Messages("group.manage.label.description"),
            'class -> "form-control",
            'placeholder -> Messages("group.manage.place.description"),
            'autocomplete -> "off"
          )
          </div>

          <div class="col-sm-3 col-xs-12">
            <div style="font-weight: bold;">@Messages("group.manage.label.groupColor")</div>
            <div id="colorSelector">
              <div style="background-color: #@group("groupColor").value"></div>
            </div>
            @* must handle error message by hand for inlineforms *@
            @group.errors.map { case (key, error) =>
              @if(key.startsWith("groupColor") && error(0).message != ""){
                <div class="label label-danger">@Messages(error(0).message)</div>
              }
            }
          </div>

          <div class="col-xs-12">
            <label class="control-label">@Messages("group.manage.label.contribution.visibility").concat(" * ")</label>
            <div id="contribution-viz" class="funkyradio">
              @for(viz <- EContributionVisibility.values()) {
                <div class="form-group funkyradio-primary col-md-4 col-sm-4 col-xs-12">
                  <input id="contributionVisibility-@viz.id" type="checkbox" value="@viz.id">
                  <label for="contributionVisibility-@viz.id">
                    @Messages("group.contribution.visibility." + viz.id)
                  </label>
                </div>
              }
            </div>
            @inputText(
              group("contributionVisibility"),
              '_label -> null,
              'class -> "form-control hidden",
              'autocomplete -> "off"
            )
          </div>

          <div class="col-xs-12">
            <label class="control-label">@Messages("group.manage.label.member.visibility")</label>
            <div id="member-viz" class="funkyradio">
              @for(viz <- EContributionVisibility.values()) {
                <div class="form-group funkyradio-primary col-sm-4 col-xs-12">
                  <input id="memberVisibility-@viz.id" type="checkbox" value="@viz.id">
                  <label for="memberVisibility-@viz.id">
                    @Messages("group.member.visibility." + viz.id)
                  </label>
                </div>
              }
            </div>
            @inputText(
              group("memberVisibility"),
              '_label -> null,
              'class -> "form-control hidden",
              'autocomplete -> "off"
            )
          </div>

          <div class="col-xs-12">
            <label class="control-label">@Messages("group.manage.label.permissions")</label>
            <div class="col-xs-12 funkyradio">
              @checkbox(
                group("editContribution"),
                '_label -> Messages("group.manage.label.permission.2")
              )(handler = funkyCheckbox, implicitly[Messages])
            </div>
            <div class="col-xs-12 funkyradio">
              @checkbox(
                group("deleteContribution"),
                '_label -> Messages("group.manage.label.permission.3")
              )(handler = funkyCheckbox, implicitly[Messages])
            </div>
            <div class="col-xs-12 funkyradio">
              @checkbox(
                group("mergeContribution"),
                '_label -> Messages("group.manage.label.permission.8")
              )(handler = funkyCheckbox, implicitly[Messages])
            </div>
          </div>
          <div class="col-xs-12">
            <label class="control-label">@Messages("group.manage.label.owner.permission")</label>
            <div class="col-xs-12 funkyradio">
            @checkbox(
              group("editMember"),
              '_label -> Messages("group.manage.label.permission.6")
            )(handler = funkyCheckbox, implicitly[Messages])
            </div>
          </div>

          <div class="col-xs-12">
            <label class="control-label">@Messages("group.manage.label.user.help")</label>
            <div class="col-xs-12 funkyradio">
              @checkbox(
                group("disableAnnotation"),
                '_label -> Messages("group.manage.label.permission.13.bis")
              )(handler = funkyCheckbox, implicitly[Messages])
            </div>
            <div class="col-xs-12 funkyradio">
              @checkbox(
                group("disableClassification1"),
                '_label -> Messages("group.manage.label.permission.14.bis")
              )(handler = funkyCheckbox, implicitly[Messages])
            </div>
          </div>
        </fieldset>
      </form>
    </div>
  </div>

  <div class="modal-footer">
    <div class="col-xs-12">
      <button id="cancel-btn" type="button" class="btn btn-default" data-dismiss="modal">@Messages("general.btn.cancel")</button>
      <button id="submit-btn" type="button" name="submit" class="btn btn-primary" title="@Messages("group.manage.btn.title")">
        @if(group("id").value == "-1") {
          @Messages("group.manage.btn.create")
        } else {
          @Messages("group.manage.btn.edit")
        }
      </button>
    </div>
  </div>

  <div id="inner-msg-div">
    @message(map)
  </div>
}
