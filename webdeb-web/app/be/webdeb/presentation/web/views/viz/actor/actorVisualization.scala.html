@*
* WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
*
* List of the contributors to the development of WebDeb: see AUTHORS file.
* Description and complete License: see LICENSE file.
*
* This program (WebDeb) is free software:
* you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License along with this program (see COPYING file).
* If not, see <http://www.gnu.org/licenses/>.
*@


@*
* Main (tabbed) template to visualize an Actor, containing its cartography (affiliations), radiography (talks),
* sociography (allies/opponents) and details.
*
* @param actor the actor to visualize
* @param filters the list of filters that apply here
* @param tab the EVizPane ito select which pane is displayed
* @param pov the point of view to select, if any in the selected pane
* @param user the user profile data
* @param map the message map
*
* @author Fabian Gilson
* @author Martin Rouffiange
*@


@import be.webdeb.presentation.web.controllers.permission.WebdebUser
@import be.webdeb.presentation.web.controllers.viz.actor.{ActorVizHolder, EActorGroupKey, EActorSortKey}
@import be.webdeb.presentation.web.controllers.viz.EVizPane
@import be.webdeb.presentation.web.views.html.util.helpButton
@import be.webdeb.presentation.web.views.html.main
@import be.webdeb.presentation.web.views.html.viz.contributionAdminActions
@import be.webdeb.presentation.web.views.html.viz.actor.{actorCartography, actorCited, actorDetails, actorRadiography, actorSociography, actorTexts}
@import be.webdeb.presentation.web.views.html.util.{breadcrumb, message, mediaShareBtn}
@import be.webdeb.presentation.web.controllers.entry.EFilterName
@import be.webdeb.presentation.web.controllers.entry.FilterTree
@import be.webdeb.presentation.web.views.html.viz.vizHeader
@import be.webdeb.presentation.web.views.html.viz.metaHolder

@import be.webdeb.core.api.actor.EActorType
@import be.webdeb.presentation.web.controllers.viz.EVizPaneName
@(
    actor : ActorVizHolder,
    filters : java.util.Map[EFilterName, java.util.List[FilterTree]],
    tab: EVizPane,
    pov: Integer,
    user : WebdebUser,
    map: java.util.Map[String, String]
)


@scripts = {
  @actor.getEActortype match {
    case EActorType.ORGANIZATION => {
      <title>@Messages("general.metatitle.org.title") @actor.getFullname ? @Messages("general.metatitle")</title>
      <meta name="description" content="@Messages("general.metatitle.org.desc")">
    }

    case EActorType.PERSON => {
      <title>@actor.getFullname - @Messages("general.metatitle.person.title") @Messages("general.metatitle")</title>
      <meta name="description" content="@Messages("general.metatitle.person.desc", actor.getFullname)">
    }

    case _ => {
      <title>@actor.getFullname @Messages("general.metatitle")</title>
      <meta name="description" content="@Messages("general.more.details.tooltip")">
    }
  }

  <script src="@routes.Assets.at("javascripts/moment.min.js")" type="text/javascript"></script>
  <script src="@routes.Assets.at("javascripts/chart.bundle.min.js")" type="text/javascript"></script>
  <script src="@routes.Assets.at("javascripts/organizationalChart.js")" type="text/javascript"></script>

  <script src="@routes.Assets.at("javascripts/argument-scripts.js")" type="text/javascript"></script>
  <script src="@routes.Assets.at("javascripts/excerpt-scripts.js")" type="text/javascript"></script>
  <script src="@routes.Assets.at("javascripts/actor-scripts.js")" type="text/javascript"></script>
  <script src="@routes.Assets.at("javascripts/actor-affiliations-scripts.js")" type="text/javascript"></script>
  <script src="@routes.Assets.at("javascripts/folder-scripts.js")" type="text/javascript"></script>

  <script type="text/javascript">
    // all filterbars must be saved in a js variable to avoid some problems
    var filterbars = [];
    $(document).ready(function() {
      handleTabs(@tab.id, @pov);

      // manage change tab for filters and content
      var params = {
        carto : {isToLoad : true, filterVisible : false},
        carto2 : {isToLoad : true, filterVisible : false},
        radio : {isToLoad : false},
        socio : {isToLoad : true},
        cited : {isToLoad : false},
        texts : {isToLoad : true}
      };
      manageChangeTab(reloadActorLinkedContributions, getActorLinkedContributions, filterbars, @actor.getId, params, );
      // auto-creation modal if any
      getAutoCreatedModals();

      initActorVisualization();
    });

    /**
     * Manage events form actor visualization
     */
    function initActorVisualization(){
      let tabs = $('#actor-viz');

      $('.nav-tabs a').on('click', function () {
        reflowsCharts(tabs);
      });

      $(document).on('filter-resize', function () {
        reflowsCharts(tabs);
      });
    }

    /**
     * Rebuild affiliation charts when tab change
     */
    function reflowsCharts(tabs){
      let type = null;

      switch (getTabType(tabs)) {
        case "@EVizPaneName.CARTO.id" :
          type = "@EActorType.ORGANIZATION.id";
          break;
        case "@EVizPaneName.CARTO2.id" :
          type = "@EActorType.PERSON.id";
          break;
      }

      if(type != null) {
        setTimeout(function () {
          initAffiliation(type);
        }, 50);
      }
    }
  </script>
}


@main(user, scripts, metaHolder(actor)) {

  <div id="msg-div">
    @message(flash)
    @message(map)
  </div>

  <div class="row" id="actor-viz">
    @vizHeader(actor)

    <!--  viz areas -->
    <div class="col-xs-12 no-padding-xs viz-body">
      <ul id="tabs" class="nav nav-tabs padding-xs" role="tablist"> @* must reverse order *@
        <li role="presentation"><a id="carto_@EVizPane.CARTO.id" href="#carto-tab" aria-controls="cartography" role="tab" data-toggle="tab">@Messages("viz.actor.pill.cartography")</a></li>
        @if(actor.getActortype == EActorType.ORGANIZATION.id() || actor.getActortype == EActorType.PROJECT.id()) {
          <li role="presentation"><a id="carto2_@EVizPane.CARTO2.id" href="#carto2-tab" aria-controls="carto2graphy" role="tab" data-toggle="tab">@Messages("viz.actor.pill.cartography2")</a></li>
        }
        <li role="presentation"><a id="cited_@EVizPane.CITATION.id" href="#cited-tab" aria-controls="cited" role="tab" data-toggle="tab">@Messages("viz.actor.pill.cited")</a></li>
        <li role="presentation"><a id="radio_@EVizPane.RADIO.id" href="#radio-tab" aria-controls="radiography" role="tab" data-toggle="tab">@Messages("viz.actor.pill.radiography")</a></li>
        <li role="presentation"><a id="socio_@EVizPane.SOCIO.id" href="#socio-tab" aria-controls="sociography" role="tab" data-toggle="tab">@Messages("viz.actor.pill.sociography")</a></li>
        <li role="presentation"><a id="details_@EVizPane.TEXTS.id" href="#texts-tab" aria-controls="texts" role="tab" data-toggle="tab">@Messages("viz.actor.pill.texts")</a></li>
        <li role="presentation"><a id="details_@EVizPane.DETAILS.id" href="#details-tab" aria-controls="details" role="tab" data-toggle="tab">@Messages("viz.actor.pill.details")</a></li>
        <li role="presentation" class="fake-tab">@contributionAdminActions(actor, false, user)</li>
        <li role="presentation" class="fake-tab pull-right">
          <div>@helpButton("viz.actor", false)</div>
        </li>

      </ul>
      <div class="tab-content" style="padding-top: 10px">
        <div class="media-share-btn">@mediaShareBtn(actor)</div>

        <div role="tabpanel" class="tab-pane padding-xs carto-@EActorType.ORGANIZATION.id" id="carto-tab">
          <span class="instructions">
            @if(actor.getActortype == EActorType.PERSON.id){
              @Messages("viz.actor.desc.cartography.pers", actor.getFullname)
            }else{
              @Messages("viz.actor.desc.cartography")
            }
          </span>
          <span class="block-xs"></span>
          <span class="text-muted">
              &nbsp;&nbsp;@Messages("viz.actor.carto.pov")&nbsp;
            <span id="point-of-view">
            @for(i <- 0 to 1){
              <button type="button" class="btn btn-pov btn-sm btn-default @(if(0 == i) "btn-success")" value="@i" data-target="cartocontent">
              @Messages("viz.actor.carto.pov." + i)
              </button>
            }
            </span>
          </span>
          @actorCartography("cartocontent")
        </div>

        @if(actor.getActortype == EActorType.ORGANIZATION.id() || actor.getActortype == EActorType.PROJECT.id()) {
          <div role="tabpanel" class="tab-pane padding-xs carto-@EActorType.PERSON.id" id="carto2-tab">
            <span class="instructions">@Messages("viz.actor.desc.cartography2")</span>
            <span class="block-xs"></span>
            <span class="text-muted">
              &nbsp;&nbsp;@Messages("viz.actor.carto.pov")&nbsp;
              <span id="point-of-view" class="block-xs">
              @for(i <- 0 to 1){
                <button type="button" class="btn btn-pov btn-sm btn-default @(if(0 == i) "btn-success")" value="@i" data-target="carto2content">
                @Messages("viz.actor.carto.pov." + i)
                </button>
              }
              </span>
            </span>
            @actorCartography("carto2content")
          </div>
        }

        <div role="tabpanel" class="tab-pane padding-xs" id="cited-tab">
          <p class="instructions">@Messages("viz.actor.desc.cited", actor.getFullname)</p>
          @actorCited(actor)
        </div>

        <div role="tabpanel" class="tab-pane padding-xs" id="radio-tab">
          <p class="instructions">@Messages("viz.actor.desc.radiography", actor.getFullname)</p>
          @actorRadiography(actor)
        </div>

        <div role="tabpanel" class="tab-pane" id="socio-tab">
          <p class="instructions padding-xs">@Messages("viz.actor.desc.sociography")</p>
          @actorSociography(actor, EActorGroupKey.AUTHOR)
        </div>

        <div role="tabpanel" class="tab-pane padding-xs" id="texts-tab">
          <p class="instructions">@Messages("viz.actor.desc.texts")</p>
          @actorTexts()
        </div>

        <div role="tabpanel" class="tab-pane padding-xs" id="details-tab">
          @actorDetails(actor, user)
        </div>
      </div>
    </div>
  </div>
}
