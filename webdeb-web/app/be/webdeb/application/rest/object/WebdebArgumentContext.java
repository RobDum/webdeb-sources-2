/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package be.webdeb.application.rest.object;

import be.webdeb.core.api.argument.ArgumentContext;
import be.webdeb.core.api.argument.ArgumentJustification;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is devoted to exchange contextualized arguments in JSON format.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class WebdebArgumentContext extends WebdebContribution {

    /**
     * unique id of originating contextual contribution
     */
    @JsonSerialize
    private Long contextId;

    /**
     * unique id of originating argument
     */
    @JsonSerialize
    private Long argumentId;

    /**
     * Title of the argument
     */
    @JsonSerialize
    private String argumentTitle;

    /**
     * The shade of the link if contextualized argument come from argumentJustification
     */
    @JsonSerialize
    private Integer linkShade;

    /**
     * list of linked folders
     */
    @JsonSerialize
    private List<WebdebSimpleFolder> folders = new ArrayList<>();

    /**
     * a list of justification of this contextualized argument is involved in
     */
    @JsonSerialize
    private List<WebdebArgumentContext> justifications;

    /**
     * the list of places involved in this folder
     */
    @JsonSerialize
    private List<WebdebPlace> places = new ArrayList<>();

    /**
     * @api {get} WebdebArgument Excerpt
     * @apiName Excerpt
     * @apiGroup Structures
     * @apiDescription An argument is an excerpt of a text of a certain type standardized in a specific form
     * @apiSuccess {Integer} textId the id of the text this argument belongs to
     * @apiSuccess {String} excerpt the excerpt from which this argument has been extracted
     * @apiSuccess {Object} argumentType the argument type for this argument
     * @apiSuccess {Integer} argumentType.argumentType the type id
     * @apiSuccess {Object} argumentType.typeNames type name of the form { "en" : "englishType", "fr" : "TypeFrançais" }
     * @apiSuccess {Integer} argumentType.argumentTiming the timing id
     * @apiSuccess {Object} argumentType.timingNames timing name of the form { "en" : "englishTiming", "fr" :
     * "TimingFrançais" }
     * @apiSuccess {Integer} argumentType.argumentShade the shade id
     * @apiSuccess {Object} argumentType.shadeNames shade name of the form { "en" : "englishShade", "fr" :
     * "NuanceFrançaise" }, names are empty for performative and opinion.
     * @apiSuccess {Boolean} argumentType.singular true if this shade applies to one actor, false otherwise
     * @apiSuccess {String} standardForm standardized form (without actors and shade)
     * @apiSuccess {WebdebSimpleFolder[]} folders list of folders linked with this argument
     * @apiSuccess {Integer[]} argumentLinks the list of ids of linked arguments
     * @apiSuccess {WebdebRole[]} actors a list of actors with their roles involved in this argument
     * @apiSuccess {WebdebPlace[]} places a list of places involved in this argument
     * @apiSuccess {String} language the iso-639-1 code of the language of this argument
     * @apiExample Excerpt (extracted from a text)
     * {
     *   "id": 776,
     *   "type": "argument",
     *   "version": 1464271112000,
     *   "textId": 774,
     *   "excerpt": "the doctor or any other person performing this illegal act upon a woman would be held legally responsible",
     *   "argumentType": {
     *     "argumentType": 1,
     *     "typeNames": { "en": "Prescriptive", "fr": "Prescriptive" },
     *     "argumentShade": 10,
     *     "shadeNames": { "en": "It is necessary to", "fr": "Il faut" },
     *     "argumentTiming": 1,
     *     "timingNames": { "en": "Present", "fr": "Présent" }
     *   },
     *   "argumentPlace": {
     *     { "lang": "fr", "name": "Namur", "subregion": "Province de Namur", "region": "Wallonie", "Country": "Belgique", "continent": "Europe" },
     *   },
     *   "standardForm": "hold legally responsible the doctor or any other person performing an abortion",
     *   "folders": {
     *      "id": 100000,
     *      "folderType": "node",
     *      "name": [ { "name": "actualité", "lang": "fr" } ],
     *   },
     *   "actors": [ @WebdebActorRole ]
     *   "language": "en"
     * }
     * @apiVersion 0.0.3
     */
    public WebdebArgumentContext(ArgumentContext argument) {
        super(argument);
        this.contextId = argument.getContextId();
        this.argumentId = argument.getArgumentId();
        this.argumentTitle = argument.getArgument().getDictionary().getTitle();

        argument.getJustificationLinksThatAreJustified().forEach(j -> this.justifications.add(new WebdebArgumentContext(j)));
        argument.getFolders().forEach(f -> folders.add(new WebdebSimpleFolder(f)));
        argument.getPlaces().forEach(p -> places.add(new WebdebPlace(p, "fr")));

    }

    public WebdebArgumentContext(ArgumentJustification justification) {
        this(justification.getDestination());
        linkShade = justification.getArgumentLinkType().getLinkType();
    }
}
