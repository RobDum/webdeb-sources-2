/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.impl.excerpt;

import be.webdeb.core.api.argument.ArgumentIllustration;
import be.webdeb.core.api.excerpt.ContextElement;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.core.api.excerpt.ExcerptFactory;
import be.webdeb.core.api.excerpt.ExternalExcerpt;
import be.webdeb.core.api.text.TextFactory;
import be.webdeb.core.impl.contribution.AbstractContributionFactory;
import be.webdeb.infra.persistence.accessor.api.ActorAccessor;
import be.webdeb.infra.persistence.accessor.api.ExcerptAccessor;

import javax.inject.Inject;
import java.util.List;

/**
 * This class implements an factory for excerpts.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public class ConcreteExcerptFactory extends AbstractContributionFactory<ExcerptAccessor> implements ExcerptFactory {

    @Inject
    private ActorAccessor actorAccessor;

    @Inject
    private TextFactory textFactory;

    @Override
    public Excerpt retrieve(Long id) {
        return id != null && id != -1L ? accessor.retrieve(id, false) : null;
    }

    @Override
    public ExternalExcerpt retrieveExternal(Long id){
        return id != null && id != -1L ? accessor.retrieveExternal(id) : null;
    }

    @Override
    public ExternalExcerpt retrieveExternal(Long id, int externalSource){
        return id != null && id != -1L ? accessor.retrieveExternal(id, externalSource) : null;
    }

    @Override
    public Excerpt retrieveWithHit(Long id) {
        return id != null && id != -1L ? accessor.retrieve(id, true) : null;
    }

    @Override
    public ContextElement retrieveContextElement(Long id) {
        return id != null && id != -1L ? accessor.retrieveContextElement(id) : null;
    }

    @Override
    public ArgumentIllustration retrieveIllustrationLink(Long argid, Long excid) {
        return accessor.retrieveIllustrationLink(argid, excid);
    }

    @Override
    public Excerpt getExcerpt() {
        return new be.webdeb.core.impl.excerpt.ConcreteExcerpt(this, accessor, textFactory, actorAccessor, contributorFactory);
    }

    @Override
    public ExternalExcerpt getExternalExcerpt() {
        return new ConcreteExternalExcerpt(this, accessor, contributorFactory);
    }

    @Override
    public ContextElement getContextElement() {
        return new ConcreteContextElement();
    }

    @Override
    public ExternalExcerpt findExistingExternalExcerpt(Long contributor, String excerpt, Long textId){
        return accessor.findExistingExternalExcerpt(contributor, excerpt, textId);
    }

    @Override
    public List<ExternalExcerpt>getExternalExcerptsByExternalSource(int externalSource, int maxResults) {
        return accessor.getExternalExcerptsByExternalSource(externalSource, maxResults);
    }
}
