/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.impl.excerpt;

import be.webdeb.core.api.argument.ArgumentIllustration;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.contributor.ContributorFactory;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.core.api.excerpt.ExcerptFactory;
import be.webdeb.core.api.excerpt.ExternalExcerpt;
import be.webdeb.core.api.text.Text;
import be.webdeb.core.api.text.TextFactory;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.core.impl.contribution.AbstractTextualContribution;
import be.webdeb.infra.persistence.accessor.api.ActorAccessor;
import be.webdeb.infra.persistence.accessor.api.ExcerptAccessor;
import be.webdeb.util.ValuesHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class ConcreteExcerpt extends AbstractTextualContribution<ExcerptFactory, ExcerptAccessor> implements Excerpt {


    private String originalExcerpt;
    private String workingExcerpt = null;
    private String workingExcerptToDisplay = null;
    private String techWorkingExcerpt;

    private Long textId;
    private Text text = null;
    private Long externalExcerptId;

    private List<ArgumentIllustration> arguments = null;
    private List<ArgumentIllustration> similars = null;

    private TextFactory textFactory;

    protected static final org.slf4j.Logger logger = play.Logger.underlying();

    /**
     * Create a Text instance
     *
     * @param factory the excerpt factory
     * @param accessor the excerpt accessor
     * @param textFactory the text factory
     * @param actorAccessor an actor accessor (to retrieve/update involved actors)
     * @param contributorFactory the contributor accessor
     */
    ConcreteExcerpt(ExcerptFactory factory, ExcerptAccessor accessor, TextFactory textFactory,
                    ActorAccessor actorAccessor, ContributorFactory contributorFactory) {
        super(factory, accessor, actorAccessor, contributorFactory);
        this.textFactory = textFactory;
        type = EContributionType.EXCERPT;
    }


    @Override
    public String getOriginalExcerpt() {
        return originalExcerpt;
    }

    @Override
    public void setOriginalExcerpt(String originalExcerpt) {
        this.originalExcerpt = originalExcerpt;
    }

    @Override
    public String getWorkingExcerpt(String lang, boolean toDisplay) {
        /*if(toDisplay){
            if(workingExcerptToDisplay == null){
                workingExcerptToDisplay = techToWorking(lang, true);
            }
            return workingExcerptToDisplay;
        }
        if(workingExcerpt == null){
            workingExcerpt = techToWorking(lang, false);
        }
        return workingExcerpt;*/
        return techWorkingExcerpt;
    }

    @Override
    public void setWorkingExcerpt(String workingExcerpt) {
        this.workingExcerpt = workingExcerpt;
    }

    @Override
    public String getTechWorkingExcerpt() {
        return techWorkingExcerpt;
    }

    @Override
    public void setTechWorkingExcerpt(String techWorkingExcerpt) {
        this.techWorkingExcerpt = techWorkingExcerpt;
    }

    @Override
    public Long getTextId() {
        return textId;
    }

    @Override
    public void setTextId(Long textId) {
        this.textId = textId;
    }

    @Override
    public Text getText() {
        if(textId != null){
            if(text == null){
                // lazy loading
                text = textFactory.retrieve(textId);
            }
            return text;
        }
        return null;
    }

    @Override
    public Long getExternalExcerptId() {
        return externalExcerptId;
    }

    @Override
    public void setExternalExcerptId(Long externalExcerptId) {
        this.externalExcerptId = externalExcerptId;
    }

    @Override
    public List<ArgumentIllustration> getArguments() {
        if(arguments == null){
            arguments = accessor.getArguments(id);
        }
        return arguments;
    }

    @Override
    public List<ArgumentIllustration> getSimilars() {
        if(similars == null){
            similars = accessor.similars(id);
        }
        return similars;
    }

    @Override
    public Map<Integer, List<Contribution>> save(Long contributor, int currentGroup) throws FormatException, PermissionException, PersistenceException {
        List<String> isValid = isValid();
        if (!isValid.isEmpty()) {
            logger.error("excerpt contains errors " + isValid.toString());
            throw new FormatException(FormatException.Key.EXCERPT_ERROR, String.join(",", isValid.toString()));
        }
        return accessor.save(this, currentGroup, contributor);
    }

    @Override
    public List<String> isValid() {
        List<String> fieldsInError = new ArrayList<>();

        if (originalExcerpt == null || originalExcerpt.equals("")) {
            fieldsInError.add("excerpt has no original excerpt");
        }

        if(textId == null){
            fieldsInError.add("excerpt has no text id");
        }
        return fieldsInError;
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj instanceof Excerpt && hashCode() == obj.hashCode();
    }

    @Override
    public int hashCode() {
        return 33 * (id == -1L ? 48 : id.hashCode()) + originalExcerpt.hashCode() + textId.hashCode();
    }



    @Override
    public String toString() {
        return "excerpt {" +
                "originale excerpt='" + originalExcerpt + '\'' +
                ", text id=" + textId +
                '}';
    }


    private String techToWorking(String lang, boolean toDisplay){
        String excerpt = techWorkingExcerpt;
        ValuesHelper helper = factory.getValuesHelper();
        Pattern regex = Pattern.compile("\\[.*?\\]");
        int index = helper.indexOfPattern(regex, excerpt);

        if(index == -1) return excerpt;
        String working = excerpt.substring(0, index);

        while(index > -1){
            String value = excerpt.substring(index + 1);
            String subExcerpt = excerpt.substring(excerpt.indexOf("]") + 1);
            value = value.substring(0, value.indexOf("]"));
            String[] values = value.split(",");
            int newIndex = helper.indexOfPattern(regex, subExcerpt);

            if(values.length == 2) {
                Contribution contribution =
                        actorAccessor.retrieve(Long.parseLong(values[0]), EContributionType.value(Integer.parseInt(values[1])));

                if (contribution != null) {
                    working += "[" +
                            (!toDisplay ? contribution.getId() + "," + contribution.getContributionType().getContributionType() + "," : "")
                            + helper.getContributionDescription(contribution, lang) + "]";
                }
            }
            working += (newIndex > -1 ? subExcerpt.substring(0, newIndex) : subExcerpt);

            index = newIndex;
            excerpt = subExcerpt;
        }

        return working;
    }
}
