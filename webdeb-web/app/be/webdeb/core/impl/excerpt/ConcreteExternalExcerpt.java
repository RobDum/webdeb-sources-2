/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.impl.excerpt;

import be.webdeb.core.api.excerpt.ExcerptFactory;
import be.webdeb.core.api.excerpt.ExternalExcerpt;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contributor.ContributorFactory;
import be.webdeb.core.api.text.*;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.core.impl.contribution.AbstractExternalContribution;
import be.webdeb.infra.persistence.accessor.api.ExcerptAccessor;

import java.util.*;

/**
 * This class implements an ExternalExcerpt.
 *
 * @author Martin Rouffiange
 */
public class ConcreteExternalExcerpt extends AbstractExternalContribution<ExcerptFactory, ExcerptAccessor> implements ExternalExcerpt {

    private Long textId = -1L;
    private Text text;
    private ExternalText externalText;
    private String originalExcerpt;
    private String workingExcerpt;

    /**
     * Create a ExternalExcerpt instance
     *
     * @param factory the excerpt factory
     * @param accessor the excerpt accessor
     */
    ConcreteExternalExcerpt(ExcerptFactory factory, ExcerptAccessor accessor, ContributorFactory contributorFactory) {
        super(factory, accessor, contributorFactory);
        this.accessor = accessor;
        type = EContributionType.EXTERNAL_EXCERPT;
    }

    @Override
    public String getOriginalExcerpt() {
        return originalExcerpt;
    }

    @Override
    public void setOriginalExcerpt(String originalExcerpt) {
        this.originalExcerpt = originalExcerpt;
    }

    @Override
    public String getWorkingExcerpt() {
        return workingExcerpt;
    }

    @Override
    public void setWorkingExcerpt(String workingExcerpt) {
        this.workingExcerpt = workingExcerpt;
    }

    @Override
    public Long getTextId() {
        return textId;
    }

    @Override
    public void setTextId(Long textId) { this.textId = textId;
    }

    @Override
    public Text getText() {
        return text;
    }

    @Override
    public void setText(Text text) {
        this.text = text;
    }

    @Override
    public ExternalText getExternalText() {
        return externalText;
    }

    @Override
    public void setExternalText(ExternalText externalText) {
        this.externalText = externalText;
    }

    @Override
    public Map<Integer, List<Contribution>> save(Long contributor, int currentGroup) throws FormatException, PersistenceException, PermissionException {
        List<String> isValid = isValid();
        if (!isValid.isEmpty()) {
            logger.error("external argument contains errors " + isValid.toString());
            throw new FormatException(FormatException.Key.ARGUMENT_ERROR, String.join(",", isValid.toString()));
        }
        return accessor.save(this, currentGroup, contributor);
    }

    @Override
    public List<String> isValid() {
        List<String> fieldsInError = new ArrayList<>();
        if (originalExcerpt == null || originalExcerpt.equals("")) {
            fieldsInError.add("original excerpt is empty");
        }
        if (textId == null || textId == -1L) {
            fieldsInError.add("textId is null");
        }
        return fieldsInError;
    }

    @Override
    public int compareTo(ExternalExcerpt o) {
        return originalExcerpt.compareToIgnoreCase(o.getOriginalExcerpt());
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj instanceof ExternalText && hashCode() == obj.hashCode();
    }

    @Override
    public int hashCode() {
        return super.hashCode() + originalExcerpt.hashCode();
    }

    @Override
    public String toString() {
        return "external excerpt : " + originalExcerpt + " linked to text " + textId + " " + super.toString() ;
    }
}
