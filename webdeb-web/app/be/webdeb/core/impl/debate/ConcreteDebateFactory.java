/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.impl.debate;

import be.webdeb.core.api.debate.Debate;
import be.webdeb.core.api.debate.DebateFactory;
import be.webdeb.core.impl.contribution.AbstractContributionFactory;
import be.webdeb.infra.persistence.accessor.api.ActorAccessor;
import be.webdeb.infra.persistence.accessor.api.DebateAccessor;

import javax.inject.Inject;

public class ConcreteDebateFactory extends AbstractContributionFactory<DebateAccessor> implements DebateFactory {

    @Inject
    private ActorAccessor actorAccessor;

    @Override
    public Debate retrieve(Long id) {
        return accessor.retrieve(id, false);
    }

    @Override
    public Debate retrieveWithHit(Long id) {
        return accessor.retrieve(id, true);
    }

    @Override
    public Debate getDebate() {
        return new ConcreteDebate(this, accessor, actorAccessor, contributorFactory);
    }

    @Override
    public Debate random() {
        return accessor.random();
    }

}
