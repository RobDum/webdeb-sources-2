/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.impl.contribution;

import be.webdeb.application.query.EQueryKey;
import be.webdeb.core.api.argument.ArgumentContext;
import be.webdeb.core.api.argument.ArgumentJustification;
import be.webdeb.core.api.argument.ArgumentType;
import be.webdeb.core.api.argument.EArgumentType;
import be.webdeb.core.api.contribution.*;
import be.webdeb.core.api.contributor.Contributor;
import be.webdeb.core.api.contributor.ContributorFactory;
import be.webdeb.core.api.folder.Folder;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.infra.persistence.accessor.api.ContributionAccessor;

import be.webdeb.util.ValuesHelper;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.text.Normalizer;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This class implements common functions to handle all type of contributions.
 *
 *
 * @author Fabian Gilson
 */
public abstract class AbstractContributionFactory<T extends ContributionAccessor> implements ContributionFactory {

  protected static final Logger logger = play.Logger.underlying();

  private Map<String, Language> languages;

  /**
   * Default language for names
   */
  private static final String DEFAULT_LANG = "fr";

  @Inject
  protected T accessor;

  @Inject
  protected ContributorFactory contributorFactory;

  @Inject
  protected ValuesHelper values;

  private Map<Integer, ContributionType> contributionTypes;
  private Map<Integer, ValidationState> validationStates;

  @Override
  public Contribution retrieve(Long id, EContributionType type) {
    return accessor.retrieve(id, type);
  }

  @Override
  public Contribution retrieveContribution(Long id) {
    return accessor.retrieveContribution(id);
  }

  @Override
  public ContextContribution retrieveContextContribution(Long id) {
    return accessor.retrieveContextContribution(id);
  }

  @Override
  public Contribution retrieveWithHit(Long id) {
    return accessor.retrieve(id, true);
  }

  @Override
  public synchronized List<ContributionType> getContributionTypes() {
    if (contributionTypes == null) {
      contributionTypes = accessor.getContributionTypes().stream().collect(Collectors.toMap(ContributionType::getContributionType, t -> t));
    }
    return new ArrayList<>(contributionTypes.values());
  }

  @Override
  public synchronized ContributionType getContributionType(EContributionType id) {
    if (contributionTypes == null) {
      contributionTypes = new LinkedHashMap<>();
      accessor.getContributionTypes().forEach(e -> contributionTypes.put(e.getContributionType(), e));
    }
    return contributionTypes.get(id.id());
  }

  @Override
  public ContributionType createContributionType(EContributionType id, Map<String, String> i18names) {
    return new ConcreteContributionType(id, i18names);
  }

  @Override
  public ContributionHistory createHistory(Contributor contributor, EModificationStatus status, String trace, Date version) {
    return new ConcreteContributionHistory(contributor, status, trace, version);
  }

  @Override
  public Contributor getCreator(Long id) {
    return accessor.getCreator(id);
  }

  @Override
  public Contributor getLastContributorInGroup(Long id, int group) {
    return accessor.getLastContributorInGroup(id, group);
  }

  @Override
  public Place findPlace(Long id) {
    return accessor.findPlace(id);
  }

  @Override
  public List<Place> findPlace(String name, int fromIndex, int toIndex) {
    return accessor.findPlace(name, fromIndex, toIndex);
  }

  @Override
  public List<Contribution> findByValue(String value, int group, int fromIndex, int toIndex) {
    return accessor.findByValue(value, group, fromIndex, toIndex);
  }

  @Override
  public List<Contribution> findByCriteria(List<Map.Entry<EQueryKey, String>> criteria, boolean strict, int fromIndex, int toIndex) {
    return accessor.findByCriteria(criteria, strict, fromIndex, toIndex);
  }

  @Override
  public List<Contribution> getLatestEntries(EContributionType type, Long contributor, int amount, int group) {
    return accessor.getLatestEntries(type, contributor, amount, group);
  }

  @Override
  public List<Contribution> getPopularEntries(EContributionType type, Long contributor, int amount, int group) {
    return accessor.getPopularEntries(type, contributor, amount, group);
  }

  @Override
  public int getAmountOf(EContributionType type, int group) {
    return accessor.getAmountOf(type, group);
  }

  @Override
  public ValuesHelper getValuesHelper() {
    return values;
  }

  @Override
  public void saveMarkings(List<Contribution> contributions) throws PersistenceException {
    accessor.saveMarkings(contributions);
  }

  @Override
  public void merge(Long origin, Long replacement, Long contributor) throws PermissionException, PersistenceException {
    accessor.merge(origin, replacement, contributor);
  }

  @Override
  public List<ContributionHistory> getHistory(Long contribution) {
    return accessor.getHistory(contribution);
  }

  @Override
  public String getDefaultLanguage() {
    return DEFAULT_LANG;
  }

  @Override
  public synchronized List<WarnedWord> getWarnedWords(int contextType, int type) {
    Map<Integer, WarnedWord>warnedWords =
            accessor.getWarnedWords(contextType, type).stream().collect(Collectors.toMap(WarnedWord::getId, e -> e));
    return new ArrayList<>(warnedWords.values());
  }

  @Override
  public WarnedWord createWarnedWord(int id, Map<String, String> i18names, int type, int contextType) {
    return new ConcreteWarnedWord(id, i18names, type, contextType);
  }

  @Override
  public Long retrievePlaceContinentCode(String code){
    return accessor.retrievePlaceContinentCode(code);
  }

  @Override
  public Long retrievePlaceByGeonameIdOrPlaceId(Long geonameId, Long placeId){
    return accessor.retrievePlaceByGeonameIdOrPlaceId(geonameId, placeId);
  }

  @Override
  public Place createSimplePlace(Long idPlace){
    return new ConcretePlace(idPlace);
  }

  @Override
  public Place createPlace(Long idPlace, Long geonameId, String code, String latitude, String longitude, Map<String, String> names){
    return new ConcretePlace(idPlace, geonameId, code, latitude, longitude, names);
  }

  @Override
  public PlaceType createPlaceType(int idPlaceType, Map<String, String> i18names){
    return new ConcretePlaceType(idPlaceType, i18names);
  }

  @Override
  public PlaceType findPlaceTypeByCode(int code){
    return accessor.findPlaceTypeByCode(code);
  }

  @Override
  public synchronized List<ValidationState> getValidationStates() {
    if (validationStates == null) {
      validationStates = accessor.getValidationStates().stream().collect(Collectors.toMap(ValidationState::getState, e -> e));
    }
    return new ArrayList<>(validationStates.values());
  }

  @Override
  public ValidationState createValidationState(int state, Map<String, String> i18names) {
    return new ConcreteValidationState(state, i18names);
  }

  @Override
  public ValidationState getValidationState(int state) {
    if (validationStates == null) {
      getValidationStates();
    }
    return validationStates.get(state);
  }

  @Override
  public ValidationState getValidationState(boolean state) {
    if (validationStates == null) {
      getValidationStates();
    }
    return validationStates.get(state ? EValidationState.VALIDATED.id() : EValidationState.INVALIDATED.id());
  }

  @Override
  public Set<Folder> getContributionsFolders(Long contribution){
    return accessor.getContributionsFolders(contribution);
  }

  @Override
  public List<Place> getContributionsPlaces(Long contribution){
    return accessor.getContributionsPlaces(contribution);
  }

  @Override
  public void  updateDiscoveredExternalContributionState(Long id, boolean rejected) throws PersistenceException {
    accessor.updateDiscoveredExternalState(id, rejected);
  }

  @Override
  public synchronized List<Language> getLanguages() {
    if (languages == null) {
      languages = accessor.getLanguages().stream().collect(Collectors.toMap(Language::getCode, e -> e));
    }
    return new ArrayList<>(languages.values());
  }

  @Override
  public Language getLanguage(String code) throws FormatException {
    if (languages == null) {
      getLanguages();
    }

    if (!languages.containsKey(code)) {
      throw new FormatException(FormatException.Key.UNKNOWN_LANGUAGE, code);
    }
    return languages.get(code);
  }

  @Override
  public Language createLanguage(String code, Map<String, String> i18names) {
    return new ConcreteLanguage(code, i18names);
  }

  @Override
  public String computeShadeAndTitle(ArgumentType type, String standardForm, String lang) {
    if(!standardForm.equals("")) {
      String shadeterm = makeShadeReaderFriendly(type.getShadeName(lang), standardForm, lang);

      EArgumentType argType = EArgumentType.value(type.getArgumentType());
      String end = "";

      switch (argType) {
        // constative, prescriptive and first argument of a debate
        case DESCRIPTIVE:
          if (type.getArgumentShade() == 0) {
            return standardForm.substring(0, 1).toUpperCase() + standardForm.substring(1) + ".";
          } else {
            return shadeterm + standardForm + end;
          }
        case PRESCRIPTIVE:
        case FIRST_ARGUMENT:
        default:
          // all other types
          return shadeterm + standardForm + end;
      }
    }

    return "";
  }

  @Override
  public List<ArgumentContext> getContextualizedArguments(Long id) {
    return accessor.getContextualizedArgumentsFromContext(id);
  }

  @Override
  public List<ArgumentJustification> getArgumentJustificationLinks(Long id) {
      return accessor.getArgumentJustificationLinks(id);
  }

  @Override
  public List<Long> getAllIdByContributionType(EContributionType type) {
    return accessor.getAllIdByContributionType(type);
  }

  /**
   * Make shade reader friendly by avoiding shade ending vowel when standard form starts with a vowel
   *
   * @param shade a shade term
   * @param follower the string that will follow the shade
   * @param lang the language code (2-char ISO) used for the shade term
   * @return a user friendly shade term (including space if needed)
   */
  private static String makeShadeReaderFriendly(String shade, String follower, String lang) {
    if ("fr".equals(lang) && !follower.equals("")) {
      StringBuilder friendlyShade = new StringBuilder(shade);
      if (shade.endsWith(" de") || shade.endsWith(" que") || shade.equals("Je")) {
        String temp = Normalizer.normalize(follower, Normalizer.Form.NFD);
        temp = temp.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");

        if ("aeiou".indexOf(Character.toLowerCase(temp.charAt(0))) >= 0) {
          friendlyShade.replace(friendlyShade.length() - 1, friendlyShade.length(), "'");
          return friendlyShade.toString();
        }
      }

      // other cases
      return friendlyShade.append(" ").toString();
    } else {
      return shade + " ";
    }
  }
}
