/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.impl.contribution;

import be.webdeb.core.api.contribution.ContributionType;
import be.webdeb.core.api.contribution.EContributionType;

import java.util.Map;

/**
 * This class implements the contribution type interface.
 *
 * @author Fabian Gilson
 */
class ConcreteContributionType implements ContributionType {

  private int type;
  private EContributionType ctype;
  private Map<String, String> i18names;

  ConcreteContributionType(EContributionType type, Map<String, String> i18names) {
    this.type = type.id();
    this.i18names = i18names;
    ctype = type;
  }

  @Override
  public int getContributionType() {
    return type;
  }

  @Override
  public String getName(String lang) {
    return i18names.get(lang);
  }

  @Override
  public Map<String, String> getNames() {
    return i18names;
  }

  @Override
  public EContributionType getEContributionType(){
    return ctype;
  }
}
