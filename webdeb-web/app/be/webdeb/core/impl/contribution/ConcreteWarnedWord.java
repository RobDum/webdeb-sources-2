/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.impl.contribution;

import be.webdeb.core.api.contribution.WarnedWord;

import java.util.Map;

/**
 * This class implements a warned word
 *
 * @author Martin Rouffiange
 */
public class ConcreteWarnedWord extends AbstractPredefinedValues implements WarnedWord {

  private int idBannedProfession;
  private int type;
  private int contextType;

  /**
   * Constructor
   *
   * @param idBannedProfession a word banned profession id
   * @param i18names a map of language ISO code and word gender names
   * @param type a int representing the type of the word (begin word, ...)
   * @param contextType a int representing the warned word type
   * @see be.webdeb.core.api.contribution.EWarnedWordType
   */
  public ConcreteWarnedWord(int idBannedProfession, Map<String, String> i18names, int type, int contextType) {
    this.idBannedProfession = idBannedProfession;
    this.i18names = i18names;
    this.type = type;
    this.contextType = contextType;
  }

  @Override
  public int getId() {
    return idBannedProfession;
  }

  @Override
  public String getName(String lang){
    if(i18names != null && lang != null && i18names.containsKey(lang)){
      return i18names.get(lang);
    }
    return "";
  }

  @Override
  public Map<String, String> getNames(){
    return i18names;
  }

  @Override
  public int getType() {
    return type;
  }

  @Override
  public int getContextType() {
    return contextType;
  }
}
