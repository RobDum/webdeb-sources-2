/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */
package be.webdeb.core.impl.argument;

import be.webdeb.core.api.argument.ArgumentFactory;
import be.webdeb.core.api.argument.ArgumentLinkType;
import be.webdeb.core.api.argument.ArgumentShadedLink;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.ValidationState;
import be.webdeb.core.api.contributor.ContributorFactory;
import be.webdeb.core.impl.contribution.AbstractContribution;
import be.webdeb.infra.persistence.accessor.api.ArgumentAccessor;

import java.util.*;

/**
 * This class implements the ArgumentShadedLink that represents a shaded link between argument or / and excerpt
 * and its validation state if the link has been automatically created.
 *
 * @author Martin Rouffiange
 */
abstract class AbstractArgumentShadedLink extends AbstractContribution<ArgumentFactory, ArgumentAccessor> implements ArgumentShadedLink {

    protected ArgumentLinkType argumentLinkType;
    protected ValidationState validationState;

    /**
     * Default constructor.
     *
     * @param factory the argument factory (to get and build concrete instances of objects)
     * @param accessor the argument accessor to retrieve and persist arguments
     * @param contributorFactory the contributor factory to get bound contributors
     */
    AbstractArgumentShadedLink(ArgumentFactory factory, ArgumentAccessor accessor, ContributorFactory contributorFactory) {
        super(factory, accessor, contributorFactory);
    }

    @Override
    public ArgumentLinkType getArgumentLinkType() {
        return argumentLinkType;
    }

    @Override
    public void setArgumentLinkType(ArgumentLinkType argumentLinkType) {
        this.argumentLinkType = argumentLinkType;
    }

    @Override
    public ValidationState getValidationState() {
        return validated;
    }

    @Override
    public void setValidationState(ValidationState validationState) {
        this.validationState = validationState;
    }

    @Override
    public List<String> isValid() {
        List<String> fieldsInError = new ArrayList<>();
        fieldsInError.addAll(argumentLinkType != null ? argumentLinkType.isValid() : Collections.singletonList("linktype is null"));
        return fieldsInError;
    }

    @Override
    public String toString() {
        return "shade " + (argumentLinkType != null ? argumentLinkType.getLinkShade() : "") + " and validation " + validationState;
    }

    @Override
    public int hashCode() {
        return id.hashCode() + argumentLinkType.hashCode() + validationState.hashCode();
    }

    /**
     * Validate a contribution by calling its isContributorValid method
     *
     * @param c a contribution
     * @return a (possibly empty) list of validation errors for given contribution
     */
    protected List<String> partialValid(Contribution c) {
        List<String> isValid = new ArrayList<>();
        if (c == null) {
            isValid.add("contribution is null");
        } else {
            isValid = c.isValid();
            if (!isValid.isEmpty()) {
                isValid.add(String.join(",", isValid));
            }
        }
        return isValid;
    }

}
