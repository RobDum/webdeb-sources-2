/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.impl.argument;

import be.webdeb.core.api.argument.ArgumentLinkType;
import be.webdeb.core.api.argument.EArgumentLinkShade;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * This class implements argument link types
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
class ConcreteArgumentLinkType implements ArgumentLinkType {

  private EArgumentLinkShade etype;
  private int linkType = -1;
  private Map<String, String> typeNames;
  private int linkShade = -1;
  private Map<String, String> shadeNames;

  ConcreteArgumentLinkType(int linkType, Map<String, String> typeNames, int linkShade, Map<String, String> shadeNames) {
    this.linkType = linkType;
    this.typeNames = typeNames;
    this.linkShade = linkShade;
    this.shadeNames = shadeNames;
    etype = EArgumentLinkShade.value(linkShade);
  }

  @Override
  public int getLinkType() {
    return linkType;
  }

  @Override
  public String getLinkTypeName(String lang) {
    return typeNames.get(lang);
  }

  @Override
  public Map<String, String> getLinkTypeNames() {
    return typeNames;
  }

  @Override
  public String getLinkShadeName(String lang) {
    return shadeNames.get(lang);
  }

  @Override
  public Map<String, String> getLinkShadeNames() {
    return shadeNames;
  }

  @Override
  public void setOppositeType(EArgumentLinkShade shade){
    etype = shade.isNeutral() ? etype.getNeutral() : etype.getOpposite();
    linkShade = etype.id();
  }

  @Override
  public int getLinkShade() {
    return linkShade;
  }

  @Override
  public void setLinkShade(EArgumentLinkShade shade) {
    linkShade = shade.id();
  }

  @Override
  public EArgumentLinkShade getEType() {
    return etype;
  }

  /*
   * Convenience methods
   */

  @JsonIgnore
  @Override
  public List<String> isValid() {
    List<String> errors = new ArrayList<>();
    if (linkType == -1) {
      errors.add("linkType is null");
    }
    if (linkShade == -1) {
      errors.add("linkShade is null");
    }
    return errors;
  }

  @Override
  public boolean equals(Object obj) {
    return obj instanceof ArgumentLinkType && obj.hashCode() == hashCode();
  }

  @Override
  public int hashCode() {
    return linkShade;
  }
}
