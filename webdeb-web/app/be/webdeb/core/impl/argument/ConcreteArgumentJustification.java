/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */
package be.webdeb.core.impl.argument;

import be.webdeb.core.api.argument.ArgumentContext;
import be.webdeb.core.api.argument.ArgumentFactory;
import be.webdeb.core.api.argument.ArgumentJustification;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.contributor.ContributorFactory;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.infra.persistence.accessor.api.ArgumentAccessor;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class implements an ArgumentJustification representing an illustration link between a contextualized argument
 * and an excerpt.
 *
 * @author Martin Rouffiange
 */
public class ConcreteArgumentJustification extends AbstractArgumentShadedLink implements ArgumentJustification, Comparable<ArgumentJustification> {

    private ArgumentContext origin;
    private ArgumentContext destination;
    protected Contribution context;
    protected Long contextId;

    /**
     * Default constructor.
     *
     * @param factory the argument factory (to get and build concrete instances of objects)
     * @param accessor the argument accessor to retrieve and persist arguments
     * @param contributorFactory the contributor factory to get bound contributors
     */
    ConcreteArgumentJustification (ArgumentFactory factory, ArgumentAccessor accessor, ContributorFactory contributorFactory) {
        super(factory, accessor, contributorFactory);
        type = EContributionType.ARG_JUSTIFICATION;
    }

    @Override
    public ArgumentContext getOrigin() {
        return origin;
    }

    @Override
    public void setOrigin(ArgumentContext origin) {
        this.origin = origin;
    }

    @Override
    public ArgumentContext getDestination() {
        return destination;
    }

    @Override
    public void setDestination(ArgumentContext destination) {
        this.destination = destination;
    }

    @Override
    public Long getContextId() {
        return contextId;
    }

    @Override
    public void setContextId(Long contextId) {
        this.contextId = contextId;
    }

    @Override
    public Contribution getContext() {
        return context;
    }

    @Override
    public void setContext(Contribution context) {
        this.context = context;
        if(context != null){
            this.contextId = context.getId();
        }
    }

    @Override
    public Map<Integer, List<Contribution>> save(Long contributor, int currentGroup) throws FormatException, PersistenceException, PermissionException {
        List<String> errors = isValid();
        if (!errors.isEmpty()) {
            logger.error("argument justification link contains error " + errors.toString());
            throw new FormatException(FormatException.Key.LINK_ERROR, String.join(",", errors));
        }
        accessor.save(this, currentGroup, contributor);
        return new HashMap<>();
    }

    @Override
    public List<String> isValid() {
        List<String> fieldsInError = super.isValid();
        if((contextId == null || contextId == -1L) && context == null) {
            fieldsInError.add("context contribution is null");
        }
        if(!argumentLinkType.getEType().isJustification()){
            fieldsInError.add("argument link type is not a justification");
        }
        return fieldsInError;
    }

    @Override
    public String toString() {
        return "justification link [" + id + "] between origin " + origin.getId() + " and destination " + destination.getId() +
                " and context " + contextId + super.toString();
    }

    @Override
    public int compareTo(@NotNull ArgumentJustification o) {
        return (int) (version - o.getVersion());
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && (obj instanceof ArgumentJustification && hashCode() == obj.hashCode());
    }

    @Override
    public int hashCode() {
        return super.hashCode() + origin.getId().hashCode() + destination.getId().hashCode();
    }

}
