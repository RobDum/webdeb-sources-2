/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.impl.argument;

import be.webdeb.core.api.argument.*;
import be.webdeb.core.api.contribution.ContextContribution;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.impl.contribution.AbstractContributionFactory;
import be.webdeb.infra.persistence.accessor.api.ActorAccessor;
import be.webdeb.infra.persistence.accessor.api.ArgumentAccessor;

import javax.inject.Inject;
import javax.inject.Singleton;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This class implements an factory for arguments, argument types and links.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
@Singleton
public class ConcreteArgumentFactory extends AbstractContributionFactory<ArgumentAccessor> implements ArgumentFactory {

  @Inject
  private ActorAccessor actorAccessor;

  // key is shade id
  private Map<Integer, ArgumentType> argumentTypes = null;
  private Map<Integer, ArgumentLinkType> argumentLinkTypes = null;

  @Override
  public Argument retrieve(Long id) {
    return id != null && id != -1L ? accessor.retrieve(id, false) : null;
  }

  @Override
  public Argument retrieveWithHit(Long id) {
    return id != null && id != -1L ? accessor.retrieve(id, true) : null;
  }

  @Override
  public ArgumentContext retrieveContextualized(Long id) {
    return id != null && id != -1L ? accessor.retrieveContextualized(id, false) : null;
  }

  @Override
  public ArgumentContext retrieveContextualizedWithHit(Long id) {
    return id != null && id != -1L ? accessor.retrieveContextualized(id, true) : null;
  }

  @Override
  public ArgumentContext retrieveContextualizedFromArg(Long id, boolean dic) {
    return accessor.retrieveContextualizedFromArg(id, dic);
  }

  @Override
  public ArgumentDictionary retrieveDictionary(Long id) {
    return accessor.retrieveDictionary(id);
  }

  @Override
  public ArgumentJustification retrieveJustificationLink(Long id) {
    return accessor.retrieveJustificationLink(id);
  }

  @Override
  public ArgumentSimilarity retrieveSimilarityLink(Long id) {
    return accessor.retrieveSimilarityLink(id);
  }

  @Override
  public ArgumentIllustration retrieveIllustrationLink(Long id) {
    return accessor.retrieveIllustrationLink(id);
  }

  @Override
  public Argument getArgument() {
    return new ConcreteArgument(this, accessor, actorAccessor, contributorFactory);
  }

  @Override
  public ArgumentContext getContextualizedArgument() {
    return new ConcreteArgumentContext(this, accessor, actorAccessor,contributorFactory);
  }

  @Override
  public ArgumentDictionary getArgumentDictionary() {
    return new ConcreteArgumentDictionary(this, accessor);
  }

  @Override
  public ArgumentJustification getArgumentJustificationLink() {
    return new ConcreteArgumentJustification(this, accessor, contributorFactory);
  }

  @Override
  public ArgumentSimilarity getArgumentSimilarityLink() {
    return new ConcreteArgumentSimilarity(this, accessor, contributorFactory);
  }

  @Override
  public ArgumentIllustration getArgumentIllustrationLink() {
    return new ConcreteArgumentIllustration(this, accessor, contributorFactory);
  }

  @Override
  public ContextContribution retrieveDebateFirstArgument(Long id) {
    return accessor.retrieveDebateFirstArgument(id);
  }

  @Override
  public List<ArgumentType> getArgumentTypes() {
    if (argumentTypes == null) {
      argumentTypes = accessor.getArgumentTypes().stream().collect(Collectors.toMap(ArgumentType::getArgumentShade, t -> t));
    }
    return new ArrayList<>(argumentTypes.values());
  }

  @Override
  public List<ArgumentType> getArgumentShades(int type) {
    List<ArgumentType> result = new ArrayList<>();
    getArgumentTypes().forEach(t -> {
      if (type == EArgumentType.ALL.id() || type == t.getArgumentType() ||
         (type == EArgumentType.NOT_FIRST_ARGUMENT.id() && t.getArgumentType() != EArgumentType.FIRST_ARGUMENT.id())) {
        result.add(t);
      }
    });
    return result;
  }

  @Override
  public ArgumentType createArgumentType(int type, Map<String, String> i18tNames, int shade, Map<String, String> i18shNames) {
    return new ConcreteArgumentType(type, i18tNames, shade, i18shNames);
  }

  @Override
  public ArgumentType getArgumentType(int shade) throws FormatException {
    if (argumentTypes == null) {
      getArgumentTypes();
    }
    if (!argumentTypes.containsKey(shade)) {
      throw new FormatException(FormatException.Key.UNKNOWN_ARGUMENT_TYPE, String.valueOf(shade));
    }
    return argumentTypes.get(shade);
  }

  @Override
  public synchronized List<ArgumentLinkType> getArgumentLinkTypes() {
    if (argumentLinkTypes == null) {
      argumentLinkTypes = accessor.getArgumentLinktypes().stream().collect(Collectors.toMap(ArgumentLinkType::getLinkShade, t -> t));
    }
    return new ArrayList<>(argumentLinkTypes.values());
  }

  @Override
  public ArgumentLinkType createArgumentLinkType(int type, Map<String, String> i18tNames, int shade,
      Map<String, String> i18sNames) {
    return new ConcreteArgumentLinkType(type, i18tNames, shade, i18sNames);
  }

  @Override
  public ArgumentLinkType getArgumentLinkType(EArgumentLinkShade shade) {
    if (argumentLinkTypes == null) {
      getArgumentLinkTypes();
    }
    return argumentLinkTypes.get(shade.id());
  }

  @Override
  public boolean linkAlreadyExists(Long origin, Long destination) {
    return accessor.linkAlreadyExists(origin, destination);
  }

  @Override
  public Argument findUniqueByTitle(String title) {
    return accessor.findUniqueByTitle(title);
  }

  @Override
  public List<ArgumentDictionary> findByTitle(String title, String lang, int fromIndex, int toIndex) {
    return accessor.findByTitle(title, lang, fromIndex, toIndex);
  }



}
