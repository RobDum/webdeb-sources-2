/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */
package be.webdeb.core.impl.argument;

import be.webdeb.core.api.argument.*;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.contributor.ContributorFactory;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.infra.persistence.accessor.api.ArgumentAccessor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class implements an ArgumentIllustration representing an illustration link between a contextualized argument
 * and an excerpt.
 *
 * @author Martin Rouffiange
 */
public class ConcreteArgumentIllustration extends AbstractArgumentShadedLink implements ArgumentIllustration {

    private ArgumentContext argument;
    private Excerpt excerpt;

    /**
     * Default constructor.
     *
     * @param factory the argument factory (to get and build concrete instances of objects)
     * @param accessor the argument accessor to retrieve and persist arguments
     * @param contributorFactory the contributor factory to get bound contributors
     */
    ConcreteArgumentIllustration(ArgumentFactory factory, ArgumentAccessor accessor, ContributorFactory contributorFactory) {
        super(factory, accessor, contributorFactory);
        type = EContributionType.ARG_ILLUSTRATION;
    }

    @Override
    public ArgumentContext getArgument() {
        return argument;
    }

    @Override
    public void setArgument(ArgumentContext argument) {
        this.argument = argument;
    }

    @Override
    public Excerpt getExcerpt() {
        return excerpt;
    }

    @Override
    public void setExcerpt(Excerpt excerpt) {
        this.excerpt = excerpt;
    }

    @Override
    public Map<Integer, List<Contribution>> save(Long contributor, int currentGroup) throws FormatException, PersistenceException, PermissionException {
        List<String> errors = isValid();
        if (!errors.isEmpty()) {
            logger.error("argument illustration link contains error " + errors.toString());
            throw new FormatException(FormatException.Key.LINK_ERROR, String.join(",", errors));
        }
        accessor.save(this, currentGroup, contributor);
        return new HashMap<>();
    }

    @Override
    public List<String> isValid() {
        List<String> fieldsInError = super.isValid();
        if(!argumentLinkType.getEType().isIllustration()){
            fieldsInError.add("argument link type is not an illustration");
        }
        return fieldsInError;
    }

    @Override
    public String toString() {
        return "illustration link [" + id + "] between argument " + argument.getId() + " and excerpt " + excerpt.getId() + super.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj instanceof ArgumentIllustration && hashCode() == obj.hashCode();
    }

    @Override
    public int hashCode() {
        return super.hashCode() +argument.getId().hashCode() + excerpt.getId().hashCode();
    }

}
