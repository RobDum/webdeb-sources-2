/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.impl.argument;

import be.webdeb.core.api.argument.ArgumentFactory;
import be.webdeb.core.api.argument.BaseArgument;
import be.webdeb.core.api.argument.EArgumentLinkShade;
import be.webdeb.core.api.contributor.ContributorFactory;

import be.webdeb.core.impl.contribution.AbstractTextualContribution;
import be.webdeb.infra.persistence.accessor.api.ActorAccessor;
import be.webdeb.infra.persistence.accessor.api.ArgumentAccessor;

import java.util.List;
import java.util.Map;

/**
 * This class implements the BaseArgument interface with common access methods to any type of Arguments.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
abstract class AbstractBaseArgument extends AbstractTextualContribution<ArgumentFactory, ArgumentAccessor> implements BaseArgument {

  /**
   * Default constructor.
   *
   * @param factory the argument factory (to get and build concrete instances of objects)
   * @param accessor the argument accessor to retrieve and persist arguments
   * @param actorAccessor an actor accessor (to retrieve/update involved actors)
   * @param contributorFactory the contributor factory to get bound contributors
   */
  AbstractBaseArgument(ArgumentFactory factory, ArgumentAccessor accessor, ActorAccessor actorAccessor,
                       ContributorFactory contributorFactory) {
    super(factory, accessor, actorAccessor, contributorFactory);
  }

  @Override
  public Map<EArgumentLinkShade, Integer> getArgumentShadesAmount(Long contributor, List<EArgumentLinkShade> shades) {
    return accessor.getArgumentShadesAmount(id, contributor, shades);
  }
}
