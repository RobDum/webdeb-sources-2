/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.impl.argument;

import be.webdeb.core.api.argument.ArgumentType;
import be.webdeb.core.api.argument.EArgumentShade;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Map;

/**
 * This class implements an ArgumentType
 *
 * @author Fabian Gilson
 */
class ConcreteArgumentType implements ArgumentType {
  // custom logger
  protected static final org.slf4j.Logger logger = play.Logger.underlying();

  private int argumentType = -1;
  private EArgumentShade atype;
  private Map<String, String> typeNames;
  private int argumentShade = -1;
  private Map<String, String> shadeNames;

  /**
   * Constructor
   *
   * @param type the argument type id
   * @param i18tNames a map of pairs of the form (2-char iso-code, type name)
   * @param shade the argument shade id
   * @param i18shNames a map of pairs of the form (2-char iso-code, shade name)
   */
  ConcreteArgumentType(int type, Map<String, String> i18tNames, int shade, Map<String, String> i18shNames) {
    argumentType = type;
    argumentShade = shade;
    typeNames = i18tNames;
    shadeNames = i18shNames;
    atype = EArgumentShade.value(shade);
  }

  @Override
  public int getArgumentType() {
    return argumentType;
  }

  @Override
  public void setArgumentType(int argumentType) {
    this.argumentType = argumentType;
  }

  @Override
  public int getArgumentShade() {
    return argumentShade;
  }

  @Override
  public void setArgumentShade(int argumentShade) {
    this.argumentShade = argumentShade;
  }

  @Override
  public String getTypeName(String lang) {
    return typeNames.get(lang);
  }

  @Override
  public Map<String, String> getTypeNames() {
    return typeNames;
  }

  @Override
  public void setTypeNames(Map<String, String> i18names) {
    typeNames = i18names;
  }

  @Override
  public String getShadeName(String lang) {
    return shadeNames.get(lang);
  }

  @Override
  public String getShadeNameWithDots(String lang) {
    return getShadeName(lang) + "...";
  }

  @Override
  public Map<String, String> getShadeNames() {
    return shadeNames;
  }

  @Override
  public void setShadeNames(Map<String, String> i18names) {
    shadeNames = i18names;
  }

  /*
   * Convenience methods
   */
  @JsonIgnore
  @Override
  public boolean isValid() {
    // argument types are between 0 and 2
    return argumentType >= 0 && argumentType <= 2
        // shades are between 0 and 10
        && argumentShade >= 0 && argumentShade <= 11;
  }


  @Override
  public EArgumentShade getEType(){
    return atype;
  }

  @Override
  public String toString() {
    return "Arg type " + argumentType + ",shade " + argumentShade;
  }
}

