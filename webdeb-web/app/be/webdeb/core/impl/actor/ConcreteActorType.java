/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.impl.actor;

import be.webdeb.core.api.actor.ActorType;
import be.webdeb.core.api.actor.EActorType;

import java.util.Map;

/**
 * This class implements an Actor type
 *
 * @author Fabian Gilson
 */
class ConcreteActorType implements ActorType {

  private int type;
  private Map<String, String> i18names;

  ConcreteActorType(EActorType type, Map<String, String> i18names) {
    this.type = type.id();
    this.i18names = i18names;
  }

  @Override
  public int getId() {
    return type;
  }

  @Override
  public String getName(String lang) {
    return i18names.get(lang);
  }

  @Override
  public Map<String, String> getNames() {
    return i18names;
  }
}
