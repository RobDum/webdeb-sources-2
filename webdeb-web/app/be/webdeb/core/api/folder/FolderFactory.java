/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.api.folder;

import be.webdeb.core.api.argument.ArgumentContext;
import be.webdeb.core.api.contribution.ContributionFactory;
import be.webdeb.core.api.debate.Debate;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.core.api.text.Text;
import be.webdeb.core.exception.FormatException;

import java.util.List;
import java.util.Map;

/**
 * This interface represents an abstract factory to handle folders.
 *
 * @author Martin Rouffiange
 */
public interface FolderFactory extends ContributionFactory {

  /**
   * Retrieve a Folder by its id
   *
   * @param id a Contribution id
   * @return a Folder if given id is a folder, null otherwise
   */
  Folder retrieve(Long id);

  /**
   * Retrieve a folder by its id and increment visualization hit of this contribution
   *
   * @param id a folder id
   * @return the folder concrete object corresponding to the given id, null if no found
   */
  Folder retrieveWithHit(Long id);

  /**
   * Retrieve a FolderLink between two folders
   *
   * @param parent the parent folder id
   * @param child the child folder id
   * @return a FolderLink if given ids are folder and linked, null otherwise
   */
  FolderLink retrieveLink(Long parent, Long child);

  /**
   * Construct an empty Folder instance
   *
   * @return a new Folder instance
   */
  Folder getFolder();

  /**
   * Construct an empty FolderLink instance
   *
   * @return a new FolderLink instance
   */
  FolderLink getFolderLink();

  /**
   * Find a folder by its complete name and the name lang
   *
   * @param name the name to find
   * @param lang a two-char iso-639-1 language code
   * @return a the matched Folder, null otherwise
   */
  Folder findUniqueByNameAndLang(String name, String lang);

  /**
   * Get a randomly chose Folder
   *
   * @return a Folder
   */
  Folder random();

  /**
   * Construct a FolderType
   *
   * @param type the folder type id
   * @param i18names a map of pairs of the form (2-char iso-code, type name)
   * @return an FolderType instance
   */
  FolderType createFolderType(int type, Map<String, String> i18names);

  /**
   * Get a folder type by its id
   *
   * @param type a folder type id
   * @return the FolderType corresponding to the given type id
   *
   * @throws FormatException if given id does not exist
   */
  FolderType getFolderType(int type) throws FormatException;

  /**
   * Retrieve all folder types
   *
   * @return the list of all folder types
   */
  List<FolderType> getFolderTypes();

  /**
   * Construct a FolderName
   *
   * @param lang a two-char iso-639-1 language code
   * @param name the folder name
   * @return a FolderName instance
   */
  FolderName createFolderName(String lang, String name);

  /**
   * Find a list of Folder by their (partial) name
   *
   * @param name a name
   * @return the list of Folders with their names containing the given name, or an empty list if none found
   */
  List<Folder> findByName(String name);

  /**
   * Find a list of Folder by their (partial) name and type
   *
   * @param name a name
   * @param type the Folder type to search
   * @return the list of Folders with their names containing the given name, or an empty list if none found
   */
  List<Folder> findByName(String name, EFolderType type);

  /**
   * Find a list of rewording name from a given folder
   *
   * @param folder a folder id
   * @return a list of FolderName
   * @see FolderName
   */
  List<FolderName> getFolderRewordingNames(Long folder);

  /**
   * Construct an empty folder HierarchyTree instance
   *
   * @return a new HierarchyTree  instance
   */
  HierarchyTree createHierarchyTree();

  /**
   * Construct a folder HierarchyNode instance
   *
   * @param id the node unique id
   * @param name the node name
   * @return a new folder HierarchyNode instance
   */
  HierarchyNode createHierarchyNode(long id, String name);

  /**
   * Construct a folder HierarchyNode instance
   *
   * @param id the node unique id
   * @param name the node name
   * @param depth the depth of the node in the hierarchy tree
   * @return a new folder HierarchyNode instance
   */
  HierarchyNode createHierarchyNode(long id, String name, int depth);

  /**
   * Get the hierarchy tree of a given folder
   *
   * @param folder a folder id
   * @return a hierarchy tree
   * @see HierarchyTree
   */
  HierarchyTree getFolderHierarchyTree(Long folder);

  /**
   * Check if a folder (as hierarchy) can be add in the hierarchy of a given folder
   *
   * @param folder a folder
   * @param hierarchy a folder to add at the given folder hierarchy
   * @param isParent true if the given folder will be the parent of the hierarchy folder
   * @return a hierarchy code depending of the issue
   * @see EHierarchyCode
   */
  EHierarchyCode checkHierarchy(Folder folder, Folder hierarchy, boolean isParent);

  /**
   * Get de default folder for rss text feeding, if nlp don't found any
   *
   * @return a hierarchy code depending of the issue
   */
  Folder getRssDefaultFolder();

}
