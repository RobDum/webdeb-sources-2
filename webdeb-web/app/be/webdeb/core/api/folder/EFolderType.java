/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.api.folder;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This enumeration holds types for folders
 * <pre>
 *   0 for root
 *   1 for node
 *   2 for tree
 * </pre>
 *
 * @author Martin Rouffiange
 */
public enum EFolderType {

  /**
   * the folder has no type (used for search)
   */
  NONE(-1),
  /**
   * Simple folder are used to classify contributions
   */
  SIMPLE(0),
  /**
   * Tag folder are short named, and create composed folder
   */
  TAG(1),
  /**
   * Composed folder are created with one - to - 3 tag folders
   */
  COMPOSED(2);

  private int id;

  private static Map<Integer, EFolderType> map = new LinkedHashMap<>();

  static {
    for (EFolderType type : EFolderType.values()) {
      map.put(type.id, type);
    }
  }

  /**
   * Constructor
   *
   * @param id an int representing an Folder type
   */
  EFolderType(int id) {
    this.id = id;
  }

  /**
   * Get the enum value for a given id
   *
   * @param id an int representing an folder type
   * @return the EFolderType enum value corresponding to the given id, null otherwise.
   */
  public static EFolderType value(int id) {
    return map.get(id);
  }

  /**
   * Get this id
   *
   * @return an int representation of this folder type
   */
  public int id() {
    return id;
  }
}
