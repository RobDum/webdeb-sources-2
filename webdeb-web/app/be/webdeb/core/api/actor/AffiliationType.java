/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package be.webdeb.core.api.actor;

import java.util.Map;

/**
 * This interface represents an Affiliation type for organizations to be affiliated to either other
 * organizations or persons.
 *
 * Used with EAffiliationType enum, where consistence rules are explained. Those rules are verified by the
 * Affiliation object itself.
 *
 * @author Fabian Gilson
 * @see EAffiliationType
 */
public interface AffiliationType {

  /**
   * Get the actor affiliation type id value
   *
   * @return a string representing an Affiliation type
   */
  int getId();

  /**
   * Get the affiliation actor type id value
   *
   * @return a value representating the actor aff type
   */
  int getActorId();

  /**
   * Get the affiliation subtype id value
   *
   * @return a value representating the aff subtype
   */
  int getSubId();

  /**
   * Get the name of this Affiliation type in given language
   *
   * @param lang a two-char ISO-639-1 code representing the language for the name
   * @return a name for this Actor type
   */
  String getName(String lang);

  /**
   * Get the whole map of names in all known languages
   *
   * @return a map of (2-char ISO-639-1 code, name in this language) of this affiliation type
   */
  Map<String, String> getNames();
}
