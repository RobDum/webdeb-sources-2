/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package be.webdeb.core.api.actor;

import java.util.Map;

/**
 * This interface represents an Actor type. This is a typed object concurrent to the EActorType enum.
 *
 * @author Fabian Gilson
 * @see EActorType
 */
public interface ActorType {

  /**
   * Get the actor type id value
   *
   * @return a string representing an Actor type
   */
  int getId();

  /**
   * Get the name of this Actor type
   *
   * @param lang a two-char ISO-639-1 code representing the language for the name
   * @return a name for this Actor type
   */
  String getName(String lang);

  /**
   * Get the whole map of names in all known languages
   *
   * @return a map of (2-char ISO-639-1 code, name in this language) of this actor type
   */
  Map<String, String> getNames();

}
