/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.api.debate;

import be.webdeb.core.api.contribution.ContributionFactory;

/**
 * This interface represents an abstract factory to handle debates.
 *
 * @author Martin Rouffiange
 */
public interface DebateFactory extends ContributionFactory {

    /**
     * Retrieve a debate by its id
     *
     * @param id a Contribution id
     * @return a debate if given id is a debate, null otherwise
     */
    Debate retrieve(Long id);

    /**
     * Retrieve a debate by its id and increment visualization hit of this contribution
     *
     * @param id a debate id
     * @return the debate concrete object corresponding to the given id, null if no found
     */
    Debate retrieveWithHit(Long id);

    /**
     * Construct an empty debate instance
     *
     * @return a new Debate instance
     */
    Debate getDebate();

    /**
     * Get a randomly chose Debate
     *
     * @return a Debate
     */
    Debate random();
}
