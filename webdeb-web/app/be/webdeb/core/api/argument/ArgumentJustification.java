/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */
package be.webdeb.core.api.argument;

import be.webdeb.core.api.contribution.Contribution;

/**
 * This interface represents the justification links between an origin and a destination contextualized arguments
 * and the shade between them.
 *
 * @author Martin Rouffiange
 */
public interface ArgumentJustification extends ArgumentShadedLink {

    /**
     * Get the origin contextualized argument of this link
     *
     * @return the contextualized argument being the origin of this justification link
     */
    ArgumentContext getOrigin();

    /**
     * Get the destination contextualized argument of this link
     *
     * @return the contextualized argument being the destination of this justification link
     */
    ArgumentContext getDestination();

    /**
     * Set the origin contextualized argument
     *
     * @param origin an contextualized argument to be bound as the origin of this justification link
     */
    void setOrigin(ArgumentContext origin);

    /**
     * Set the destination contextualized argument
     *
     * @param  destination an contextualized argument to be bound as the destination of this justification link
     */
    void setDestination(ArgumentContext destination);

    /**
     * Get the context id of this link.
     *
     * @return the context contribution id of this link.
     */
    Long getContextId();

    /**
     * Set the the context id of this link.
     *
     * @param contextId the context contribution id of this link.
     */
    void setContextId(Long contextId);

    /**
     * Get the context of this link.
     *
     * @return the context contribution of this link.
     */
    Contribution getContext();

    /**
     * Set the the context of this link.
     *
     * @param context the context contribution of this link.
     */
    void setContext(Contribution context);
}
