/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.api.argument;

import java.util.List;
import java.util.Map;

/**
 * This interface represent a type of link between arguments. A link is always unidirectional, such as it is
 * called "forward" when it is traversed in its original direction and "backward" in the other way.
 *
 * @author Fabian Gilson
 */
public interface ArgumentLinkType {

  /**
   * Get the link type
   *
   * @return an int representing the type of link between two Excerpt
   */
  int getLinkType();

  /**
   * Get the name of this link type
   *
   * @param lang a two-char ISO-639-1 code representing the language for the name
   * @return the name corresponding to the type of this ArgumentLinkType
   */
  String getLinkTypeName(String lang);

  /**
   * Get the whole map of names in all known languages
   *
   * @return a map of (2-char ISO-639-1 code, name in this language) of this link type
   */
  Map<String, String> getLinkTypeNames();

  /**
   * Get the link type shade
   *
   * @return an int representing the link type shade
   */
  int getLinkShade();

  /**
   * Set the link shade between two Excerpt
   *
   * @param shade a link shade enum value
   */
  void setLinkShade(EArgumentLinkShade shade);

  /**
   * Get the name of this link shade
   *
   * @param lang a two-char ISO-639-1 code representing the language for the name
   * @return the name corresponding to the shade of this ArgumentLinkType
   */
  String getLinkShadeName(String lang);

  /**
   * Get the whole map of names in all known languages
   *
   * @return a map of (2-char ISO-639-1 code, name in this language) of this link shade
   */
  Map<String, String> getLinkShadeNames();

  /**
   * Set the argument type as the opposite as is currently is
   *
   * @param shade a link shade enum value for deal with neutral type
   */
  void setOppositeType(EArgumentLinkShade shade);


  /*
   * CONVENIENCE METHOD
   */


  /**
   * Check if this argument link type is valid, i.e., is fully defined with a type and a shade
   *
   * @return true if both type and shade are defined
   */
  List<String> isValid();

  /**
   * Get the corresponding EArgumentLinkType value to this.
   *
   * @return the EArgumentLinkType enum value corresponding to this link type
   */
  EArgumentLinkShade getEType();

}
