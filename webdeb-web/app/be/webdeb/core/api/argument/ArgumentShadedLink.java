/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */
package be.webdeb.core.api.argument;

import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.ValidationState;

/**
 * This interface represents a shaded link between argument or / and excerpt and its validation state if the link
 * has been automatically created.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 * @see ArgumentLinkType
 * @see ValidationState
 */
public interface ArgumentShadedLink extends Contribution {

    /**
     * Get the type of link that is binding the owning argument of this ArgumentLinkType and this argument
     *
     * @return the ArgumentLinkType that is binding this argument to this class owner
     *
     * @see ArgumentLinkType
     */
    ArgumentLinkType getArgumentLinkType();

    /**
     * Set the type of link between this class owner and this argument
     *
     * @param type an ArgumentType object
     * @see ArgumentLinkType
     */
    void setArgumentLinkType(ArgumentLinkType type);

    /**
     * Get the validation state of this shaded link.
     *
     * @return the ValidationState of this link
     * @see ValidationState
     */
    ValidationState getValidationState();

    /**
     * Set the validation state of this shaded link.
     *
     * @param validationState the ValidationState of this link
     * @see ValidationState
     */
    void setValidationState(ValidationState validationState);
}
