/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */
package be.webdeb.core.api.argument;

import be.webdeb.core.api.contribution.TextualContribution;

import java.util.List;
import java.util.Map;

/**
 * This interface gathers common access methods to any type of arguments as considered into the Webdeb system.
 * This interface is meant to be extended by more concrete interfaces with particularities of arguments.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public interface BaseArgument extends TextualContribution {

    /**
     * Get the amount of argument links linked with this argument that a given contributor can see by given shade
     *
     * @param contributor a contributor
     * @param shades the list of shades to compute
     * @return a map of shade / amount
     */
    Map<EArgumentLinkShade, Integer> getArgumentShadesAmount(Long contributor, List<EArgumentLinkShade> shades);
}
