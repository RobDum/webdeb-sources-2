/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.api.argument;

import java.util.*;

/**
 * This enumeration holds shades for argumentation links
 * <pre>
 * Similarity (bidirectional, ie, A SIMILAR B is equivalent to B SIMILAR B)
 *   0 for SIMILAR
 *   1 for OPPOSES
 *
 * Justification; (uni-directional ,ie A SUPPORTS B is not equivalent to B SUPPORTS A)
 *   3 for SUPPORTS
 *   5 for REJECTS
 *
 * Illustration: (Excerpt illustrates contextualized arguments)
 *   6 for ILLUSTRATE
 *   7 for SHADES
 *   8 for COUNTER_EXAMPLE
 * </pre>
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public enum EArgumentLinkShade {

  /**
   * similar arguments, expressing the same opinion (similarity-type link, bi-directional)
   */
  SIMILAR(0),
  /**
   * opposed arguments, expressing a contrary opinion (similarity-type link, bi-directional)
   */
  OPPOSES(1),
  /**
   * supporting arguments, sustaining an argument (justification-type link, uni-directional)
   */
  SUPPORTS(2),
  /**
   * rejecting arguments, disagreeing on an argument (justification-type link, uni-directional)
   */
  REJECTS(3),
  /**
   * an excerpt illustrate a contextualized argument
   */
  ILLUSTRATE(4),
  /**
   * an excerpt shade a contextualized argument
   */
  SHADED_EXAMPLE(5),
  /**
   * an excerpt opposes a contextualized argument
   */
  COUNTER_EXAMPLE(6),
  /**
   * used to know that an author has excerpts in many shades.
   */
  INDECISION(7);

  private int id;

  private static Map<Integer, EArgumentLinkShade> map = new LinkedHashMap<>();

  static {
    for (EArgumentLinkShade type : EArgumentLinkShade.values()) {
      map.put(type.id, type);
    }
  }

  /**
   * Constructor
   *
   * @param id an int representing an Excerpt link shade
   */
  EArgumentLinkShade(int id) {
    this.id = id;
  }

  /**
   * Get the enum value for a given id
   *
   * @param id an int representing an argument link shade
   * @return the EArgumentLinkShade enum value corresponding to the given id, null otherwise.
   */
  public static EArgumentLinkShade value(int id) {
    return map.get(id);
  }

  /**
   * Get this id
   *
   * @return an int representation of this argument link shade
   */
  public int id() {
    return id;
  }

  /**
   * Check if this is a similarity link
   *
   * @return true if this link is either SIMILAR or OPPOSES
   */
  public boolean isSimilarity() {
    return id <= OPPOSES.id();
  }

  /**
   * Check if this is a justification link
   *
   * @return true if this link is either SUPPORTS or REJECTS
   */
  public boolean isJustification() {
    return id >= SUPPORTS.id() && id <= REJECTS.id();
  }

  /**
   * Check if this is a illustration link
   *
   * @return true if this link is either ILLUSTRATE, SHADED_EXAMPLE, COUNTER_EXAMPLE
   */
  public boolean isIllustration() {
    return id >= ILLUSTRATE.id() && id <= COUNTER_EXAMPLE.id();
  }

  /**
   * Check whether this shade is an approbation
   *
   * @return true if this link is an approbation
   */
  public boolean isApprobation() {
    return id == SIMILAR.id() || id == SUPPORTS.id() || id == ILLUSTRATE.id();
  }

  /**
   * Check whether this shade is an opposition
   *
   * @return true if this link is an opposition
   */
  public boolean isOpposition() {
    return id == OPPOSES.id() || id == REJECTS.id() || id == COUNTER_EXAMPLE.id();
  }

  /**
   * Check whether this shade is neutral
   *
   * @return true if this link is neutral
   */
  public boolean isNeutral() {
    return id == SHADED_EXAMPLE.id();
  }

  /**
   * Returns the opposite link type all kind of links, i.e.
   *
   * <pre>
   *   SIMILAR <-> OPPOSES
   *   SUPPORTS <-> REJECTS
   *   ILLUSTRATE <-> COUNTER_EXAMPLE
   *   other (shades and qualifies) remains unchanged
   * </pre>
   * @return the opposite link shade enum value of this object
   */
  public EArgumentLinkShade getOpposite() {
    switch (this) {
      case SIMILAR:
        return OPPOSES;
      case OPPOSES:
        return SIMILAR;

      case SUPPORTS:
        return REJECTS;
      case REJECTS:
        return SUPPORTS;

      case ILLUSTRATE:
        return COUNTER_EXAMPLE;
      case COUNTER_EXAMPLE:
        return ILLUSTRATE;

      default:
        return this;
    }
  }

  /**
   * Returns the neutral link type all kind of links, i.e.
   *
   * <pre>
   *   ILLUSTRATE, SHADED_EXAMPLE, COUNTER_EXAMPLE <-> SHADED_EXAMPLE
   *   other (shades and qualifies) remains unchanged
   * </pre>
   * @return the neutral link shade enum value of this object
   */
  public EArgumentLinkShade getNeutral() {
    switch (this) {
      case ILLUSTRATE:
      case COUNTER_EXAMPLE:
      case INDECISION:
        return SHADED_EXAMPLE;
      default:
        return this;
    }
  }

  /**
   * Returns the neutral or opposite link type all kind of links, i.e.
   *
   * <pre>
   *   SHADED_EXAMPLE <-> SHADED_EXAMPLE
   *   ILLUSTRATE, COUNTER_EXAMPLE <-> COUNTER_EXAMPLE
   * </pre>
   * @return the neutral or opposite link shade enum value of this object
   */
  public EArgumentLinkShade getNeutralOrOpposite() {
    switch (this) {
      case ILLUSTRATE:
      case COUNTER_EXAMPLE:
        return COUNTER_EXAMPLE;
      default:
        return this;
    }
  }

  /**
   * Returns the positive link type all kind of links, i.e.
   *
   * <pre>
   *   ILLUSTRATE, SHADED_EXAMPLE, COUNTER_EXAMPLE <-> ILLUSTRATE
   *   other (IllUSTRATE) remains unchanged
   * </pre>
   * @return the positive link shade enum value of this object
   */
  public EArgumentLinkShade getPositive() {
    switch (this) {
      case ILLUSTRATE:
      case COUNTER_EXAMPLE:
      case INDECISION:
        return ILLUSTRATE;
      default:
        return this;
    }
  }

  /**
   * Returns the list of similar shades
   *
   * @return the list of similar shades enum
   */
  public static List<EArgumentLinkShade> getSimilars() {
    return Arrays.asList(SIMILAR, OPPOSES);
  }

  /**
   * Returns the list of justification shades
   *
   * @return the list of justification shades enum
   */
  public static List<EArgumentLinkShade> getJustifications() {
   return Arrays.asList(SUPPORTS, REJECTS);
  }

  /**
   * Returns the list of illustration shades
   *
   * @return the list of illustration shades enum
   */
  public static List<EArgumentLinkShade> getIllustrations() {
    return Arrays.asList(ILLUSTRATE, SHADED_EXAMPLE, COUNTER_EXAMPLE);
  }
}
