/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.api.text;

import java.util.Map;

/**
 * This enumeration holds values for word genders.
 *
 * Valid values are
 * <ul>
 *   <li>F for feminine</li>
 *   <li>M for masculine</li>
 *   <li>N for neuter</li>
 * </ul>
 *
 * @author Martin Rouffiange
 */
public interface WordGender {

    /**
     * Get the word gender id value
     *
     * @return a string representing a word gender, ie, M, F and N
     */
    String getId();

    /**
     * Get the name of this word gender
     *
     * @param lang a two-char ISO 639-1 code representing the language for the name
     * @return a name for this word gender
     */
    String getName(String lang);

    /**
     * Get the whole map of names in all known languages
     *
     * @return a map of (2-char ISO 639-1 code, name in this language) of this word gender
     */
    Map<String, String> getNames();


}
