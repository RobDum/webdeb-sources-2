/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.api.excerpt;

import be.webdeb.core.api.argument.ArgumentIllustration;
import be.webdeb.core.api.contribution.ContributionFactory;
import java.util.List;

/**
 * This interface represents an abstract factory to handle excerpts.
 *
 * @author Fabian Gilson
 */
public interface ExcerptFactory extends ContributionFactory {

    /**
     * Retrieve an excerpt by its id
     *
     * @param id a Contribution id
     * @return an excerpt if given id is an argument, null otherwise
     */
    Excerpt retrieve(Long id);

    /**
     * Retrieve an external excerpt by its id
     *
     * @param id a Contribution id
     * @return an ExternalExcerpt if given id is an external excerpt, null otherwise
     */
    ExternalExcerpt retrieveExternal(Long id);

    /**
     * Retrieve an external excerpt by its id and the external source id
     *
     * @param id a Contribution id
     * @param externalSource the source where the excerpts come from
     * @return an ExternalExcerpt if given id is an external excerpt, null otherwise
     */
    ExternalExcerpt retrieveExternal(Long id, int externalSource);

    /**
     * Retrieve an excerpt by its id and increment visualization hit of this contribution
     *
     * @param id an excerpt id
     * @return the excerpt concrete object corresponding to the given id, null if no found
     */
    Excerpt retrieveWithHit(Long id);

    /**
     * Retrieve a context element by its idd
     *
     * @param id a context contribution id
     * @return a ContextElement if given id is an ContextElement, null otherwise
     */
    ContextElement retrieveContextElement(Long id);

    /**
     * Retrieve an illustration link by argid and excid.
     *
     * @param argid an contextualized argument id
     * @param excid an excerpt id
     * @return the ArgumentIllustration, null if not found
     */
    ArgumentIllustration retrieveIllustrationLink(Long argid, Long excid );

    /**
     * Construct an empty excerpt instance
     *
     * @return a new excerpt instance
     */
    Excerpt getExcerpt();

    /**
     * Construct an empty external excerpt instance
     *
     * @return a new ExternalExcerpt instance
     */
    ExternalExcerpt getExternalExcerpt();

    /**
     * Construct an empty context element instance
     *
     * @return a new ContextElement instance
     */
    ContextElement getContextElement();

    /**
     * Get the list of all external excerpts waiting for contributors' validation that come from a given external source
     * and with a maximum of results. Those excerpts have been retrieved from external services like WDTAL services
     * (eg twitter) and are waiting for validation before being transformed to Excerpts and sent into the webdeb database.
     *
     * @param externalSource the source where the excerpts come from
     * @param maxResults the maximum of excerpts to return
     * @return a (possibly empty) list of temporary excerpts
     */
    List<ExternalExcerpt> getExternalExcerptsByExternalSource(int externalSource, int maxResults);

    /**
     * Find already recorded external excerpt that hasn't has been added to Webdeb yet.
     *
     * @param contributor a contributor id
     * @param excerpt the original excerpt
     * @param textId the text id of the excerpt
     * @return the retrieved external excerpt or null
     */
    ExternalExcerpt findExistingExternalExcerpt(Long contributor, String excerpt, Long textId);

}