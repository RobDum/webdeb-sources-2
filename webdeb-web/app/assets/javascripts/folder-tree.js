/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */


/**
 * This javascript file contains the functions for display the hierarchy tree of a folder
 *
 * @author Martin Rouffiange
 */

var FOLDER_VIZ_PANE = 0;

/**
 * Create and initialize the folder hierarchy tree for a specific node
 *
 * @param parents the parents hierarchy as json where the first element is the current node
 * @param children the children hierarchy as json where the first element is the current node
 * @param container the container where the tree can be displayed
 */
function initFolderTree(parents, children, container){
    let folder_container = container.find('.folder-container');
    if(!folder_container.exists()){
        folder_container = $('<div class="folder-container"></div>');
        container.append(folder_container);
    }
    folder_container.empty();
    createTree(parents, children, folder_container);
}

/**
 * Create the folder hierarchy tree
 *
 * @param parents the parents hierarchy as json where the first element is the current node
 * @param children the children hierarchy as json where the first element is the current node
 * @param container the container where the tree can be displayed
 */
function createTree(parents, children, container) {
    let connector = initLinkConnector();
    // create it first to draw links
    let main = createFolderHtml(parents, container, true);
    let parentsGenerations = createHierarchy(parents, container, connector, false);
    let childrenGenerations = createHierarchy(children, container, connector, true);
    let generations = mergeGenerations(parentsGenerations.slice(), childrenGenerations.slice(), main);
    let containerHeight = computePositionYFoldersGenerations(generations);
    computePositionXFoldersGenerations(generations, container);
    container.height(containerHeight);
    connector.repaintEverything();
}

/**
 * Merge parents generations, current node and children generations to have to complete generations hierarchy array in one
 *
 * @param parents the parents generations hierarchy array
 * @param children the parents generations hierarchy array
 * @param main the current node
 */
function mergeGenerations(parents, children, main){
    parents.push([main]);
    return parents.concat(children.reverse());
}

/**
 * Create a folder node in html to display the hierarchy tree
 *
 * @param folder the concerned folder
 * @param container the html element where the folder node can be added
 * @param isCurrentNode true if the given folder node is the current node
 */
function createFolderHtml(folder, container, isCurrentNode){
    isCurrentNode = isCurrentNode || false;
    let tableContainer = $('<div id="' + getFolderHtmlId(folder) +'" class="folder-tree-box"></div>');
    let titleContainer = $('<div class="folder-tree-box-title">' +
        '<a class="normal-style" href="' + urlOfFolderViz(folder.id, FOLDER_VIZ_PANE) + '">' + folder.name + '</a></div>');
    tableContainer.append(titleContainer);
    tableContainer.draggable();
    if(isCurrentNode) tableContainer.addClass("folder-tree-main-box");
    container.append(tableContainer);
    return tableContainer;
}

/**
 * Get the folder id on a folder html element or a folder object
 *
 * @param folder the concerned folder
 * @param selector true if the folder is an html element, false if it is an object
 */
function getFolderHtmlId(folder, isSelector){
    isSelector = isSelector || false;
    let selector = "folder" + '-' + folder.id;
    return (isSelector ? $("#"+selector) : selector);
}

/**
 * Create the generations hierarchy array, and draw it in html
 *
 * @param currentNode the current node
 * @param container the html element where the generations nodes can be added
 * @param connector an instance for draw the hierarchy direction with arrows
 * @param isChild true if the given hierarchy is the child hierarchy of the current node, false otherwise
 */
function createHierarchy(currentNode, container, connector, isChild){
    let generations = [];
    browse([{parent : currentNode, children :currentNode.children}], generations, container, connector, isChild);
    return generations;
}

/**
 * Create a specific generation of the hierarchy tree
 *
 * @param generationNodes the nodes of the current generation and theirs parents
 * @param generations the array of generations where this generation will be added
 * @param container the html element where the generations nodes can be added
 * @param connector an instance for draw the hierarchy direction with arrows
 * @param isChild true if the given hierarchy is the child hierarchy of the current node, false otherwise
 */
function browse(generationNodes, generations, container, connector, isChild){
    if(Array.isArray(generationNodes)){
        let generation = [];
        let nextGeneration = [];
        for (let i in generationNodes) {
            let children = generationNodes[i].children;
            if(Array.isArray(children)) {
                for (let child in children) {
                    let c = children[child];
                    generation.push(createFolderHtml(c, container));
                    drawFolderLinkToHtml(getFolderHtmlId(generationNodes[i].parent), getFolderHtmlId(c), connector, isChild);
                    nextGeneration.push({parent: c, children: c.children});
                }
            }
        }
        if (nextGeneration.length > 0) {
            browse(nextGeneration, generations, container, connector, isChild);
        }
        generations.push(generation);
    }
}

/**
 * Compute the y position of each generations in the tree
 *
 * @param generations the array of hierarchy generations
 * @return the total height of generations
 */
function computePositionYFoldersGenerations(generations){
    let top = 0;
    TOP_HEIGHT = 120;
    for(let g in generations){
        for(let f in generations[g]){
            generations[g][f].css({top:top,left:0});
        }
        top += TOP_HEIGHT;
    }

    return top;
}

/**
 * Compute the x position of each parents and children elements in the tree by taking as reference the currentNode
 *
 * @param generations the array of hierarchy generations
 * @param container the html container of the hierarchy tree
 */
function computePositionXFoldersGenerations(generations, container){
    var MARGIN_WIDTH = 15;

    for(let i in generations) {
        var generation = generations[i];
        var width = MARGIN_WIDTH;
        container.width(MARGIN_WIDTH);
        for(let j in generation) {
            let elem_width = generation[j].width();
            container.width(width + elem_width + MARGIN_WIDTH);
            generation[j].css({left: getBNumberAsPixel(width)});
            width = container.width();
        }
    }
}

/**
 * Center the given folder on is container
 *
 * @param folder the html element to centered
 * @param container the html container of the hierarchy tree
 */
function centerInContainer(folder, container){
    folder.css({left: (container.width() / 2) - (folder.width() / 2)});
}

/**
 * Create a connector instance for draw the hierarchy direction with arrows
 */
function initLinkConnector(){
    let lineConnector = jsPlumb.getInstance();
    lineConnector.importDefaults({
        Connector : [ "Flowchart", { } ]
    });
    return lineConnector;
}

/**
 * Draw the folder as an html element and draw the hierarchy arrow too
 *
 * @param leftNode a folder node to place relatively to the rightNode
 * @param rightNode another folder node to place relatively to the leftNode
 * @param lineConnector an instance for draw the hierarchy direction with arrows
 * @param isChild true if the given hierarchy is the child hierarchy of the current node, false otherwise
 */
function drawFolderLinkToHtml(leftNode, rightNode, lineConnector, isChild){
    lineConnector.connect({
        source : (isChild ? rightNode : leftNode),
        target : (isChild ? leftNode : rightNode),
        anchor:["BottomCenter", "TopCenter"],
        overlays:[
            [ "Arrow", { location:1, direction:0} ]
        ],
        endpoint:[ "Rectangle", { width:1, height:1 } ]
    });
    lineConnector.draggable(leftNode);
    lineConnector.draggable(rightNode);
}

/**
 * Get the given value as pixel
 *
 * @param number a number
 * @return the value in pixel
 */
function getBNumberAsPixel(number){
    return number + "px";
}