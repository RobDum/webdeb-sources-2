/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

/*!
 * dataLoader v1.0 is a class to display data with an async loading
 *
 * Includes jquery.js
 * https://jquery.com/
 *
 * Includes webdeb-pager.js
 */
class DataLoader {

    constructor(container, options){
        this.initContainer(container);
        this.initOptions(options);
        this.initPager();
        this.initFilterBtn();
        this.initSpinner();
        this.initListener();

        this.INDEX_STATE_RESET = 0;
        this.INDEX_STATE_INCREASE = 1;
        this.INDEX_STATE_NOTHING = 2;

        this.fromIndex = 0;
        this.numberOfLoadedData = 0;
        this.toIndex = this.eOptions.step;
        this.spinner_timeout = null;
        this.allDataLoaded = false;
        this.filterbar = null;

        if(this.eOptions.params != null || this.eOptions.firstExec){
            this.exec();
        }
    }

    initContainer(container){
        this.containerClassName = 'dataloader-container';
        this.container = container instanceof jQuery ? container : $(container);
        if(!container.hasClass(this.containerClassName)){
            container.addClass(this.containerClassName);
        }

        this.pageableClassName = 'pageable';
        if(!container.hasClass(this.pageableClassName)){
            container.addClass(this.pageableClassName);
        }

    }

    initOptions(options){
        this.eOptions = {
            toCallBefore : options.toCallBefore,
            params : options.params === undefined ? null : Array.isArray(options.params) ? options.params : [options.params],
            toCallAfter : options.toCallAfter === undefined ? null : options.toCallAfter,
            step : options.step === undefined ? 100 : options.step,
            emptyFormerDataOnIndexMove : options.emptyFormerDataOnIndexMove === undefined ? false : options.emptyFormerDataOnIndexMove,
            firstExec : options.firstExec === undefined ? false : options.firstExec,
            enableFilters : options.enableFilters === undefined ? true : options.enableFilters,
            filterButtonContainer : options.filterButtonContainer === undefined || !(options.filterButtonContainer instanceof jQuery) ? null : options.filterButtonContainer,
            pager : {
                perPage : options.pager === undefined || options.pager.perPage === undefined ? 10 : options.pager.perPage,
                callback : options.pager === undefined ? undefined : options.pager.callback,
                goToTop : options.pager === undefined ? undefined : options.pager.goToTop,
                onlyArrows : options.pager === undefined || options.pager.onlyArrows === undefined ? false : options.pager.onlyArrows
            }
        };
    }

    initPager(){
        let that = this;

        let container = createPagerContainer(that.container);
        that.sub_container =  container[0];
        that.pager_container = container[1];

        that.pager = new Pager(that.container, that.eOptions.pager.perPage, that.pager_container,
            that.eOptions.pager.callback, that.eOptions.pager.goToTop, that.eOptions.pager.onlyArrows);

        that.load_more_btn = $('<button type="button" class="btn" style="margin-left : 15px">' + Messages("general.btn.load.more") + '</button>').appendTo(that.sub_container);
        that.load_more_btn.hide();
    }

    initFilterBtn(){
        this.filterButtonClassName = "dataloader-filterbtn";
        this.filterButton = this.eOptions.enableFilters ? createFilterButton(this.filterButtonClassName, this.eOptions.filterButtonContainer == null)
            .insertBefore(this.eOptions.filterButtonContainer == null ? this.container : this.eOptions.filterButtonContainer) : null;
    }

    initSpinner(){
        this.spinner = $('<div class="text-muted huge-font"><span class="fa fa-spinner fa-spin"></span>' +
            '&nbsp;' + Messages("general.dialog.wait") + '</div>').insertBefore(this.container);
        this.spinner.hide();
    }

    initListener(){
        let that = this;

        /*
         * Button to ask to load more data async
         */
        that.load_more_btn.on('click', function(){
            if(that.numberOfLoadedData > 0 || !this.allDataLoaded){
                that.exec(undefined, that.INDEX_STATE_INCREASE);
            }
        });

        /*
         * Button to ask to reload current data
         */
        $(document).on('dataloader-loaded', function(){
            that.exec(undefined, that.INDEX_STATE_NOTHING);
        });

        /*
         * Event triggered when the container is focused
         */
        that.container.on('dataloader-container-focused', function(){
            let buttons = $('.' + that.filterButtonClassName);

            if(buttons.exists())
                buttons.hide();

            if(that.eOptions.enableFilters && that.filterbar != null){
                handleFilter(that.filterButton, that.pager, that.container, null, null, true, that.filterbar);
            }

            if(that.pager !== undefined)
                that.pager.reset(true);
        });
    }

    static triggerDataLoaded(){
        $(document).trigger("dataloader-loaded");
    }

    exec(args, indexState) {
        args = args === undefined ? this.eOptions.params : Array.isArray(args) ? args : [args];
        indexState = indexState === undefined ? this.INDEX_STATE_RESET : indexState;

        this.eOptions.params = args.slice(0);
        let that = this;

        this.spinner.show();
        that.container.hide();
        this.load_more_btn.hide();
        this.numberOfLoadedData = 0;

        that.spinner_timeout = setTimeout(function () {
            showMe(that.spinner, false, true);
        }, 5000);

        switch(indexState){
            case this.INDEX_STATE_INCREASE :
                that.increaseIndexes();
                break;
            case this.INDEX_STATE_RESET :
                that.resetIndexes();
                break;
        }

        args.push(this.fromIndex);
        args.push(this.toIndex);

        that.eOptions.toCallBefore.apply(this, args).done(function (data, textStatus, xhr) {

            if(data !== undefined){
                that.numberOfLoadedData = isNaN(data.length) ? (isNaN(data.size) ? 0 : data.size) : data.length;
                if(that.numberOfLoadedData >= that.eOptions.step || xhr.status === 206){
                    that.load_more_btn.show();
                }
            }

            if(that.eOptions.emptyFormerDataOnIndexMove || indexState !== that.INDEX_STATE_INCREASE) {
                that.container.empty();
                that.filterbar = null;
            }

            if(that.eOptions.enableFilters && data.filters !== undefined){
                that.filterbar = createFilterbar(JSON.parse(data.filters), that.filterbar);
                if(elementIsVisible(that.container.parent()))
                    handleFilter(that.filterButton, that.pager, that.container, null, null, null, that.filterbar);
            }

            if(that.eOptions.toCallAfter == null && data !== undefined) {
                if(data.nodata !== undefined && (that.eOptions.emptyFormerDataOnIndexMove || indexState !== that.INDEX_STATE_INCREASE)) {
                    that.container.append(JSON.parse(data.nodata));
                }else if(data.data !== undefined){
                    that.container.append(data.data);
                }
            }else{
                that.eOptions.toCallAfter(data, that.container);
            }
            that.pager.reset(true);

        }).fail(function (jqXHR) {
            if(that.eOptions.toCallAfter !== undefined) {
                that.eOptions.toCallAfter(jqXHR, that.container);
            }
        }).always(function (jqXHR) {
            clearTimeout(that.spinner_timeout);
            showMe(that.spinner, false, true);
            that.container.show();
        });
    }

    resetIndexes() {
        this.fromIndex = 0;
        this.toIndex = this.eOptions.step;
    }

    increaseIndexes() {
        this.fromIndex += this.eOptions.step;
        this.toIndex += this.eOptions.step;
    }

}