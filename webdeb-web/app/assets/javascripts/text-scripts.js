/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * This javascript file contains all reusable function for text contributions' screens
 *
 * @author Fabian Gilson
 * @author Nicolas Forêt (initial contributor of JustificationTree)
 * @author Martin Rouffiange
 */

/*
 * ADD / EDIT TEXT
 */

/**
 * Get all text source names and auto-compleiton to given element
 *
 * @param element an hmtl element id
 * @param key the key (field from retrieved jspn data) to the field to be displayed
 */
function addSourceNameTypeahead(element, key) {
	$(element).typeahead({
		hint: false,
		highlight: true,
		autoselect: false,
		minLength: 2,
		limit: MAX_TYPEAHEAD
	}, {
		displayKey: key,
		source: function (query, process) {
			setTimeout(function () {
				return searchTextSource(query, MIN_TYPEAHEAD, MAX_TYPEAHEAD).done(function (data) {
					return process($.makeArray(data));
				});
			}, 300);
		},
		templates: {
			suggestion: function (item) {
				return item.sourceName;
			}
		}
	});
}

/**
 * Manage toggling of textType
 */
function manageTextType() {
  var boxes = $('#b-textType').find('input[type="checkbox"]');
  var field = $('#actortype');
  manageExclusiveCheckboxes(boxes, field, true);
}

/**
 * Manage typeahead selection of a text in fill information with data
 *
 *    @param element the html field where the typeahead will be put
 */
function manageExistingText(element) {
  addTextTypeahead(element, false);
    $(element).on('typeahead:selected', function (obj, datum) {
        // create modal popup for confirmation
        bootbox.dialog({
            message: Messages("entry.text.existing.text"),
            title: Messages("entry.text.existing.title"),
            buttons: {
                main: {
                    // do nothing if user chose not to load existing text
                    label: Messages("entry.text.existing.nok"),
                    className: "btn-primary",
                    callback: function () { /* do nothing, just dispose frame */
                    }
                },
                modify: {
                    label: Messages("entry.text.existing.ok"),
                    className: "btn-default",
                    callback: function () {
                        redirectToEdit(datum.id);
                    }
                }
            }
        });
    });
}

/**
 * Reload workwith text page when text is selected from pick list
 *
 * @param element the element to attache the typeahead to
 */
function textTypeahead(element) {
  addTextTypeahead(element, false);
  $(element).on('typeahead:selected', function (obj, datum) {
		redirectToTextExcerpts(datum.id);
  });
}

/**
 * Call annotation service for a given text
 *
 * @param container the container where add annotation
 * @param data previous loaded data if any
 * @param textid the id of the text to retrieve the annotated text
 * @param widthSize the container width size
 * @param heightSize the container hight size
 */
function updateAnnotated(container, data, textid, widthSize, heightSize) {
  if (textid !== -1) {
      if (data != null) {
          loadTextarea(container, data, widthSize, heightSize);
          return data;
      }else{
        var r = "";
        // launch ajax request to get annotated text
        getAnnotatedText(textid, true).done(function (data) {
            let objData = JSON.parse(data);

            // ensure user did not load another one
            if ($('#text-id').val() === textid) {
                loadTextarea(container, objData, widthSize, heightSize);
            }
            r = objData;
        }).fail(function (jqXHR) {
            if (jqXHR.status === 401) {
                loadTextarea(container, jqXHR.responseJSON, widthSize, heightSize);
                container.find('#textarea').html('<h4 class="text-muted">' + Messages('text.args.unautautorized') + '</h4>');

            } else if (jqXHR.status === 404) {
                loadTextarea(container, jqXHR.responseJSON, widthSize, heightSize);
                container.find('#textarea').html(jqXHR.responseJSON);
            } else {
                // will receive the full text back
                loadTextarea(container, jqXHR.responseJSON, widthSize, heightSize);
                showMe(container.find('#annotation-error'), true, true);
                showMe(container.find('#nlpboxes'), false, false);
            }
        });
        return r;
    }
  }
  return "";
}

/**
 * Load a text into a textarea, hide preview message and show legend
 *
 * @param container the container where add annotation
 * @param data the data to be loaded into the textarea
 * @param widthSize the container width size
 * @param heightSize the container hight size
 */
function loadTextarea(container, data, widthSize, heightSize) {
  showMe(container.find('#annotation-error'), false, false);
  var textarea = container.find('#textarea');

  var textareaWidthTemp = widthSize === undefined ? textarea.width() : widthSize;
  textarea.width((textareaWidthTemp-10)/2);
  textarea.css("height", "60vh");
  container.find('.next-page-indicator').parent().height(textarea.height());
  showMe(textarea, false, false);

  if(typeof data === 'object') {
      textarea.empty().append(data.text);
      textarea.annotator(data.excerpts, window.location.origin, false);
  }else{
      textarea.empty().append(data);
  }

  showMe(container.find('#preview'), false, true);
  showMe(textarea, true, true);
  showMe(container.find('#nlpboxes'), true, false);

  var textareaHeightTemp = heightSize === undefined ?  textarea.prop('scrollHeight') : heightSize;
  textarea.width(textareaWidthTemp);

  togglexxs();
  textPager(textarea, textareaHeightTemp);

  // detect window resize to reload text content
  var doit;
  window.onresize = function (){
      clearTimeout(doit);
      doit = setTimeout(function() {
          updateAnnotated(container, data, $('#text-id').val(), widthSize, heightSize);
      }, 50);
  };

  // auto-paging and highlights
  autoPageAndHighlights(textarea);
  initializeSearchText(textarea, true);
  showMe($('#search'), false, true);
  showMe($('#searchtext'), true, true);
  showMe($('#show-search'), true, true);
  updateTextarea(textarea);
}

/**
 * Register highlighter plugin for new excerpt extraction from text
 *
 * @param container the container concerned by the highlight
 * @param textareadiv the div containing the text
 * @param yGap true if there is a gap in Y axe
 */
// TODO refaire totalement highlighter
function setHighlighter(container, textareadiv, yGap) {
  yGap = yGap || false;
  $(textareadiv).highlighter({
    'selector': '.highlighter-container',
    'minWords': 2,
    'complete': function (data) {
      // must manually set min-width, buggy plugin
      let highlighter_container = container.find(".highlighter-container");

      highlighter_container.find(".popover").css("min-width", "100%");
      highlighter_container.find("#originalExcerpt").val(data.replace('\n', ' '));
    }
  });

  $(textareadiv).on('mouseup', function(e){
      let decal = container.find(".modal-content").exists() ? container.find(".modal-content").offset().top * -1 : 0;
      let clientY = e.pageY - (yGap ? 10 : 20) + decal;
      let clientX = e.pageX;

      let minX = $(textareadiv).offset().left;
      let maxX = $(textareadiv).offset().left + $(textareadiv).width();

      clientX = clientX < minX ? minX : (clientX > maxX ? maxX : clientX);

      setTimeout(function(){
          var highlighter_container = container.find(".highlighter-container");

          if(highlighter_container.exists()) {
              highlighter_container.css("top", clientY + "px");
              highlighter_container.css("left", clientX + "px");
          }
      }, 500);
  });
}
/**
 * Show icon for auto-paging
 *
 * @param textareadiv div containing the text area
 */
function autoPageAndHighlights(textareadiv) {
  var mouseDown = 0;
  var nextpage = textareadiv.parents("#textzone").parent().find('.next-page-indicator');

  // show button to auto-page
  var doit;
  if(nextpage.exists()) {
      textareadiv.on('mousedown', function () {
          ++mouseDown;
          clearTimeout(doit);
          doit = setTimeout(function () {
              nextpage.show();
          }, 1000);

      }).on('mouseup', function () {
          clearTimeout(doit);
          nextpage.hide();
          --mouseDown;

      }).on('mouseout', function (e) {
          // must recalculate offset of next-page-indicator each time (since it needs to be visible)
          var offset = nextpage.offset();
          if (e.pageY > offset.top && (e.pageY < offset.top + 25)
              && (e.pageX > offset.left - 10) && (e.pageX < offset.left + 25)
              && mouseDown && nextpage.is(':visible')) {

              $(document).trigger('next-page');
              nextpage.hide();
          }
      });

      $(document).on('mousemove', function (e) {
          var offset = nextpage.offset();
          if (e.pageY > offset.top && (e.pageY < offset.top + 25)
              && (e.pageX > offset.left - 10) && (e.pageX < offset.left + 25)
              && mouseDown && nextpage.is(':visible')) {
              $(document).trigger('next-page');
              nextpage.hide();
          }
      });
  }

  // display excerpt on hover
  textareadiv.find('span.highlight').each(function () {
    $(this).popover(); // opt-in init

    $(this).on('mouseenter', function () {
      if (mouseDown === 0) {
        $(this).popover("show");
      }
    }).on('mouseleave', function () {
      $(this).popover("hide");

    }).on('click', function () {
      var popup = $('#popover-' + $(this).attr('data-excid'));
      popup.popover("toggle");
      // must handle size by hand on demand
      var popover = $(popup).next('.popover');
      // must add manually class to effective popup if linked span has class
      if (popup.hasClass('btn-only')) {
        $(popover).addClass("btn-only");
      }
    });

    // close on click outside
    $('body').on('click', function (e) {
      if (!($(e.target).parent().hasClass('highlight')
          || $(e.target).hasClass('highlight'))
          || $(e.target).parent().hasClass('popover')) {
        $(".popoverbox").each(function () {
          $(this).popover("hide");
        });
      }
    });
  });
}

/**
 * Get a text content from an url and fill in form with response, if any
 *
 * @param url the url to make a request to
 * @param waitForIt the wait-for-it panel to display while loading text
 */
function getTextFromUrl(url, waitForIt) {
	if (url.val().trim().length > 0) {
        waitForIt.modal('show');
		// check on type, if ends with .pdf => ignore call
		if (url.val().indexOf('.pdf') < 0) {
            extractWebContent(url, waitForIt);
		} else {
            extractPDFContentAction(url, waitForIt);
        }
	}
}

/**
 * Extract data from given url pointing to a web page to get the content from (typically a press article)
 *
 * @param url an url
 * @param waitForIt the "waiting pane" to hide at the end of the process
 */
function extractWebContent(url, waitForIt) {
  getHtmlContent(url.val()).done(function (data) {
      let title = $('#title');
      let textarea = $('#textarea');
      let source = $('#sourceTitle');
      let publicationDate = $('#publicationDate');
      let authors = $('#authors');
      let domainName = null;

      url.parents('.form-group').addClass('has-success');

      // fill title and highlight element
      if(data.title != null && data.title.length > 0) {
          title.typeahead('val', data.title);
          title.parents('.form-group').addClass('has-success');
      }

      let urls = new URL(url.val()).hostname.split('.');

      if(urls.length >= 2){
          domainName = urls[urls.length-2] + "." + urls[urls.length-1];
          source.find('input').val(domainName).parent('.form-group').addClass('has-success');
      }

      // fill content and highlight element
      if(domainName != null) {
          checkFreeSource(domainName).done(function (data2) {
              updateTextContent(data2, textarea, data.content);
          }).fail(function (error) {
              updateTextContent(true, textarea, data.content);
          });
      }else{
          updateTextContent(true, textarea, data.content);
      }

      // try to parse date and highlight element
      if (data.date != null && data.date.length > 0) {
          publicationDate.val(extractDateFromString(data.date)).parent('.form-group').addClass('has-success');
      }

      // fill authors
      if (data.author != null && data.author.length > 0) {
        // clear all author values
          authors.find('.btn-remove').each(function () {
          removeGenericEntry(this);
        });

         authors.find('[name="authors[0].fullname"]').typeahead('val', data.author).parents('.form-group').addClass('has-success');
      }

      hideAndDestroyModal(waitForIt);

  }).fail(function (jqXHR) {
    handleFailedExtract(url, waitForIt, jqXHR.responseText)
  });
}

function updateTextContent(isFree, textarea, content){
    if(isFree){
        textarea.attr("readonly", false).html(makeTextContentReadable(content))
            .parent('.form-group').addClass('has-success').removeClass('has-error');
    }else{
        textarea.attr("readonly", true).html(" ")
            .parent('.form-group').addClass('has-error').removeClass('has-success');
    }
}

/**
 * Extract data from given url pointing to a pdf to get the content from
 *
 * @param url an url
 * @param waitForIt the "waiting pane" to hide at the end of the process
 */
function extractPDFContentAction(url, waitForIt) {
  extractPDFContent(url.val()).done(function (data) {
    $("#language").val(data.language).change().parent('.form-group').addClass('has-success');
    var ch = splitExtractHead(data.text);
    $('#textarea').val(ch).parent('.form-group').addClass('has-success');
    onPreExecuteExtractPDFContentAction(waitForIt);
    showInformativePopup("text.extract.", data.text);

  }).fail(function (jqXHR) {
      console.log(jqXHR);
    onPreExecuteExtractPDFContentAction(waitForIt);
    showInformativePopup("text.extract.error.", "");
    //handleFailedExtract(url, waitForIt, jqXHR.responseText);
  });
}

/**
 * Split the head of the extracted text from PDF and ignore it
 *
 * @param text the extracted text
 * @return the reconstructed text
 */
function splitExtractHead(text){
  var resultText = "";
  if(typeof text === 'string' || text instanceof String) {
    var splitted = text.split("\n");
    if (splitted.length >= 3) {
      for (var i = 3; i < splitted.length; i++) {
        resultText += splitted[i] + "\n";
      }
    }
  }
  return resultText;
}

/**
 * Instructions to excecute before the extraction
 *
 * @param waitForIt the "waiting pane" to hide at the end of the process
 */
function onPreExecuteExtractPDFContentAction(waitForIt){
  hideAndDestroyModal(waitForIt);
  showMe('#upload-div', true, true);
}

/**
 * Handle failed retrieval of the content from a given url. Will display content of given response in
 * dedicated message div (present div anchor with id "msg-div" present in main html template)
 *
 * @param urlInput the input of the url (to be highlighted)
 * @param waitForIt the pane to ask user to wait for the end of the process
 * @param response the html code of the response msg to be rendered
 */
function handleFailedExtract(urlInput, waitForIt, response) {
  hideAndDestroyModal(waitForIt);
  $('#msg-div').append(response);
  urlInput.parent('.form-group').addClass('has-warning');
  fadeMessage();
}

/**
 * Handle modal frame where texts with same title are displayed to ask user if his newly introduced
 * text is not already present (mainly used for text that could have been added privately to avoid
 * duplicates)
 *
 * @param isNotSame hidden input element that will hold "true" to tell that this text must be created
 * @param submit the modal submit button
 */
function handleCandidates(isNotSame, submit) {
	var modal = "#text_candidate_modal";
	var okbtn = "#createnew";
	var loadbtn = '#load';

	toggleSummaryBoxes('#candidate-boxes', '.toggleable');
	$(modal).modal('show');

	// handle click on create a "new text" button
	$(modal).on('click', okbtn, function () {
		isNotSame.val('true');
        hideAndDestroyModal(modal);
        submit.off();
		submit.trigger('click');
	});

	// handle click on load, simply reload page with known text
	$(modal).on('click', loadbtn, function () {
		redirectToEdit($('.selected').prop('id').split("_")[1]);
	});
}

//////////////////////////////////////////////////////////////////////////////////////Search Text

function handleSearchText(container){
    container.find('#searchtextbtn').on('click', function(e) {
        e.preventDefault();
        var query = $('#searchtext').find("input[type=text]").val();
        $('#searchtextbtn').blur();
        doSearchText(query);
    });

    container.find('#query_searchtext').keypress(function(e) {
        if (e.which === 13) {
            e.preventDefault();
            var query = $('#searchtext').find("input[type=text]").val();
            doSearchText(query);
        }
    });
}

var searchTextResultManager = null;

/**
 * Initialize the search text manager
 *
 */
function initializeSearchText(textarea, textpager) {
    searchTextResultManager = new SearchTextResultManager(textarea, textpager);
    searchTextResultListener();
}

/**
 * Manage search inside the text
 * @param query a string to search inside the text
 *
 */
function doSearchText(query){
  if(searchTextResultManager instanceof SearchTextResultManager)
    searchTextResultManager.setSearch(query);
}

/**
 * Prepare the search text function, verify the textarea and the query
 *
 * param textarea a JQuery object of the text to browse
 * param query a string to search inside the text
 * @return the number of results
 */
function textSearchHandler(textarea, query){
  var nbResults = 0;
  if(textarea instanceof jQuery && typeof query === 'string'
      && query.length > 0 && query.replace(/\s/g, '').length){
    nbResults = textSearch(textarea, query, nbResults);
  }
  return nbResults;
}

/**
 * Search a string inside a text
 *
 * @param elem the Jquery elem to browse
 * @param query the string to search inside the text
 * @param nbResults the number of results in the text
 * @return the number of results
 */
function textSearch(elem, query, nbResults){
  elem.contents().each(function () {
    var elemChild = $(this);
    if(!isNodeText(elemChild)){
      nbResults = textSearch(elemChild, query, nbResults);
    }
    else{
      nbResults = findMatchAndhighlightSearchResults(elemChild, query, nbResults);
    }
  });
  return nbResults;
}

/**
 * Draw search result after the updating of the textarea
 *
 * @param textarea the textarea to update
 */
function updateTextarea(textarea){
  if(searchTextResultManager instanceof SearchTextResultManager && searchTextResultManager.hasTextarea()){
    searchTextResultManager.carryOutSearch(textarea);
  }
}

/**
 * Search a string on a part of the text
 *
 * @param textNode the Jquery element pointing on the text to perform the search
 * @param query the string to search inside the text
 * @param nbResults the number of results in the text
 * @return the number of results
 */
function findMatchAndhighlightSearchResults(textNode, query, nbResults){
    var chfinal = "";
    var textString = textNode.text();
    var r = searchString(textString, query);
    while (r > -1) {
        //Separate the string in three part, before the query match, the match and after the match
        var chpre = textString.substring(0, r);
        var chquery = textString.substring(r, r + query.length);
        textString = textString.substring(r + query.length, textString.length);
        chfinal += chpre + "<span id='searchTextResult" + (nbResults++) + "' class='searchTextResult searchTextResultSpan'>" + chquery + "</span>";
        //Search after another match in the text
        r = searchString(textString, query);
    }
    if(chfinal.length > 0){
        //Replace the text content by the new one
        elemTextReplace(textNode, chfinal+textString);
    }
    return nbResults;
}

/**
 * Remove all annotation of search results
 *
 */
function removeSearchResults(){
  var resultNodes = $(".searchTextResultSpan");
  resultNodes.each(function () {
    $(this).contents().unwrap();
  });
}

/**
 * Search a string (query) on another one (textString)
 *
 * @param textString the string to perform the search
 * @param query the string to search inside the textString
 */
function searchString(textString, query){
  var textStringToSearch = textString.toLowerCase();
  return textStringToSearch.search(query);
}

/**
 * Replace a Jquery node text content
 *
 * @param the element which the content must be remplaced
 * @param content the content that remplace the content node
 */
function elemTextReplace(elem, content){
  var textNodePerm = elem.after(content);
  elem.remove();
  return textNodePerm;
}

/**
 * Manage the events linked with the display of the results of a search text
 *
 */
function searchTextResultListener(){
  $(document).on( "keydown",function(e) {
    if (searchTextResultManager instanceof SearchTextResultManager && searchTextResultManager.inSearch){
      switch(e.keyCode) {
        case 13 : //Enter
        case 39 : //Cursor right
          searchTextResultManager.nextResult();
          break;
        case 37 : //Cursor left
          searchTextResultManager.previousResult();
          break;
        case 27 : //Escape
          removeSearchResults();
          break;
        default:
          // ignore
      }
    }
  });
  /*$(window).on('hashchange', function(e){
    if (searchTextResultManager instanceof SearchTextResultManager && searchTextResultManager.inSearch) {
      var result = getUrlParam("searchTextResult");
      console.log(result);
      //searchTextResultManager.changeResult(anchor.substr("searchTextResult".length, anchor.length));
    }
  });*/
}

/**
 * Determine if a DOM node is a text (return true if it is)
 *
 * @param node the node to determine
 */
function isNodeText(node){
  //3 is for a jQuery text node type
  return node.get(0).nodeType === 3;

}

function unlockSearch(){
  if (searchTextResultManager instanceof SearchTextResultManager && searchTextResultManager.inSearch){
    searchTextResultManager.unlock();
  }
}

/**
 * Class that manage the search inside a text
 *
 */
class SearchTextResultManager {
  constructor(textarea, textpager) {
    this.setTextarea(textarea);
    this.textpager = textpager;
    this.locked = false;
    this.resultId = "#searchTextResult";
    this.inSearch = false;
    this.initialize();
  }

  hasTextarea(){
    return this.textarea instanceof jQuery;
  }

  getAnchorLink(){
    if(this.hasTextarea()){
      if(this.textpager) {
        return "#"+this.textarea.attr("id");
      }
      else{
        return this.getCurrentSearchResultNodeName();
      }
    }
  }

  initialize(query){
    query = query || "";
    if(this.verifyQuery(query)){
      this.query = query.toLowerCase();
    }else{
      this.query = "";
    }
    this.nbResults = 0;
    this.currentResult = -1;
    this.resetTextarea();
  }

  verifyQuery(query){
    return typeof query === 'string' && query.length > 0;
  }

  resetTextarea(){
    if(this.hasTextarea() && this.inSearch){
      removeSearchResults();
    }
  }

  hasTextPager(){
    return this.textpager;
  }

  getCurrentSearchResultNodeName(){
    return this.resultId + this.currentResult;
  }

  getFirstSearchResultNode(){
    if(this.inSearch) {
      var node = $(this.resultId + 0);
      if(node !== undefined)
        return node;
    }
    return false;
  }

  getCurrentSearchResultNode(){
    if(this.inSearch && this.currentResult >= 0) {
      return $(this.getCurrentSearchResultNodeName());
    }
    return false;
  }

  setTextarea(textarea){
    this.textarea = textarea;
    if(textarea instanceof jQuery){
      if(textarea.attr("id") === undefined){
        textarea.attr("id", "searchTextArea");
      }
      this.textareaClone = textarea.children();
    }
  }

  setSearch(query){
    if(this.hasTextarea()) {
      this.initialize(query);
      return this.carryOutSearch();
    }
    return false;
  }

  carryOutSearch(textarea){
    textarea = textarea || this.textarea;
    if(this.hasTextarea() && this.verifyQuery(this.query)) {
      var search = textSearchHandler(textarea, this.query);
      if (!search || search <= 0) {
        this.inSearch = false;
      }
      else {
        this.inSearch = true;
        this.nbResults = search;
        this.nextResult();
      }
      return this.inSearch;
    }
    return false;
  }

  previousResult(){
    return this.changeResult(this.currentResult-1);
  }

  nextResult(){
    return this.changeResult(this.currentResult+1);
  }

  changeResult(nbr){
    var nbrInt = parseInt(nbr);
    if(this.verifySearchStatus() && this.isLock() && isInt(nbr) && nbrInt >= 0 && nbrInt < this.nbResults){
      this.locker();
      this.resetCurrentResult();
      this.currentResult = nbrInt;
      this.setCurrentResult();
      this.anchorPage();
      return this.getCurrentSearchResultNodeName();
    }
    return false;
  }

  verifySearchStatus(){
    return this.getFirstSearchResultNode();

  }

  setCurrentResult(){
    var rElem = this.getCurrentSearchResultNode();
    if(rElem) {
      rElem.addClass("searchTextResultPointed");
      rElem.removeClass("searchTextResult");
    }
    return false;
  }

  resetCurrentResult(){
    var rElem = this.getCurrentSearchResultNode();
    if(rElem && rElem.hasClass("searchTextResultPointed")) {
      rElem.removeClass("searchTextResultPointed");
      rElem.addClass("searchTextResult");
    }
    return false;
  }

  anchorPage(){
    var searchNode = this.getCurrentSearchResultNode();
    if (this.hasTextPager() && searchNode) {
      $(document).trigger("textPage", [searchNode.position().left]);
    }

    window.location.href = this.getAnchorLink();
  }

  locker(){
    this.lock();
    setTimeout(unlockSearch, 400);
  }

  lock(){
    this.locked = true;
  }

  unlock(){
    this.locked = false;
  }

  isLock(){
    return !this.locked;
  }
}
