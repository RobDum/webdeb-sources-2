/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * This javascript file contains the script for creation of a justification tree for arguments in a context
 *
 * @author Fabian Gilson
 * @author Nicolas Forêt (initial contributor of JustificationTree)
 * @author Martin Rouffiange
 */

/**
 * Create a drag and drop facility for argument links.
 *
 * @param linksdiv justification div where all links are displayed in a tree
 * @param actiondiv the div selector containing the buttons to enable when this view is updated and to show loading message
 * @param waitforit spinner bar to display the load time to the user
 * @param contextId the contextualized contribution where links come from
 * @param firstArgumentId the contextualized argument id of the first argument of the context
 * @param linkShade the link shade type to filter on, or null
 * means we must display a non editable tree with no arguments)
 * @param firstWithoutShade true if the first argument line must be desplayed without shade
 * @param levelThreshold the number of argumentation level to display by default
 * @constructor
 */
class JustificationTree {
    constructor(linksdiv, actiondiv, waitforit, contextId, firstArgumentId, linkShade, firstWithoutShade, levelThreshold) {
        var that = this;
        that.actiondiv = actiondiv;
        that.waitforit = waitforit;
        that.waitforit.show();

        that.JUSTIFY = 2;
        that.REJECT = 3;
        that.CITATION_OK = 4;
        that.CITATION_SHADED = 5;
        that.CITATION_NOTOK = 6;
        that.ALL = -1;
        linkShade = linkShade != null && linkShade >= that.JUSTIFY && linkShade <= that.REJECT ? linkShade : that.ALL;
        that.linkShade = linkShade;
        that.firstWithoutShade = firstWithoutShade !== undefined ? firstWithoutShade : false;

        that.firstArgumentId = firstArgumentId != null && firstArgumentId !== undefined ? firstArgumentId : null;
        that.contextId = contextId;
        that.context = null;

        that.linksdiv = linksdiv;
        // default values
        that.config = {
            // icons
            iconRemove: 'far fa-times-circle',
            iconExpanded: 'fas fa-caret-square-down',
            iconCollapsed: 'fas fa-caret-square-right',
            iconExpandedIcon: 'caret-square-down',
            iconCollapsedIcon: 'caret-square-right',
            // link types (hardcoded...)
            linkTypes: [-1, 6, 7, 8],
            linkTypesText: [Messages("text.links.shade.select"), Messages("argument.links.shade.2"), Messages("argument.links.shade.3")], // Texte des types de lien
            // amount of visible items
            maxVisibleItems: 15,
            // amount of nested levels, 2 by default
            levelThreshold: levelThreshold !== undefined ? levelThreshold : 1
        };
        //argument.links.shade.2=fas fa-thumbs-up
        // argument.links.shade.3=fas fa-thumbs-down

        that.arguments = new Map(); // contains all arguments
        that.links = new Map(); // contains all links
        that.nestedLinks = new Map(); // contains all links as nested elements
        that.used = []; // contains all arguments present in links
        that.unused = []; // contains all arguments no present in links
        that.level = []; // used to build tree-view (nested levels)
        that.newLinkId = 0; // counter for new ids
        that.modified = false;
        that.maxCircles = 1;

        argumentOptionsHandler(that.linksdiv);

        // reinitialize the tree when a realod-justtre is triggered
        $(document).on("reload-viz", function(){
            that.init();
        });
    }

    /**
     * Manage ajax calls to retrieve arguments and links
     */
    init() {
        var that = this;
        if(that.firstArgumentId == null || that.firstArgumentId === -1) {
            getFirstArgument(that.contextId).done(function (data) {
                that.firstArgumentId = data != null && data !== undefined && data !== -1 ? data : null;
            });
        }

        getContextContribution(that.contextId).done(function (data) {
            that.context = data;

            var justs = that.linkShade === that.ALL ?
            getJustificationLinks(that.contextId) : getJustificationLinksByShade(that.contextId, that.linkShade);
            justs.done(function (data) {
                that.links = new Map();
                $.each(data, function (key, link) {
                    that.links.set(link.id, link);
                });

                var args = that.linkShade === that.ALL ?
                getContextualizedArguments(that.contextId) : getContextualizedArgumentsByShade(that.contextId, that.linkShade);
                args.done(function (data) {
                    that.arguments = new Map();
                    $.each(data, function (key, argument) {
                        that.arguments.set(argument.id, argument);
                    });
                    that.buildTree();
                    $(document).trigger("justification-tree-loaded");
                });
            });
        });
    }

    /**
     * Change de level threshold and rebuild the tree
     *
     * @param level the level threshold
     */
    changeLevelThresholdAndRebuild(level) {
        var l = parseInt(level);
        this.config.levelThreshold =
            ((isNaN(l) || l < 0 || l >= 4) ? -1 : l);
        this.buildTree();
    }

    getLevelThreshold(){
        if(this.config.levelThreshold < 1) return 1;
        if(this.config.levelThreshold >= 5) return 5;
        return this.config.levelThreshold;
    }

    /**
     * Build initial nested tree structure that will be used to create html rendering
     * Requires that the links are correctly sorted by serial number
     */
    buildTree() {
        // initialize used/unused list in case of editable tree
        this.linksdiv.empty();

        // build nested representation
        this.nestedLinks = this.strucToNestedStructure(this.links);

        this.levelsComputation(this.nestedLinks.get(0).children, 0);
        this.levelThresholdComputation(this.config.maxVisibleItems);
        this.waitforit.hide();

        this.displayLinks();
    }


    /**
     * From data in this.arguments and links, create tree-view and initializes :
     * # this.unused with ids of all arguments not present in links
     * # this.used with ids of all arguments present in links
     */
    initUsedUnused() {
        var that = this;
        var id_active = [];
        var id_inactive = [];
        that.arguments.forEach(function (aff, affID) {
            var isUsed = false;
            that.links.forEach(function (link) {
                if (affID === link.originId || affID === link.destinationId) {
                    isUsed = true;
                    return false;
                }
            }, that.links);
            if (isUsed) {
                id_active.push(affID);
            } else {
                id_inactive.push(affID);
            }
        }, that.arguments);

        that.used = id_active;
        that.unused = id_inactive;
    }

    /**
     * Simply show all links in a read-only tree
     */
    displayLinks() {
        var that = this;
        if (that.links.size > 0) {
            that.linksdiv.html(that.buildLinksHtml());
            that.updateExpandIcons();
            that.linksdiv.find('.menuDiv').on('click', '[data-fa-i2svg]', function () {
                that.initMenudivEvents($(this));
            });
        } else {
            //that.actiondiv.find('.alert-info').show();
        }
        that.linksdiv.append(that.createNewJustificationBtn());
    };

    /**
     * Create a button to add a new argument as top argument
     *
     */
    createNewJustificationBtn() {
        var source = this.firstArgumentId;
        var nodeClass, title;
        switch(this.linkShade){
            case this.JUSTIFY :
                nodeClass = "similar";
                title = Messages("debate.options.edit.argument.add.yes");
                break;
            case this.REJECT :
                nodeClass = "opposes";
                title = Messages("debate.options.edit.argument.add.no");
                break;
            default :
                nodeClass = "primary-a";
                title = Messages("debate.options.edit.argument.add");

        }

        return '<button type="button" class="btn btn-link btn-simple-link argument-add-argument-btn ' + nodeClass + '" ' +
            'data-id="' + (source == null ? -1 : source) + '" data-context-id="' + this.contextId + '" ' +
            'data-linktype="' + this.linkShade + '" type="button" style="margin-left: 27px;">' +
            '<span style="font-size : 16px"><i class="fas fa-plus-square"></i>&nbsp;' + title + '</span></button>';
    }

    /**
     * Initialize events to expand and remove menuDiv
     *
     * @param elem the menuDiv to init
     */
    initMenudivEvents(elem){
        if(elem.hasClass("expand")){
            this.expandListener(elem);
        }else if(elem.hasClass("remove")){
            this.removeListener(elem, true);
        }
    }

    /**
     * Calculate this.level used to know the amount of links per tree levels at building time
     * (this.level is not maintained)
     *
     * @param nestedLinks tree-structure with all links
     * @param level current level
     */
    levelsComputation(nestedLinks, level) {
        var that = this;
        if (typeof that.level[level] === 'undefined') {
            that.level[level] = 0;
        }
        that.level[level] += nestedLinks.length;
        nestedLinks.forEach(function (value) {
            that.levelsComputation(that.nestedLinks.get(value).children, level + 1);
        }, nestedLinks);
    }

    /**
     * Calculate the amount of tree-levels to display at display time, set in this.levelThreshold.
     * Will be used at display time
     *
     * @param max the max amount of tree-levels to be displayed
     */
    levelThresholdComputation(max) {
        var level = 0;
        var sum = this.level[level];
        if (sum > max) {
            this.levelThreshold = 0;
        } else {
            while (sum <= max) {
                level++;
                sum += this.level[level];
            }
            this.levelThreshold = level - 1;
        }
    }

    /**
     * Build html representation for all links
     *
     * @returns {string} the string representation of the generated html code for the links tree
     */
    buildLinksHtml() {
        var that = this;
        var html = '<ol data-id=0 class="sortable ui-sortable readonly">';
        that.nestedLinks.get(0).children.forEach(function (value) {
            html += that.elementToHTML(that.nestedLinks.get(value), "link", 0, that.nestedLinks.get(value).shade);
        }, that.nestedLinks);

        html += '</ol>';
        return html;
    }

    /**
     * Build html representation of given element
     *
     * @param element current element to display
     * @param type element type (either "aff" of "links")
     * @param level current tree-level (used to know if children must be expanded or not)
     * @param realShade
     * @returns {string} the nestable div element to be appended to the justification tree or argument list
     */
    elementToHTML(element, type, level, realShade) {
        var that = this;
        var html = "";
        var elemDataId = element.id;
        var argument = that.arguments.get(elemDataId);
        if(argument !== undefined && (level !== 0 || element.parent === that.firstArgumentId)) {
            var select_default_value;
            var link;

            if (type === 'link') {
                link = element.linkId === 0 ? {
                    id: 'root_to_' + element.id,
                    linkshade: -1
                } : that.links.get(element.linkId);
                realShade = realShade === 3 ? (link.linkshadeId === 3 ? 2 : 3) : link.linkshadeId;
                select_default_value =
                    element.parent === that.firstArgumentId && that.firstWithoutShade ? -1 : realShade;
            } else {
                link = {id: 'aff', linkshade: 0};
                select_default_value = -1;
            }

            var element_expanded = !(that.config.levelThreshold !== -1 && level >= that.config.levelThreshold);
            var expanded_style = element_expanded ? 'expanded' : 'collapsed';
            var li_style = type === 'aff' ? '' : ' link-' + select_default_value;

            html = '<li id=aff_' + elemDataId + ' data-id=' + elemDataId + ' data-link-id=' + link.id
                + ' class="mjs-nestedSortable-' + expanded_style + li_style + '">';

            html += '<div class="menuDiv readonly" style="display: flex!important;align-items: center;">';

            // expand / collapse element
            html += '<div title="' + Messages('text.links.tooltip.expand') + '">' +
            '<i class="expand" style="font-size : 20px !important"></i></div>';

            // add remove icon (top right)
            var pane = (that.shadeAmountIsEmpty(argument.excerptMetrics) ? 0 : 1);
            html += '<div class="itemContent" ><a class="normal-style" href="' + urlOfArgumentContextViz(elemDataId, pane) + '">'
               // + that.arguments.get(elemDataId).descriptionWithLink + '</a></div>';
                + argument.fullTitle + '</a>' + (that.linkShade === that.ALL ? '' : argument.descriptionWithLink) + '</div>';
            html += '<div class="shadesAmount" style="flex-grow: 1;display:flex!important;align-items: center;">'
                + that.shadesAmountToHTML(argument.excerptUserMetrics) + '</div>';
            html += '<div style="display:flex!important;align-items: center;">'
                + createArgumentOptions(argument.id, that.contextId, argument.contextFirstArgumentId, argument.firstArgument) + '</div>';

            // add closing div of menuDiv class element
            html += '</div>';
            level++;
        }

        // for links having children, compute html for them (may not have cycles)
        if (type === 'link' && element.children.length > 0) {
            var keys = [];
            for (var i = 0; i < element.children.length; i++) {
                keys.push([element.children[i], that.nestedLinks.get(element.children[i]).serial]);
            }
            keys.sort(function (a, b) {
                return a[1] - b[1]
            });
            html += '<ol data-id=' + element.id + '>';
            for (var x = 0; x < keys.length; x++) {
                html += that.elementToHTML(that.nestedLinks.get(keys[x][0]), type, level, realShade);
            }
            html += '</ol>';
        }
        html += '</li>';
        return html;
    }

    getNodeId(node){
        return node.parents('li[id^="aff"]').data("id");
    }

    getNodeDebateId(node){
        return node.parents('li[id^="aff"]').data("debate-id");
    }

    /**
     * Check if shade amount is empty
     *
     * @param shadesAmount the amount of linked argument of a given argument by shade
     */
    shadeAmountIsEmpty(shadesAmount) {
        shadesAmount = shadesAmount || [];
        return (shadesAmount[this.CITATION_OK] === 0 && shadesAmount[this.CITATION_SHADED] === 0 && shadesAmount[this.CITATION_NOTOK] === 0);
    }

    /**
     * Display the amount of linked argument by shade
     *
     * @param shadesAmount the amount of linked argument of a given argument by shade
     */
    shadesAmountToHTML(shadesAmount) {
        shadesAmount = shadesAmount || [];
        var nbSimilars = shadesAmount[this.CITATION_OK];
        var nbQualifies = shadesAmount[this.CITATION_SHADED];
        var nbOpposes = shadesAmount[this.CITATION_NOTOK];
        var total = nbSimilars + nbQualifies + nbOpposes;

        if (isInt(total) && total > 0) {
            var html = '';
            var i;

            nbSimilars = nbSimilars > this.maxCircles ? this.maxCircles : nbSimilars;
            nbQualifies = nbQualifies > this.maxCircles ? this.maxCircles : nbQualifies;
            nbOpposes = nbOpposes > this.maxCircles ? this.maxCircles : nbOpposes;

            for (i = 0; i < nbSimilars; i++) {
                html += this.writeShadesAmount(nbSimilars, "similar");
            }
            for (i = 0; i < nbQualifies; i++) {
                html += this.writeShadesAmount(nbSimilars, "qualifies");
            }
            for (i = 0; i < nbOpposes; i++) {
                html += this.writeShadesAmount(nbSimilars, "opposes");
            }
            return html;
        }
        return "";
    }

    writeShadesAmount(nbExcerpts, shade){
        return '<a class="button no-decoration cursor-pointer argument-see-citations-btn" ' +
            'title="' + Messages("viz.argument.carto.nbexcerpts." + shade, nbExcerpts) + '"  data-from-debate="true">' +
            '<span class="' + shade + '"><i class="fas fa-align-left"></i></span></a>&nbsp;';
    }

    /**
     * Listener on expand events (show / hide children in links)
     *
     * @param element the element on which the expand event has been triggered
     */
    expandListener(element) {
        element.closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
        changeFaIcon(element, this.config.iconExpandedIcon, this.config.iconCollapsedIcon);
    }


    /**
     * Listener on select box to choose shade type
     *
     * @param element select box triggering the change event
     */
    linkTypeListener(element) {
        var linkshade;
        for (var i = 0; i < this.config.linkTypes.length; i++) {
            linkshade = this.config.linkTypes[i];
            element.closest('li').removeClass('link-' + linkshade);
        }
        linkshade = element.val();
        element.closest('li').addClass('link-' + linkshade);
        var linkId = this.getIdFromLi(element.closest('li'));
        if (!isNaN(linkId) || linkId.match(/newLink/)) {
            this.links.get(linkId).linkshade = linkshade;
        }
    }

    /**
     * Update visual state of expand icons for links
     */
    updateExpandIcons() {
        var that = this;
        that.linksdiv.find('.expand').each(function () {
            if ($(this).closest('li').hasClass('mjs-nestedSortable-expanded')) {
                $(this).removeClass(that.config.iconCollapsed).addClass(that.config.iconExpanded);
            } else {
                $(this).removeClass(that.config.iconExpanded).addClass(that.config.iconCollapsed);
            }
            if ($(this).closest('li').find('ol').length === 0) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });
    }

    /**
     * Update visibility of linktype selection boxes and add/remove full-size class (for proper rendering)
     *
     * @param iterable a list select boxes to show/hide
     * @param visible true if boxes must be shown (remove full-size class accordingly)
     */
    linktypeVisibility(iterable, visible) {
        iterable.each(function () {
            if (visible) {
                $(this).show();
                $(this).siblings('.itemContent').removeClass('full-size');
            } else {
                $(this).hide();
                $(this).siblings('.itemContent').addClass('full-size');
            }
        });
    }

    /**
     * Get link id from li element (stored in data-link-id attribute)
     *
     * @param li jquery li selector
     * @returns {*}
     */
    getIdFromLi(li) {
        var linkId = li.attr('data-link-id');
        if (!isNaN(linkId)) {
            linkId = parseInt(linkId);
        }
        return linkId;
    }


    /**
     * Enable buttons, if a change actually happened
     */
    enableButtons() {
        this.modified = true;
        this.actiondiv.find('button').prop('disabled', false);
    }

    /**
     * Serialize links to json object (minimum arglink form objects)
     *
     * @returns {string}
     */
    serialize() {
        var that = this;
        var keys = [];
        var json = '';
        that.links.forEach(function (value, key) {
            keys.push([key, value.serial]);
        }, that.links);
        keys.sort(function (a, b) {
            return a[1] - b[1]
        });
        for (var x = 0; x < keys.length; x++) {
            var link = jQuery.extend(true, {}, that.links.get(keys[x][0]));
            if (link.originId !== 0) {
                delete link.destinationArgument;
                delete link.destinationExcerpt;
                delete link.originExcerpt;
                delete link.originArgument;
                json += JSON.stringify(link) + ",";
            }
        }
        return '{"links":[' + json.substr(0, json.length - 1) + ']}';
    }

    /**
     * Return tree-view structure from given links
     *
     * @param links a map of (id, LinkForms)
     * @returns a map represented a tree-like structure of all links
     *
     * @author Nicolas Forêt (initial contributor)
     */
    strucToNestedStructure(links) {
        var nestedLinks = new Map();
        nestedLinks.set(0, {id: 0, children: []});
        var elementAdded = [];
        var parentShade = null;
        links.forEach(function (link, linkID) {
            if (!isInArray(link.originId, elementAdded) && !isInArray(link.destinationId, elementAdded)) {
                nestedLinks.set(link.originId, {
                    linkId: 0,
                    id: link.originId,
                    serial: link.serial,
                    shade: parentShade,
                    parent: 0,
                    children: [link.destinationId]
                });
                nestedLinks.set(link.destinationId, {
                    linkId: linkID,
                    id: link.destinationId,
                    serial: link.serial,
                    shade: parentShade,
                    parent: link.originId,
                    children: []
                });
                nestedLinks.get(0).children.push(link.originId);
                elementAdded.push(link.originId);
                elementAdded.push(link.destinationId);
            } else if (isInArray(link.originId, elementAdded) && !isInArray(link.destinationId, elementAdded)) {
                nestedLinks.get(link.originId).children.push(link.destinationId);
                nestedLinks.set(link.destinationId, {
                    linkId: linkID,
                    id: link.destinationId,
                    serial: link.serial,
                    shade: parentShade,
                    parent: link.originId,
                    children: []
                });
                elementAdded.push(link.destinationId);
            } else if (!isInArray(link.originId, elementAdded) && isInArray(link.destinationId, elementAdded)) {
                if (nestedLinks.get(link.destinationId).parent === 0) {
                    nestedLinks.get(link.destinationId).parent = link.originId;
                    nestedLinks.get(link.destinationId).linkId = linkID;
                    nestedLinks.set(link.originId, {
                        linkId: 0,
                        id: link.originId,
                        serial: link.serial,
                        shade: parentShade,
                        parent: 0,
                        children: [link.destinationId]
                    });
                    nestedLinks.get(0).children = removeValueFromArray(link.destinationId, nestedLinks.get(0).children);
                    nestedLinks.get(0).children.push(link.originId);
                    elementAdded.push(link.originId);
                } else {
                    links.delete(linkID);
                }
            } else {
                if (isInArray(link.destinationId, nestedLinks.get(link.originId).children) && nestedLinks.get(link.destinationId).parent === link.originId) {
                    links.delete(linkID);
                } else {
                    if (nestedLinks.get(link.destinationId).parent === 0) {
                        nestedLinks.get(link.destinationId).parent = link.originId;
                        nestedLinks.get(link.destinationId).linkId = linkID;
                        nestedLinks.get(link.destinationId).shade = parentShade;
                        nestedLinks.get(link.originId).children.push(link.destinationId);
                        nestedLinks.get(0).children = removeValueFromArray(link.destinationId, nestedLinks.get(0).children);
                    } else {
                        links.delete(linkID);
                    }
                }
            }
            parentShade = link.linkshadeId;
        }, links);

        return nestedLinks;
    }
}