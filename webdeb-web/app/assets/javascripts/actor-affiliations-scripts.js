/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see COPYING file).  If not,
 * see <http://www.gnu.org/licenses/>.
 *
 */

function initAffiliationListeners(container, actorType, actorId){

    // handle add affiliation buttons
    var modalanchor = $("#modal-anchor");
    var cartotab = $(container);

    cartotab.off('click');
    cartotab.on('click', '.add-aff', function() {
        newAffiliations(actorId, actorType === 0).done(function (data) {
            // load modal content
            modalanchor.empty().append(data);
            modalanchor.children('.modal').modal('show');

        }).fail(function (jqXHR) {
            var response = document.createElement('html');
            response.innerHTML = jqXHR.responseText;
            $('body').empty().append(response.getElementsByTagName('body'));
        });
    });
    // handle add member buttons
    cartotab.off('click', '.add-member');
    cartotab.on('click', '.add-member', function() {
        newMember(actorId, actorType === 0).done(function (data) {
            // load modal content
            modalanchor.empty().append(data);
            modalanchor.children('.modal').modal('show');

        }).fail(function (jqXHR) {
            var response = document.createElement('html');
            response.innerHTML = jqXHR.responseText;
            $('body').empty().append(response.getElementsByTagName('body'));
        });
    });

    cartotab.off('click', '.toggle-history');
    cartotab.on('click', '.toggle-history', function() {
        $(".organizational-chart").toggle();
        $(".organizational-list").toggle();
        $(this).find(".btn-message").toggle();
    });

    $(".aff-chart-container").each(function( index ) {
        if($(this).find(".flex-container").find(".actor-thumbnail").length < 3)
            $(this).find(".flex-container").find(".chart-labels").css("top", "70px");
        $(this).find(".flex-container").find(".chart-holder").height(($(this).find(".aff-chart").height() - 79) / (($(this).find(".flex-container").find(".actor-thumbnail").length*0.999)-1));
    });
}

/**
 * Initialize affiliation view
 *
 */
function initAffiliation(actorType){
    // affiliations part
    let affiliation = $('#affiliation-list-' + actorType);
    let affiliateds = $('#affiliated-list-' + actorType);
    let canvas = $('#affiliation-chart-' + actorType);
    let filterBtn = $('#filter-carto-' + actorType);
    let waitForIt = $('.waiting-carto-' + actorType);

    if(canvas.exists()){
        initCartoChart("affiliation", affiliation, affiliateds, canvas, filterBtn, waitForIt);
    }

    $(document).on('click', '.thumbnail-holder', function(){
        if($(this).data("href") !== undefined){
            window.location = $(this).data("href");
        }
    });
}

/**
 * Initialize and build chart
 *
 * param chartName the name of the chart
 * param affiliationList the list of affiliations elements (actors) to build the chart from
 * param affiliatedList the list of affiliated elements (actors) to build the chart from
 * param chartCanvas the canvas of the chart
 * param filterBtn a filter button
 */
function initCartoChart(chartName, affiliationList, affiliatedList, chartCanvas, filterBtn,  waitForIt) {

    var affiliations = transformToAffiliations(affiliationList);
    var affiliateds = transformToAffiliations(affiliatedList);
    var canvas = chartCanvas instanceof jQuery ? chartCanvas : $('#' + chartCanvas);
    canvas.empty();

    if(affiliations.length === 0 && affiliateds.length === 0){
        // Hide the canvas if there is no affiliations
        canvas.hide();
    }else {
        // Make graph
        let actor_data = chartCanvas.parent().find("#actor-data");
        canvas.organizationalChartJs({
            data : transformAffiliationToData(affiliations, affiliateds),
            beginDate : actor_data.data("begin-date") !== "" ? stringToDate(actor_data.data("begin-date")) : null,
            endDate : actor_data.data("end-date") !== "" ? stringToDate(actor_data.data("end-date")) : null,
            separatedChart : (affiliations.length > 0 && affiliateds.length > 0) ? 1 : 0,
            filterBtn : filterBtn,
            waitForIt : waitForIt
        });
    }
}

function transformAffiliationToData(affiliations, affiliateds){
    if(affiliations.length > 0 && affiliateds.length > 0){
        let r = [];
        r.push({label: Messages("viz.actor.carto.belongsto.n"), affiliations: affiliations});
        r.push({label: Messages("viz.actor.carto.belongings.n"), affiliations: affiliateds});
        return r;
    }

    return affiliations.length > 0 ? affiliations : affiliateds;
}

function transformToAffiliations(list){
    let affiliations = [];

    if(list.exists()) {
        // Fill the affiliation data by retrieving the not filtered actor thumbnail
        list.find('.actor-thumbnail:not(.filtered)').each(function (i, actor) {
            var actor_obj = {};
            var avatar = $(actor).clone().addClass('chart');
            avatar.find(".hidden").remove();
            actor_obj.name = "<div>" + avatar.prop('outerHTML') + "</div>";
            actor_obj.affiliations = [];

            $(actor).find(".actor-affiliation").each(function (i, aff) {
                let a = {};
                let affiliation = $(aff).text().split(";");
                let iAff = 0;
                // graph data values
                a.affType = affiliation[iAff++];
                a.profession = affiliation[iAff++];

                a.affiliatedId = parseInt(affiliation[iAff++]);
                a.affiliatedName = affiliation[iAff++];

                a.dateBegin = stringToDate(affiliation[iAff++]);
                a.dateBeginType = {};
                a.dateBeginType.id = parseInt(affiliation[iAff++]);
                a.dateBeginType.name = affiliation[iAff++];

                a.dateEnd = stringToDate(affiliation[iAff++]);
                a.dateEndType = {};
                a.dateEndType.id = parseInt(affiliation[iAff++]);
                a.dateEndType.name = affiliation[iAff++];

                a.filtered = $(aff).hasClass("filtered");

                actor_obj.affiliations.push(a);
            });

            affiliations.push(actor_obj);
        });
    }

    return affiliations;
}

function stringToDate(date){
    var parts = String(date).trim().split("/");

    switch (parts.length) {
        case 1 :
            switch (date) {
                case "-1" :
                    return -1;
                case "null" :
                    return 0;
                default :
                    return new Date(date);
            }
        case 2 :
            return new Date(parts[1], parts[0] - 1);
        case 3 :
            return new Date(parts[2], parts[1] - 1, parts[0]);
    }
}