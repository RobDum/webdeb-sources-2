/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * This collection of functions are meant to handle argument-related javascript functions, mainly dedicated to
 * manage insertion of arguments from either texts annotation or tweet validation workflow.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */

/**
 * Add all needed listeners to panels
 *
 * @param content main div containing the form
 * @param submitBtn the button to submit form
 * @param withActors true if the argument form is with an actor field
 * @param fromDebate true if listeners for debate
 */
function initArgumentListeners(content, submitBtn, withActors, fromDebate) {
    withActors = withActors || false;
    fromDebate = fromDebate || false;

    // do we have a namesake modal
    if (fromDebate && $('#namesake-modal').length > 0) {
        if ($('#isActor').val() === 'true') {
            handleContributionNameMatches($(content).find('[id$="fullname"]'), '[id$="_id"]', '[id$="_isDisambiguated"]', submitBtn, "Actor");
        } else if($('#isFolder').val() === 'true') {
            handleContributionNameMatches($(content).find('#folders').find('[id$="name"]'), '[id$="_folderId"]', '[id$="_isDisambiguated"]', submitBtn, "Folder");
        }
    }

    addArgumentTypeahead(content.find('#title'));
    addFoldersTypeahead(content.find('#folders'));
    addPlacesTypeahead(content.find("#places"));
    manageAddRmButton(content.find('#places'), ['completeName'], "entry.delete.confirm.", null, addPlacesTypeahead);

    // tooltip bubble management
    handleHelpBubble(content);

    if(withActors){
        addActorsTypeahead($('#citedactors'), 'fullname', '1');
        manageActorButton('citedactors', ['fullname'], '-1');
    }
}


/**
 * Get the first classification for this excerpt
 *
 * @param disable boolean saying if suggestions for first classification must be disabled (speech of act)
 */
function getArgumentType(disable) {
  // handle labels for argument types
  var boxes = $('#b-argtype').find('input[type="checkbox"]');
  var typeIsFilled = boxes.is(':checked');

  if (typeIsFilled || disable) {
    $('#suggestionMsg').empty();
  } else {
    boxes.prop('disabled', true);
  }

  // call to WDTAL service if not yet done and user may do so
  if ($('#suggestedType').text() === '' || !disable) {
    // excerpt must be closed by a "." for classifier to work
    var excerpt = $('#excerpt').val();
    if (excerpt.substring(excerpt.length -1, excerpt.length) !== ".") {
      excerpt = excerpt + ".";
    }
  }
}

/**
 * Manage AJAX calls to fill available shades
 *
 * @param timeValue the value of the field timing
 *       this value could be get by jQuery selector but the value could be possibly not up-to-date
 *       at the moment of getting it
 */
function manageShade(timeValue) {
  getArgumentShades($('#argtype').val(), timeValue, $('#argLang').val()).done(function(data) {
    loadSelectData('#shade', data);
  });
}

/**
 * Load data in a select group
 *
 * @param element the select html element (id)
 * @param data (json) data to put as options
 */
function loadSelectData(element, data) {
  var placeholder = element + " .blank";
  var optionsValues = '<option class="blank" value="-1">' + $(placeholder).text() + '</option>';
  if (data != null) {
    $.each(data, function (id, e) {
      optionsValues += '<option value="' + e.id + '">' + getDescription(e) + '</option>';
    });
  }
  $(element).empty().append(optionsValues);
}

/**
 * Extract the description part of the given json element (type specific). Used for ajax-based filling of dropdown boxes
 * Kind of polymorphic dispatcher
 *
 * @param element a json element
 * @returns {String} 'description' of the given element (depending on the actual type of the json object) or '' if unfound
 */
function getDescription(element) {
  if (element.hasOwnProperty('shade')) {
    return element.shade;
  }
  if (element.hasOwnProperty('origin')) {
    return element.origin;
  }
  return '';
}

/**
 * Verify if the title of the argument is valid or not
 *
 * @param type the type of the argument
 * @param lang a two char iso-639-1 code that represents the language of the argument
 */
function verifyArgument(type, lang){
  var standardInput = $("#standardPart");
  var output = $("#standardPart_warning");
  var submitBtn = $("#submit-form");

  standardInput.on("focusin", function() {
    standardInput.attr("data-warned", true);
  });

  submitBtn.on("click", function(e) {
    var warned = standardInput.attr("data-warned");

    if (warned !== undefined && warned === "true") {
      e.preventDefault();
      output.empty();
      verifyStandardForm(standardInput.val(), type, lang).done(function (data) {
        if (data[0]) {
          if (data[2]) {
            standardInput.parent().addClass("has-warning");
            output.append('<div class="div-warning"> - ' + Messages("argument.standard.part.warning.words") + '</div>');
          }
          standardInput.attr("data-warned", false);
        }else{
          submitBtn.closest("form").submit();
        }
      }).fail(function(){
        submitBtn.closest("form").submit();
      });
    }
  });
}

function initArgumentContextModalListeners(argId, modal){
    // handle submit button
    modal.find('[name="submit"]').on('click', function () {
        doSaveArgumentContext(argId, modal.find('#argument-context-form'), null, modal, $(this))
    });
}

function doSaveArgumentContext(argId, form, groupId, modal, submitbtn, reasking, contextId, sourceId, shade){
    saveArgumentContext(argId, form, groupId).done(function () {
        hideAndDestroyModal(modal);
        slideMessage($('#success-save'));
        triggerReloadVizEvent();
        if(reasking){
            setTimeout(function(){   openArgumentSearchModal(contextId, sourceId, shade); }, 250);
        }
    }).fail(function (xhr) {
        switch (xhr.status) {
            case 400:
                modal.show();
                // form has errors => clear form and rebuilt
                form.find("#argument-context-div").empty().append(xhr.responseText);
                initArgumentListeners(form, submitbtn, true);
                fadeMessage();
                break;

            case 406:
                hideAndDestroyModal(modal);
                slideMessage($('#warning-link-exists'));
                break;

            case 409:
                // folder name-match to handle, hide this modal and show the other one
                modal.hide();
                // show new one (name-matches resolution)
                var container = $('#autocreated');
                loadAndShowModal(container, xhr.responseText);
                if (container.find('#isActor').val() === 'true') {
                    handleContributionNameMatches(modal.find('[id$="fullname"]'), '[id$="_id"]', '[id$="_isDisambiguated"]', submitbtn, "Actor");
                } else if(container.find('#isFolder').val() === 'true') {
                    handleContributionNameMatches(modal.find('#folders').find('[id$="name"]'), '[id$="_folderId"]', '[id$="_isDisambiguated"]', submitbtn, "Folder");
                }
                break;

            default:
                // any other (probably a crash)
                hideAndDestroyModal(modal);
                slideMessage($('#error-save'));
        }
    });
}

function createArgumentOptions(argId, contextId, debateId, isFirstArgument){
    return createContributionOptions(createArgumentVizOptions(argId, debateId, isFirstArgument),
        createArgumentEditOptions(argId, contextId, isFirstArgument), createArgumentAddOptions(argId, contextId));
}

function createArgumentVizOptions(argId, debateId, isFirstArgument){
    var ul = '<ul class="dropdown-menu dropdown-menu-right" data-id="' + argId + '" data-debate-id="' + debateId + '">';
    if(isFirstArgument){
        ul += createContributionOptionContextMenuBtn("argument-see-debate-btn", "argument.options.viz.debate.see", "fa-comment", "");
    }
    ul += createContributionOptionContextMenuBtn("argument-see-citations-btn", "argument.options.viz.citation.see", "fa-comment-alt", "");
    ul += createContributionOptionContextMenuBtn("argument-see-history-btn", "viz.admin.history.label", "fa-history", "");
    ul += createContributionOptionContextMenuBtn("argument-show-details-btn", "viz.admin.details.label", "fa-info-circle", "");
    ul += '</ul>';
    return ul;
}

function createArgumentEditOptions(argId, contextId, isFirstArgument){
    var ul = '<ul class="dropdown-menu dropdown-menu-right" data-id="' + argId + '" data-context-id="' + contextId + '">';
    ul += createContributionOptionContextMenuBtn("argument-edit-arg-btn", "argument.edit.title", "fa-edit", "viz.admin.update.tooltip");
    if(!isFirstArgument){
        ul += createContributionOptionContextMenuBtn("argument-create-debate-btn", "argument.options.edit.debate.create", "fa-plus", "");
    }
    //ul += createContributionOptionContextMenuBtn("argument-delete-link-btn", "general.label.btn.clearlink", "fa-trash-alt", "viz.admin.update.tooltip");
    ul += createContributionOptionContextMenuBtn("argument-delete-arg-btn", "general.label.btn.clear", "fa-trash-alt", "");
    ul += '</ul>';
    return ul;
}

function createArgumentAddOptions(argId, contextId){
    var ul = '<ul class="dropdown-menu dropdown-menu-right" data-id="' + argId + '" data-context-id="' + contextId + '">';
    ul += createContributionOptionContextMenuBtn("argument-add-argument-btn", "argument.options.edit.argument.add", "fa-plus-square", "");
    ul += createContributionOptionContextMenuBtn("argument-add-citation-btn", "argument.options.edit.citation.add", "fa-plus-square", "");
    ul += '</ul>';
    return ul;
}

function argumentOptionsHandler(container){
    var modalanchor = $('#merge-modal-anchor');
    container.off('click');

    // display the contribution history of the selected argument
    container.on('click', '.argument-see-history-btn', function() {
        getContributionHistoryModal(getContributionOptionData($(this), "id"), 3).done(function (html) {
            modalanchor.empty().append(html);
            modalanchor.find('.modal').modal('show');
        });
    });

    // add new argument modal
    container.on('click', '.argument-add-argument-btn', function() {
        var sourceId = getContributionOptionData($(this), "id");
        var contextId = getContributionOptionData($(this), "context-id");
        var shade = $(this).data("linktype");
        // open modal with "search argument" bar
        openArgumentSearchModal(contextId, sourceId, shade);
    });

    // add new citation argument modal
    container.on('click', '.argument-add-citation-btn', function() {
        var sourceId = getContributionOptionData($(this), "id");
        var contextId = getContributionOptionData($(this), "context-id");
        console.log($(this));
        console.log(contextId);
        var shade = $(this).data("linktype");
        // open modal with "search citation" bar
        openExcerptSearchModal(sourceId, contextId, shade);
    });

    // open edit argument modal
    container.on('click', '.argument-edit-arg-btn', function() {
        openEditArgumentContextModal(getContributionOptionData($(this), "id"));
    });

    // redirect to the debate linked to an argument
    container.on('click', '.argument-see-debate-btn', function() {
        redirectToDebateViz(getContributionOptionData($(this), "debate-id"));
    });

    // create a debate with selected first argument
    container.on('click', '.argument-create-debate-btn', function() {
        doCreateFromArgumentContext(getContributionOptionData($(this), "id"));
    });

    // See citation linked to selected argument
    container.on('click', '.argument-see-citations-btn', function() {
        let id = getContributionOptionData($(this), "id");
        let contributionId = getContributionOptionData($(this), "contribution-id");
        openArgumentExcerptsModal(id, contributionId);
    });


    // open show places modal
    container.on('click', '.btn-place-viz', function() {
        openPlacesModal(getContributionOptionData($(this), "id"), $(this).data("place-id"));
    });

    // delete the given justification link
    /*container.on('click', '.argument-delete-link-btn', function() {
        var that = $(this);
        showConfirmationPopup(function(){doDeleteContributionAsync(getContributionOptionData(that, "link-id"), 8, 0, true)}, null, "viz.admin.modal.delete.8.");
    });*/

    // delete the given argument
    container.on('click', '.argument-delete-arg-btn', function() {
        var that = $(this);
        showConfirmationPopup(function(){doDeleteContributionAsync(getContributionOptionData(that, "id"), 3, 0, true)}, null, "viz.admin.modal.delete.3.");
    });

    // show details
    container.on('click', '.argument-show-details-btn', function() {
        redirectToDetailsPage(getContributionOptionData($(this), "id"));
    });
}

function doCreateFromArgumentContext(argId){
    createFromArgumentContext(argId).done(function (redirect) {
        redirectToGivenLink(redirect);
    }).fail(function (jqXHR) {
        slideMessage($('#error-save-debate'));
    });
}

/**
 * open the modal to display all excerpts linked to a given argument
 *
 * @param id a contextualized argument id
 * @param actorId the id of the actor to be focused on if any
 */
function openArgumentExcerptsModal(id, actorId) {
    seeArgumentExcerpts(id, actorId).done(function (html) {
        loadAndShowModal( $('#modal-anchor'), html);
    }).fail(function (jqXHR) {
        console.log("Error with argument excerpts modal");
    });
}