/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * This file gathers cross-cutting custom scripts
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */

/*
 * GLOBAL TYPEAHEADS
 */

let MIN_TYPEAHEAD = 0;
let MAX_TYPEAHEAD = 30;
let SUBMIT_BTN_SELECTOR = 'button[type="submit"], button[name="submit"], button[class="submit-btn"]';

let BOOTSTRAP_XS_MAX = 576;
let BOOTSTRAP_SM_MAX = 768;
let BOOTSTRAP_MD_MAX = 992;
let BOOTSTRAP_LG_MAX = 1200;

/**
 * Trigger the disable a submit button after one click to avoid multiple form send event when the document is ready.
 */
$(document).ready(function() {
    disableButtonsAfterSubmit();
});

/**
 * Disable a submit button after one click to avoid multiple form send
 */
function disableButtonsAfterSubmit(){
    $("body").on('click', 'button', doDisableButtonsAfterSubmit);
}

/**
 * Disable a submit button after one click to avoid multiple form send
 */
function doDisableButtonsAfterSubmit(e) {
    var that = $(this);
    if (that.is(SUBMIT_BTN_SELECTOR)) {
      if(that.hasClass("disabled")){
        e.preventDefault();
      }else{
          that.addClass("disabled");
          setTimeout(function () {
              that.removeClass('disabled');
          }, 5000);
      }
  }
}

/**
 * Enable all submit buttons
 */
function enableSubmitButtons(){
    $(SUBMIT_BTN_SELECTOR).prop('disabled', false);
}

/**
 * Get all input sub-elements from the given element identified by id (possibly filtered on child name)
 * and add place-related typeaheads (autocompletion)
 *
 * @param element parent div element (input group) for all inputs to add typeahead to
 */
function addPlacesTypeahead(element) {
    var elements = element.find(' :input[type="text"]');
    // add typeahead to all children elements containing 'child' name
    elements.each(function () {
        avoidSubmitOnTypeahead(this);
        // add typeahead to that element
        addPlaceTypeahead(this);
    });
}

/**
 * Add place typeahead to a given input element
 *
 * @param element an html element
 */
function addPlaceTypeahead(element) {
  var ttimeout;
  $(element).typeahead({
    hint: false,
    highlight: true,
    autoselect: false,
    minLength: 3,
    limit: MAX_TYPEAHEAD
  }, {
    displayKey: "completeName",
    source: function (query, process) {
      if (ttimeout) {
        clearTimeout(ttimeout);
      }
      ttimeout = setTimeout(function () {
        return searchPlace(query).done(function (data) {
          return process($.makeArray(data));
        });
      }, 300); // threshold
    },
    templates: {
      suggestion: function (item) {
        return item.name;
      }
    }
  });

    // manage selected event => save id and name lang in hidden field
    $(element).on('typeahead:selected', function (obj, datum) {
        // will fill in either the affid and the actor id (since this typeahead works for both affiliations and authors
        var elmnt = $(this);
        elmnt.typeahead('val', datum['name']);
        elmnt.parents('.entry').find('[name$=".id"]').val(datum['idPlace']);
        elmnt.parents('.entry').find('[name$="geonameId"]').val(datum['idGeoname']);
    });
}

/**
 * Get all input sub-elements from the given element identified by id (possibly filtered on child name)
 * and add text-related typeaheads (autocompletion)
 *
 * @param element parent div element (input group) for all inputs to add typeahead to
 */
function addTextsTypeahead(element) {
    var elements = element.find(' :input[type="text"]');
    // add typeahead to all children elements containing 'child' name
    elements.each(function () {avoidSubmitOnTypeahead(this);
        if(!$(this).hasClass("hidden")) {
            // add typeahead to that element
            addTextTypeahead(this, true);
        }
    });
}

/**
 * Add text typeahead to a given input element
 *
 * @param element an html element
 * @param withTypeaheadselection true if the selection must be done
 */
function addTextTypeahead(element, withTypeaheadselection) {
  var ttimeout;
  $(element).typeahead({
    hint: false,
    highlight: true,
    autoselect: false,
    minLength: 3,
    limit: MAX_TYPEAHEAD
  }, {
    displayKey: 'title',
    source: function (query, process) {
      if (ttimeout) {
        clearTimeout(ttimeout);
      }
      ttimeout = setTimeout(function () {
        return searchText(query, MIN_TYPEAHEAD, MAX_TYPEAHEAD).done(function (data) {
          return process($.makeArray(data));
        });
      }, 300); // threshold
    },
    templates: {
      suggestion: function (item) {
          return contributionTypeaheadSuggestion(item, "TEXT");
      }
    }
  });

  if(withTypeaheadselection) {
      // manage selected event => save id and name lang in hidden field
      $(element).on('typeahead:selected', function (obj, datum) {
          $(this).typeahead('val', datum['title']);
          $(this).parents(".padding-field").find('input[name$="id"]').val(datum['id']);
      });
  }
}

/**
 * Add text typeahead to a given input element
 *
 * @param element an html element
 * @param key     the key (field from json data) to be displayed
 *
 */
function addArgumentTypeahead(element, key) {
  var ttimeout;
  $(element).typeahead({
    hint: false,
    highlight: true,
    autoselect: false,
    minLength: 3,
    limit: MAX_TYPEAHEAD
  }, {
    displayKey: key,
    source: function (query, process) {
      if (ttimeout) {
        clearTimeout(ttimeout);
      }
      ttimeout = setTimeout(function () {
        return searchArgument(query, MIN_TYPEAHEAD, MAX_TYPEAHEAD).done(function (data) {
          return process($.makeArray(data));
        });
      }, 300); // threshold
    },
    templates: {
      suggestion: function (item) {
        return item.title;
      }
    }
  });

    // manage selected event => save title
    $(element).on('typeahead:selected', function (obj, datum) {
        $(this).parents('.input-group').find('[name="title"]').typeahead('val', datum['title']);
        $(this).parents('.input-group').find('[name="languageCode"]').val(datum['lang']);
    });
}

/**
 * Get all input sub-elements from the given element identified by id (possibly filtered on child name)
 * and add argument contextualized-related typeaheads (autocompletion)
 *
 * @param element parent div element (input group) for all inputs to add typeahead to
 * @param contextIdToIngore the context id to ignore, if any
 */
function addArgumentsContextTypeahead(element, contextIdToIngore) {
    var elements = element.find(' :input[type="text"]');
    // add typeahead to all children elements containing 'child' name
    elements.each(function () {avoidSubmitOnTypeahead(this);
        if(!$(this).hasClass("hidden")) {
            // add typeahead to that element
            addArgumentContextTypeahead(this,contextIdToIngore);
        }
    });
}

/**
 * Add contextualized argument typeahead to a given input element
 *
 * @param element an html element
 * @param contextIdToIngore the context id to ignore, if any
 * @param key the key (field from json data) to be displayed
 *
 */
function addArgumentContextTypeahead(element, contextIdToIngore, key) {
    contextIdToIngore = contextIdToIngore || -1;
    element = element instanceof jQuery ? element : $(element);
    var ttimeout;
    element.typeahead({
        hint: false,
        highlight: true,
        autoselect: false,
        minLength: 3,
        limit: MAX_TYPEAHEAD
    }, {
        displayKey: 'fullTitle',
        source: function (query, process) {
            if (ttimeout) {
                clearTimeout(ttimeout);
            }
            ttimeout = setTimeout(function () {
                return searchArgumentContext(query, contextIdToIngore, MIN_TYPEAHEAD, MAX_TYPEAHEAD).done(function (data) {
                    return process($.makeArray(data));
                });
            }, 300); // threshold
        },
        templates: {
            suggestion: function (item) {
                return contributionTypeaheadSuggestion(item, "ARGUMENT_CONTEXTUALIZED");
            }
        }
    });

    // manage selected event => save title
    element.on('typeahead:selected', function (obj, datum) {
        $(this).typeahead('val', datum['fullTitle']);
        $(this).parents('.input-group').find('input[name="' + key + '"]').val(datum['id']);
    });
}

/**
 * Get all input sub-elements from the given element identified by id (possibly filtered on child name)
 * and add excerpt-related typeaheads (autocompletion)
 *
 * @param element parent div element (input group) for all inputs to add typeahead to
 * @param argIdToIgnore the contextualized argument id to ignore, if any
 * @param textToLook the text id where to search, if any
 */
function addExcerptsTypeahead(element, argIdToIgnore, textToLook) {
    var elements = element.find(' :input[type="text"]');
    // add typeahead to all children elements containing 'child' name
    elements.each(function () {avoidSubmitOnTypeahead(this);
        if(!$(this).hasClass("hidden")) {
            // add typeahead to that element
            addExcerptTypeahead(this, argIdToIgnore, textToLook)
        }
    });
}

/**
 * Add excerpt typeahead to a given input element
 *
 * @param element an html element
 * @param argIdToIgnore the contextualized argument id to ignore, if any
 * @param textToLook the text id where to search, if any
 *
 */
function addExcerptTypeahead(element, argIdToIgnore, textToLook) {
    element = element instanceof jQuery ? element : $(element);
    var ttimeout;
    element.typeahead({
        hint: false,
        highlight: true,
        autoselect: false,
        minLength: 3,
        limit: MAX_TYPEAHEAD
    }, {
        displayKey: 'originalExcerpt',
        source: function (query, process) {
            if (ttimeout) {
                clearTimeout(ttimeout);
            }
            ttimeout = setTimeout(function () {
                return searchExcerpt(query, argIdToIgnore, textToLook, MIN_TYPEAHEAD, MAX_TYPEAHEAD).done(function (data) {
                    return process($.makeArray(data));
                });
            }, 300); // threshold
        },
        templates: {
            suggestion: function (item) {
                return contributionTypeaheadSuggestion(item, "EXCERPT");
            }
        }
    });

    // manage selected event => save title
    $(element).on('typeahead:selected', function (obj, datum) {
        $(this).typeahead('val', datum['originalExcerpt']);
        $(this).parents('.input-group').find('input[name="' + key + '"]').val(datum['id']);
    });
}

/**
 * Get all input sub-elements from the given element identified by id (possibly filtered on child name)
 * and add actor-related typeaheads (autocompletion)
 *
 * @param element parent div element (input group) for all inputs to add typeahead to
 * @param child filter name (among child elements from id)
 * @param actortype a given actortype to filter among actors for typeahead call
 * @param professionType a given professiontype to filter among professions for typeahead call
 */
function addActorsTypeahead(element, child, actortype, professionType) {
  var elements = element.find(' :input').not('[type="button"]');
  // add typeahead to all children elements containing 'child' name

  elements.each(function () {
    // filter on child-like names only
    if ($(this).prop('id').indexOf(child) !== -1) {
      avoidSubmitOnTypeahead(this);
      // add typeahead to that element
      addActorTypeahead(this, actortype);

      // manage selected event => save id and name lang in hidden field
      $(this).on('typeahead:selected', function (obj, datum) {
        // will fill in either the affid and the actor id (since this typeahead works for both affiliations and authors
        $(this).parents('.input-group').find('[name$=".id"]').val(datum['id']);
        $(this).parents('.input-group').find('[name$="affid"]').val(datum['id']);
        $(this).parents('.input-group').find('[name$="lang"]').val(datum['lang']);
      });

      // check whether we have a "function" field in same input-group
      var func = $(this).parents('.input-group').find('[name$="function"]');
      if (func.length > 0) {
        var id = $(this).parents('.input-group').find('[name$=".id"]');
        // if a value exists in hidden actor id field (form has been pre-loaded with actor), load actor functions
        id = id.length === 0 ? -1 : $(id).val();

        /*addActorFunctionTypeahead(id, func, 0, professionType);
        // manage selected event => save id in hidden field and fetch functions, if necessary
        $(this).on('typeahead:selected', function (obj, datum) {
          // add functions of selected actor to next input box (function)
          addActorFunctionTypeahead(datum['id'], func, 0, professionType);
        });*/
      }
    }
  });
}

/**
 * Add typeahead to an actor input element
 *
 * @param element an html element
 * @param actortype the type of the actors we want to retrieve to be displayed (-1 for any, 0 for person, 1 for org)
 *
 */
function addActorTypeahead(element, actortype) {
  var atimeout;
  $(element).typeahead({
    hint: false,
    highlight: true,
    autoselect: false,
    minLength: 3,
    limit: MAX_TYPEAHEAD
  }, {
    displayKey: "fullname",
    source: function (query, process) {
      if (atimeout) {
        clearTimeout(atimeout);
      }
      atimeout = setTimeout(function () {
        return searchActor(query, actortype, MIN_TYPEAHEAD, MAX_TYPEAHEAD).done(function (data) {
          return process($.makeArray(data));
        });
      }, 300);
    },
    templates: {
      suggestion: function (item) {
          return contributionTypeaheadSuggestion(item, "ACTOR");
      }
    }
  });


}
/**
 * Add typeahead to an actor input element
 *
 * @param element an html element
 *
 */
function addMemberElectionTypeahead(element) {
    var atimeout;
    $(element).typeahead({
        hint: false,
        highlight: true,
        autoselect: false,
        minLength: 3
    }, {
        displayKey: "fullname",
        source: function (query, process) {
            if (atimeout) {
                clearTimeout(atimeout);
            }
            atimeout = setTimeout(function () {
                return searchPartyMembers(query).done(function (data) {
                    return process($.makeArray(data));
                });
            }, 300);
        },
        templates: {
            suggestion: function (item) {
                return item;
            }
        }
    });

    // manage selected event => save title
    $(element).on('typeahead:selected', function (obj, datum) {
        $(this).typeahead('val', datum.split(" (")[0]);
        $(this).parents("form").find('button').click();
    });
}

/**
 * Get all functions of a given actor
 *
 * @param actorId the id of the actor selected in the preceeding input box
 * @param element the html element to which the typeahead will be added
 * @param minLength the minimum typing length
 * @param professionType a given professiontype to filter among professions for typeahead call
 */
function addActorFunctionTypeahead(actorId, element, minLength, professionType) {
  var ftimeout;
  avoidSubmitOnTypeahead(element);
  $(element).typeahead('destroy'); // must explicitly destroy typeahead before rebuilding it
  $(element).typeahead({
    hint: false,
    highlight: true,
    autoselect: false,
    minLength: minLength
  }, {
    displayKey: 'fullfunction', // get affiliationwrapper list
    source: function (query, process) {
      if (ftimeout) {
        clearTimeout(ftimeout);
      }

      ftimeout = setTimeout(function () {
        return getActorFunctions(actorId, professionType, query).done(function (data) {
          return process($.makeArray(data));
        });
      }, 300);
    },
    templates: {
      suggestion: function (item) {
        var tips = item.fullfunction;
        if (item.aha !== -1) {
          tips = '<span class="fa fa-save"></span>' + ' ' + '<i>' + tips + '<i>';
        }
        return tips;
      }
    }
  });
  $(element).on('typeahead:selected', function (obj, datum) {
    var group = $(this).closest('.input-group');
    // set affname, function and ids in separated field
    $(group).find('[name$="function"]').typeahead('val', datum['function']);
    if (datum['affname'] !== '') {
      $(group).find('[name$="affname"]').typeahead('val', datum['affname']);
      $(group).find('[name$="affid"]').val(datum['affid']);
    }
    if (datum['aha'] !== -1) {
      // do not reset aha since we may have a value (updating an existing affiliation)
      $(group).find('[name$="aha"]').val(datum['aha']);
    }
    var functionid = $(this).closest('.input-group').find('[name$="functionid"]');
    if (functionid !== undefined) {
      $(functionid).val(datum['functionid']);
    }
  });
}

/**
 * Get all input sub-elements from the given element identified by id (possibly filtered on child name)
 * and add folder-related typeaheads (autocompletion)
 *
 * @param element parent div element (input group) for all inputs to add typeahead to
 * @param idToIgnore the folder id to ignore, or null
 * @param child filter name (among child elements from id)
 */
function addFoldersTypeahead(element, idToIgnore, child) {
  var elements = element.find(' :input[type="text"]');
  // add typeahead to all children elements containing 'child' name
  elements.each(function () {avoidSubmitOnTypeahead(this);
    if(!$(this).hasClass("hidden")) {
        // add typeahead to that element
        addFolderTypeahead(this, idToIgnore);
    }
  });
}

/**
 * Get all folders and add auto-completion to given element
 *
 * @param element an html element
 * @param idToIgnore the folder id to ignore, or null
 */
function addFolderTypeahead(element, idToIgnore) {
  var ftimeout;
  $(element).typeahead({
    hint: false,
    highlight: true,
    autoselect: false,
    minLength: 2,
    limit: MAX_TYPEAHEAD
  }, {
    displayKey: 'name',
    source: function (query, process) {
      if (ftimeout) {
        clearTimeout(ftimeout);
      }

      ftimeout = setTimeout(function () {
        return  searchFolder(query, idToIgnore, MIN_TYPEAHEAD, MAX_TYPEAHEAD).done(function (data) {
            return process($.makeArray(data));
        });
      }, 300);
    },
    templates: {
      suggestion: function (item) {
          return contributionTypeaheadSuggestion(item, "FOLDER");
      }
    }
  });

    // manage selected event => save id and name lang in hidden field
    $(element).on('typeahead:selected', function (obj, datum) {
        var group = $(this).closest('.input-group').exists() ? $(this).closest('.input-group') :  $(this).closest('.padding-field');
        // set name and id
        $(group).find('[name$="name"]').typeahead('val', datum['name']);
        var folderId = $(group).find('[name$="folderId"]');
        if (folderId !== undefined) {
            $(folderId).val(datum['folderId']);
        }
    });
}

/**
 * Get all input sub-elements from the given element identified by id (possibly filtered on child name)
 * and add folder-related typeaheads (autocompletion)
 *
 * @param element parent div element (input group) for all inputs to add typeahead to
 */
function addContributorGroupsTypeahead(element) {
    var elements = element.find(' :input[type="text"]');
    // add typeahead to all children elements containing 'child' name
    elements.each(function () {avoidSubmitOnTypeahead(this);
        if(!$(this).hasClass("hidden")) {
            // add typeahead to that element
            addContributorGroupTypeahead(this);
            // manage selected event => save id and name lang in hidden field
            $(this).on('typeahead:selected', function (obj, datum) {
                // will fill in either the affid and the actor id (since this typeahead works for both affiliations and authors
                var elmnt = $(this);
                elmnt.typeahead('val', datum['name']);
                elmnt.parents('.input-group').find('[name$=".groupId"]').val(datum['id']);
            });
        }
    });
}

/**
 * Get all folders and add auto-completion to given element
 *
 * @param element an html element
 */
function addContributorGroupTypeahead(element) {
    var ftimeout;
    $(element).typeahead({
        hint: false,
        highlight: true,
        autoselect: false,
        minLength: 2,
        limit: MAX_TYPEAHEAD
    }, {
        displayKey: 'name',
        source: function (query, process) {
            if (ftimeout) {
                clearTimeout(ftimeout);
            }

            ftimeout = setTimeout(function () {
                return  searchGroups(query, MIN_TYPEAHEAD, MAX_TYPEAHEAD).done(function (data) {
                    return process($.makeArray(data));
                });
            }, 300);
        },
        templates: {
            suggestion: function (item) {
                return item.name;
            }
        }
    });
}

/**
 * Get all input sub-elements from the given element identified by id (possibly filtered on child name)
 * and add debate-related typeaheads (autocompletion)
 *
 * @param element parent div element (input group) for all inputs to add typeahead to
 * @param idToIgnore the debate id to ignore, or null
 * @param child filter name (among child elements from id)
 */
function addDebatesTypeahead(element, idToIgnore, child) {
    var elements = element.find(' :input[type="text"]');
    // add typeahead to all children elements containing 'child' name
    elements.each(function () {
        avoidSubmitOnTypeahead(this);
        // add typeahead to that element
        addDebateTypeahead(this, idToIgnore);

        // manage selected event => save id and name lang in hidden field
        $(this).on('typeahead:selected', function (obj, datum) {
            var group = $(this).closest('.input-group');
            // set name and id
            $(group).find('[name$="name"]').typeahead('val', datum['name']);
            var debateId = $(this).closest('.input-group').find('[name$="debateId"]');
            if (debateId !== undefined) {
                $(debateId).val(datum['debateId']);
            }
        });
    });
}

/**
 * Get all debates and add auto-completion to given element
 *
 * @param element an html element
 * @param idToIgnore the debate id to ignore, or null
 */
function addDebateTypeahead(element, idToIgnore) {
    var ftimeout;
    $(element).typeahead({
        hint: false,
        highlight: true,
        autoselect: false,
        minLength: 2,
        limit: MAX_TYPEAHEAD
    }, {
        displayKey: 'fullTitle',
        source: function (query, process) {
            if (ftimeout) {
                clearTimeout(ftimeout);
            }

            ftimeout = setTimeout(function () {
                return searchDebate(query, MIN_TYPEAHEAD, MAX_TYPEAHEAD).done(function (data) {
                    return process($.makeArray(data));
                });
            }, 300);
        },
        templates: {
            suggestion: function (item) {
                return contributionTypeaheadSuggestion(item, "DEBATE");
            }
        }
    });

    // add value to element when typeahead is selected
    $(element).on('typeahead:selected', function (obj, datum) {
        // fill the value with the folder name
        $(element).typeahead('val', datum['title']);
    });
}

/**
 * Manage add/remove buttons generic
 *
 * @param id the parent input group id to clone (no leading dash)
 * @param child array of filter elements
 * @param delete_msg a message at the deletion
 * @param functiondel the function that called after deletation
 * @param addTypeahead a typehead to add at the new entry
 * @param typeaheadArg other argument for typehead
 * @param specificBtn a specific button to listen to
 */
function manageAddRmButton(id, child, delete_msg, functiondel, addTypeahead, typeaheadArg, specificBtn) {
    specificBtn = specificBtn || "";
  // manage add button (clone input)
  var parent = id instanceof jQuery ? id : $('#' + id);
  parent.on('click', '.' + specificBtn + 'btn-add', function () {
    addField($(this), child, addTypeahead, typeaheadArg);
  });

  // manage 'remove' button
  parent.on('click', '.' + specificBtn + 'btn-remove', function () {
    var entry = $($(this).parents('.entry')[0]).parent().children('.entry:last');
    var descr = child != null && child.length > 0 ?
      $(entry).find('[name$="' + child[0] + '"]')
      : $(entry).find('[name$="first"]').val() + " " + $(entry).find('[name$="last"]');
    if (descr !== undefined && descr.val() !== undefined && descr.val() !=='') {
      descr = "" + descr.val() + "";
    }
    else{
      descr = "";
    }
    if(typeof functiondel === "function") {
      functiondel(this);
    }else {
      showConfirmationPopup(removeGenericEntry, this, delete_msg, descr);
    }
    return false;
  });
}

/**
 * Add new general fields in form
 *
 * @param element the parent input group
 * @param child array of filter elements
 * @param addTypeahead a typehead to add at the new entry
 * @param typeaheadArg other argument for typehead
 */
function addField(element, child, addTypeahead, typeaheadArg) {
  // clone them and add clone at the end of list
  var entry = $(element.parents('.entry')[0]).parent().children('.entry:last');
  entry.find(':input[type=text]').typeahead('destroy'); // avoid having typeahead troubles on cloned input
  var newEntry = $(entry.clone());
  entry.after(newEntry);

  // build new ids for any elements in cloned elements
  var splitId = newEntry.prop('id').split('_');
  var idx = parseInt(splitId[1]) + 1;
  newEntry.prop('id', splitId[0] + "_" + idx + "_" + splitId[2]);
    newEntry.find('.entry-to-increment').text(parseInt(newEntry.find('.entry-to-increment').text()) + 1);

  newEntry.find('*').each(function () {
    if (this.hasAttribute('id')) {
      splitId = $(this).prop('id').split('_');
      $(this).prop('id', splitId[0] + "_" + idx + "_" + splitId[2]);

      // many ifs, but no other way...
      if (this.hasAttribute('name')) {
        $(this).prop('name', splitId[0] + "[" + idx + "]." + splitId[2]);
      }

      // for text inputs, clear values
      if (($(this).prop('type') === 'text')){
        $(this).val("");
      }

    } else {
      if (this.hasAttribute("for")) { // labels
        splitId = $(this).prop('for').split('_');
        $(this).prop('for', splitId[0] + "_" + idx + "_" + splitId[2]);
      }
    }
  });

  if(addTypeahead != null && addTypeahead !== undefined){
    if(typeaheadArg != null && typeaheadArg !== undefined){
      addTypeahead(findEntryTypeaheadInput(newEntry, child), typeaheadArg);
      addTypeahead(findEntryTypeaheadInput(entry, child), typeaheadArg);
    }else{
      addTypeahead(findEntryTypeaheadInput(newEntry, child));
      addTypeahead(findEntryTypeaheadInput(entry, child));
    }
  }
}

function findEntryTypeaheadInput(entry, child){
    return entry.find('input[name$="' + child[0] + '"]');
}

/*
 * ACTOR/AUTHOR/AFFILIATION-RELATED MANAGEMENT
 */

/**
 * Manage add/remove buttons for actor fields as affiliations or authors
 *
 * @param id the parent input group id to clone (no leading dash)
 * @param child array of filter elements (will add 'actor typeaheads' to each passed elements)
 * @param actortype a given actortype to filter among actors
 */
function manageActorButton(id, child, actortype) {

  // manage add button (clone input)
  var parent = $('#' + id);
  parent.on('click', '.btn-add', function () {
    addActorField($('#' + id), child, actortype);
  });

  // manage 'remove' button
  parent.on('click', '.btn-remove', function () {
    var entry = $(this).parents('.entry:first');
    var descr = child.length > 0 ?
        $(entry).find('[name$="' + child[0] + '"]').val()
        : $(entry).find('[name$="first"]').val() + " " + $(entry).find('[name$="last"]').val();
    var func = $(entry).find('[name$="function"]');
    if (func !== undefined && func.val() !== undefined && func.val() !=='') {
      descr += " (" + func.val() + ")";
    }
    showConfirmationPopup(removeGenericEntry, this, "actor.delete.confirm." + id + ".", descr);
    return false;
  });
}

/**
 * Add new actor fields in form
 *
 * @param element the parent input group
 * @param child array of ids to filter child elements from given elements to add typeahead feature
 * @param actortype a given actortype to filter among actors for the added typeahead feature
 */
function addActorField(element, child, actortype) {
  // clone them and add clone at the end of list
  var entry = element.find('.entry:last');
  $(entry).find('input').typeahead('destroy'); // avoid having typeahead troubles on cloned input
  var newEntry = $(entry.clone());
  entry.after(newEntry);

  // build new ids for any elements in cloned elements
  // id of the form "somename_i_somefield", name of the form  "affiliation[i].field (ifexists)
  var splitId = $(newEntry).prop('id').split('_');
  var idx = parseInt(splitId[1]) + 1;
  $(newEntry).prop('id', splitId[0] + "_" + idx + "_" + splitId[2]);

  $(newEntry).find('*').each(function () {
    if (this.hasAttribute('id')) {
      splitId = $(this).prop('id').split('_');
      $(this).prop('id', splitId[0] + "_" + idx + "_" + splitId[2]);

      // many ifs, but no other way...
      if (this.hasAttribute('name')) {
        $(this).prop('name', splitId[0] + "[" + idx + "]." + splitId[2]);
      }

      // for text inputs, clear values
      if (($(this).prop('type') === 'text' && $(this).prop('id') !== 'lang') || $(this).prop('tagName') === 'SELECT') {
        $(this).val('');
        $(this).prop("readonly", false);
      } else {
        if ($(this).prop('type') === 'checkbox') {
          // reset disambiguated hidden value, in case of resubmission
          $(this).prop('checked', false);
        }
      }

      // rounded toggle box
      if ($(this).hasClass('roundedbox')) {
        $(this).find('input').val("false");
        updateRoundedbox($(this).parent());
        listenerOnRoundedBox($(this).parent(), '[name$="ongoingDate"]');
      }

    } else {
      if (this.hasAttribute("for")) { // labels
        splitId = $(this).prop('for').split('_');
        $(this).prop('for', splitId[0] + "_" + idx + "_" + splitId[2]);
      }
    }
  });

  // add typeahead to cloned inputs
  for (var i = 0; i < child.length; i++) {
    addActorsTypeahead(entry, child[i], actortype);
    addActorsTypeahead(newEntry, child[i], actortype);
  }

  managePrecisionDate();
}

/**
 * Manage name matches for contributions fields (author or folder fields in arguments or texts)
 *
 * @param name2update the selector to search in for selected name in modal
 * @param id2update the id selector to retrieve the id element to update with selected contribution from modal if needed
 * @param flag the selector to a checkbox to search in name2update parent div to set to true when this name has been disambiguated
 * @param submit the submit button to click after the decision is made
 * @param type the contribution type name
 */
function handleContributionNameMatches(name2update, id2update, flag, submit, type) {
  var modal = $('#namesake-modal');
  if (modal.length > 0) {
    var okbtn = "#createnew";
    var loadbtn = '#load';

    new Pager( modal.find('.pageable'), 5).reset();

    // reset load button label, activate toggling between multiple choices (namesakes cards) and show modal
    $(loadbtn).text(Messages('entry.'+type.toLowerCase()+'.existing.use'));
    $(loadbtn).prop('disabled', true);
    toggleSummaryBoxes('#namesake-boxes', '.toggleable');
    modal.modal();

    // clamping affiliations list
    modal.find('li').each(function () {
      $(this).addClass('forcewrap');
    });

    // handle click on create a "new contribution" button (this creating a namesake)
    $(modal).on('click', okbtn, function () {
      updateNameMatch(modal, name2update, undefined, flag, submit, type);
    });

    // handle click on load
    $(modal).on('click', loadbtn, function () {
      // get selected element and fill form with this wrapper
      if ($(modal).find('.selected').prop('id').split("_")[1] === null) {
        // no selected element, display error message (should not happen thanks to toggleSummaryBoxes
        showMe($(modal).find("#msg-div"), true, true);
      } else {
        updateNameMatch(modal, name2update, id2update, flag, submit, type);
      }
    });
  }
}

/**
 * Update name of a matched contribution (actor or folder) from DB in given div as selected from given modal
 *
 * @param modal the namesake modal where user selects a namesake or choose to create a new one
 * @param name2update the selector to search in for selected name in modal
 * @param id2update the selector to search in name2update parent div to update with the selected id (if updateValue is true)
 * @param flag the selector to a checkbox to search in name2update parent div to set to true when this name has been disambiguated
 * @param submit the submit button to trigger in order to send the form (in given div)
 * @param type the contribution type name
 */
function updateNameMatch(modal, name2update, id2update, flag, submit, type) {

  // fill all possible spellings (displayed name, acronym and hidden alternative spellings)
  var spellings = [];
  spellings.push($(modal).find('#namesake-name').text().removeAccents());
  spellings.push($(modal).find('#namesake-acro').text().removeAccents());
  $(modal).find('.namesake').each(function() {
    spellings.push($(this).text().trim().removeAccents());
  });

  // find all elements with same name as modal title (or any .namesake element) with no namesake value
  name2update.each(function () {
    var parent = $(this).closest('div.input-group');
    parent = parent.exists() ? parent : $(this).closest('div.input-div-group');
    var isDisambiguated = parent.find(flag);
    if ($(this).val() !== '' && !isDisambiguated.prop('checked')) {
      var fetched = parent.find('[id$="fetched"]').val();
      if (fetched !== undefined && fetched.length > 0) {
        // For actor contribution
        // of the form "{"organization":{"fr":"fr name","en":"en name","de":"de name"}}"
        fetched = $.parseJSON(fetched).organization;
      } else {
        // put empty array in it (for proper use of Object.keys function without raising errors)
        fetched = [];
      }
      // add current displayed name in fetched array with empty key
      fetched[''] = $(this).val().removeAccents();
      // now check that shown name or any of fetched value correspond to one of the spellings
      var keys = Object.keys(fetched);
      for (var j = 0; j < keys.length && !isDisambiguated.prop('checked'); j++) {
        var indexContributionForm = modal.find("#index"+type+"Form").val();
        var indexUnknown = (indexContributionForm === undefined || indexContributionForm == -1);
        var current = (indexUnknown ? fetched[keys[j]].removeAccents() : name2update.eq(indexContributionForm).val().removeAccents());
        for (var i = 0; i < spellings.length; i++) {
          if ((!indexUnknown && fetched[keys[j]] === current) || (indexUnknown && spellings[i] === current)) {
            isDisambiguated.prop('checked', true);
            if (id2update !== undefined) {
              // ensure name is the selected contribution's name, and for actors not the acronym (or further server-side checking won't agree on name match)
              $(this).typeahead('val', $('.selected').find('[name="card-name"]').val());
              // put selected id (from modal) in given id2update parent-relative element;
              parent.find(id2update).val($(modal).find('.selected').prop('id').split("_")[1]);
            }
            // we found it, break current loop and go to next name (because isDisambiguated will be true)
            break;
          }
        }
      }
    }
  });
  // emulate submit click to trigger right event (submitting the form)
  hideAndDestroyModal(modal, submit);
}

/**
 * Hide given modal, remove it from DOM tree when completely hidden, remove modal-backdrop and trigger
 * click on given submit button.
 *
 * @param modal a modal frame
 * @param submit the submit button selector to trigger a click on (optional)
 */
function hideAndDestroyModal(modal, submit) {
    modal = modal instanceof jQuery ? modal : $(modal);
    submit = submit instanceof jQuery ? submit : $(submit);

    modal.modal('hide');
    modal.on('hidden.bs.modal', function() {
    modal.remove();
    $('.modal-backdrop').remove();
    if (submit.exists()) {
        submit.trigger('click');
    }
  });
}

/**
 * Hide given modal, remove it from DOM tree when completely hidden, remove modal-backdrop and trigger
 * click on given submit button and call a given function.
 *
 * @param modal a modal frame
 * @param func the event function to call
 * @param submit the submit button selector to trigger a click on (optional)
 */
function hideAndDestroyModalAndCallFunction(modal, func, submit){
  func();
  hideAndDestroyModal(modal, submit);
}


/*
 * CROSS-CUTTING
 */

/**
 * open modal for edit a contextualized argument
 *
 * @param argId the argument id to edit
 */
function openEditArgumentContextModal(argId) {
    editArgumentContext(argId).done(function (html) {
        loadAndShowModal( $('#modal-anchor'), html);
        initArgumentListeners($("#argument-context-form"), null, true);
        initArgumentContextModalListeners(argId, $("#modal-argument-context"));
    }).fail(function (jqXHR) {
        if(jqXHR.status === 401) {
            redirectToLogin();
        }else{
            console.log("Error with argument context edit modal");
        }
    });
}

/**
 * open modal for edit an excerpt
 *
 * @param textId the id of the excerpt text
 * @param excId the excerpt id to edit
 */
function openEditExcerptModal(textId, excId) {
    editExcerptFromModal(textId, excId).done(function (html) {
        hideAndDestroyModal($('#modal-excerpts'));
        setTimeout(function(){
            loadAndShowModal( $('#modal-anchor'), html);
            initExcerptFieldSet($("#excerpt-form"));
            initExcerptModalListeners(textId, excId, $("#modal-excerpt"));
        }, 300);
    }).fail(function (jqXHR) {
        if(jqXHR.status === 401) {
            redirectToLogin();
        }else{
            console.log("Error with excerpt edit modal");
        }
    });
}

/**
 * open modal with "search text" bar
 *
 * @param contextId the context contribution where add this link
 * @param argId the argument id that will be bound with given shade to selected / newly created
 * @param shade the shade that will be stored for the link to create
 */
function openArgumentSearchModal(contextId, argId, shade) {
    shade = shade || -1;
  argumentSelection(contextId, argId, shade).done(function (html) {
      loadAndShowModal( $('#modal-anchor'), html);
  }).fail(function (jqXHR) {
      if(jqXHR.status === 401) {
          redirectToLogin();
      }else{
          console.log("Error with argument context search modal");
      }
  });
}

/**
 * open modal with "search text" bar
 *
 * @param contributionType a given contribution
 */
function openContributionSearchModal(contributionType) {
    contributionSelection(contributionType).done(function (html) {
        loadAndShowModal( $('#merge-modal-anchor'), html);
    }).fail(function (jqXHR) {
        console.log("Error with contribution search modal");
    });
}

/**
 * open modal with "search text" bar
 *
 * @param argId an argument id that will be linked to the selected excerpt
 * @param contextId the context contribution id where an excertp selection is called
 * @param shade the shade that will be stored for the link to create, if any
 */
function openExcerptSearchModal(argId, contextId, shade) {
    excerptSelection(argId, contextId, shade).done(function (html) {
        loadAndShowModal( $('#modal-anchor'), html);
    }).fail(function (jqXHR) {
        if(jqXHR.status === 401) {
            redirectToLogin();
        }else{
            console.log("Error with excerpt search modal");
        }
    });
}

/**
 * open modal with "search text" bar
 *
 * @param id a contribution id
 * @param type its contribution type id
 * @param group the contributor group
 * @param force true to force
 */
function doDeleteContributionAsync(id, type, group, force) {
    deleteContributionAsync(id, type, group, force).done(function (html) {
        triggerReloadVizEvent();
        slideMessage($('#success-delete'));
    }).fail(function (jqXHR) {
        slideMessage($('#error-delete'));
    });
}

/**
 * Handle simple dynamic input form with + and - buttons
 * (other than actors, arguments, etc. for which dedicated functions are provided)
 *
 * @param parentdiv the parent div that contains the input element
 * @param control the controls class element id
 * @param selector input selector to be renamed
 * @param typeahead the typeahead function to apply to cloned input (optional)
 */
function handleAddRemoveButton(parentdiv, control, selector, typeahead) {
  var controlid = '#' + control;

  // magic of jquery, add click listener also on newly created elements
  parentdiv.on('click', controlid + ' .btn-add', function () {
    cloneGenericEntry(parentdiv, control, selector, typeahead);
  });

  parentdiv.on('click', controlid + ' .btn-remove', function () {
    var descr = $(this).parents('.entry:first').find('[name^="' + control + '"] option:selected').text();
    showConfirmationPopup(removeGenericEntry, this, "actor." + control + ".delete.confirm.", descr);
    return false;
  });
}

/**
 * Reusable modal popup to show informations to user
 *
 * param msg partial message key for title (.title) and content (.msg) to display in popup
 * param content the content to be passed to the message content as dynamic content
 */
function showInformativePopup(msg, content) {
  bootbox.dialog({
    message: (content == null ? Messages(msg + "msg") : Messages(msg + "msg", '<b>' + content + '</b>')),
    title: Messages(msg + "title"),
    buttons: {
      main: {
        label: Messages("general.btn.ok"),
        className: "btn-primary",
        callback: function () { /* ignore */ }
      }
    }
  });
}

/**
 * Reusable modal popup to ask a confirmation to user about something
 *
 * param callback the function to call as a "confirm" callback
 * param element the element to pass as parameter to the callback function
 * param msg partial message key for title (.title) and content (.msg) to display in popup
 * param content the content to be passed to the message content as dynamic content
 */
function showConfirmationPopup(callback, element, msg, content) {
  bootbox.dialog({
    message: Messages(msg + "text", '<b>' + content + '</b>'),
    title: Messages(msg + "title"),
    buttons: {
      main: {
        label: Messages("general.btn.cancel"),
        className: "btn-default",
        // do nothing when cancel
        callback: function () { /* ignore */ }
      },
      modify: {
        label: Messages("general.btn.confirm"),
        className: "btn-primary",
        callback: function() { callback(element) }
      }
    }
  });
}

/**
 * Clone any "add-remove" entry other than actors/authors one. Get the last '.entry' element with id
 * from parentdiv context, clone and rename it (suffixed by counter). Add typeahead function, if any
 *
 * @param parentdiv the parent div that contains the input element
 * @param control the controls class element id
 * @param selector input selector to be renamed
 * @param typeahead the typeahead function to apply to cloned input (optional)
 */
function cloneGenericEntry(parentdiv, control, selector, typeahead) {
  // clone fields and add clone at the end of list
  var controlid = '#' + control;
  var entry = parentdiv.find(controlid).find('.entry:last');
  if (selector === 'input' && typeahead !== undefined) {
    // avoid having typeahead troubles on cloned input
    $(entry).find('input').typeahead('destroy');
  }
  var newEntry = $(entry.clone());
  entry.after(newEntry);
  var newInputs = newEntry.find(selector);
  var input = newInputs[0];

  // rename and set id by incrementing the index
  var splitId = $(input).prop('id').split('_');
  var idx = parseInt(splitId[1]) + 1;
  var suffixId = splitId.length > 2 ? '_' + splitId[2] : '';
  var suffixName = splitId.length > 2 ? '.' + splitId[2] : '';
  $(input).prop('id', splitId[0] + '_' + idx + suffixId);
  $(input).prop('name', splitId[0] + '[' + idx + ']' + suffixName);
  $(input).val('');

  // if a typeahead has been passed, apply it
  if (typeahead !== undefined) {
    typeahead($(entry).find('input'));
    typeahead(input);
  }
}

/**
 * Remove an .entry element from a .controls
 *
 * @param toremove the sub-button of the line to remove
 */
function removeGenericEntry(toremove) {
  // check if this line is the last displayed one, if not, remove it, otherwise clean inputs
  if ($(toremove).parents('.controls').children('.entry').length > 1) {
    $(toremove).parents('.entry:first').remove();
  } else {
    $(toremove).parents('.entry:first').find('[type="text"]').each(function () {
      if ($(this).hasClass('hidden')) {
        $(this).val(($(this).prop('id').indexOf('id') >= 0 || $(this).prop('id').indexOf('aha') >= 0) ? -1 : '');
      } else {
        $(this).val('');
        $(this).typeahead('val', ''); // for typeahead fields
      }
    });
    // put default values in select element (if any)
    $(toremove).parents('.entry:first').find('select option').prop('selected', false);
    // updated rounded box, if any
    var rounded = $(toremove).parents('.entry:first').find('.roundedbox');
    if (rounded.length > 0) {
      rounded.find('input').val("false");
      updateRoundedbox(rounded.parent());
    }
  }
}

/**
 * Show/hide an element
 *
 * @param element an html element id
 * @param b a boolean saying if the element must be shown (true) or hide (false)
 * @param effect boolean saying if the show/hide transition must be showed with a fade effect
 */
function showMe(element, b, effect) {
  if (effect) {
    b ? $(element).fadeIn(400) : $(element).fadeOut(400);
  } else {
    b ? $(element).show() : $(element).hide();
  }
}

/**
 * Disable submit on enter-key pressed (mainly used for typeahead fields)
 *
 * @param element an html element
 * @return false for "return" key pressed
 */
function avoidSubmitOnTypeahead(element) {
  $(element).keydown(function (event) {
    if (event.keyCode === 13) {
      return false;
    }
  });
}

/**
 * Call given callback method when enter key is pressed
 *
 * @param element a selector to an html element
 * @param callback a javascript method to be called when the 'enter' key is pressed
 */
function submitOnEnterKey(element, callback) {
  $(element).on('keydown', function (event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      callback();
    }
  });
}

/**
 * Load given modal content into given selector
 *
 * @param selector a selector that will contain the modal
 * @param content a modal content (full modal)
 */
function loadAndShowModal(selector, content) {
  $(selector).empty().append(content);
  $(selector).children('.modal').modal('show');
}

/**
 * Replace the content of given selector object with given content
 *
 * @param selector the selector string that will be replaced by the new content
 * @param content the content to replace given selector
 * @param tag tag name to get content from in given content
 */
function replaceContent(selector, content, tag) {
  var toappend = document.createElement('html');
  toappend.innerHTML = content;
  $(selector).replaceWith(toappend.getElementsByTagName(tag)[0]);
}

/**
 * Show a given message (html content, typically a bootstrap alert) in given div
 *
 * @param msg a message to display
 * @param div the div that will contain the given message
 */
function showMessage(msg, div) {
  $(document).find(div).append(msg);
  fadeMessage();
}

/**
 * Generic error message handling
 *
 * @param jqXHR an ajax request error object
 */
function showErrorMessage(jqXHR) {
  showMessage(jqXHR.responseText, '#msg-div');
}

/**
 * Simple helper to slide up given pre-filled message div without disposing it from html tree
 *
 * @param msg a msg div (jquery) to slide (show-hide)
 */
function slideMessage(msg) {
  msg.show();
  window.setTimeout(function () {
    msg.fadeTo(500, 0, function() {
      msg.hide();
      msg.css("opacity", "100");
    });
  }, 6000);
}

function slideDefaultErrorMessage(){
    slideMessage($('#general-message-danger'));
}

function slideDefaultWarningMessage(){
    slideMessage($('#general-message-warning'));
}

function slideDefaultSuccessMessage(){
    slideMessage($('#general-message-success'));
}

/**
 * Fade alert messages of type .alert-fixed after 6 seconds
 */
function fadeMessage() {
  window.setTimeout(function () {
    $('.alert-fixed.removeable').fadeTo(500, 0).slideUp(500, function (){ $(this).remove(); });
  }, 6000);
}

/**
 * Manage a set of checkboxes for only one selection
 *
 * @param boxes an array of boxes
 * @param field the field that will contain the selected value
 * @param forceSelect true if one field must be selected (acting as radio boxes)
 */
function manageExclusiveCheckboxes(boxes, field, forceSelect) {
  // page load
  if (field !== null && field.val() !== '' && boxes.length > 1) {
    boxes.each(function () { // must explicitly loop manually, direct jquery not working
      if ($(this).val() === field.val()) {
        $(this).prop('checked', true);
      }
    });
  }
  boxes.on('change', function () {
    if (field !== null && $(this).prop('checked')) {
      field.val($(this).val());
    } else {
      if (forceSelect) {
        $(this).prop('checked', true);
      } else {
        if (field !== null) {
          field.val('');
        }
      }
    }
    boxes.not(this).prop('checked', false);
  });
}

/**
 * Initialize popover features for all helpbubbles in the given div
 *
 * @param div a div in which helpbubbles are present
 */
function handleHelpBubble(div) {
  $(div).find('.helpbubble').each(function () {
    $(this).popover(); // to be initialized (opt-in)
    $(this).on("click", function () {
      $(this).popover("toggle");
      return true;
    });
  });

  $('body').on('click', function (e) {
    $(div).find('.helpbubble').each(function () {
      //the 'is' for buttons that trigger popups
      //the 'has' for icons within a button that triggers a popup
      if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
        $(this).find('input').prop('checked', false);
        $(this).popover("hide");
      }
    });
  });
}

/**
 * Add click event listener on rounded check box (toggle button)
 *
 * @param elem the element to attach the listener to
 * @param selector a jquery selector for the field holding the boolean value of the toggle box
 */
function listenerOnRoundedBox(elem, selector) {
  $(elem).find('.roundedbox').on("click", function () {
    var text = $(this).find(selector);
    text.val(text.val() !== "true"); // boolean as string ...
    updateRoundedbox(elem);
  });
}

/**
 * Update graphically a input field that contains a rounded box to enable/disable it (to specify "unknown" value)
 *
 * @param boxgroup a group of inputs containing a rounded box
 */
function updateRoundedbox(boxgroup) {
  var checked = $(boxgroup).find('input.hidden').val() === "true";
  $(boxgroup).find('.fa').attr('class', checked ? 'fa fa-check-circle-o' : 'fa fa-circle-o');
  $(boxgroup).find('.roundedbox').toggleClass('colored', checked);
  // in case we have a text input to disable
  $(boxgroup).find('input[type="text"]').not('.hidden').prop('disabled', checked);
}

/**
 * Put ellipsis on text boxes (multiline)
 *
 * @param el an html element
 * @param size the height of the box
 */
function ellipsizeTextBox(el, size) {
  var wordArray = el.html().split(' ');
  while(el.prop('scrollHeight') > size) {
    wordArray.pop();
    el.html(wordArray.join(' ') + '...');
    el.css('height', size + 'px');
  }
}

/**
 * Get a Y-M-D date and transform it in an appropriate format
 *
 * @param date a Y-M-D date (M and D maybe not present)
 * @returns {String} corresponding (DD/MM/)YYYY date (year possibly negative and with one to 4 digits)
 */
function formatDate(date) {
  var isBC = false;
  // date starts with negative sign -> negative date, remove sign
  if (date.indexOf('-') === 0) {
    isBC = true;
    date = date.substr(1, date.length - 2);
  }
  var split = date.split('-');
  switch (split.length) {
    case 1:
      return date;
    case 2:
      return split[1] + '/' + (isBC ? '-' : '') + split[0];
    case 3:
      // we may have received 0's
      return (split[2] !== '00' ? split[2] + '/' : '') + (split[1] !== '00' ? split[1] + '/' : '') + (isBC ? '-' : '') + split[0];
    default:
      return '';
  }
}

/*
 * CROSS-CUTTING VISUALIZATION HELPERS
 */

/**
 * open the modal to display all linked places to a given contribution
 *
 * @param id a contribution id
 * @param selectedPlace the selected place by the user if any
 */
function openPlacesModal(id, selectedPlace) {
    getContributionPlaces(id, selectedPlace).done(function (html) {
        loadAndShowModal( $('#modal-anchor'), html);
        setTimeout( function() { $(document).trigger("reload-map")}, 500);
    }).fail(function (jqXHR) {
        if(jqXHR.status === 401) {
            redirectToLogin();
        }else{
            console.log("Error with places modal");
        }
    });
}

/**
 * Handle displayed tab: add listeners on tabs and update url with tab id (pane url attribute)
 *
 * @param tab tab to be displayed
 * @param pov the point of vue value
 */
function handleTabs(tab, pov) {

    // resize viz head picture if necessary
    let header_img = $(".viz-header-img");
    if(header_img.exists()){
        if(header_img.height() <= header_img.width() * 1.10) {
            header_img.height(header_img.parent().height());
        }
        if(header_img.width() > 200){
            header_img.width(200);
        }
    }

  $('.nav-tabs a').on('click', function () {
    replaceHistory('pane', $(this).prop('id').split("_")[1]);
    $(document).trigger("on-focus");
    //on-focus
    var filterbar = $('.vertical_filter__opened');
    if(filterbar.exists()){
      filterbar.removeClass("vertical_filter__opened");
      filterbar.addClass("vertical_filter");
    }
      if(pov !== undefined) {
          var pov_element = $($(this).attr("href")).find('.btn-pov.btn-success');
          if (pov_element.exists()) {
              replaceHistory('pov', pov_element.val());
          }
      }
  });

    $('.btn-pov').on('click', function () {
        replaceHistory('pov', $(this).val());
    });

  $('#tabs').find('a').on('click', function (e) {
    e.preventDefault();
    $(this).tab('show');
  });

  let tab_element = $('.nav li a[id$="' + tab + '"]');
    tab_element.trigger('click');
    if(pov !== undefined) {
        let container = $(tab_element.attr("href"));
        let pov_element = container.find('.btn-pov[value="' + pov + '"]');

        if (pov_element.exists()) {
            triggerPovWhenLoaded(container, pov_element);
        }
    }

  // manage scroll event to keep the argument standard form visible on the top of the screen
  vizHeadScrollAnimation();
}

function triggerPovWhenLoaded(container, pov_element){
    setTimeout(function(){
        if($("#citedcontent").children().length === 0) {
            triggerPovWhenLoaded(container, pov_element);
        }else{
            pov_element.trigger('click');
        }
    }, 100);
}

/*
 * REUSABLE SEARCH
 */

function addContributionTypeahead(element, contributionId){
    switch(contributionId){
        case 0 :
            addActorTypeahead(element);
            break;
        case 1 :
            addDebateTypeahead(element);
            break;
        case 3 :
            addArgumentContextTypeahead(element);
            break;
        case 4 :
            addExcerptTypeahead(element);
            break;
        case 5 :
            addTextTypeahead(element, true);
            break;
        case 6 :
            addFolderTypeahead(element);
            break;
    }
}

function addSearchTypeahead(element, groupId) {
  var stimeout;
  $(element).typeahead({
    hint: false,
    highlight: true,
    autoselect: false,
    minLength: 3,
    limit: MAX_TYPEAHEAD
  }, {
    source: function (query, process) {
        if (stimeout) {
          clearTimeout(stimeout);
        }
        stimeout = setTimeout(function () {
          return searchContributions(query, groupId, MIN_TYPEAHEAD, MAX_TYPEAHEAD).done(function (data) {
            return process($.makeArray(data));
          });
        }, 300);
    },
    templates: {
      suggestion: function (item) {
        return contributionTypeaheadSuggestion(item);
      }
    }
  });

  $(element).on('typeahead:selected', function (obj, datum) {
    // must fill in data in field based on actual type
    switch (datum.type) {
      case 'ACTOR':
        $(element).typeahead('val', datum.fullname);
        break;
      case 'DEBATE':
      case 'ARGUMENT_CONTEXTUALIZED':
        $(element).typeahead('val', datum.fullTitle);
        break;
        case 'EXCERPT':
            $(element).typeahead('val', datum.workingExcerpt);
            break;
      case 'TEXT':
        $(element).typeahead('val', datum.title);
        break;
      case 'FOLDER':
          $(element).typeahead('val', datum.folderName);
        break;
      default:
        // ignore
    }
    $(element).attr('data-id', datum.id);
    $(element).trigger( "typehead-selected" );
  });
}

function contributionTypeaheadSuggestion(item, contributionType){
    // must put twice the force wrap, dunno why
    let container = $('<div style="margin-top : 5px;"></div>');

    let containerHead = $('<div class="flex-container"></div>').appendTo(container);
    let picture = $('<div class="tt-picture"></div>').appendTo(containerHead);

    let containerInfo = $('<div></div>').appendTo(containerHead);
    let info = $('<div class="tt-info"></div>').appendTo(containerInfo);
    let details = $('<div class="text-muted small-font" style="margin-top:5px;"></div>').appendTo(containerInfo);
    let detailsText = "— ";

    let type = contributionType === undefined ? item.type : contributionType;

    switch (type) {
        case 'ACTOR':
            detailsText += item.headOffice != null ? item.headOffice : item.residence;
            picture.css("background-image", "url(" + item.defaultAvatar + ")");
            info.text(item.fullname);
            break;
        case 'DEBATE':
        case 'ARGUMENT_CONTEXTUALIZED':
            if(item.folderTags != null)
                detailsText += stringSeparator(item.folderTags, "name");
            info.text(item.fullTitle);
            break;
        case 'EXCERPT' :
            if(item.authors != null)
                detailsText += stringSeparator(item.authors, "fullname");
            info.text(item.workingExcerpt);
            break;
        case 'TEXT':
            if(item.authors != null)
                detailsText += stringSeparator(item.authors, "fullname");
            info.text(item.title);
            break;
        case 'FOLDER':
            info.text(item.folderName);
            if(item.nbContributions >= 10)
                detailsText += Messages('folder.composition.nbContributions', item.nbContributions);
            break;
        default:
            // ignore and return empty string
            return '';
    }

    if(contributionTypeAsNum(type) > 0){
        $('<i class="color-primary ' + Messages('browse.search.tip.' + type) + '"></i>').appendTo(picture);
    }

    if((typeof detailsText=== 'string' || detailsText instanceof String) && detailsText.length > 2) {
        details.text(detailsText);
    }
    container.append('<hr class="hr-small-top">');

    return container;
}

function contributionTypeAsNum(type){
    switch (type) {
        case 'ACTOR':
            return 0;
        case 'DEBATE':
            return 1;
        case 'ARGUMENT_CONTEXTUALIZED':
            return 3;
        case 'EXCERPT' :
            return 4;
        case 'TEXT':
            return 5;
        case 'FOLDER':
            return 6;
        default:
            return -1;
    }
}

/**
 * Execute a search request with given searchInput form. Will fill in results of request into given
 * resultDiv html container. If defined, toggle given buttons on selection of one results.
 *
 * @param searchInput the search input form to be used to build the search query
 * @param resultDiv the div panel where the result html code must be placed
 * @param pager the pager selector that will handle paging for this search results (jquery-like selector)
 * @param perPage the amount of results per page
 * @param toggleBtn selector to associate buttons that must be toggled when a result entry is selected (optional)
 * @param detailed boolean saying if contribution cards (more details) must be retrieved (optional, default false)
 * @param embedded boolean to say if the search is embedded in a modal frame, ie, elements will be selectable (optional)
 * @returns {Promise} the pager Object (used to re-page if post filtering is done on results)
 */
function executeSearch(searchInput, resultDiv, pager, perPage, toggleBtn, detailed, embedded) {
  return displayResults(doSearch((searchInput instanceof jQuery ? searchInput : $(searchInput)), detailed, embedded), resultDiv, pager, perPage, toggleBtn, embedded);
}

/**
 * Fill in given results (html) into given resultDiv html container. If defined, toggle given buttons on
 * selection of one results
 *
 * @param results the html code to be added into given result div
 * @param resultDiv the div panel where the result html code must be placed
 * @param pagerId the pager ul id that will handle the paging for this search results (jquery-like selector)
 * @param perPage the amount of results per page
 * @param toggleBtn selector to associate buttons that must be toggled when a result entry is selected (optional)
 * @param embedded boolean to say if the search is embedded in a modal frame, ie, elements will be selectable (optional)
 * @returns {Promise} the pager Object (used to re-page if post filtering is done on results)
 */
function displayResults(results, resultDiv, pagerId, perPage, toggleBtn, embedded) {
  // start timer for long search queries
  var timeout = startWaitingModal();
  var previousFilters = $('.vertical_filter');

  return new Promise(function (resolve, reject) {
    results.done(function (html) {
      // close possible suggestions, if any
      $(resultDiv).parent().find('.tt-dropdown-menu').hide();
      $(resultDiv).empty().append(html);

      stopWaitingModal(timeout);

      if (toggleBtn !== undefined) {
        // we are in an embedded search panel
        $(toggleBtn).show();
        toggleSummaryBoxes(resultDiv, toggleBtn);
        var pageable = $(resultDiv).find('.pageable');
        pageable.addClass('pointable-content');
        pageable.prev('.instructions').show();

      } else {
        // replace query string in url bar since we are not in an embedded search panel
        var fullquery = $('#fullquery').val();
        if (fullquery !== "") {
          var url = window.location.pathname.substring(0, window.location.pathname.lastIndexOf('browse') + 6);
          history.replaceState({}, document.title, url + "/" + encodeURIComponent(fullquery));
        }
      }
      var pager = new Pager($(resultDiv).find('.pageable'), perPage, pagerId);
      pager.reset();
      if(previousFilters.length > 0) previousFilters.remove();
      var filter = $('.vertical_filter');
      if (!embedded && filter.length > 0) {
        handleFilter($('.toggle_filter'), pager);
        // show it by default if we are not in an embedded search and screen width is at least 850px
        if (screen.width > 850) {
          filter.addClass('vertical_filter__opened');
          $('.vertical_filter_remaining').addClass('opened');
        }
      }
      // force cleanup of search query (because we maybe have been redirected from an specialised search
      $('#queryString').val('');
      resolve(pager);

    }).fail(function () {
      // show general error div
      stopWaitingModal(timeout);
      var errorDiv = $('#error-div');
      showMe($(errorDiv), true, true);
      window.setTimeout(function () { showMe($(errorDiv), false, true); }, 6000);
      reject();
    });
  });
}

/**
 * Handle toggling of summary boxes and enable/disable linked button
 *
 * @param context selector for the context to which the summary boxes must be retrieved and handled
 * @param toggleBtn selector for buttons that must be toggled when a summary box is selected
 */
function toggleSummaryBoxes(context, toggleBtn) {
  toggleBtn = (toggleBtn instanceof jQuery) ? toggleBtn : $(toggleBtn);
  context = (context instanceof jQuery) ? context : $(context);
  var summary = context.find('.summary');

  summary.each(function() {
    // toggle 'selected' class of clicked summary (only one at a time) and toggle disabled state of toggleBtn
    $(this).on('click', function(e) {
      var target = $(e.target);
      if (target.prop('tagName') !== 'BUTTON' && target.prop('tagName') !== 'A' && target.parents('a').length === 0) {
        var that = $(this);
        // was not previously selected
        if (!that.hasClass('selected')) {
          var previous = context.find('.selected');
          if (previous.length > 0) {
            // another one was selected and we selected a new one => do not toggle buttons
            previous.removeClass('selected');
          } else {
            // no one was selected => toggle buttons
            toggleBtn.each(function() { $(this).prop('disabled', !$(this).prop('disabled')); });
          }
        } else {
          // unselected this one => toggle buttons
          toggleBtn.each(function() { $(this).prop('disabled', !$(this).prop('disabled')); });
        }
        that.toggleClass('selected');
      }
    });
  });
}

/**
 * Image preview and file input (thanks to http://bootsnipp.com/snippets/featured/input-file-popover-preview-image)
 *
 * @param subdiv the subdiv pointing either on person or organization
 * @param path the path where the picture must be retrieved from
 */
function managePictureField(subdiv, path) {
  // Create the close button
  var closebtn = $('<button/>', {
    type:'button',
    text: 'x',
    id: 'close-preview',
    style: 'font-size: initial;'
  });

  closebtn.attr('class', 'close pull-right');
  // Set the popover default content
  $(subdiv + ' .file-input-group').popover({
    trigger:'manual',
    html:true,
    title: Messages("general.label.pic.preview") + $(closebtn)[0].outerHTML,
    content: Messages("general.label.pic.noimage"),
    placement:'top'
  });

  $(subdiv).find('#close-preview').on('click', function(){
    $(subdiv + ' .file-input-group').popover('hide');
    // Hover before close the preview
    $(subdiv + ' .file-input-group').hover(
			function () {
				$(subdiv + ' .file-input-group').popover('show');
			},
			function () {
				$(subdiv + ' .file-input-group').popover('hide');
			}
    );
  });

  // Clear event
  $(subdiv + ' .file-input-clear').on('click', function () {
    $(subdiv + ' .file-input-group').attr("data-content",'').popover('hide');
    $(subdiv + ' .file-input-group-filename').val('');
    $(subdiv + ' .file-input-clear').hide();
    $(subdiv + ' .file-input-group-input input:file').val('');
  });

  // Create the preview image
  $(subdiv + ' .file-input-group-input').find('input:file').on('change', function () {
    var timeout = setTimeout(function () {
      $('#wait-for-it').modal('show');
    }, 1000);

    var img = $('<img/>').addClass('avatar');
    var file = this.files[0];
    if (file.size > 5242880) {
      clearTimeout(timeout);
      hideAndDestroyModal('#wait-for-it');
      slideMessage($('#error-image'));
    } else {
      // Set preview image into the popover data-content if content looks appropriate
      checkOffensivePicture($(subdiv).find('form').length > 0 ? $(subdiv).find('form') : $(subdiv).closest('form')).done(function () {
        var reader = new FileReader();
        reader.onload = function (e) {
          clearTimeout(timeout);
          hideAndDestroyModal('#wait-for-it');

          $(subdiv + ' .file-input-clear').show();
          $(subdiv + ' .file-input-group-filename').val(file.name);
          img.attr('src', e.target.result);
          $(subdiv + ' .file-input-group').attr('data-content', $(img)[0].outerHTML).popover('show');
        };
        reader.readAsDataURL(file);

      }).fail(function (xhr) {
        clearTimeout(timeout);
        hideAndDestroyModal('#wait-for-it');
        showErrorMessage(xhr);
      });
    }
  });

  $(document).on('click', '.popover .close' , function(){
    $(this).parents(".popover").popover('hide');
  });

  $(subdiv + ' .file-input-group-filename').on('click', function() {
    $(this).parent().popover('toggle');
  });

  // if elements are pre-filled (update), manage picture and preview
  loadExistingPicture(subdiv, path);
}

/**
 * Load and display an image in the popover for an actor
 *
 * @param subdiv the actual subdiv where the picture field is
 * @param path the path where the picture must be retrieved from
 */
function loadExistingPicture(subdiv, path) {
  var file = $(subdiv + ' .avatar-field').val();
  if (file !== undefined && file !== '') {
    var img = $('<img/>').addClass('avatar');
    var id = $('#id').val();
    // check where we have to find the avatar image, since the form may be re-loaded in error
    img.attr('src', (id !== -1 && file.indexOf(id) === 0 ? path : '/tmp/') + file);
    $(subdiv + ' .file-input-group').attr("data-content",$(img)[0].outerHTML).popover("show");
  } else {
    $(subdiv + ' .file-input-clear').hide();
  }
}

/**
 * Dispatching function to get a modal page from given ajax call and add it to given html anchor
 *
 * @param id the id being the parameter of given callme function
 * @param callme a function to call
 * @param anchor the html anchor to replace the content with the result of given function call
 */
function getModalFromFunction(functionCall, id, anchor) {
  functionCall(id).done(function (modal) {
    loadAndShowModal(anchor, modal);
  }).fail(function (xhr) {
    if (xhr.status === 400) {
      showErrorMessage(xhr);
    } else {
      // error -> full rebuild
      replaceContent('body', xhr.responseText, 'body');
    }
  })
}

/**
 * Remove all accents and set to lower case this string
 * @returns {string} this string where all strings have been replaced and all characters set to lower case
 */
String.prototype.removeAccents = function () {
  return this
      .replace(/[áàãâä]/gi,"a")
      .replace(/[éè¨ê]/gi,"e")
      .replace(/[íìïî]/gi,"i")
      .replace(/[óòöôõ]/gi,"o")
      .replace(/[úùüû]/gi, "u")
      .replace(/[ç]/gi, "c")
      .replace(/[ñ]/gi, "n")
      .replace(/[^a-zA-Z0-9]/g," ")
      .toLowerCase();
};

/**
 * Remove all blank spaces
 * @returns {string} this string where all blank spaces are removed
 */
String.prototype.removeSpaces = function () {
  return this
    .replace(" ","")
};

function getUrlParam(param){
  var vars = {};
  window.location.href.replace( location.hash, '' ).replace(
    /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
    function( m, key, value ) { // callback
      vars[key] = value !== undefined ? value : '';
    }
  );

  if ( param ) {
    return vars[param] ? vars[param] : null;
  }
  return vars;
}

function getAutoCreatedModals(){
  autoCreateModalsFunctions();
  autoCreateModalsListener();
}

function autoCreateModalsFunctions(){
  // actor auto-creation if any
  getAutoCreatedActors().done(function (data, status, xhr) {
    if (xhr.status === 200) {
      $('#autocreated').empty().append(data);
      manageActorModal($(document).find('#modal-actor'), '#msg-div');
      fadeMessage();
    }
  })
  .fail(function () {
    // folder auto-creation if any
    getAutoCreatedFolders().done(function (data, status, xhr) {
      if (xhr.status === 200) {
        $('#autocreated').empty().append(data);
        manageFolderModal($(document).find('#modal-folder'), '#msg-div');
        fadeMessage();
      }
    })
    .fail(function () {
      // profession auto-creation if any
      getAutoCreatedProfessions().done(function (data, status, xhr) {
        if (xhr.status === 200) {
          $('#autocreated').empty().append(data);
          $('#editProfession').modal('show');
          fadeMessage();
        }
      });
    });
  });
}

function autoCreateModalsListener(){
  $(document).on( "autoCreateModalRemoved", function( event ) {
    autoCreateModalsFunctions();
  });
}

function autoCreateModalRemoved(){
  $( document ).trigger( "autoCreateModalRemoved");
}


/**
 * Handle response of contribution (actor or folder) submission from modal frame, may replace modal content with new modal
 * or simply dispose it and show the message to body
 *
 * @param modal the modal frame
 * @param data the data to put in modal frame, if any
 * @param msgdiv the div where the messages (e.g. submission statuses) will be shown
 * @param managePanel the dedicated form manage panel
 */
function handleContributionModalResponse(modal, data, msgdiv, managePanel) {
  // get modal data, if any
  var modaldata = $('<div>').append(data).append('</div>');
  var content = modaldata.find('.modal-body');
  if (content.length > 0) {
    var title = modaldata.find('.modal-header');
    // replace header and body content
    $(modal).find('.modal-header').empty().append($(title).children());
    $(modal).find('.modal-body').empty().append($(content).children());
    // re-add content-dependant listeners
    managePanel();
    fadeMessage();
  } else {
    // no more modals, put message on body and hide modal
    hideAndDestroyModalAndCallFunction(modal, autoCreateModalRemoved);
    showMessage(data, msgdiv);
  }
}

function convertDate(date){
    return date.getDate() + "/" + (date.getMonth() < 9 ? '0' : '') + (date.getMonth()+1) + "/" + date.getFullYear();
}

/**
 * Convert string to boolean. If string is not equels to true or false return undefined
 *
 * @param string the string to convert
 * @return the converted boolean
 */
function convertStringToBoolean(string){
  return (string === "true" ? true : (string === "false" ? false : undefined));
}

/**
 * Manage change tab for visualization (filters and dependances)
 *
 * @param reloadLinkedContributions function to reload linked contributions in depend of the tab viz
 * @param getLinkedContributions function to get linked contributions in depend of the tab viz
 * @param filterbars the list of tabs filterbar
 * @param id the id of the visualized contribution
 * @param params for each pane, reload true if the pane doesn't need to be reloaded
 * @return the converted boolean
 */
function manageChangeTab(reloadLinkedContributions, getLinkedContributions, filterbars, id, params) {
    var tabs = $("#tabs");

  loadVisualizations(reloadLinkedContributions, getLinkedContributions, id, params, filterbars);
  $('.nav-tabs a').on('click', function () {
      var tabType = getTabType(tabs);
      var container = $("#"+tabType+"content");
      applyChangeVizTab(container, filterbars, false, params[tabType].filterVisible);

      if(container.exists()) {
          if(container.hasClass('dataloader-container')){
              container.trigger('dataloader-container-focused');
          }else{
              container.find('.dataloader-container:visible').each(function (key, element) {
                  $(element).trigger('dataloader-container-focused');
              });
          }
          updatePOV(container.parent().find('.btn-pov[value="0"]'), tabType);
      }
  });

    $(document).on("reload-viz", function(){
        loadVisualizations(reloadLinkedContributions, getLinkedContributions, id, params, filterbars);
    });

  tabs.parent().on('click', '#show-filter', function (e) {
      var tabType = getTabType(tabs);
      var content = $("#"+tabType+"content");

      if(params[tabType].isToLoad && content.exists() && !$('#main-content').children('.vertical_filter').exists()){
        applyChangeVizTab(content, filterbars, true, params[tabType].filterVisible);
      }
  });

    managePOV(tabs);
}

/**
 * Manage change tab for visualization V2
 *
 */
function manageChangeTab2() {
    var tabs = $("#tabs");

    $('.nav-tabs a').on('click', function () {
        let container = $("#"+$(this).attr("aria-controls").split("graphy")[0]+"content");

        $(document).trigger("close_filterbar");

        if(container.exists()) {
            if(container.hasClass('dataloader-container')){
                container.trigger('dataloader-container-focused');
            }else{
                container.find('.dataloader-container:visible').each(function (key, element) {
                    $(element).trigger('dataloader-container-focused');
                });
            }
        }
    });

    managePOV(tabs);
}

function managePOV(tabs){
    tabs.parents('div[id$="-viz"]').on('click', '.btn-pov', function () {
        let tabType = getTabType(tabs);
        updatePOV($(this), tabType);
    });
}

function updatePOV(btn, tabType){
    let target = $("#" + btn.data("target"));
    let nochange = btn.data("nochange");
    let current;

    if (!btn.hasClass('btn-success')) {
        current = btn.siblings('.btn-success');
        if(!nochange) {
            btn.addClass('btn-success');
            current.removeClass('btn-success');
        }
    }else{
        current = btn;
    }

    let val = isNaN(btn.val()) ? 0 : btn.val();

    replaceHistory('pov', val);

    let staticDataSelector = "#" + tabType + "-data-display-" + val;
    let staticData = target.exists() ? target.find(staticDataSelector) : $(staticDataSelector);

    if (staticData.exists()) {

        if(staticData.find('.dataloader-container').exists()) {
            $(document).trigger("close_filterbar");
            staticData.find('.dataloader-container').trigger('dataloader-container-focused');
        }

        let currentData = $("#" + tabType + "-data-display-" + current.val());

        if (staticData.parents(".chart-container").exists()) {
            currentData.parent().parent().parent().hide();
            staticData.parent().parent().parent().show();
        } else {
            staticData.siblings().hide();
            staticData.show();
        }
    }
}

function getTabType(tabs){
    return tabs.find(".active").children().first().attr("aria-controls").split("graphy")[0];
}

/**
 * Get the current viz and manage behavior
 *
 * @param reloadLinkedContributions function to reload linked contributions in depend of the tab viz
 * @param getLinkedContributions function to get linked contributions in depend of the tab viz
 * @param id the id of the visualized contribution
 * @param params for each pane, onlyBar is true if the callback only return filterbar
 * @param filterbars the list of tabs filterbar
 * @return the converted boolean
 */

function loadVisualizations(reloadLinkedContributions, getLinkedContributions, id, params, filterbars) {
    if(reloadLinkedContributions != null && getLinkedContributions != null) {
        reloadLinkedContributions(id).done(function (data) {
            for (var index in params) {
                if (params[index].isToLoad) {
                    var vizSelector = $('#' + index + "content");
                    if (vizSelector.exists()) {
                        var waitForIt = vizSelector.parent().find(".waiting");
                        showMe(waitForIt, true, true);
                        doLoadVisualizationsCall(getLinkedContributions(id, index), waitForIt, vizSelector, filterbars)
                    }
                }
            }
        }).fail(function (jqXHR) {
            // display error message
            $('#msg-div').append(jqXHR.responseText);
        });
    }
}

function doLoadVisualizationsCall(call, waitForIt, container, filterbars){
    call.done(function (html) {
        showMe(waitForIt, false, true);
        container.empty().append(html);
        var filter = container.find(".vertical_filter");
        if(filter.exists()) {
            filterbars[container.attr('id')] = filter.clone();
            filter.remove();
        }

    }).fail(function (jqXHR) {
        // display error message
        showMe(waitForIt, false, true);
        $('#msg-div').append(jqXHR.responseText);
    });
}

/**
 * Apply the change viz
 *
 * @param container the tab container
 * @param filterbars the list of tabs filterbar
 * @param triggerFilter true if filters must be triggered at the end
 * @param filterVisible true if filter must match only visible filterable, false if onyl hidden and null if the whole.
 */
function applyChangeVizTab(container, filterbars, triggerFilter, filterVisible){
    removeFilterbar();

    var togglerFilters = container.find('.toggle_filter');
    togglerFilters.prop('disabled', false);

    var pager;
    if(container != null && container !== undefined) {
        var pagerContainer = container.parent().find('#pager');
        var pageable = container.find('.pageable');
        if (pageable  !== undefined && pagerContainer !== undefined) {
            pager = new Pager(pageable, 15, container.parent().find('#pager'));
            pager.reset();
        }
    }

    handleFilter(togglerFilters, pager, container, null, null, true, filterbars[container.attr("id")], filterVisible);
    if(triggerFilter)
        $(document).trigger('open_filterbar');
}

/**
 * Remove the current mail filterbar if any
 *
 */
function removeFilterbar(){
  var container = $('#main-content');
  var filterbar = container.children('.vertical_filter');
  if(filterbar !== undefined && filterbar.exists()) {
    filterbar.removeClass('vertical_filter__opened');
    container.removeClass('opened');
    filterbar.remove();
  }
}

/**
 * Replace the url address given param with given value
 *
 * @param name the name of the param
 * @param replaceValue the value to give at the param
 */
function replaceHistory(name, replaceValue){
    var url = window.location.href;
    if(url.lastIndexOf(name) > -1) {
        var url2 = url.split("");
        url2[url.lastIndexOf(name) + name.length + 1] = replaceValue;
        history.replaceState({}, document.title, url2.join(""));
    }else{
        history.replaceState({}, document.title,
            url + (window.location.href.indexOf("?") === -1 ? "?" : "&") + name + "=" + replaceValue);
    }
}

/**
 * Switch font awesome icones
 *
 * @param elem the element where the icones are
 * @param icon1 the first icone to switch
 * @param icon2 the second icone to switch
 */
function changeFaIcon(elem, icon1, icon2){
    if (elem.attr('data-icon') === icon1 ) {
        elem.attr('data-icon', icon2);
    } else {
        elem.attr('data-icon', icon1);
    }
}

/**
 * Manage on scroll animation event for contribution visualization.
 * Keep the contribution viz title visible on the top of the screen when scroll down.
 *
 */
function vizHeadScrollAnimation() {
    var viz_header = $(".viz-head");
    var viz_header_flex = $(".viz-head-flex");
    var viz_header_title = $(".viz-head-title");

    $( window ).scroll(function () {
        var viz_header_scrolled = "viz-head-scrolled";
        var viz_header_flex_scrolled = "viz-head-flex-scrolled";
        var scrollTop = $(this).scrollTop();

        if (scrollTop < 50 && viz_header.hasClass(viz_header_scrolled)) {
            viz_header.removeClass(viz_header_scrolled);
            viz_header_flex.removeClass(viz_header_flex_scrolled);
            viz_header_title.trunk8({ lines : 2});
        } else if (scrollTop >= 50 && !viz_header.hasClass(viz_header_scrolled)) {
            viz_header.addClass(viz_header_scrolled);
            viz_header_flex.addClass(viz_header_flex_scrolled);
            viz_header_title.trunk8({ lines : 1});
        }
    });
}


/**
 * Build both charts (displayed one and exportable/hidden one)
 *
 * param chartData the data needed to create the chart
 * param label the label of the y axe
 */
function initSocioChart(id, chartData, label) {
    var graph = '#socio-data-display-' + id;
    var content = $('#socio-data-content-' + id);

    doInitSocioChart(chartData, label, $(graph).parent(), $(graph).clone(), graph, content);
}

/**
 * Build both charts (displayed one and exportable/hidden one) and reload when filtered
 *
 * param chartData the data needed to create the chart
 * param label the label of the y axe
 * param graphContainer the container the contains the graph
 * param graphCopy the copy of the graph canvas to reset it when reload
 * param graphName the id of the graph
 * param content the data needed to build the graph
 */
function doInitSocioChart(chartData, label, graphContainer, graphCopy, graphName, content) {
    var categories = [];
    graphContainer.empty();
    graphContainer.append(graphCopy.clone());

    makeCategories(content, categories, chartData, label);
    makeSocioGraph(graphName, categories, chartData);

    $(document).on('change-filter', function(){
        doInitSocioChart(chartData, label, graphContainer, graphCopy, graphName, content);
    });
}

/**
 * Fill in the categories for the socio graph
 *
 * param content the container where data are
 * param categories a dictionary of categories to be used as graph labels
 * param chartData data to be shown
 * param label the label of the y axe
 */
function makeCategories(content, categories, chartData, label) {
    var values;

    chartData.labels = [];
    for (var i = 0; i < chartData.datasets.length; i++) {
        chartData.datasets[i].data = [];
    }

    content.find('.actor-thumbnail:not(.filtered)').each(function () {
        categories.push($(this).prop("outerHTML"));

        var value = 0;
        values = $(this).find('span').text().split(',');
        for (var i = 0; i < chartData.datasets.length; i++) {
            value += parseInt(values[i]);
            chartData.datasets[i].data.push(parseInt(values[i]));
        }

        chartData.labels.push(value + " " + label);
    });
}

/**
 * Create the sociography graph to happen to given graph div with given categories
 *
 * param graphName the graph container id
 * param categories the categories of the graph (names to be shown in front of each lines)
 * param data the data to show
 */
function makeSocioGraph(graphName, categories, data) {
  // Make label
  if(categories.length === 0){
    $(graphName).hide();
      $(graphName).closest(".chart-container").find(".no-socio").show();
  }else {
      var graph = $(graphName);
      graph.show();
      $("#no-socio").hide();
      var container = graph.parent().siblings(".chart-labels");
      container.empty();
      for (var i in categories) {
          container.append(categories[i]);
      }
      // Make graph
      graph.prop("height", 70 + 40 * categories.length);
      var width = graph.parent().parent().parent().width() - 50;
      var labelWidth = container.width();
      graph.prop("width", (width - labelWidth < 700 ? 700 : width - labelWidth));
      makeHorizontalBarGraph(graph, data, null, {display : true});
  }
}

/**
 * Create a horizontal bar graph
 *
 * param graph the graph canvas
 * param data the data to show
 * param title the title param
 * param legend the legend param
 */
function makeHorizontalBarGraph(graph, data, title, legend) {
    var ctx = graph[0].getContext('2d');
    window.myBar = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: data.labels,
            datasets: [{
                label: data.datasets[0].label,
                data: data.datasets[0].data,
                backgroundColor: data.datasets[0].backgroundColor
            },{
                label: data.datasets[1].label,
                data: data.datasets[1].data,
                backgroundColor: data.datasets[1].backgroundColor
            },{
                label: data.datasets[2].label,
                data: data.datasets[2].data,
                backgroundColor: data.datasets[2].backgroundColor
            }]
        },
        options: {
            responsive: false,
            maintainAspectRatio: false,
            tooltips: {
                callbacks: {
                    title: function (tooltipItem, data) {
                        return "";
                    },
                    label: function (tooltipItem, data) {
                        return tooltipItem.xLabel > 0 ? (tooltipItem.xLabel + " " + data.datasets[tooltipItem.datasetIndex].label) : "";
                    }
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    position: 'top',
                    ticks: {
                        beginAtZero: true,
                        stepSize: 1
                    }
                }],
                yAxes: [{
                    stacked: true,
                    position: 'right',
                    barThickness: 20,
                    gridLines: {
                        display: false
                    }
                }]
            },
            title : title,
            legend : legend
        }
    });
}

/**
 * Start timer for long of the operation to show waiting modal
 *
 * return the timeout to stop the timer
 */
function startWaitingModal(){
    // start timer for long search queries
    return setTimeout(function () {
        $('#wait-for-it').modal('show');
    }, 1000);
}

/**
 * Stop waiting modal
 *
 * param the current timer to stop
 */
function stopWaitingModal(timeout){
    clearTimeout(timeout);
    $('#wait-for-it').modal('hide');
}

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
            tmp = item.split("=");
            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}

/**
 * Check if an given object if empty or not
 *
 * @param obj the object to check
 */
function isEmpty(obj){
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}

function loadDataAsync(toCall, params, postTreat){
    toCall(params, )
}


/*
 * various helper functions
 */

/**
 * Check whether given value exists in given array
 *
 * @param value a value
 * @param array an array
 * @returns {boolean}
 */
function isInArray (value, array) {
    return array.indexOf(value) > -1;
}


/**
 * Remove given value from given array
 *
 * @param value a value to remove
 * @param array an array
 * @returns {*}
 */
function removeValueFromArray (value, array) {
    var index = array.indexOf(value);
    if (index > -1) {
        array.splice(index, 1);
    }
    return array;
}


/**
 * Determine if a variable is a int
 *
 * @param node the node to determine
 * @return true if it is
 */
function isInt(value) {
    return !isNaN(value) &&
        parseInt(Number(value)) === value &&
        !isNaN(parseInt(value, 10));
}


function createContributionOptions(vizOptions, editOpitons, addOptions){
    return createContributionOption("fa-eye", vizOptions, "general.btn.contribution.see.tooltip", "")
        + createContributionOption("fa-pencil-alt", editOpitons, "general.btn.contribution.edit.tooltip", '')
        + createContributionOption("fa-plus", addOptions, "general.btn.contribution.add.tooltip", 'padding-right:0;');
}

/**
 *
 *
 * @param
 */
function createContributionOption(faType, options, tooltip, style){
    return '<div class="dropdown" style="margin: 0;padding: 0;"><button type="button" ' +
        'class="btn primary btn-link btn-simple-link tree_node_option dropdown-toggle" ' +
        'style="font-size : 16px; ' + style + '" data-toggle="dropdown" title="' + Messages(tooltip) + '">' +
        '<i class="fas ' + faType + '"></i></span></button>' + options + '</div>';
}

function createContributionOptionContextMenuBtn(id, title, fa, tooltip, nodeClass){
    nodeClass = nodeClass || '';
    return '<li><button class="btn btn-link primary ' + id + '" title="' + Messages("empty") + '">' +
        '<span class="' + nodeClass + '"><span class="fas ' + fa + ' fixed-size"></span>&nbsp' + Messages(title) + '</span></button></li>';
}

function getContributionOptionData(option, data){
    return option.data(data) !== undefined ? option.data(data) :
        option.closest("ul").data(data) !== undefined ? option.closest("ul").data(data) : option.closest('li[id^="aff"]').data(data);
}

/**
 * Trigger an event from anywhere to ask to reload viz or tree
 */
function triggerReloadVizEvent(){
    $(document).trigger("reload-viz");
}

/**
 * EXPANDABLE ZONE
 */

function initExpandableZone() {

    centerAllContents();

    $(".expandable-btn-left").on('click', function (e) {
        doSideChange($(this), true);
    });

    $(".expandable-btn-right").on('click', function (e) {
        doSideChange($(this), false);
    });

    $(".expandable-btn-middle").on('click', function (e) {
        centerContents($(this).parents(".expandable-container"));
        $(document).trigger("reload-annotated-text");
    });
}

function doSideChange(container, toLeft){
    var parent = container.parents(".expandable-container");
    var zone1 = toLeft ? parent.find(".expandable-left") : parent.find(".expandable-right");
    var zone2 = toLeft ?  parent.find(".expandable-right") : parent.find(".expandable-left");
    zone1.css("width", "0");
    if(toLeft) zone1.css("visibility", "hidden");
    zone2.css("width", "100%");
    zone2.css("visibility", "visible");
    $(document).trigger("reload-annotated-text");
}

function centerAllContents(){
    var container = $(".expandable-container");
    container.each(function(){
        centerContents($(this));
    });
}

function centerContents(container){
    var children = container.find('.expandable-zone');
    var child_width = (container.width() - 10) / children.length;

    children.each(function(){
        $(this).width(child_width);
        $(this).css("visibility", "visible");
    });
}

function togglexxs(){
    $(".hidden-xxs").each(function() {
        if ($(this).width() < 300) {
            $(this).hide();
        } else{
            $(this).show();
        }
    });
}

function treatSaveLinkFail(modal, status){
    hideAndDestroyModal(modal);
    switch (status) {
        case 400:
            slideMessage($('#warning-link-exists'));
            break;
        default :
            slideMessage($('#error-save'));
    }
}

function convertTextAnnotationModalWidth(modal, half){
    half = half || false;
    var width = modal.width() * 0.9;
    return half ? width / 2 : width - 102;
}

function initContainerPagers(container, perPage){
    perPage = perPage || 5;

    container.find(".pageable").each(function (key, element){
        element = $(element);

        new Pager(element, perPage,
            element.next().hasClass("pager-container") ? element.next() : createPagerContainer(element)[0],
            undefined, undefined, false, 2).reset();
    });
}

function createPagerContainer(container){
    let r = [];
    r[0] = $('<div class="col-xs-12 flex-container pager-container"></div>').insertAfter(container);
    r[1] = $('<ul class="pager pagination pagination-sm no-margin"></ul>').appendTo( r[0] );
    return r;
}

function toggleFollowedGroups(container, pager){
    pager = pager || null;
    // toggle groups by followed state
    container.find('#toggle-follow-group').on('click', function (e) {
        if (pager == null){
            container.find('.notfollowed-group').toggle();
        }else{
            container.find('.notfollowed-group').toggleClass("filtered");
            container.find('.notfollowed-group').hide();
            pager.reset();
        }
        $(this).find('span').toggle();
    });
}

function makeTextContentReadable(content, nbParagraph){
    nbParagraph = nbParagraph || 3;
    let response = "";
    let sentences = content.split(".");

    for(let iSentence = 1; iSentence < sentences.length; iSentence++){
        response += sentences[iSentence - 1].trim() + ". ";
        if(iSentence % nbParagraph === 0){
            response += "\n\n";
        }
    }

    return response;
}

/**
 * Extract a date from a given string.
 *
 * @param text the string.
 */
function extractDateFromString(text){
    let date = new Date(text);

   if(isNaN(date.getTime())) {

       let months = {
           "jan": "01",
           "jan.": "01",
           "janvier": "01",
           "january": "01",
           "januari": "01",
           "fev": "02",
           "fev.": "02",
           "feb": "02",
           "février": "02",
           "february": "02",
           "februari": "02",
           "mar." : "03",
           "mar" : "03",
           "mars" : "03",
           "march" : "03",
           "maart" : "03",
           "avr" : "04",
           "avr." : "04",
           "apr" : "04",
           "avril" : "04",
           "april" : "04",
           "mai." : "05",
           "mai" : "05",
           "may" : "05",
           "mei" : "05",
           "jun." : "06",
           "jun" : "06",
           "juin" : "06",
           "june" : "06",
           "juni" : "06",
           "jul." : "07",
           "jul" : "07",
           "juillet" : "07",
           "july" : "07",
           "juli" : "07",
           "aou." : "08",
           "aou" : "08",
           "aug" : "08",
           "août" : "08",
           "august" : "08",
           "augustus" : "08",
           "sep" : "09",
           "sep." : "09",
           "septembre" : "09",
           "september" : "09",
           "oct." : "10",
           "oct" : "10",
           "okt" : "10",
           "octobre" : "10",
           "october" : "10",
           "oktober" : "10",
           "nov" : "11",
           "nov." : "11",
           "novembre" : "11",
           "november" : "11",
           "dec." : "12",
           "dec" : "12",
           "decembre" : "12",
           "december" : "12"
       };

       let patternMonth = new RegExp('( )(' + getTabAsPattern(months) + ')( )', 'i');
       let match = patternMonth.exec(text);

       if(match != null){
           let patternDay = new RegExp('0[1-9]|[12][0-9]|3[01]', 'i');
           let patternYear = new RegExp('[12][0-9]{3}', 'i');

           let splitted = text.split(" ");
           let monthName = match[0].toLowerCase().trim();
           let indexOfMonth = text.substr(0, match.index + monthName.length).split(" ").length - 1;
           // search day
           let day = matchPatternInStrings(splitted, patternDay, indexOfMonth);
           // search year
           let year = matchPatternInStrings(splitted, patternYear, indexOfMonth);

           if(day != null && year != null){
               return day + "/" + months[monthName] + "/" + year;
           }
       }
   } else{
       return date.getDate() + "/" + (parseInt(date.getMonth()) + 1) + "/" + date.getFullYear()
   }
    return null;
}

/**
 * Get a list indexed array of strings into a regex pattern.
 *
 * @param tab the indexed array
 */
function getTabAsPattern(tab){
    let ch = "";
    Object.keys(tab).map((e, i) => ch += e + "|");
    return ch.substring(0, ch.length -1);
}

/**
 * Match a given pattern in a list of strings.
 *
 * @param strings the list of strings
 * @param pattern the pattern to match
 * @param indexOfMonth the index where the month has been discovered.
 */
function matchPatternInStrings(strings, pattern, indexOfMonth) {
    if (indexOfMonth < strings.length) {
        let i = indexOfMonth;
        while (i >= 0) {
            let match = pattern.exec(strings[i]);
            if(match != null) return strings[i];
            i--;
        }

        i = indexOfMonth;
        while (i < strings.length) {
            let match = pattern.exec(strings[i]);
            if(match != null) return strings[i];
            i++;
        }
    }
    return null;
}

function checkIfObjectIsEmpty(obj){
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

function elementIsVisible(element, maxCheck){
    maxCheck = isNaN(maxCheck) ? 10 : maxCheck;

    let iCheck = 0;
    while(iCheck < maxCheck++ && element.exists() && !element.is('body')){
        if(element.css('display') === 'none'){
            return false;
        }
        element = element.parent();
    }

    return true;
}

/**
 * Get some linked contribution data async
 *
 * @param container the container where the btn that will trigger the call is
 * @param btn that trigger the call event
 * @param toCall the function to call async to get that data
 */
function getLinkedDataAsync(container, btn, toCall){
    $(container).on('click', btn, function(){
        let that = $(this);
        if(!that.hasClass("loaded")) {
            let target = $(that.data("target"));
            let waitForIt = that.siblings('.wait-spinner').show();

            toCall(that.data("id")).done(function (data) {
                that.addClass("loaded");
                waitForIt.hide();
                target.html(data);
            }).fail(function () {
                console.log("Error with " + toCall);
            });
        }
    });
}

function generateHashcode(str){
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
        var character = str.charCodeAt(i);
        hash = ((hash<<5)-hash)+character;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
}

/**
 * Get the image modal for the given image element
 *
 * @param img the image element
 */
function getImageModal(img){
    img.on('click', function(){
        imageModal($(this).prop("src")).done(function(html){
            loadAndShowModal($('#modal-anchor'), html);
        }).fail(function(){
            console.log("Image modal error");
        });
    });
}

function stringSeparator(array, fieldName){
    let ch = "";
    array.forEach(function(element) {
        ch += (fieldName !== undefined ? element[fieldName] : element) + ", ";
    });
    return ch.substr(0, ch.length - 2);
}

/*
 * WORKERS
 */

function callWorkers(workerName, datum, datumJson, commonDatum, commonDatumJson, toCallIfNotSupported, withWorker, callback, limit){
    return new Promise(resolve => {
        if (Array.isArray(datum) && datum.length > 0) {
            if (typeof (Worker) !== "undefined" && withWorker) {
                // Worker supported
                let promises = [];
                let separatedDatum = [];
                let separatedDatumJson = [];

                for(let limited = 0; limited < datum.length; limited += limit){
                    separatedDatum.push(datum.slice(limited, limited + limit - 1));
                    separatedDatumJson.push(datumJson.slice(limited, limited + limit - 1));
                }

                for(let i in separatedDatum){
                    promises.push(ttest(separatedDatum[i], separatedDatumJson[i], commonDatum, commonDatumJson, workerName, callback));
                }

                Promise.all(promises).then(function(){
                    resolve(true);
                });
            } else if (toCallIfNotSupported !== undefined) {
                datum.forEach(function (data) {
                    toCallIfNotSupported(data, commonDatum);
                });
                resolve(true);
            } else {
                resolve(false);
            }
        } else {
            resolve(false);
        }
    });
}

function ttest(separatedDatum, separatedDatumJson, commonDatum, commonDatumJson, workerName, callback){
    return new Promise(resolve => {
        let w = new Worker(workerName);

        w.postMessage(JSON.stringify({datum: separatedDatumJson, commonDatum: commonDatumJson}));
        w.onmessage = function (e) {
            if (callback !== undefined) {
                e.data === undefined ? callback() : callback(separatedDatum, commonDatum, e.data);
            }
            w.terminate();
            w = undefined;
            resolve(true);
        };
    });
}