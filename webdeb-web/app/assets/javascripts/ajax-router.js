/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * This collection of functions are proxies for all Play ajax reverse routing.
 * Javascript functions have the same names as the ones declared in routes file (for easy maintenance).
 *
 * Ajax calls (play reverse routing) must also be declared in Application.javascriptRoutes() method.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */

/*
 * ACTORS
 */

/**
 * Search for an actor based on a name and a type
 *
 * @param query the term (name) to look for
 * @param actortype the actor type (int value)
 * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
 * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
 * @return {*}
 */
function searchActor(query, actortype, fromIndex, toIndex) {
  actortype = actortype === undefined ? -1 : actortype;
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.searchActor(query, actortype, fromIndex, toIndex).ajax({ async: true });
}

/**
 * Search for an actor based on a name among party members for election 2019
 *
 * @param query the term (name) to look for
 * @return {*}
 */
function searchPartyMembers(query) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.searchPartyMembers(query).ajax({ async: true });
}

/**
 * Search for details of a given unknown actor (DB-pedia call)
 *
 * @param type the actor type id (0 for person, 1 for organization)
 * @param isUrl boolean saying if we search from an url or from a name (put in value param)
 * @param value the value to look for
 * @param optional an optional value that will be used if given type is 0 and value is not an url
 * @returns {*|{}}
 */
function searchActorDetails(type, isUrl, value, optional) {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.searchActorDetails(type, isUrl, value, optional).ajax({ async: true });
}

/**
 * Get all linked contributions with an actor by viz pane type
 *
 * @param actor the actor id
 * @param type the name of the viz pane type
 * @returns {*}
 */
function getActorLinkedContributions(actor, type) {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.getLinkedContributions(actor, type).ajax({ async: true });
}


/**
 * Reload all linked contributions with an actor by viz pane type
 *
 * @param actor the actor id
 * @returns {*}
 */
function reloadActorLinkedContributions(actor, type) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.reloadLinkedContributions(actor, type).ajax({ async: true });
}

/**
 * Find the list of contextualized arguments where the given author is thinker of an excerpt linked with
 *
 * @param actor an actor id
 * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
 * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
 * @returns {*}
 */
function findActorArgumentsWhereThinker(actor, fromIndex, toIndex) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.findActorArgumentsWhereThinker(actor, fromIndex, toIndex).ajax({ async: true });
}

/**
 * Find the list of excerpts where the given author is thinker limited by indexes
 *
 * @param actor an actor id
 * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
 * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
 * @returns {*}
 */
function findActorExcerptsWhereThinker(actor, fromIndex, toIndex) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.findActorExcerptsWhereThinker(actor, fromIndex, toIndex).ajax({ async: true });
}

/**
 * Find the list of contextualized arguments where the given author is cited of an excerpt linked with or directly with the argument
 *
 * @param actor an actor id
 * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
 * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
 * @returns {*}
 */
function findActorArgumentsWhereCited(actor, fromIndex, toIndex) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.findActorArgumentsWhereCited(actor, fromIndex, toIndex).ajax({ async: true });
}

/**
 * Find the list of excerpts where the given author is cited limited by indexes
 *
 * @param actor an actor id
 * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
 * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
 * @returns {*}
 */
function findActorExcerptsWhereCited(actor, fromIndex, toIndex) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.findActorExcerptsWhereCited(actor, fromIndex, toIndex).ajax({ async: true });
}

/**
 * Find the list of excerpts where the given author is thinker limited by indexes
 *
 * @param actor an actor id
 * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
 * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
 * @returns {*}
 */
function findActorAffiliations(actor, fromIndex, toIndex) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.findActorExcerptsWhereThinker(actor, fromIndex, toIndex).ajax({ async: true });
}

/**
 * Retrieve a raw picture file from given url and get filename back (will be used to be retrieved at actor form submit)
 *
 * @param url the url where we have to get the picture
 * @returns {*|{}}
 */
function getPictureFile(url) {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.getPictureFile(url).ajax({ async: true });
}

/**
 * Ajax call to retrieve an actor by its id
 *
 * @param actorId the actor ID to retrieve
 * @return {*}
 */
function getActor(actorId) {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.getActor(actorId).ajax({ async: true });
}

/**
 * Ajax call to retrieve all functions of a given actor (by id)
 *
 * @param actorId the actor id to fetch the functions for
 * @param type a profession type
 * @param term a string term to look for also
 * @return {*}
 */
function getActorFunctions(actorId, type, term) {
    actorId = actorId !== '' ? actorId : -1;
    type = type == null ? -1 : type;
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.getActorFunctions(actorId, type, term).ajax();
}

/**
 * Handle actor form submission from modal frame
 *
 * @param form the actor form
 * @returns either a bad request with the content of the form in error, or a success status code
 */
function saveFromModal(form) {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.saveFromModal().ajax({
    type: 'POST',
    processData: false,
    contentType: false,
    data: new FormData(form[0]),
		async: true
  });
}

/**
 * Warn user canceled the submission of an Actor from a modal form
 *
 * @returns an empty response with the status code
 */
function cancelFromModal() {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.cancelFromModal().ajax({ async: true });
}

/**
 * Upload a picture for a given actor (in form)
 *
 * @param form the form query (post request) with the file and the id to post
 * @param actorId the actor id for whom we add a new picture
 * @returns {*|{}}
 */
function uploadActorPicture(form, actorId) {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.uploadActorPicture(actorId).ajax({
    type: 'POST',
    data: new FormData(form[0]),
    contentType: false,
    processData: false,
		async: true
  });
}

/**
 * Get modal frames to handle auto-created actors, if any
 * @returns {*}
 */
function getAutoCreatedActors() {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.getAutoCreatedActors().ajax({ async: true });
}

/**
 * Get modal frames to handle auto-created folders, if any
 * @returns {*}
 */
function getAutoCreatedFolders() {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.folder.FolderActions.getAutoCreatedFolders().ajax({ async: true });
}

/**
 * Get modal frames to handle auto-created professors, if any
 * @returns {*}
 */
function getAutoCreatedProfessions() {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.getAutoCreatedProfessions().ajax({ async: true });
}

/**
 * Get modal frame to add new affiliations to existing actor (id)
 *
 * @param id an actor id
 * @param isPerson true if the new affiliation concerns a person, false for a organization
 * @returns {*|{}}
 */
function newAffiliations(id, isPerson) {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.newAffiliations(id, isPerson).ajax({ async: true });
}

/**
 * Post new affiliations for given actor id
 *
 * @param id an actor id
 * @param isPerson true if the new affiliation concerns a person, false for a organization
 * @param form the ActorAffiliationForm to submit
 * @returns {*|{}}
 */
function addAffiliations(id, isPerson, form) {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.addAffiliations(id, isPerson).ajax({
    type: 'POST',
    data: form.serialize(),
		async: true
  });
}

/**
 * Get modal frame to add new member to existing actor (id)
 *
 * @param id an actor id
 * @param isPerson true if the new member concerns a person, false for a organization
 * @returns {*|{}}
 */
function newMember(id, isPerson) {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.newMember(id, isPerson).ajax({ async: true });
}

/**
 * Post new member for given actor id
 *
 * @param id an actor id
 * @param isPerson true if the new member concerns a person, false for a organization
 * @param form the ActorAffiliationForm to submit
 * @returns {*|{}}
 */
function addMember(id, isPerson, form) {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.addMember(id, isPerson).ajax({
    type: 'POST',
    data: form.serialize(),
		async: true
  });
}

/**
 * Get an actor card (properties and avatar pane)
 *
 * @param id an actor id
 * @returns {*|{}}
 */
function getActorCard(id) {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.getActorCard(id).ajax({ async: true });
}

/**
 * Get all contributions of given actor
 *
 * @param id an actor id
 *
 * @returns the partial searchResult scala template, or an error message, if an error occurred
 */
function getActorContributions(id) {
	return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.getActorContributions(id).ajax({ async: true });
}

/**
 * Reset the actor type of a given actor. This operation also delete the linked person or organization from db
 *
 * @param id an actor id
 * @return the edit page for this actor
 */
function resetActorType(id) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.resetActorType(id).ajax({ async: true });
}

/*
 * TEXTS
 */

/**
 * Find the list of contextualized arguments that are part of the given text limited by index
 *
 * @param text a text id
 * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
 * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
 */
function findTextArguments(text, fromIndex, toIndex) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.text.TextActions.findTextArguments(text, fromIndex, toIndex).ajax({ async: true });
}

/**
 * Find the list of contextualized arguments that are part of the given text limited by index
 *
 * @param text a text id
 * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
 * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
 */
function findTextExcerpts(text, fromIndex, toIndex) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.text.TextActions.findTextExcerpts(text, fromIndex, toIndex).ajax({ async: true });
}

/**
 * Get the text content of a html content found by the given url
 *
 * @param url a url for which we need is content
 * @return the html text content of the given url
 */
function getHtmlContent(url) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.text.TextActions.getHtmlContent(url).ajax({ async: true });
}

/**
 * Get modal frames to display text content, if any
 *
 * @param id a text id
 * @returns {*}
 */
function getTextContentModal(id) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.text.TextActions.getTextContentModal(id).ajax({ async: true });
}

/**
 * Search for a text
 *
 * @param query a string query
 * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
 * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
 * @return {*}
 */
function searchText(query, fromIndex, toIndex) {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.text.TextActions.searchText(query, fromIndex, toIndex).ajax({ async: true });
}

/**
 * Search for all text source name containing this term
 *
 * @param query the term to search for
 * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
 * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
 * @returns {*}
 */
function searchTextSource(query, fromIndex, toIndex) {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.text.TextActions.searchTextSource(query, fromIndex, toIndex).ajax({ async: true });
}

/**
 * Get the annotated version of given text (from WDTAL service or cache)
 *
 * @param textId a text id
 * @returns {*|{}}
 */
function getAnnotatedText(textId, synchronous, highlighted) {
  synchronous = synchronous || false;
  highlighted = highlighted === undefined ? true : highlighted;
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.text.TextActions.getAnnotatedText(textId, highlighted).ajax({ async: !synchronous });
}

/**
 * Get the content of a text from a given url
 *
 * @param url an url to get the text and other properties from (typically a web page)
 * @returns {*|{}}
 */
function getTextContent(url) {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.text.TextActions.getTextContent(url).ajax({ async: true });
}

/**
 * Get the content of a PDF from a given url
 *
 * @param url an url pointing to a pdf file
 * @returns {*|{}}
 */
function extractPDFContent(url) {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.text.TextActions.extractPDFContent(url).ajax({ async: true });
}

/**
 * Get text language
 *
 * @param text a text content
 * @returns {*|{}}
 */
function getTextLanguage(text) {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.text.TextActions.getTextLanguage(text).ajax({ async: true });
}

/**
 * Get a text radiography (all texts having arguments with similarity links to this text's arguments)
 *
 * @param id a text id
 * @param sortby int value representing a ERadiographyViewKey
 * @returns {*}
 */
function textRadiography(id, sortby) {
  return jsRoutes.be.webdeb.presentation.web.controllers.viz.VizActions.textRadiography(id, sortby).ajax({ async: true });
}

/**
 * Get all linked contributions with a text by viz pane type
 *
 * @param text the text id
 * @param type the name of the viz pane type
 * @returns {*}
 */
function getTextLinkedContributions(text, type) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.text.TextActions.getLinkedContributions(text, type).ajax({ async: true });
}

/**
 * Reload all linked contributions with a text by viz pane type
 *
 * @param text the text id
 * @returns {*}
 */
function reloadTextLinkedContributions(text, type) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.text.TextActions.reloadLinkedContributions(text, type).ajax({ async: true });
}

/**
 * Get all excerpts for given text id
 *
 * @param id a text id
 * @returns {*}
 */
function getTextExcerpts(id) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.text.TextActions.getExcerpts(id).ajax({ async: true });
}

/**
 * Get the first argument id for given context contribution id
 *
 * @param id a context contributio id
 * @returns {*}
 */
function getFirstArgument(id) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.text.TextActions.getFirstArgument(id).ajax({ async: false });
}

/**
 * Check if a given domain name is a free source or not. Free means free of rights.
 *
 * @param domain the domain name to check
 * @return true if the given domain name is a free source
 */
function checkFreeSource(domain) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.text.TextActions.checkFreeSource(domain).ajax({ async: false });
}

/*
 * CONTEXT CONTRIBUTION
 */

/**
 * Get the context contribution behind the contribution id
 *
 * @param id a context contribution id
 * @returns a debate or a text
 */
function getContextContribution(id) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.text.TextActions.getContextContribution(id).ajax({ async: true });
}

/*
 * ARGUMENTS
 */

/**
 * See all excerpts that are linked with the given argument
 *
 * @param argId a contextualized argument id
 * @param contributionId the id of the contribution to be focused on if any
 * @return {*}
 */
function seeArgumentExcerpts(argId, contributionId) {
    contributionId = contributionId || -1;
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.argument.ArgumentActions.seeArgumentExcerpts(argId, contributionId).ajax({ async: true });
}

/**
 * Search for arguments with given query (in standard form and authors)
 *
 * @param query a query string
 * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
 * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
 * @returns {*}
 */
function searchArgument(query, fromIndex, toIndex) {
    fromIndex = fromIndex === undefined ? -1 : fromIndex;
    toIndex = toIndex === undefined ? -1 : toIndex;
	return jsRoutes.be.webdeb.presentation.web.controllers.entry.argument.ArgumentActions.searchArgument(query, fromIndex, toIndex).ajax({ async: true });
}

/**
 * Search for contextualized arguments with given query title
 *
 * @param query a query string
 * @param context the context contribution to ignore
 * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
 * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
 * @returns {*}
 */
function searchArgumentContext(query, context, fromIndex, toIndex) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.argument.ArgumentActions.searchArgumentContext(query, context, fromIndex, toIndex).ajax({ async: true });
}

/**
 * Get modal frame to add a new link by either searching for an existing context contribution or adding new one
 *
 * @param contextId the context contribution where add this link
 * @param id an argument or excerpt id that will be linked to the new argument
 * @param shade the link shade that will bind both argument (or excerpt) (give one and future one), if any
 * @returns {*|{}}
 */
function argumentSelection(contextId, id, shade) {
    shade = shade || -1;
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.argument.ArgumentActions.argumentContextSelection(contextId, id, shade).ajax({ async: true });
}

/**
 * Get all shades of a given argument type and timing
 *
 * @param type an argument type
 * @param timing a timing
 * @param lang language code to fetch the shade for
 * @return the list of all shades corresponding to the given argument type and timing
 */
function getArgumentShades(type, timing, lang) {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.argument.ArgumentActions.getArgumentShades(type, timing, lang).ajax({ async: true });
}

/**
 * Get the justification map for a given argument
 *
 * @param argument an argument id
 * @return the argument cartography pane
 */
function getJustificationMap(argument) {
    return jsRoutes.be.webdeb.presentation.web.controllers.viz.VizActions.getJustificationMap(argument).ajax({ async: true });
}

/**
 * Get modal frame to add a new link by either searching for an existing text or adding new one
 *
 * @param id an argument id that will be linked to the new argument
 * @param shade the argument link shade that will bind both argument (give one and future one)
 * @returns {*|{}}
 */
function argumentContextSelection(id, shade) {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.argument.ArgumentActions.argumentContextSelection(id, shade).ajax({ async: true });
}

/**
 * open modal for edit a contextualized argument
 *
 * @param argId the argument id to edit
 * @returns {*}
 */
function editArgumentContext(argId) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.argument.ArgumentActions.editContextualizedFromModal(argId).ajax({ async: true });
}

/**
 * Submit contextualized argument form
 *
 * @param argId the argument id
 * @param form the form with the argument details
 * @param groupId the group where save this contextualized
 * @returns {*|{}} either an empty ok response (200) or the full page to be reloaded
 */
function saveArgumentContext(argId, form, groupId) {
    var serial = form.serialize();
    if(groupId != null && groupId !== undefined){
        serial += "&groupId=" + groupId;
    }

    return jsRoutes.be.webdeb.presentation.web.controllers.entry.argument.ArgumentActions.saveContextualizedFromModal(argId).ajax({
        type: 'POST',
        data: serial,
        async: true
    });
}

/**
 * open modal for edit an argument dictionary
 *
 * @param argId the argument dictionary id to edit
 * @returns {*}
 */
function editArgumentDictionary(argId) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.argument.ArgumentActions.editDictionary(argId).ajax({ async: true });
}

/**
 * Submit argument dictionary form
 *
 * @param argId the argument dictionary id
 * @param form the form with the argument dictionary details
 * @returns {*|{}} either an empty ok response (200) or the full page to be reloaded
 */
function saveArgumentDictionary(argId, form) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.argument.ArgumentActions.saveDictionary(argId).ajax({
        type: 'POST',
        data: form.serialize(),
        async: true
    });
}

/**
 * Save links from argument viz
 *
 * @param fromArgument the argument where the save is performed
 * @param type the name of the viz pane type
 * @param linkType the linkType from carto
 * @param links json-representation of LinkForms with all justification links of given text
 * @returns {*}
 */
function saveLinks(fromArgument, type, linkType, links) {
    linkType = (linkType == null ? -1 : linkType);
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.argument.ArgumentActions.saveLinks(fromArgument, type, linkType).ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        data: links,
        async: true
    });
}

/**
 * Save a link between two contextualized or simple arguments or between a contextualized argument and an excerpt
 *
 * @param from an (contextualized) argument id
 * @param to an other (contextualized) argument or an excerpt id
 * @param shade the shade id that will be used to bind both argument
 * @param group the group where save this link
 * @param context a context contribution or -1 or undefined if not needed
 * @returns {*}
 */
function saveLink(from, to, shade, group, context) {
    context = context || -1;
    shade = shade || -1;
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.argument.ArgumentActions.saveLink(from, to, context, shade, group).ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        data: "{}",
        async: true
    });
}

/**
 * Cancel the creation of a new argument to be linked to another one
 *
 * @returns {*|{}}
 */
function abortLinkCreation() {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.argument.ArgumentActions.abortLinkCreation().ajax({ async: true });
}

/*
 * EXCERPTS
 */

/**
 * Get modal frame to add a new illustration link by either searching for an existing excerpt or adding new one
 *
 * @param id an argument id that will be linked to the selected excerpt
 * @param contextId the context contribution id where an excertp selection is called
 * @param shade the link shade that will bind for the illustration link, if any
 * @returns {*|{}}
 */
function excerptSelection(id, contextId, shade) {
    contextId= contextId || -1;
    shade = shade || -1;
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.excerpt.ExcerptActions.excerptSelection(id, contextId, shade).ajax({ async: true });
}

/**
 * Add selected shade in session on the excerptSelection process.
 *
 * @param argId an argument id
 * @param shade an argument link shade, if any
 * @return ok if given argId and shade are good
 */
function excerptSelectionShade(argId, shade) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.excerpt.ExcerptActions.excerptSelectionShade(argId, shade).ajax({ async: true });
}

/**
 * See all arguments that are linked with the given excerpt
 *
 * @param excId an excerpt id
 * @return {*}
 */
function seeExcerptArguments(excId) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.excerpt.ExcerptActions.seeExcerptArguments(excId).ajax({ async: true });
}

/**
 * open modal for edit an excerpt
 *
 * @param textId the id of the excerpt text
 * @param excId the excerpt id to edit
 * @returns {*}
 */
function editExcerptFromModal(textId, excId) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.excerpt.ExcerptActions.editFromModal(textId, excId).ajax({ async: true });
}

/**
 * Submit excerpt form
 *
 * @param textId the id of the excerpt text
 * @param excId the excerpt id
 * @param form the form with the excerpt details
 * @param groupId the group where save this contextualized
 * @returns {*|{}} either an empty ok response (200) or the full page to be reloaded
 */
function saveExcerpt(textId, excId, form, groupId) {
    var serial = form.serialize();
    if(groupId !== undefined){
        serial += "groupId=" + groupId;
    }
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.excerpt.ExcerptActions.saveFromModal(textId, excId).ajax({
        type: 'POST',
        data: serial,
        async: true
    });
}

/**
 * Get the edit temporary excerpt form for given tmp excerpt to be added into current page
 *
 * @param excId id of tmp excerpt to retrieve
 * @returns {*}
 */
function editExternalExcerpt(excId) {
    return jsRoutes.be.webdeb.presentation.web.controllers.validate.ValidateActions.editExternalExcerpt(excId).ajax({ async: true });
}

/**
 * Submit temporary excerpt form after manual validation
 *
 * @param excId the temporary excerpt id
 * @param form the form with the excerpt details
 * @returns {*|{}} either an empty ok response (200) or the full page to be reloaded
 */
function submitExternalExcerpt(excId, form) {
  return jsRoutes.be.webdeb.presentation.web.controllers.validate.ValidateActions.submitExternalExcerpt(excId).ajax({
    type: 'POST',
    data: form.serialize(),
    async: true
  });
}

/**
 * Set the rejected state for given temporary argument, ie, if rejected is true, this argument will not be shown again.
 * Reversible action until the user quit the validation screen.
 *
 * @param excId a temporary argument id
 * @param rejected true to reject this argument
 * @returns {*}
 */
function setExternalContributionState(excId, rejected) {
  return jsRoutes.be.webdeb.presentation.web.controllers.validate.ValidateActions.setExternalContributionState(excId, rejected).ajax({ async: true });
}

/**
 * Set the rejected state for given temporary argument, ie, if rejected is true, this argument will not be shown again.
 * Reversible action until the user quit the validation screen.
 *
 * @param tweetID the WDTAL ID of the Tweet
 * @returns {*}
 */
function rejectTweet(tweetID) {
  return jsRoutes.be.webdeb.presentation.web.controllers.validate.ValidateActions.rejectTweet(tweetID).ajax({ async: true });
}

/**
 * Get all linked contributions with an argument by viz pane type
 *
 * @param argument the argument id
 * @param type the name of the viz pane type
 * @returns {*}
 */
function getArgumentLinkedContributions(argument, type) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.argument.ArgumentActions.getLinkedContributions(argument, type).ajax({ async: true });
}

/**
 * Reload all linked contributions with an argument by viz pane type
 *
 * @param argument the argument id
 * @returns {*}
 */
function reloadArgumentLinkedContributions(argument, type) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.argument.ArgumentActions.reloadLinkedContributions(argument, type).ajax({ async: true });
}

/**
 * Get all contextualized argument for given context contribution id
 *
 * @param id a context contribution id
 * @returns {*}
 */
function getContextualizedArguments(id) {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.text.TextActions.getContextualizedArguments(id).ajax({ async: true });
}

/**
 * Get all contextualized argument for given context contribution id filter on given shade link type
 *
 * @param id a context contribution id
 * @param shade the link shade type to filter on
 * @returns {*}
 */
function getContextualizedArgumentsByShade(id, shade) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.text.TextActions.getContextualizedArgumentsByShade(id, shade).ajax({ async: true });
}

/**
 * Get all justification links for given text id
 *
 * @param id a context contribution id
 * @returns {*}
 */
function getJustificationLinks(id) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.text.TextActions.getJustificationLinks(id).ajax({ async: true });
}

/**
 * Get all justification links for given text id filter on given shade link type
 *
 * @param id a context contribution id
 * @param shade the link shade type to filter on
 * @returns {*}
 */
function getJustificationLinksByShade(id, shade) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.text.TextActions.getJustificationLinksByShade(id, shade).ajax({ async: true });
}

/**
 * Save given justification links passed as serialized LinkForms
 *
 * @param text text id
 * @param links json-representation of LinkForms with all justification links of given text
 * @param group the group id in which the link must be saved
 * @returns {*}
 */
function saveJustificationLinks(text, links, group) {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.text.TextActions.saveJustificationLinks(text, group).ajax({
    type: 'POST',
    contentType: "application/json; charset=utf-8",
    data: links,
		async: true
  });
}

/**
 * Search for excerpts with given query title
 *
 * @param query a query string
 * @param argIdToIgnore the contextualized argument id to ignore, if any
 * @param textToLook the text id where to search, if any
 * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
 * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
 * @returns {*}
 */
function searchExcerpt(query, argIdToIgnore, textToLook, fromIndex, toIndex) {
    argIdToIgnore = argIdToIgnore || -1;
    textToLook = textToLook || -1;
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.excerpt.ExcerptActions.searchExcerpt(query, argIdToIgnore, textToLook, fromIndex, toIndex).ajax({ async: true });
}

/**
 * Search for an illustration link between an argument and a excerpt
 *
 * @param argId contextualized argument id
 * @param excId an excerpt id
 * @return not found if form has errors, unauthorized if contributor or text are not defined or a db crash, or the
 * @returns {*}
 */
function findIllustrationLink(argId, excId) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.excerpt.ExcerptActions.findIllustrationLink(argId, excId).ajax({ async: true });
}

/*
 * FOLDERS
 */

/**
 * Search for folders with given query
 *
 * @param query a query string
 * @param idToIgnore the folder id to ignore, or null
 * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
 * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
 * @returns {*}
 */
function searchFolder(query, idToIgnore, fromIndex, toIndex) {
    idToIgnore = idToIgnore || -1;
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.folder.FolderActions.searchFolder(query, idToIgnore, fromIndex, toIndex).ajax({ async: true });
}

/**
 * Submit a link between folders
 *
 * @param parent the parent folder id
 * @param child the child folder id
 * @returns {*|{}} either an empty ok response (200) or the full page to be reloaded
 */
function saveFolderLink(parent, child) {
  return jsRoutes.be.webdeb.presentation.web.controllers.folder.saveFolderLink(parent, child).ajax({
    type: 'POST',
    data: form.serialize(),
    async: true
  });
}

/**
 * Handle folder form submission from modal frame
 *
 * @param form the folder form
 * @returns either a bad request with the content of the form in error, or a success status code
 */
function saveFolderFromModal(form) {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.folder.FolderActions.saveFromModal().ajax({
    type: 'POST',
    processData: false,
    contentType: false,
    data: new FormData(form[0]),
    async: true
  });
}

/**
 * Find the list of contextualized arguments that are contained in a given folder limited by index
 *
 * @param folder a folder id
 * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
 * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
 * @returns {*}
 */
function findFolderArguments(folder, fromIndex, toIndex) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.folder.FolderActions.findFolderArguments(folder, fromIndex, toIndex).ajax({ async: true });
}

/**
 * Find the list of excerpts that are contained in a given folder limited by index
 *
 * @param folder a folder id
 * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
 * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
 * @returns {*}
 */
function findFolderExcerpts(folder, fromIndex, toIndex) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.folder.FolderActions.findFolderExcerpts(folder, fromIndex, toIndex).ajax({ async: true });
}

/**
 * Find the list of debates that are contained in a given folder limited by index
 *
 * @param folder a folder id
 * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
 * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
 * @returns {*}
 */
function findFolderDebates(folder, fromIndex, toIndex) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.folder.FolderActions.findFolderDebates(folder, fromIndex, toIndex).ajax({ async: true });
}

/**
 * Find the list of texts that are contained in a given folder limited by index
 *
 * @param folder a folder id
 * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
 * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
 * @returns {*}
 */
function findFolderTexts(folder, fromIndex, toIndex) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.folder.FolderActions.findFolderTexts(folder, fromIndex, toIndex).ajax({ async: true });
}

/*
 * DEBATES
 */

/**
 * Create a debate from a given contextualized argument
 *
 * @param argId the contextualized argument id
 * @returns either a bad request, internal server error, or a success status code
 */
function createFromArgumentContext(argId) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.debate.DebateActions.createFromArgumentContext(argId).ajax({
        type: 'POST',
        async: true
    });
}

/**
 * Search for debates with given query
 *
 * @param query a query string
 * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
 * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
 * @returns {*}
 */
function searchDebate(query, fromIndex, toIndex) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.debate.DebateActions.searchDebate(query, fromIndex, toIndex).ajax({ async: true });
}

/**
 * Get all linked contributions with a debate by viz pane type
 *
 * @param debate the debate id
 * @param type the name of the viz pane type
 * @returns {*}
 */
function getDebateLinkedContributions(debate, type) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.debate.DebateActions.getLinkedContributions(debate, type).ajax({ async: true });
}

/**
 * Reload all linked contributions with a debate by viz pane type
 *
 * @param debate the debate id
 * @returns {*}
 */
function reloadDebateLinkedContributions(debate, type) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.debate.DebateActions.reloadLinkedContributions(debate, type).ajax({ async: true });
}

/*
 * CONTRIBUTORS and GROUPS
 */

/**
 * Get edit advices modal page
 *
 * @returns {*}
 */
function editAdvices() {
    return jsRoutes.be.webdeb.presentation.web.controllers.account.ContributorActions.editAdvices().ajax({ async: true });
}

/**
 * Post contributors advices
 *
 * @param form the whole form containing the advices form
 * @returns {*}
 */
function sendAdvices(form) {
    return jsRoutes.be.webdeb.presentation.web.controllers.account.ContributorActions.sendAdvices().ajax({
        type: 'POST',
        data: new FormData(form[0]),
        contentType: false,
        processData: false,
        async: true
    });
}

/**
 * Get send mail to group modal page
 *
 * @returns {*}
 */
function editContributionsToExplore(contributionType) {
    contributionType = contributionType || -1;
    return jsRoutes.be.webdeb.presentation.web.controllers.account.group.GroupActions.editContributionsToExplore(contributionType).ajax({ async: true });
}

/**
 * Post contributions to explore
 *
 * @param form the whole form containing the contributions to explore
 * @returns {*}
 */
function sendContributionsToExplore(contributionType, form) {
    contributionType = contributionType || -1;
    return jsRoutes.be.webdeb.presentation.web.controllers.account.group.GroupActions.sendContributionsToExplore(contributionType).ajax({
        type: 'POST',
        data: new FormData(form[0]),
        contentType: false,
        processData: false,
        async: true
    });
}

/**
 * Get modal frame to search and select after an existing contribution
 *
 * @param contributionType a given contribution
 * @returns {*|{}}
 */
function contributionSelection(contributionType) {
    contributionType = contributionType === undefined ? -1 : contributionType;
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.EntryActions.contributionSelection(contributionType).ajax({ async: true });
}

/**
 * Get contact us modal frame to contact administrator
 *
 * @returns {*}
 */
function getContactus() {
    return jsRoutes.be.webdeb.presentation.web.controllers.account.ContributorActions.contactus().ajax({ async: true });
}

/**
 * Get contact us modal frame to contact administrator V2
 *
 * @returns {*}
 */
function getContactus2() {
    return jsRoutes.be.webdeb.presentation.web.controllers.account.ContributorActions.contactus2().ajax({ async: true });
}

/**
 * Set contact us modal frame to contact administrator
 *
 * @param form the whole form containing the contact form
 * @returns {*}
 */
function sendContactus(form) {
    return jsRoutes.be.webdeb.presentation.web.controllers.account.ContributorActions.sendContactus().ajax({
        type: 'POST',
        data: new FormData(form[0]),
        contentType: false,
        processData: false,
        async: true
    });
}

/**
 * Upload a picture for a given contributor (in form)
 *
 * @param form the form query (post request) with the file and the id to post
 * @param contributorId the contributor id for whom we add a new picture
 * @returns {*|{}}
 */
function uploadContributorPicture(form, contributorId) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.ContributorActions.uploadContributorPicture(contributorId).ajax({
    type: 'POST',
    data: new FormData(form[0]),
    contentType: false,
    processData: false,
		async: true
  });
}

/**
 * Get modal page to change current user's email
 *
 * @param id the contributor id (used to check match with user id in session cookie)
 * @returns {*|{}}
 */
function askChangeMail(id) {
	return jsRoutes.be.webdeb.presentation.web.controllers.account.ContributorActions.askChangeMail(id).ajax({ async: true });
}

/**
 * Send request to change contributor mail
 *
 * @param id the contributor id asking for the request (used to check match with user id in session cookie)
 * @param form the form with the new email
 * @returns {*|{}}
 */
function changeMail(id, form) {
	return jsRoutes.be.webdeb.presentation.web.controllers.account.ContributorActions.changeMail(id).ajax({
		type: 'POST',
		data: form.serialize(),
		async: true
	});
}

/**
 * Get modal page to change current user's password
 *
 * @param id the contributor id (used to check match with user id in session cookie)
 * @returns {*|{}}
 */
function askChangePassword(id) {
	return jsRoutes.be.webdeb.presentation.web.controllers.account.ContributorActions.askChangePassword(id).ajax({ async: true });
}

/**
 * Send request to change contributor password
 *
 * @param id the contributor id asking for the request (used to check match with user id in session cookie)
 * @param form the form with the new password
 * @returns {*|{}}
 */
function changePassword(id, form) {
	return jsRoutes.be.webdeb.presentation.web.controllers.account.ContributorActions.changePassword(id).ajax({
		type: 'POST',
		data: form.serialize(),
		async: true
	});
}

/**
 * Get modal page to delete contributor's account
 *
 * @param id the contributor id (used to check match with user id in session cookie)
 * @returns {*|{}}
 */
function askDeleteAccount(id) {
    return jsRoutes.be.webdeb.presentation.web.controllers.account.ContributorActions.askDeleteAccount(id).ajax({ async: true });
}

/**
 * Send request to delete contributor's account
 *
 * @param id the contributor id (used to check match with user id in session cookie)
 * @param form the form with the deletion
 * @returns {*|{}}
 */
function deleteAccount(id, form) {
    return jsRoutes.be.webdeb.presentation.web.controllers.account.ContributorActions.deleteAccount(id).ajax({
        type: 'POST',
        data: form.serialize(),
        async: true
    });
}

/**
 * Get modal page to ask a password recovery
 *
 * @returns {*}
 */
function recoverPassword() {
	return jsRoutes.be.webdeb.presentation.web.controllers.account.ContributorActions.recoverPassword().ajax({ async: true });
}

/**
 * Send request to recover a lost password for email passed in form
 *
 * @param form a form containing an email
 * @returns {*}
 */
function sendPasswordRecovery(form) {
	return jsRoutes.be.webdeb.presentation.web.controllers.account.ContributorActions.sendPasswordRecovery().ajax({
		type: 'POST',
		data: form.serialize(),
		async: true
	});
}

/**
 * Get modal page to resend the signup mail
 *
 * @returns {*}
 */
function resendSignupMail() {
    return jsRoutes.be.webdeb.presentation.web.controllers.account.ContributorActions.resendSignupMail().ajax({ async: true });
}

/**
 * Send the mail to confirme the subscription to WebDeb
 *
 * @param form a form containing an email
 * @returns {*}
 */
function sendSignupMail(form) {
    return jsRoutes.be.webdeb.presentation.web.controllers.account.ContributorActions.sendSignupMail().ajax({
        type: 'POST',
        data: form.serialize(),
        async: true
    });
}

/**
 * Get places by query
 *
 * @param query the query to excecute
 * @returns {*}
 */
function searchPlace(query){
  return jsRoutes.be.webdeb.presentation.web.controllers.account.ContributorActions.searchPlace(query).ajax({ async: true });
}

/**
 * Set the user warned about old browser danger
 *
 * @returns {*}
 */
function userIsBrowserWarned(){
  return jsRoutes.be.webdeb.presentation.web.controllers.account.ContributorActions.userIsBrowserWarned().ajax({ async: true });
}

/**
 * Leave from given group for current user 
 *
 * @param group a group id
 * @returns {*}
 */
function leaveGroup(group) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.group.GroupActions.leaveGroup(group).ajax({ async: true });
}

/**
 * Revoke given user from given group
 *
 * @param user a user id
 * @param group a group id
 * @param ban true if user must be banned, false to unban
 * @returns {*}
 */
function setBannedInGroup(user, group, ban) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.group.GroupActions.setBannedInGroup(user, group, ban).ajax({ async: true });
}

/**
 * Change default group for current user
 *
 * @param group a group id
 * @returns {*}
 */
function changeDefaultGroup(group) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.group.GroupActions.changeDefaultGroup(group).ajax({ async: true });
}

/**
 * Get modal page to search for groups
 *
 * @returns {*}
 */
function newSubscription() {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.group.GroupActions.newSubscription().ajax({ async: true });
}

/**
 * Retrieve a contributor group
 *
 * @param groupId the id of the group to retrieve
 * @returns {*}
 */
function retrieveContributorGroup(groupId) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.group.GroupActions.retrieveContributorGroup(groupId).ajax({ async: true });
}

/**
 * Search for groups (for typeahead)
 * 
 * @param query a partial group name to search for
 * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
 * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
 * @returns {*}
 */
function searchGroups(query, fromIndex, toIndex) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.group.GroupActions.searchGroups(query, fromIndex, toIndex).ajax({ async: true });
}

/**
 * Search for groups and get partial result page
 *
 * @param query a partial group name to search for
 * @returns {*}
 */
function getGroupResults(query) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.group.GroupActions.getGroupResults(query).ajax({ async: true });
}

/**
 * Add a contributor (retrieved from passed session cookie) as new member to given group
 * 
 * @param group a group id
 * @returns {*}
 */
function joinGroup(group) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.group.GroupActions.joinGroup(group).ajax({ async: true });
}

/**
 * Edit details of given group
 * 
 * @param group a group id 
 * @returns {*}
 */
function editGroup(group) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.group.GroupActions.editGroup(group).ajax({ async: true });
}

/**
 * Save details of given group
 *
 * @param form the form element containing the data to save
 * @returns {*}
 */
function saveGroup(form) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.group.GroupActions.saveGroup().ajax({
    type: 'POST',
    data: form.serialize(),
		async: true
  });
}

/**
 * Empty all contributions and member in given group and possibly delete completely the group itself
 * @param group a group id
 * @param deleteGroup true if the group itself must be deleted too
 * @returns {*}
 */
function emptyGroup(group, deleteGroup) {
	return jsRoutes.be.webdeb.presentation.web.controllers.account.group.GroupActions.emptyGroup(group, deleteGroup).ajax({ async: true });
}

/**
 * Change roles of member inside a given group
 * @param group a group id
 * @param userId the user to change the role if we only change one (-1 otherwise)
 * @returns {*}
 */
function changeMemberRole(group, userId) {
  userId = userId || -1;
  return jsRoutes.be.webdeb.presentation.web.controllers.account.group.GroupActions.changeMemberRole(group, userId).ajax({ async: true });
}

/**
 * Change members role
 *
 * @param group a group id
 * @param form the form element containing the data to save
 * @returns {*}
 */
function sendChangeMemberRole(group, form) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.group.GroupActions.sendChangeMemberRole(group).ajax({
    type: 'POST',
    data: form.serialize(),
    async: true
  });
}

/**
 * Send followed state to given group for given contributor
 *
 * @param group the id of group to follow
 * @param contributor the contributor that want to follow
 * @param follow the follow state to put
 * @returns {*}
 */
function followGroup(group, contributor, follow) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.group.GroupActions.followGroup(group, contributor, follow).ajax({ async: true });
}

/**
 * Send followed state for all groups linked with given contributor
 *
 * @param contributor the contributor that want to follow
 * @param form the form element containing the data to save
 * @returns {*}
 */
function followGroups(contributor, form) {
    return jsRoutes.be.webdeb.presentation.web.controllers.account.group.GroupActions.followGroups(contributor).ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        data: form,
        async: true
    });
}

/**
 * Apply given role to given contributor in given group
 * 
 * @param contributor a contributor id
 * @param group a group id
 * @param role a role id
 * @returns {*|{}}
 */
function changeRole(contributor, group, role) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.AdminActions.changeRole(contributor, group, role).ajax({ async: true });
}

/**
 * Get modal page to invite new members in group
 * 
 * @param group a group id
 * @returns {*|{}}
 */
function inviteInGroup(group) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.group.GroupActions.inviteInGroup(group).ajax({ async: true });
}

/**
 * Send invitations to filled-in persons/emails 
 * 
 * @param form the form to serialize
 * @returns {*|{}}
 */
function sendInvitations(form) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.group.GroupActions.sendInvitations().ajax({
    type: 'POST',
    data: form.serialize(),
		async: true
  });
}

/**
 * Get modal page to send mail to group
 *
 * @param group a group id
 * @returns {*|{}}
 */
function getMailToGroupModal(group) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.group.GroupActions.getMailToGroupModal(group).ajax({ async: true });
}

/**
 * Send mail to group
 *
 * @param form the form to serialize
 * @returns {*|{}}
 */
function sendMailToGroup(form) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.group.GroupActions.sendMailToGroup().ajax({
    type: 'POST',
    data: form.serialize(),
    async: true
  });
}

/**
 * Search for contributors as a list of contributor holders
 * 
 * @param query a query string
 * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
 * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
 * @returns {*}
 */
function searchContributors(query, fromIndex, toIndex) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.group.GroupActions.searchContributors(query, fromIndex, toIndex).ajax({ async: true });
}

/**
 * Search for contributors as a partial result page (with their groups and roles)
 *
 * @param query a query string
 * @param sort either 'name' or 'date' for resp. sorting the result by name or registration date
 * @returns {*}
 */
function searchContributorsAndRoles(query, sort) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.AdminActions.searchContributorsAndRoles(query, sort).ajax({ async: true });
}

/**
 * Search for contributor's contributions
 *
 * @param searchText a query string
 * @param id the contributor id
 * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
 * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
 * @returns {*}
 */
function searchContributorContributions(searchText, id, fromIndex, toIndex) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.ContributorActions.searchContributorContributions(searchText, id, fromIndex, toIndex).ajax({ async: true });
}

/**
 * Get the partial page with all csv reports
 */
function getCsvReports() {
	return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.AdminActions.getCsvReports().ajax({ async: true });
}

/**
 * Get modal page to upload csv files
 * @returns {*|{}}
 */
function importCsvFile() {
	return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.AdminActions.importCsvFile().ajax({ async: true });
}

/**
 * Upload given csv file(s) passed in form to be pushed into the DB
 *
 * @param form the form to serialize (containing the csv file-s-)
 * @param charset the selected character set for the csv file
 * @param delimiter the selected delimiter for the csv file
 * @param groupid the group id in which the import must be performed
 * @returns {*|{}}
 */
function uploadCsvFile(form, charset, delimiter, groupid) {
	return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.AdminActions.uploadCsvFile(charset, delimiter, groupid).ajax({
		type: 'POST',
		data: new FormData(form[0]),
		contentType: false,
		processData: false,
		async: true
	});
}

/**
 * Get the list of rss sources (partial html page)
 *
 * @returns {*}
 */
function getRssFeeders() {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.AdminActions.getRssFeeders().ajax({ async: true });
}

/**
 * Get the modal page to edit given rss channel
 *
 * @returns {*}
 */
function editRssFeed(id) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.AdminActions.editRssFeed(id).ajax({ async: true });
}

/**
 * Activate / Deactivate Rss source feeding
 *
 * @param id an Rss source id
 * @param activate true if this source must be activated
 * @returns {*|{}}
 */
function activateRssFeed(id, activate) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.AdminActions.activateRssFeed(id, activate).ajax({ async: true });
}

/**
 * Save given rss feed form
 *
 * @param form an rss form to save
 * @returns {*}
 */
function saveRssFeed(form) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.AdminActions.saveRssFeed().ajax({
    type: 'POST',
    data: form.serialize(),
    async: true
  });
}

/**
 * Remove Rss source feeding
 *
 * @param id an Rss source id
 * @returns {*|{}}
 */
function removeRssFeed(id) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.AdminActions.removeRssFeed(id).ajax({ async: true });
}

/**
 * Get the list of twitter accounts (partial html page)
 *
 * @returns {*}
 */
function getTwitterAccounts() {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.AdminActions.getTwitterAccounts().ajax({ async: true });
}

/**
 * Get the modal page to edit given twitter account
 *
 * @returns {*}
 */
function editTwitterAccount(id) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.AdminActions.editTwitterAccount(id).ajax({ async: true });
}

/**
 * Save given twitter account form
 *
 * @param form a twitter form to save
 * @returns {*}
 */
function saveTwitterAccount(form) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.AdminActions.saveTwitterAccount().ajax({
    type: 'POST',
    data: form.serialize(),
    async: true
  });
}

/**
 * Delete Twitter account from feeding sources
 *
 * @param account a twitter account name
 * @returns {*|{}}
 */
function removeTwitterAcccount(account) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.AdminActions.removeTwitterAccount(account).ajax({ async: true });
}

/**
 * Delete TextFreeCopyrightSource
 *
 * @param idSource the id of the free source to delete
 * @returns {*|{}}
 */
function removeFreeCopyrightSource(idSource) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.AdminActions.removeFreeCopyrightSource(idSource).ajax({ async: true });
}


/*
 * PROJECT
 */

/**
 * Get the page to manage projects
 *
 * @returns {*|{}}
 */
function manageProjects() {
    return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.project.ProjectActions.manageProjects().ajax({ async: true });
}

/**
 * Get a project activity modal as project form for the given id
 *
 * @param projectId the project id
 * @returns {*|{}}
 */
function getProjectActivity(projectId) {
    return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.project.ProjectActions.getProjectActivity(projectId).ajax({ async: true });
}

/**
 * Generate project users
 *
 * @param projectId a project id
 * @returns {*|{}}
 */
function generateProjectUsers(projectId) {
    return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.project.ProjectActions.generateProjectUsers(projectId).ajax({
        type: 'POST',
        async: true
    });
}

/**
 * Delete all project users
 *
 * @param projectId a project id
 * @returns {*|{}}
 */
function deleteProjectUsers(projectId) {
    return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.project.ProjectActions.deleteProjectUsers(projectId).ajax({
        type: 'POST',
        async: true
    });
}

/**
 * Get the modal page to edit given project
 *
 * @param projectId a project id
 * @returns {*|{}}
 */
function editProject(projectId) {
    return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.project.ProjectActions.editProject(projectId).ajax({ async: true });
}

/**
 * Get the modal page to edit given project group
 *
 * @param projectId the project id that owns the group
 * @param projectGroupId the project group id to edit
 * @returns {*|{}}
 */
function editProjectGroup(projectId, projectGroupId) {
    return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.project.ProjectActions.editProjectGroup(projectId, projectGroupId).ajax({ async: true });
}

/**
 * Get the modal page to edit given project subgroup
 *
 * @param projectId the project id context
 * @param projectGroupId the project group id that owns the subgroup
 * @param projectSubgroupId the project subgroup id to edit
 * @returns {*|{}}
 */
function editProjectSubgroup(projectId, projectGroupId, projectSubgroupId) {
    return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.project.ProjectActions.editProjectSubgroup(projectId, projectGroupId, projectSubgroupId).ajax({ async: true });
}

/**
 * Submit project form
 *
 * @param form the form with the project details
 * @returns {*|{}} either an empty ok response (200) or the full page to be reloaded
 */
function saveProject(form) {
    return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.project.ProjectActions.saveProject().ajax({
        type: 'POST',
        data: form.serialize(),
        async: true
    });
}

/**
 * Submit project group form
 *
 * @param form the form with the project group details
 * @returns {*|{}} either an empty ok response (200) or the full page to be reloaded
 */
function saveProjectGroup(form) {
    return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.project.ProjectActions.saveProjectGroup().ajax({
        type: 'POST',
        data: form.serialize(),
        async: true
    });
}

/**
 * Submit project subgroup form
 *
 * @param form the form with the project subgroup details
 * @returns {*|{}} either an empty ok response (200) or the full page to be reloaded
 */
function saveProjectSubgroup(form) {
    return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.project.ProjectActions.saveProjectSubgroup().ajax({
        type: 'POST',
        data: form.serialize(),
        async: true
    });
}


/**
 * Change current scope for contributor, ie, update scope id in cookie
 * 
 * @param group the selected group id (for current scope)
 */
function changeScope(group) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.group.GroupActions.changeCurrentScope(group).ajax({ async: true });
}

/**
 * Add given contribution to given group's scope
 *
 * @param group a group id
 * @param contribution a contribution id
 * @param type the contribution type id
 */
function addContributionToGroup(group, contribution, type) {
	return jsRoutes.be.webdeb.presentation.web.controllers.account.group.GroupActions.addContributionToGroup(group, contribution, type).ajax({ async: true });
}


/*
 * AJAX-based SEARCH
 */

/**
 * Execute a search query (post request) with a given search form
 *
 * @param form the form element that will be serialized to be sent to server
 * @param card boolean saying if contribution cards must be retrieved (optional, default false)
 * @param embedded boolean saying if the search is done from an embedded frame
 * @returns {*|{}}
 */
function doSearch(form, card, embedded) {
	return jsRoutes.be.webdeb.presentation.web.controllers.browse.BrowseActions.doSearch(card, embedded).ajax({
    type: 'POST',
    data: form.serialize(),
		async: true
  });
}

/**
 * Search for contributions (auto-complete feature)
 *
 * @param query the search query
 * @param group the group to search for
 * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
 * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
 * @returns a list of jsonified contribution holders
 */
function searchContributions(query, group, fromIndex, toIndex) {
	return jsRoutes.be.webdeb.presentation.web.controllers.entry.EntryActions.searchContributions(query, group, fromIndex, toIndex).ajax({ async: true });
}

/**
 * Search for professions as a partial result page
 *
 * @param query a query string
 * @param json true if the return must be a json
 * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
 * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
 * @returns {*}
 */
function searchProfessions(query, json, fromIndex, toIndex) {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.searchProfessions(query, json, fromIndex, toIndex).ajax({ async: true });
}

/**
 * Get modal page to edit profession names
 *
 * @param profession the profession concerned
 * @returns {*|{}}
 */
function editProfession(profession) {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.editProfession(profession).ajax({ async: true });
}

/**
 * Confirm the editing of profession names
 *
 * @param form the form to serialize
 * @returns {*|{}}
 */
function sendEditProfession(form) {
  return jsRoutes.be.webdeb.presentation.web.controllers.entry.actor.ActorActions.sendEditProfession(form).ajax({
    type: 'POST',
    data: form.serialize(),
    async: true
  });
}

/**
 * Get modal page to edit link between professions
 *
 * @param profession the profession concerned
 * @returns {*|{}}
 */
function editProfessionHasLink(profession) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.AdminActions.editProfessionHasLink(profession).ajax({ async: true });
}

/**
 * Confirm the editing of profession links
 *
 * @param form the form to serialize
 * @returns {*|{}}
 */
function sendProfessionHasLink(form) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.AdminActions.sendProfessionHasLink(form).ajax({
    type: 'POST',
    data: form.serialize(),
    async: true
  });
}

/**
 * Get modal page to merge professions
 *
 * @param profession the profession concerned
 * @returns {*|{}}
 */
function mergeProfessions(profession) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.AdminActions.mergeProfessions(profession).ajax({ async: true });
}

/**
 * Confirm the merge of professions
 *
 * @param form the form to serialize
 * @returns {*|{}}
 */
function sendMergeProfessions(form) {
  return jsRoutes.be.webdeb.presentation.web.controllers.account.admin.AdminActions.sendMergeProfessions(form).ajax({
    type: 'POST',
    data: form.serialize(),
    async: true
  });
}

/*
 * OTHERS
 */

/**
 * Get all linked places to a given contribution
 *
 * @param id a contribution id
 * @param selectedPlace the selected place by the user if any
 */
function getContributionPlaces(id, selectedPlace){
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.EntryActions.getContributionPlaces(id, selectedPlace).ajax({ async: true });
}

/**
 * Get the modal frame to search a contribution to merge with given contribution
 *
 * @param id a contribution id
 * @param type its contribution type id
 * @returns {*|{}}
 */
function getMergeContributionModal(id, type) {
	return jsRoutes.be.webdeb.presentation.web.controllers.entry.EntryActions.getMergeContributionsModal(id, type).ajax({ async: true });
}

/**
 * Get the modal frame to search a contribution to merge with given contribution
 *
 * @param id a contribution id
 * @param type its contribution type id
 * @param group the contributor group
 * @param force true to force
 * @returns {*|{}}
 */
function deleteContributionAsync(id, type, group, force) {
    return jsRoutes.be.webdeb.presentation.web.controllers.entry.EntryActions.delete(id, type, group, force, true).ajax({ async: true });
}

/**
 * Get the modal frame to show the history of a contribution
 *
 * @param id a contribution id
 * @param type its contribution type id
 * @returns {*|{}}
 */
function getContributionHistoryModal(id, type) {
	return jsRoutes.be.webdeb.presentation.web.controllers.entry.EntryActions.getContributionHistoryModal(id, type).ajax({ async: true });
}

/**
 * Check if the "accept cookies" message has already been showed on main page
 *
 * @returns {*}
 */
function acceptCookies() {
  return jsRoutes.be.webdeb.presentation.web.controllers.Application.acceptCookies().ajax({ async: true });
}

/**
 * Check if given picture file is offensive (nudity or violence)
 *
 * @param form the whole form containing the picture
 * @returns {*}
 */
function checkOffensivePicture(form) {
  return jsRoutes.be.webdeb.presentation.web.controllers.Application.checkOffensivePicture().ajax({
    type: 'POST',
    data: new FormData(form[0]),
    contentType: false,
    processData: false,
    async: true
  });
}

/**
 * Get help modal
 *
 * @returns {*}
 */
function getHelpModal(names){
    return jsRoutes.be.webdeb.presentation.web.controllers.Application.getHelpModal().ajax({
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(names),
        async: true
    });
}

/**
 * Get a image modal for the given image
 *
 * @param path the path of the image
 * @returns {*}
 */
function imageModal(path) {
    return jsRoutes.be.webdeb.presentation.web.controllers.Application.imageModal(path).ajax({ async: true });
}

/*
 * ELECTIONS 2019
 */

/**
 * Get data async about stats page for election 2019
 *
 * @param id the id of the page
 * @return {*}
 */
function otherElectionStatsPageAsync(id) {
    return jsRoutes.be.webdeb.presentation.web.controllers.Application.otherElectionStatsPageAsync(id).ajax({ async: true });
}

/*
 * MANUAL REDIRECTS
 * 
 * Using the same "pattern" as Play routes file to abstract concrete routes behind javascript functions.
 * Must be maintained together with the routes file
 */

function cancelExcerptEdit(textId, excId) {
    window.location = "/entry/excerpt/cancel?textId=" + textId + "&excId=" + excId;
}

function redirectToEdit(id) {
  window.location = "/entry/edit?id=" + id;
}

function redirectToTextExcerpts(id) {
  window.location = "/entry/text/edit?id=" + id;
}

function redirectToExplore() {
	window.location = "/browse";
}

function redirectToContribute() {
	window.location = "/entry";
}

function redirectToBrowse(query, contributionId) {
    var contribution = (contributionId !== undefined ? "+id_contribution=" + contributionId : "");
	window.location = "/browse/query=" + encodeURIComponent(query) + contribution + "+all=true";
}

function deleteContribution(contribution, type, group, force) {
	window.location = '/entry/delete?id=' + contribution + '&type=' + type + '&group=' + group + '&force=' + force + '&async=false';
}

function redirectToEditFolder(id) {
    window.location = "/entry/folder?id=" + id;
}

function redirectToEditDebate(id) {
    window.location = "/entry/debate?id=" + id;
}

function redirectToDoMerge(originId, replacementId, contributionId) {
    window.location = "/entry/domerge?origin=" + originId + '&replacement=' + replacementId + '&type=' + contributionId;
}

function redirectToGivenLink(link) {
    window.location = link;
}

function redirectToLogin() {
    window.location = "/login";
}

function redirectToDebateViz(id, pane) {
    pane = pane || 0;
    window.location = '/viz/debate?id=' + id + '&pane=' + pane;
}

function redirectToArgumentViz(id, dic) {
    window.location = urlOfArgumentViz(id, dic);
}

function redirectToTextViz(id, pane) {
    window.location = '/viz/text?id=' + id + '&pane=' + pane;
}

function redirectToDetailsPage(id) {
    window.location = '/viz/details?id=' + id;
}

function openInNewTab(url){
    let win = window.open(url, '_blank');
    win.focus();
}

/*
 * URL locations
 */

function urlOfArgumentViz(id, dic) {
  return '/viz/argument?id=' + id + '&dic=' + dic;
}

function urlOfArgumentContextViz(id, pane) {
    return '/viz/argument/context?id=' + id + '&pane=' + pane + '&linkType=2';
}

function urlOfFolderViz(id, pane) {
    return '/viz/folder?id=' + id + '&pane=' + pane;
}